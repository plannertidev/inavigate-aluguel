import { HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { RequestService } from "./request.service";
import { ResponseDTO } from "./response.dto";

@Injectable()
export class TokenService {
    constructor(private requestService: RequestService) {
    }

    getToken(): Observable<ResponseDTO> {

        const body = new HttpParams()
        .set('client_id', 'iNavidateAPI')
        .set('client_secret', 'SuperSecretPassword')
        .set('grant_type', 'client_credentials')
        .set('scope', 'api1.read');
      const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
  
    //   return this.http.post(`auth/login`, body.toString(), { headers, observe: 'response' })
    //     .map((res: HttpResponse<Object>) => res.ok)
    //     .catch((err: any) => Observable.of(false));

        return  this.requestService.post(environment.ssoUrl + "connect/token", body.toString(), headers);
      
    }
}