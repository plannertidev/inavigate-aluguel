import { Injectable } from "@angular/core";

@Injectable()
export class UserContextService {
    public _isLoged = false;

    public login = () => {this._isLoged = true}
    public logoff = () => {this._isLoged = false}
    public isLoged = () => {return this._isLoged}
    
}
