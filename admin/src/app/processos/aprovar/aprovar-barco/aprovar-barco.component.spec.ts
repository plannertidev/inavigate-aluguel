import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AprovarBarcoComponent } from './aprovar-barco.component';

describe('AprovarBarcoComponent', () => {
  let component: AprovarBarcoComponent;
  let fixture: ComponentFixture<AprovarBarcoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AprovarBarcoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AprovarBarcoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
