import { Component, OnInit } from '@angular/core';
import { FormGroup,Validators,FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BarcoDataService } from 'src/app/parametros/barco/barco-dataservice';
import {
  faSave
} from '@fortawesome/free-solid-svg-icons';
import { NotificationService } from 'src/app/core/services/notification.service';

@Component({
  selector: 'app-aprovar-barco',
  templateUrl: './aprovar-barco.component.html',
  styleUrls: ['./aprovar-barco.component.scss'],
  providers: [BarcoDataService, NotificationService ]
})
export class AprovarBarcoComponent implements OnInit {
  public barcoForm: FormGroup;
  public controls: any;

  faSave = faSave;

  constructor( 
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private barcoDataService: BarcoDataService,
    private notificationService: NotificationService) {


      this.barcoForm = this.formBuilder.group({
        barcoId: [''],
        fabricanteId: [''],
        modeloId: [''],
        nome: ['', Validators.required],
        fabricante: ['', Validators.required],
        descricao: ['', Validators.required],
        modelo: ['', Validators.required],
        anoFabricacao: ['', Validators.required],
        ultimaReforma: ['', Validators.required],
        tipo: ['', Validators.required],
        valor: [''],
        tripulacaoIncluidaPreco: ['', Validators.required],
        capacidade: ['', Validators.required],
        capacidadePernoite: ['', Validators.required],
        numeroCabines: ['', Validators.required],
        numeroBanheiros: ['', Validators.required],
        cumprimento: ['', Validators.required],
        boca: ['', Validators.required],
        calado: ['', Validators.required],
        tipoCombustivel: ['', Validators.required],
        potencia: ['', Validators.required],
        velocidadeCruzeiro: ['', Validators.required],
        velocidadeMaxima: ['', Validators.required],
        consumoMedio: ['', Validators.required],
        depositoAguaDoce: ['', Validators.required],
        depositoCombustivel: ['', Validators.required],
        estadoConservacao: ['', Validators.required],
        tamanho: ['', Validators.required],
        passageiroDia: ['', Validators.required],
        passageiroNoite: ['', Validators.required],
        motor: ['', Validators.required],
        marca: ['', Validators.required],
        quantoJaRodou: ['', Validators.required],
        localizacao: ['', Validators.required],
        acessorioEquipamento: ['', Validators.required],
        comunicacaoNavegacao: ['', Validators.required],
        eletronico: ['', Validators.required],



      });

      this.controls = {
        barcoId: this.barcoForm.get('barcoId'),
        descricao: this.barcoForm.get('descricao'),
        nome: this.barcoForm.get('nome'),
        tipo: this.barcoForm.get('tipo'),
        valor: this.barcoForm.get('valor'),
        modeloId: this.barcoForm.get('modeloId'),
        modelo: this.barcoForm.get('modelo'),
        fabricanteId: this.barcoForm.get('fabricanteId'),
        fabricante: this.barcoForm.get('fabricante'),
        tipoCombustivel: this.barcoForm.get('tipoCombustivel'),
        estadoConservacao: this.barcoForm.get('estadoConservacao'),
        tamanho: this.barcoForm.get('tamanho'),
        capacidade: this.barcoForm.get('capacidade'),
        capacidadePernoite: this.barcoForm.get('capacidadePernoite'),
        motor: this.barcoForm.get('motor'),
        marca: this.barcoForm.get('marca'),
        quantoJaRodou: this.barcoForm.get('quantoJaRodou'),
        localizacao: this.barcoForm.get('localizacao'),
        acessorioEquipamento: this.barcoForm.get('localizacao'),
        comunicacaoNavegacao: this.barcoForm.get('comunicacaoNavegacao'),
        eletronico: this.barcoForm.get('comunicacaoNavegacao'),
        anoFabricacao: this.barcoForm.get('anoFabricacao'),
        boca: this.barcoForm.get('boca'),
        potencia: this.barcoForm.get('potencia'),
        calado: this.barcoForm.get('calado'),
        cumprimento: this.barcoForm.get('cumprimento'),
        ultimaReforma: this.barcoForm.get('ultimaReforma'),
        depositoAguaDoce: this.barcoForm.get('depositoAguaDoce'),
        depositoCombustivel: this.barcoForm.get('depositoCombustivel'),
        consumoMedio: this.barcoForm.get('consumoMedio'),

      }
     }

  ngOnInit(): void {
    this.route.params.subscribe(params => this.getBarco(params['id']));
  }

  private getBarco(barcoId: string){
    this.barcoDataService.getById(barcoId).subscribe(response =>this.getPropriedadeCallback(response.data));
  }

  private getModelo(modeloId: string){
    this.barcoDataService.getByIdModelo(modeloId).subscribe(response => this.getModeloDescription(response.data));
  }

  private getModeloDescription(data){
    this.controls.modelo.setValue(data.descricao);
  }

  private getFabricante(fabricanteId: string){
    this.barcoDataService.getByIdFabricante(fabricanteId).subscribe(response => this.getFabricanteDescription(response.data));
  }

  private getFabricanteDescription(data){
    this.controls.fabricante.setValue(data.nome);
  }

  private getPropriedadeCallback(data){
    console.log(data);

    if (data != null) {
      this.controls.barcoId.setValue(data.barcoId);
      this.controls.descricao.setValue(data.descricao);
      this.controls.tipo.setValue(data.tipo);
      this.controls.nome.setValue(data.nome);
      this.controls.valor.setValue(data.valor);
      this.controls.estadoConservacao.setValue(data.estadoConservacao);
      this.controls.tipoCombustivel.setValue(data.tipoCombustivel);
      this.controls.tamanho.setValue(data.tamanho);
      this.controls.capacidade.setValue(data.capacidade);
      this.controls.capacidadePernoite.setValue(data.capacidadePernoite);
      this.controls.motor.setValue(data.motor);
      this.controls.marca.setValue(data.marca);
      this.controls.quantoJaRodou.setValue(data.quantoJaRodou);
      this.controls.localizacao.setValue(data.localizacao);
      this.controls.acessorioEquipamento.setValue(data.acessorioEquipamento);
      this.controls.comunicacaoNavegacao.setValue(data.comunicacaoNavegacao);
      this.controls.eletronico.setValue(data.eletronico);
      this.controls.anoFabricacao.setValue(data.anoFabricacao);
      this.controls.boca.setValue(data.boca);
      this.controls.potencia.setValue(data.potencia);
      this.controls.calado.setValue(data.calado);
      this.controls.cumprimento.setValue(data.cumprimento);
      this.controls.ultimaReforma.setValue(data.ultimaReforma);  
      this.controls.depositoAguaDoce.setValue(data.depositoAguaDoce);
      this.controls.depositoCombustivel.setValue(data.depositoCombustivel);;
      this.controls.consumoMedio.setValue(data.consumoMedio);
      this.controls.modeloId.setValue(data.modeloId);
      this.controls.fabricanteId.setValue(data.fabricanteId);
      
      this.getModelo(data.modeloId);
      this.getFabricante(data.fabricanteId);
    }
  }
  

  aprovarEmbarcacao() : void {
    this.barcoDataService.aprovarBarco(this.controls.barcoId.value).subscribe( response => {
      this.notificationService.success("Barco aprovado com sucesso.");
      this.router.navigateByUrl('/barco-list');
    } );
  }
}
