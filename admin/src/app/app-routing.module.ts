import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavigationComponent } from './layout/navigation/navigation/navigation.component';
import { BarcoListComponent } from './parametros/barco/barco-list/barco-list.component';
import { AprovarBarcoComponent } from './processos/aprovar/aprovar-barco/aprovar-barco.component';

const routes: Routes = [
  { path: 'navigation', component: NavigationComponent},
  { path: 'barco-list', component: BarcoListComponent},
  { path: 'aprovar-barco/:id', component: AprovarBarcoComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
