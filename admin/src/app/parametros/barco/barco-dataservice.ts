import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { RequestService } from "src/app/core/services/request.service";
import { ResponseDTO } from "src/app/core/services/response.dto";
import { BarcoDTO } from "./barco-dto";

@Injectable()
export class BarcoDataService {
    constructor(private requestService: RequestService) {
    }

    public getAll(): Observable<ResponseDTO> {
        return this.requestService.getApi('api/barco/getAll');
    }

    public getById(propriedadeId: string): Observable<ResponseDTO> {
        const params: any = [{ key: "parametroJson", value: propriedadeId }];
        return this.requestService.getApi("api/barco/getById", params);
    }
    public getByIdModelo(modeloId: string): Observable<ResponseDTO> {
        const params: any = [{ key: "parametroJson", value: modeloId }];
        return this.requestService.getApi("api/modelo/getById", params);
    }
    public getByIdFabricante(fabricanteId: string): Observable<ResponseDTO> {
        const params: any = [{ key: "parametroJson", value: fabricanteId }];
        return this.requestService.getApi("api/fabricante/getById", params);
    }

    public getByFilter(barcoDTO: BarcoDTO): Observable<ResponseDTO> {
        const params: any = [{ key: "parametroJson", value: JSON.stringify(barcoDTO) }];
        return this.requestService.getApi("api/barco/getByFilter", params);
    }

    public create(barcoDTO: BarcoDTO): Observable<ResponseDTO> {
        return this.requestService.postApi("api/barco/create", barcoDTO);
    }

    public update(barcoDTO: BarcoDTO): Observable<ResponseDTO> {
        return this.requestService.postApi("api/barco/update", barcoDTO);
    }

    public delete(barcoDTO: BarcoDTO): Observable<ResponseDTO> {
        return this.requestService.postApi("api/barco/delete", barcoDTO);
    }

    public aprovarBarco(barcoId: string): Observable<ResponseDTO> {
        let barco: BarcoDTO = new BarcoDTO();
        barco.barcoId = barcoId;
        return this.requestService.postApi("api/barco/aprovar", barco);
    }
}
