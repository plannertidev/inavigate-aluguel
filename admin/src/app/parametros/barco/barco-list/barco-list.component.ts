import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GridService } from 'src/app/core/services/grid.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { BarcoDataService } from '../barco-dataservice';
import { BarcoDTO } from '../barco-dto';

@Component({
  selector: 'app-barco-list',
  templateUrl: './barco-list.component.html',
  styleUrls: ['./barco-list.component.scss'],
  providers: [GridService, BarcoDataService, NotificationService]
})
export class BarcoListComponent implements OnInit {

  constructor(    
    private router: Router,
    public gridService: GridService<BarcoDTO>,
    public barcoDataService: BarcoDataService,
    private notificationService: NotificationService) {

 }

  ngOnInit(): void {
    this.barcoDataService.getAll().subscribe(response => this.atualizarGrid(response.data));
  }
  
  private atualizarGrid(data) {
    this.gridService.setDataSource(data)
  }

  aprovarBarco = (barco: BarcoDTO) => {
    this.router.navigate(['/aprovar-barco', barco.barcoId]);
  }
}
