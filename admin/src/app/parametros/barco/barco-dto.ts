export class BarcoDTO{
    public barcoId : string;
    public proprietarioId : string;
    public nome : string;
    public tipo : number;
    public fabricanteId : string;
    public modeloId : string;
    public tipoCombustivel : string; 
    public anoFabricacao: string;
    public ultimaReforma: string;
    public tripulacaoIncluidaPreco: string;
    public capacidade: string
    public capacidadePernoite: string;
    public numeroCabines: number;
    public numeroBanheiros: number;
    public cumprimento: number;
    public boca: string;
    public calado: string;
    public potencia: string;
    public velocidadeCruzeiro: number;
    public velocidadeMaxima: number;
    public consumoMedio: number;
    public depositoAguaDoce: number;
    public depositoCombustivel: number;
    public status : number;
    public statusDescription: string;
}