import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './layout/navigation/navigation/navigation.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { UserContextService } from './core/services/user-context-service';
import { BarcoListComponent } from './parametros/barco/barco-list/barco-list.component';
import { HttpClientModule } from '@angular/common/http';
import { RequestService } from './core/services/request.service';
import { AprovarBarcoComponent } from './processos/aprovar/aprovar-barco/aprovar-barco.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    BarcoListComponent,
    AprovarBarcoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    FormsModule,

  ],
  providers: [UserContextService, RequestService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
