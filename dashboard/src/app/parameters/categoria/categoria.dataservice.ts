import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { RequestService } from "src/app/services/request.service";
import { ResponseDTO } from "src/app/services/response.dto";
import { CategoriaDTO } from "./categoria.dto";

@Injectable()
export class CategoriaDataService {
    constructor(private requestService: RequestService) {
    }

    public getAll(): Observable<ResponseDTO> {
        return this.requestService.get("api/categoria/getAll");
    }

    public getById(categoriaId: string): Observable<ResponseDTO> {
        const params:any = [{key: "parametroJson",value: categoriaId}];
        return this.requestService.get("api/categoria/getById",  params);
    }

    public getByFilter(categoriaDTO: CategoriaDTO): Observable<ResponseDTO> {
        const params:any = [{key: "parametroJson", value: JSON.stringify(categoriaDTO)}];
        return this.requestService.get("api/categoria/getByFilter",  params);
    }

    public create(categoriaDTO: CategoriaDTO): Observable<ResponseDTO> {
        return this.requestService.post("api/categoria/create",  categoriaDTO);
    }

    public update(categoriaDTO: CategoriaDTO): Observable<ResponseDTO> {
        return this.requestService.post("api/categoria/update", categoriaDTO);
    }

    public delete(categoriaDTO: CategoriaDTO): Observable<ResponseDTO> {
        return this.requestService.post("api/categoria/delete",  categoriaDTO);
    }
}