import { Component,EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-categoria-form',
  templateUrl: './categoria-form.component.html',
  styleUrls: ['./categoria-form.component.scss']
})
export class CategoriaFormComponent implements OnInit {
  public form: any;
  public controls: any;

  @Output() save: EventEmitter<any> = new EventEmitter();
  @Output() bindControls: EventEmitter<any> = new EventEmitter();
  @Input() title;

  categoria_validation_messages = {
    'descricao': [
      { type: 'required', message: 'Informe a descrição' },
      { type: 'minlength', message: 'Descrição precisa ter no mínimo 5 caracteres' },
      { type: 'maxlength', message: 'Descrição não pode ter mais do que 50 caracteres' },
      { type: 'pattern', message: 'A Descrição so pode conter letras e números' }
    ]
  }
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({});
    this.form.addControl('categoriaId', new FormControl());
    this.form.addControl('descricao', new FormControl(), Validators.required);

    this.controls = {
      descricao: this.form.get('descricao'),
      categoriaId : this.form.get('categoriaId')
    }
    this.bindControls.emit(this.controls);
  }

  public onSubmit() {
    if (this.validFormEntry()) {
      this.save.emit(this.controls);
    }
  }

  private validFormEntry(): boolean {
    return !this.controls.descricao.hasError();
  }

  }


