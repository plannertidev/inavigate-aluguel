import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ResponseDTO } from 'src/app/services/response.dto';
import { CategoriaDataService } from '../categoria.dataservice';
import { CategoriaDTO } from "../categoria.dto";


@Component({
  selector: 'app-categoria-create',
  templateUrl: './categoria-create.component.html',
  styleUrls: ['./categoria-create.component.scss'],
  providers: [CategoriaDataService]
})
export class CategoriaCreateComponent implements OnInit {

  constructor( private router: Router,
    private snackBar: MatSnackBar,
    private categoriaDataService: CategoriaDataService) { }

  ngOnInit(): void {
  }
  private mapToDto(controls) : CategoriaDTO{
    let categoriaDTO : CategoriaDTO = new CategoriaDTO()
    categoriaDTO.descricao = controls.descricao.value;
    return categoriaDTO;
  }

  private createCallbackSuccess(response : ResponseDTO){
    this.snackBar.open('Parametro incluido com sucesso!', 'Success', {duration: 3000 });
    this.router.navigateByUrl('/categoria-list');
  }

  public onSubmit(controls: any) {
    this.categoriaDataService.create(this.mapToDto(controls)).subscribe( 
      response => this.createCallbackSuccess(response), error => this.createCallbackError(error));
  }

  private createCallbackError(error): any{
    this.snackBar.open(error.message, 'Error', {duration: 3000});
  }
}
