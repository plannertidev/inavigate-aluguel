import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { element } from 'protractor';
import { ConfirmationDialogComponent } from 'src/app/core/confirmation-dialog/confirmation-dialog.component';
import { GridService } from 'src/app/services/grid.service';
import { CategoriaDataService } from '../categoria.dataservice';
import { CategoriaDTO } from "../categoria.dto";
@Component({
  selector: 'app-categoria-list',
  templateUrl: './categoria-list.component.html',
  styleUrls: ['./categoria-list.component.scss'],
  providers: [GridService, CategoriaDataService]
})
export class CategoriaListComponent implements OnInit {

  public displayedColumns: string[] =
  [
    'descricao', 'action'
  ];

form = this.formBuilder.group({
  descricao: ['']
});

controls = {
  descricao: this.form.get('descricao')
}

constructor(
  private router: Router,
  private dialog: MatDialog,
  private snackBar: MatSnackBar,
  public gridService: GridService<CategoriaDTO>,
  private formBuilder: FormBuilder,
  public categoriaDataService: CategoriaDataService
) { }

ngOnInit(): void {
  this.categoriaDataService.getAll()
    .subscribe(response => this.atualizarGrid(response.data));
}
private atualizarGrid(data) {
  this.gridService.setDataSource(data)
}

public onNewApplicationClick(){
  this.router.navigateByUrl('/categoria-create');
}

public onEditAccount(element){
  this.router.navigate(['/categoria-edit', element.categoriaId]);
}

public onDeleteAccount(element){

  const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
    width: '350px',
    data: 'Você confirma a exclusão desta Categoria?'
  });

  dialogRef.afterClosed().subscribe(result => {
    if (result) {
      this.categoriaDataService.delete(element)
        .subscribe(
          () => { this.deleteCallback(); },
          error => {
            this.snackBar.open('Something went wrong.', 'Warning', { duration: 3000 });
          }
        );
    }
  });    
}

private deleteCallback() {
  this.categoriaDataService.getByFilter(new CategoriaDTO())
    .subscribe(response => this.gridService.setDataSource(response.data));

  this.snackBar.open('Categoria excluido com sucesso!', 'Success', { duration: 3000 });
}  
}
