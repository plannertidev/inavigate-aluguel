import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { ResponseDTO } from 'src/app/services/response.dto';
import { CategoriaDataService } from '../categoria.dataservice';
import { CategoriaDTO } from '../categoria.dto';

@Component({
  selector: 'app-categoria-edit',
  templateUrl: './categoria-edit.component.html',
  styleUrls: ['./categoria-edit.component.scss'],
  providers: [CategoriaDataService]
})
export class CategoriaEditComponent implements OnInit {
  private _categoriaDTO: CategoriaDTO;
  public controls: any;

  public newTitle:string = "Editar uma Categoria";

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private categoriaDataService: CategoriaDataService) { }

  ngOnInit(): void {
     this.route.params.subscribe(params => this.getCategoria(params['id']));
  }

  public bindControls(controls) {
    this.controls = controls;
  }
  
  private getCategoria(categoriaId: string){
    this.categoriaDataService.getById(categoriaId).subscribe(response =>this.getCategoriaCallback(response.data));
  }
  
  private getCategoriaCallback(data){
    this._categoriaDTO = data;
    if (this._categoriaDTO != null) {
      this.controls.categoriaId.setValue(this._categoriaDTO.categoriaId);
      this.controls.descricao.setValue(this._categoriaDTO.descricao);
    }
  }

  private mapToDto(controls) : CategoriaDTO {
    let categoriaDTO: CategoriaDTO = new CategoriaDTO();
    categoriaDTO.categoriaId = controls.categoriaId.value;
    categoriaDTO.descricao = controls.descricao.value;
    return categoriaDTO;
  }

  private createCallback(response: ResponseDTO){
    this.snackBar.open('Categoria alterado com sucesso!', 'Success', { duration: 3000 });
    this.router.navigateByUrl('/categoria-list');
  }

  public onSubmit(controls: any) {
    this.categoriaDataService.update(this.mapToDto(controls)).subscribe( response => this.createCallback(response));
  }

}