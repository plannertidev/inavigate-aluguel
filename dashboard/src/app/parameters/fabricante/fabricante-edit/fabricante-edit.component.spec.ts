import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FabricanteEditComponent } from './fabricante-edit.component';

describe('FabricanteEditComponent', () => {
  let component: FabricanteEditComponent;
  let fixture: ComponentFixture<FabricanteEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FabricanteEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FabricanteEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
