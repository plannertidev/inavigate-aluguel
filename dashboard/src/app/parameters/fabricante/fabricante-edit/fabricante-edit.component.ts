import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { ResponseDTO } from 'src/app/services/response.dto';
import { FabricanteDataService } from '../fabricante.dataservice';
import { FabricanteDTO } from '../fabricante.dto';

@Component({
  selector: 'app-fabricante-edit',
  templateUrl: './fabricante-edit.component.html',
  styleUrls: ['./fabricante-edit.component.scss'],
  providers: [FabricanteDataService]
})
export class FabricanteEditComponent implements OnInit {

  private _fabricanteDTO: FabricanteDTO;
  public controls: any;

  public newTitle:string = "Editar um Fabricante";

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private clienteDataService: FabricanteDataService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => this.getFabricante(params['id']));
  }

  public bindControls(controls) {
    this.controls = controls;
  }
  
  private getFabricante(fabricanteId: string){
    this.clienteDataService.getById(fabricanteId).subscribe(response =>this.getClienteCallback(response.data));
  }
  
  private getClienteCallback(data){
    this._fabricanteDTO = data;
    if (this._fabricanteDTO != null) {
      this.controls.fabricanteId.setValue(this._fabricanteDTO.fabricanteId);
      this.controls.nome.setValue(this._fabricanteDTO.nome);
    }
  }

  private mapToDto(controls) : FabricanteDTO {
    let fabricanteDTO: FabricanteDTO = new FabricanteDTO();
    fabricanteDTO.fabricanteId = controls.fabricanteId.value;
    fabricanteDTO.nome = controls.nome.value;
    return fabricanteDTO;
  }

  private createCallback(response: ResponseDTO){
    this.snackBar.open('Parametro alterado com sucesso!', 'Success', { duration: 3000 });
    this.router.navigateByUrl('/fabricante-list');
  }

  public onSubmit(controls: any) {
    this.clienteDataService.update(this.mapToDto(controls)).subscribe( response => this.createCallback(response));
  }

}
