import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { element } from 'protractor';
import { ConfirmationDialogComponent } from 'src/app/core/confirmation-dialog/confirmation-dialog.component';
import { GridService } from 'src/app/services/grid.service';
import { FabricanteDataService } from '../fabricante.dataservice';
import { FabricanteDTO } from "../fabricante.dto";

@Component({
  selector: 'app-fabricante-list',
  templateUrl: './fabricante-list.component.html',
  styleUrls: ['./fabricante-list.component.scss'],
  providers: [GridService, FabricanteDataService]
})
export class FabricanteListComponent implements OnInit {

  public displayedColumns: string[] =
    [
      'nome', 'action'
    ];

  form = this.formBuilder.group({
    nome: ['']
  });

  controls = {
    nome: this.form.get('nome')
  }

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    public gridService: GridService<FabricanteDTO>,
    private formBuilder: FormBuilder,
    public fabricanteDataService: FabricanteDataService) { }

  ngOnInit(): void {
    this.fabricanteDataService.getAll()
      .subscribe(response => this.atualizarGrid(response.data));
  }

  private atualizarGrid(data) {
    this.gridService.setDataSource(data)
  }

  public onNewApplicationClick() {
    this.router.navigateByUrl('/fabricante-create');
  }
  public onEditAccount(element){
    this.router.navigate(['/fabricante-edit', element.fabricanteId]);
  }
  public onDeleteAccount(element){

    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: 'Você confirma a exclusão deste Fabricante?'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.fabricanteDataService.delete(element)
          .subscribe(
            () => { this.deleteCallback(); },
            error => {
              this.snackBar.open('Something went wrong.', 'Warning', { duration: 3000 });
            }
          );
      }
    });    
  }

  private deleteCallback() {
    this.fabricanteDataService.getByFilter(new FabricanteDTO())
      .subscribe(response => this.gridService.setDataSource(response.data));

    this.snackBar.open('Fabricante excluido com sucesso!', 'Success', { duration: 3000 });
  }  
}
