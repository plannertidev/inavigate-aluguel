import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ResponseDTO } from 'src/app/services/response.dto';
import { FabricanteDataService } from '../fabricante.dataservice';
import { FabricanteDTO } from "../fabricante.dto";

@Component({
  selector: 'app-fabricante-create',
  templateUrl: './fabricante-create.component.html',
  styleUrls: ['./fabricante-create.component.scss'],
  providers: [FabricanteDataService]
})
export class FabricanteCreateComponent implements OnInit {

  public newTitle:string = "Criar um novo Fabricante";

  constructor(
    private router: Router,
    private snackBar: MatSnackBar,
    private fabricanteDataService: FabricanteDataService) { }

  ngOnInit(): void {
  }

  private mapToDto(controls) : FabricanteDTO {
    let fabricanteDTO: FabricanteDTO = new FabricanteDTO();
    fabricanteDTO.nome = controls.nome.value;
    return fabricanteDTO;
  }

  private createCallbackSuccess(response: ResponseDTO){
    this.snackBar.open('Fabricante incluido com sucesso!', 'Success', { duration: 3000 });
    this.router.navigateByUrl('/fabricante-list');
  }

  public onSubmit(controls: any) {
    this.fabricanteDataService.create(this.mapToDto(controls)).subscribe( 
      response => this.createCallbackSuccess(response), error => this.createCallbackError(error));
  }

  private createCallbackError(error): any{
    this.snackBar.open(error.message, 'Error', { duration: 3000 });
  }
}
