import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FabricanteCreateComponent } from './fabricante-create.component';

describe('FabricanteCreateComponent', () => {
  let component: FabricanteCreateComponent;
  let fixture: ComponentFixture<FabricanteCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FabricanteCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FabricanteCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
