import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { RequestService } from "src/app/services/request.service";
import { ResponseDTO } from "src/app/services/response.dto";
import { FabricanteDTO } from "./fabricante.dto";

@Injectable()
export class FabricanteDataService {
    constructor(private requestService: RequestService) {
    }

    public getAll(): Observable<ResponseDTO> {
        return this.requestService.get("api/fabricante/getAll");
    }

    public getById(fabricanteId: string): Observable<ResponseDTO> {
        const params:any = [{key: "parametroJson",value: fabricanteId}];
        return this.requestService.get("api/fabricante/getById",  params);
    }

    public getByFilter(fabricanteDTO: FabricanteDTO): Observable<ResponseDTO> {
        const params:any = [{key: "parametroJson", value: JSON.stringify(fabricanteDTO)}];
        return this.requestService.get("api/fabricante/getByFilter",  params);
    }

    public create(fabricanteDTO: FabricanteDTO): Observable<ResponseDTO> {
        return this.requestService.post("api/fabricante/create",  fabricanteDTO);
    }

    public update(fabricanteDTO: FabricanteDTO): Observable<ResponseDTO> {
        return this.requestService.post("api/fabricante/update", fabricanteDTO);
    }

    public delete(fabricanteDTO: FabricanteDTO): Observable<ResponseDTO> {
        return this.requestService.post("api/fabricante/delete",  fabricanteDTO);
    }
}