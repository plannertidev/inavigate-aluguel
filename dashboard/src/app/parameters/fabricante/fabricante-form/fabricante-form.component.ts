import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-fabricante-form',
  templateUrl: './fabricante-form.component.html',
  styleUrls: ['./fabricante-form.component.scss']
})
export class FabricanteFormComponent implements OnInit {
  public form: any;
  public controls: any;

  @Output() save: EventEmitter<any> = new EventEmitter();
  @Output() bindControls: EventEmitter<any> = new EventEmitter();
  @Input() title;

  fabricante_validation_messages = {
    'nome': [
      { type: 'required', message: 'Informe o nome' },
      { type: 'minlength', message: 'Nome precisa ter no mínimo 5 caracteres' },
      { type: 'maxlength', message: 'Nome não pode ter mais do que 50 caracteres' },
      { type: 'pattern', message: 'O Nome so pode conter letras e números' }
    ]
  }

  constructor(private formBuilder: FormBuilder) {
    this.title = "Default Form";
   }

  ngOnInit(): void {
    this.form = this.formBuilder.group({});
    this.form.addControl('fabricanteId', new FormControl());
    this.form.addControl('nome', new FormControl(), Validators.required);

    this.controls = {
      nome: this.form.get('nome'),
      fabricanteId : this.form.get('fabricanteId')
    }
    this.bindControls.emit(this.controls);
  }

  public onSubmit() {
    if (this.validFormEntry()) {
      this.save.emit(this.controls);
    }
  }

  private validFormEntry(): boolean {
    return !this.controls.nome.hasError();
  }
}
