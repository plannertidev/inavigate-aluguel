import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoPropriedadeFormComponent } from './tipo-propriedade-form.component';

describe('TipoPropriedadeFormComponent', () => {
  let component: TipoPropriedadeFormComponent;
  let fixture: ComponentFixture<TipoPropriedadeFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TipoPropriedadeFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoPropriedadeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
