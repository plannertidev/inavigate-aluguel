import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoPropriedadeListComponent } from './tipo-propriedade-list.component';

describe('TipoPropriedadeListComponent', () => {
  let component: TipoPropriedadeListComponent;
  let fixture: ComponentFixture<TipoPropriedadeListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TipoPropriedadeListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoPropriedadeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
