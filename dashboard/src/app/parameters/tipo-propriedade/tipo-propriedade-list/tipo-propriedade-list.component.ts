import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/core/confirmation-dialog/confirmation-dialog.component';
import { GridService } from 'src/app/services/grid.service';
import { TipoDataService } from '../tipo.dataservice';
import { TipoDTO } from '../tipo.dto';

@Component({
  selector: 'app-tipo-propriedade-list',
  templateUrl: './tipo-propriedade-list.component.html',
  styleUrls: ['./tipo-propriedade-list.component.scss'],
  providers: [GridService, TipoDataService]
})
export class TipoPropriedadeListComponent implements OnInit {

  public displayedColumns: string[] =
  [
    'descricao', 'action'
  ];
  
  form = this.formBuilder.group({
    descricao: ['']
  });

  controls = {
    descricao: this.form.get('descricao')
  }

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
     public gridService: GridService<TipoDTO>,
    private formBuilder: FormBuilder,
    public tipoDataService: TipoDataService
  ) { }

  ngOnInit(): void {
    this.tipoDataService.getAll()
      .subscribe(response => this.atualizarGrid(response.data))
  }

  private atualizarGrid(data){
    this.gridService.setDataSource(data)
  }

  public onNewApplicationClick(){
    this.router.navigateByUrl('/tipo-propriedade-create');
  }

  public onEditAccount(element){
    this.router.navigate(['/tipo-propriedade-edit', element.tipoPropriedadeId]);
  }
  
  public onDeleteAccount(element){
  
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: 'Você confirma a exclusão deste Tipo de Propriedade?'
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.tipoDataService.delete(element)
          .subscribe(
            () => { this.deleteCallback(); },
            error => {
              this.snackBar.open('Something went wrong.', 'Warning', { duration: 3000 });
            }
          );
      }
    });    
  }
  
  private deleteCallback() {
    this.tipoDataService.getByFilter(new TipoDTO())
      .subscribe(response => this.gridService.setDataSource(response.data));
  
    this.snackBar.open('Tipo de Propriedade excluido com sucesso!', 'Success', { duration: 3000 });
  }  
  }
  
