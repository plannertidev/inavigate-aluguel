import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { RequestService } from "src/app/services/request.service";
import { ResponseDTO } from "src/app/services/response.dto";
import { TipoDTO} from "./tipo.dto";

@Injectable()
export class TipoDataService {
    constructor(private requestService: RequestService) {
    }

    public getAll(): Observable<ResponseDTO> {
        return this.requestService.get("api/tipopropriedade/getAll");
    }

    public getById(tipoPropriedadeId: string): Observable<ResponseDTO> {
        const params:any = [{key: "parametroJson",value: tipoPropriedadeId}];
        return this.requestService.get("api/tipopropriedade/getById",  params);
    }

    public getByFilter(tipoDTO: TipoDTO): Observable<ResponseDTO> {
        const params:any = [{key: "parametroJson", value: JSON.stringify(tipoDTO)}];
        return this.requestService.get("api/tipopropriedade/getByFilter",  params);
    }

    public create(tipoDTO: TipoDTO): Observable<ResponseDTO> {
        return this.requestService.post("api/tipopropriedade/create",  tipoDTO);
    }

    public update(tipoDTO: TipoDTO): Observable<ResponseDTO> {
        return this.requestService.post("api/tipopropriedade/update", tipoDTO);
    }

    public delete(tipoDTO: TipoDTO): Observable<ResponseDTO> {
        return this.requestService.post("api/tipopropriedade/delete",  tipoDTO);
    }
}