import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoPropriedadeEditComponent } from './tipo-propriedade-edit.component';

describe('TipoPropriedadeEditComponent', () => {
  let component: TipoPropriedadeEditComponent;
  let fixture: ComponentFixture<TipoPropriedadeEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TipoPropriedadeEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoPropriedadeEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
