import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ResponseDTO } from 'src/app/services/response.dto';
import { TipoDTO } from '../tipo.dto';
import { TipoDataService } from '../tipo.dataservice';

@Component({
  selector: 'app-tipo-propriedade-edit',
  templateUrl: './tipo-propriedade-edit.component.html',
  styleUrls: ['./tipo-propriedade-edit.component.scss'],
  providers: [TipoDataService]
})
export class TipoPropriedadeEditComponent implements OnInit {
  private _tipoDTO: TipoDTO;
  public controls: any;

  public newTitle:string = "Editar um Tipo de Propriedade";

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private tipoDataService: TipoDataService) { }

  ngOnInit(): void {
     this.route.params.subscribe(params => this.getTipo(params['id']));
  }

  public bindControls(controls) {
    this.controls = controls;
  }
  
  private getTipo(tipoPropriedadeId: string){
    this.tipoDataService.getById(tipoPropriedadeId).subscribe(response =>this.getTipoCallback(response.data));
  }
  
  private getTipoCallback(data){
    this._tipoDTO = data;
    if (this._tipoDTO != null) {
      this.controls.tipoPropriedadeId.setValue(this._tipoDTO.tipoPropriedadeId);
      this.controls.descricao.setValue(this._tipoDTO.descricao);
    }
  }

  private mapToDto(controls) : TipoDTO {
    let tipoDTO: TipoDTO = new TipoDTO();
    tipoDTO.tipoPropriedadeId = controls.tipoPropriedadeId.value;
    tipoDTO.descricao = controls.descricao.value;
    return tipoDTO;
  }

  private createCallback(response: ResponseDTO){
    this.snackBar.open('Tipo de propriedade alterado com sucesso!', 'Success', { duration: 3000 });
    this.router.navigateByUrl('/tipo-propriedade-list');
  }

  public onSubmit(controls: any) {
    this.tipoDataService.update(this.mapToDto(controls)).subscribe( response => this.createCallback(response));
  }

}