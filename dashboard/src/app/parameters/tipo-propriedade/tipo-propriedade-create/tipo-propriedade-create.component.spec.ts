import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoPropriedadeCreateComponent } from './tipo-propriedade-create.component';

describe('TipoPropriedadeCreateComponent', () => {
  let component: TipoPropriedadeCreateComponent;
  let fixture: ComponentFixture<TipoPropriedadeCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TipoPropriedadeCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoPropriedadeCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
