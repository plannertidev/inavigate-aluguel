import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ResponseDTO } from 'src/app/services/response.dto';
import { TipoDataService } from '../tipo.dataservice';
import { TipoDTO } from '../tipo.dto';

@Component({
  selector: 'app-tipo-propriedade-create',
  templateUrl: './tipo-propriedade-create.component.html',
  styleUrls: ['./tipo-propriedade-create.component.scss'],
  providers: [TipoDataService]
})
export class TipoPropriedadeCreateComponent implements OnInit {

  constructor(  private router: Router,
    private snackBar: MatSnackBar,
    private tipoDataService: TipoDataService) { }

  ngOnInit(): void {
  }

  private mapToDto(controls) : TipoDTO{
    let tipoDTO : TipoDTO = new TipoDTO()
    tipoDTO.descricao = controls.descricao.value;
    return tipoDTO;
  }

  private createCallbackSuccess(response : ResponseDTO){
    this.snackBar.open('Parametro incluido com sucesso!', 'Success', {duration: 3000 });
    this.router.navigateByUrl('tipo-propriedade-list');
  }

  public onSubmit(controls: any) {
    this.tipoDataService.create(this.mapToDto(controls)).subscribe( 
      response => this.createCallbackSuccess(response), error => this.createCallbackError(error));
  }

  private createCallbackError(error): any{
    this.snackBar.open(error.message, 'Error', {duration: 3000});
  }

  
}
