import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { RequestService } from "src/app/services/request.service";
import { ResponseDTO } from "src/app/services/response.dto";
import { CidadeDTO } from "./cidade.dto";

@Injectable()
export class CidadeDataService{
    constructor(private requestService: RequestService){
    }

    public getAll(): Observable<ResponseDTO> {
        return this.requestService.get("api/cidade/getAll");
    }

    public getById(cidadeId: string): Observable<ResponseDTO> {
        const params:any = [{key: "parametroJson",value: cidadeId}];
        return this.requestService.get("api/cidade/getById",  params);
    }

    public getByFilter(cidadeDTO: CidadeDTO): Observable<ResponseDTO> {
        const params:any = [{key: "parametroJson", value: JSON.stringify(cidadeDTO)}];
        return this.requestService.get("api/cidade/getByFilter",  params);
    }

    public create(cidadeDTO: CidadeDTO): Observable<ResponseDTO> {
        return this.requestService.post("api/cidade/create",  cidadeDTO);
    }

    public update(cidadeDTO: CidadeDTO): Observable<ResponseDTO> {
        return this.requestService.post("api/cidade/update", cidadeDTO);
    }

    public delete(cidadeDTO: CidadeDTO): Observable<ResponseDTO> {
        return this.requestService.post("api/cidade/delete",  cidadeDTO);
    }
}