import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ResponseDTO } from 'src/app/services/response.dto';
import { CidadeDataService } from '../cidade.dataservice';
import { CidadeDTO } from '../cidade.dto';

@Component({
  selector: 'app-cidade-create',
  templateUrl: './cidade-create.component.html',
  styleUrls: ['./cidade-create.component.scss']
})
export class CidadeCreateComponent implements OnInit {

  constructor( private router: Router,
    private snackBar: MatSnackBar,
    private cidadeDataService: CidadeDataService) { }

  ngOnInit(): void {
  }
  private mapToDto(controls) : CidadeDTO{
    let cidadeDTO : CidadeDTO = new CidadeDTO()
    cidadeDTO.nome = controls.nome.value;
    return cidadeDTO;
  }

  private createCallbackSuccess(response : ResponseDTO){
    this.snackBar.open('Parametro incluido com sucesso!', 'Success', {duration: 3000 });
    this.router.navigateByUrl('/cidade-list');
  }

  public onSubmit(controls: any) {
    this.cidadeDataService.create(this.mapToDto(controls)).subscribe( 
      response => this.createCallbackSuccess(response), error => this.createCallbackError(error));
  }

  private createCallbackError(error): any{
    this.snackBar.open(error.message, 'Error', {duration: 3000});
  }

}
