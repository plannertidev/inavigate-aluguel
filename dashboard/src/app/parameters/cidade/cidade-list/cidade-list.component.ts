import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/core/confirmation-dialog/confirmation-dialog.component';
import { GridService } from 'src/app/services/grid.service';
import { CidadeDataService } from '../cidade.dataservice';
import { CidadeDTO } from '../cidade.dto';

@Component({
  selector: 'app-cidade-list',
  templateUrl: './cidade-list.component.html',
  styleUrls: ['./cidade-list.component.scss'],
  providers: [GridService, CidadeDataService]
})
export class CidadeListComponent implements OnInit {
  public displayedColumns: string[] =
  [
    'nome', 'estado','action'
  ];

  form = this.formBuilder.group({
    nome: [''],
    estadoId: ['']
  });

  controls = {
    nome: this.form.get('nome'),
    estadoId: this.form.get('estadoId')
  }

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    public gridService: GridService<CidadeDTO>,
    private formBuilder: FormBuilder,
    public cidadeDataService: CidadeDataService
  ) { }

  ngOnInit(): void {
    this.cidadeDataService.getAll()
      .subscribe(response => this.atualizarGrid(response.data));
  }
  private atualizarGrid(data) {
    this.gridService.setDataSource(data)
  }
  
  public onNewApplicationClick(){
    this.router.navigateByUrl('/cidade-create');
  }
  
  public onEditAccount(element){
    this.router.navigate(['/cidade-edit', element.estadoId]);
  }
  
  public onDeleteAccount(element){
  
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: 'Você confirma a exclusão desta Cidade?'
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.cidadeDataService.delete(element)
          .subscribe(
            () => { this.deleteCallback(); },
            error => {
              this.snackBar.open('Something went wrong.', 'Warning', { duration: 3000 });
            }
          );
      }
    });    
  }
  
  private deleteCallback() {
    this.cidadeDataService.getByFilter(new CidadeDTO())
      .subscribe(response => this.gridService.setDataSource(response.data));
  
    this.snackBar.open('Cidade excluida com sucesso!', 'Success', { duration: 3000 });
  }  
}
