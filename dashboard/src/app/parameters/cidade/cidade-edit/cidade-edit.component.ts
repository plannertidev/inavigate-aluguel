import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ResponseDTO } from 'src/app/services/response.dto';
import { CidadeDataService } from '../cidade.dataservice';
import { CidadeDTO } from '../cidade.dto';

@Component({
  selector: 'app-cidade-edit',
  templateUrl: './cidade-edit.component.html',
  styleUrls: ['./cidade-edit.component.scss']
})
export class CidadeEditComponent implements OnInit {
  private _cidadeDTO : CidadeDTO;
  public controls: any;

  public newTitle:string = "Editar uma Cidade";

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private cidadeDataService: CidadeDataService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => this.getCidade(params['id']));
  } 
  public bindControls(controls) {
    this.controls = controls;
  }
  
  private getCidade(cidadeId: string){
    this.cidadeDataService.getById(cidadeId).subscribe(response =>this.getCidadeCallback(response.data));
  }
  
  private getCidadeCallback(data){
    this._cidadeDTO = data;
    if (this._cidadeDTO != null) {
      this.controls.cidadeId.setValue(this._cidadeDTO.cidadeId);
      this.controls.estadoId.setValue(this._cidadeDTO.estadoId);
      this.controls.nome.setValue(this._cidadeDTO.nome);
    }
  }

  private mapToDto(controls) : CidadeDTO {
    let cidadeDTO: CidadeDTO = new CidadeDTO();
    cidadeDTO.cidadeId = controls.cidadeId.value;
    cidadeDTO.estadoId = controls.estadoId.value;
    cidadeDTO.nome = controls.nome.value;
    return cidadeDTO;
  }

  private createCallback(response: ResponseDTO){
    this.snackBar.open('Cidade alterado com sucesso!', 'Success', { duration: 3000 });
    this.router.navigateByUrl('/cidade-list');
  }

  public onSubmit(controls: any) {
    this.cidadeDataService.update(this.mapToDto(controls)).subscribe( response => this.createCallback(response));
  }

}
