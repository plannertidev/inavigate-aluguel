import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { RequestService } from "src/app/services/request.service";
import { ResponseDTO } from "src/app/services/response.dto";
import { MarcaDTO } from "./marca.dto";

@Injectable()
export class MarcaDataService {
    constructor(private requestService: RequestService) {
    }

    public getAll(): Observable<ResponseDTO> {
        return this.requestService.get("api/marca/getAll");
    }

    public getById(marcaId: string): Observable<ResponseDTO> {
        const params:any = [{key: "parametroJson",value: marcaId}];
        return this.requestService.get("api/marca/getById",  params);
    }

    public getByFilter(marcaDTO: MarcaDTO): Observable<ResponseDTO> {
        const params:any = [{key: "parametroJson", value: JSON.stringify(marcaDTO)}];
        return this.requestService.get("api/marca/getByFilter",  params);
    }

    public create(marcaDTO: MarcaDTO): Observable<ResponseDTO> {
        return this.requestService.post("api/marca/create",  marcaDTO);
    }

    public update(marcaDTO: MarcaDTO): Observable<ResponseDTO> {
        return this.requestService.post("api/marca/update", marcaDTO);
    }

    public delete(marcaDTO: MarcaDTO): Observable<ResponseDTO> {
        return this.requestService.post("api/marca/delete",  marcaDTO);
    }
}