import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { element } from 'protractor';
import { ConfirmationDialogComponent } from 'src/app/core/confirmation-dialog/confirmation-dialog.component';
import { GridService } from 'src/app/services/grid.service';
import { MarcaDataService } from '../marca.dataservice';
import { MarcaDTO } from "../marca.dto";

@Component({
  selector: 'app-marca-list',
  templateUrl: './marca-list.component.html',
  styleUrls: ['./marca-list.component.scss'],
  providers: [GridService, MarcaDataService]
})
export class MarcaListComponent implements OnInit {

  public displayedColumns: string[] =
    [
      'descricao', 'action'
    ];

  form = this.formBuilder.group({
    descricao: ['']
  });

  controls = {
    descricao: this.form.get('descricao')
  }

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    public gridService: GridService<MarcaDTO>,
    private formBuilder: FormBuilder,
    public marcaDataService: MarcaDataService
  ) { }

  ngOnInit(): void {
    this.marcaDataService.getAll()
      .subscribe(response => this.atualizarGrid(response.data));
  }
  private atualizarGrid(data) {
    this.gridService.setDataSource(data)
  }

  public onNewApplicationClick(){
  this.router.navigateByUrl('/marca-create');
  }
  public onEditAccount(element){
    this.router.navigate(['/marca-edit', element.marcaId]);
  }
  public onDeleteAccount(element){
  
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: 'Você confirma a exclusão desta Marca?'
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.marcaDataService.delete(element)
          .subscribe(
            () => { this.deleteCallback(); },
            error => {
              this.snackBar.open('Something went wrong.', 'Warning', { duration: 3000 });
            }
          );
      }
    });    
  }
  
  private deleteCallback() {
    this.marcaDataService.getByFilter(new MarcaDTO())
      .subscribe(response => this.gridService.setDataSource(response.data));
  
    this.snackBar.open('Marca excluida com sucesso!', 'Success', { duration: 3000 });
  }

  }