import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { ResponseDTO } from 'src/app/services/response.dto';
import { MarcaDataService } from '../marca.dataservice';
import { MarcaDTO } from '../marca.dto';

@Component({
  selector: 'app-marca-edit',
  templateUrl: './marca-edit.component.html',
  styleUrls: ['./marca-edit.component.scss'],
  providers: [MarcaDataService]
})
export class MarcaEditComponent implements OnInit {

  private _marcaDTO: MarcaDTO;
  public controls: any;
  
  public newTitle:string = "Editar uma Marca";

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private marcasDataService: MarcaDataService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => this.getMarcas(params['id']));
  }

  public bindControls(controls){
    this.controls = controls;
  }

  private getMarcas(marcaId: string){
    this.marcasDataService.getById(marcaId).subscribe(response =>this.getMarcasCallback(response.data));
  }

  private getMarcasCallback(data){
    this._marcaDTO = data;
    if(this._marcaDTO != null){
      this.controls.marcaId.setValue(this._marcaDTO.marcaId);
      this.controls.descricao.setValue(this._marcaDTO.descricao);
    }
  }

  private mapToDto(controls) : MarcaDTO {
    let marcaDTO: MarcaDTO = new MarcaDTO();
    marcaDTO.marcaId = controls.marcaId.value;
    marcaDTO.descricao = controls.descricao.value;
    return marcaDTO;
  }

  private createCallback(response: ResponseDTO){
    this.snackBar.open('Parametro alterado com sucesso!', 'Success', { duration: 3000 });
    this.router.navigateByUrl('/marca-list');
  }

  public onSubmit(controls: any) {
    this.marcasDataService.update(this.mapToDto(controls)).subscribe( response => this.createCallback(response));
  }
}
