import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ResponseDTO } from 'src/app/services/response.dto';
import { MarcaDataService } from '../marca.dataservice';
import { MarcaDTO } from "../marca.dto";

@Component({
  selector: 'app-marca-create',
  templateUrl: './marca-create.component.html',
  styleUrls: ['./marca-create.component.scss'],
  providers: [MarcaDataService]
})
export class MarcaCreateComponent implements OnInit {

  constructor( private router: Router,
    private snackBar: MatSnackBar,
    private marcaDataService: MarcaDataService) { }

  ngOnInit(): void {
  }

  private mapToDto(controls) : MarcaDTO{
    let marcaDTO : MarcaDTO = new MarcaDTO()
    marcaDTO.descricao = controls.descricao.value;
    return marcaDTO;
  }

  private createCallbackSuccess(response : ResponseDTO){
    this.snackBar.open('Parametro incluido com sucesso!', 'Success', {duration: 3000 });
    this.router.navigateByUrl('/marca-list');
  }

  public onSubmit(controls: any) {
    this.marcaDataService.create(this.mapToDto(controls)).subscribe( 
      response => this.createCallbackSuccess(response), error => this.createCallbackError(error));
  }

  private createCallbackError(error): any{
    this.snackBar.open(error.message, 'Error', {duration: 3000});
  }

}
