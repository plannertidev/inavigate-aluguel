import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ResponseDTO } from 'src/app/services/response.dto';
import { EstadoDataService } from '../estado.dataservice';
import { EstadoDTO } from '../estado.dto';

@Component({
  selector: 'app-estado-create',
  templateUrl: './estado-create.component.html',
  styleUrls: ['./estado-create.component.scss']
})
export class EstadoCreateComponent implements OnInit {

  constructor( private router: Router,
    private snackBar: MatSnackBar,
    private estadoDataService: EstadoDataService) { }

  ngOnInit(): void {
  }
  private mapToDto(controls) : EstadoDTO{
    let estadoDTO : EstadoDTO = new EstadoDTO()
    estadoDTO.nome = controls.nome.value;
    return estadoDTO;
  }

  private createCallbackSuccess(response : ResponseDTO){
    this.snackBar.open('Parametro incluido com sucesso!', 'Success', {duration: 3000 });
    this.router.navigateByUrl('/estado-list');
  }

  public onSubmit(controls: any) {
    this.estadoDataService.create(this.mapToDto(controls)).subscribe( 
      response => this.createCallbackSuccess(response), error => this.createCallbackError(error));
  }

  private createCallbackError(error): any{
    this.snackBar.open(error.message, 'Error', {duration: 3000});
  }
}