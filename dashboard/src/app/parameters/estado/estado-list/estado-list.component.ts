import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/core/confirmation-dialog/confirmation-dialog.component';
import { GridService } from 'src/app/services/grid.service';
import { EstadoDataService } from '../estado.dataservice';
import { EstadoDTO } from '../estado.dto';

@Component({
  selector: 'app-estado-list',
  templateUrl: './estado-list.component.html',
  styleUrls: ['./estado-list.component.scss'],
  providers: [GridService, EstadoDataService]
})
export class EstadoListComponent implements OnInit {
   
  public displayedColumns: string[] =
  [
    'nome', 'action'
  ];

  form = this.formBuilder.group({
    nome: ['']
  });

  controls = {
    nome: this.form.get('nome')
  }

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    public gridService: GridService<EstadoDTO>,
    private formBuilder: FormBuilder,
    public estadoDataService: EstadoDataService
  ) { }

  ngOnInit(): void {
    this.estadoDataService.getAll()
      .subscribe(response => this.atualizarGrid(response.data));
  }
  private atualizarGrid(data) {
    this.gridService.setDataSource(data)
  }
  
  public onNewApplicationClick(){
    this.router.navigateByUrl('/estado-create');
  }
  
  public onEditAccount(element){
    this.router.navigate(['/estado-edit', element.estadoId]);
  }
  
  public onDeleteAccount(element){
  
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: 'Você confirma a exclusão deste Estado?'
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.estadoDataService.delete(element)
          .subscribe(
            () => { this.deleteCallback(); },
            error => {
              this.snackBar.open('Something went wrong.', 'Warning', { duration: 3000 });
            }
          );
      }
    });    
  }
  
  private deleteCallback() {
    this.estadoDataService.getByFilter(new EstadoDTO())
      .subscribe(response => this.gridService.setDataSource(response.data));
  
    this.snackBar.open('Estado excluido com sucesso!', 'Success', { duration: 3000 });
  }  
}