import { EventEmitter, Input, Output } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-estado-form',
  templateUrl: './estado-form.component.html',
  styleUrls: ['./estado-form.component.scss']
})
export class EstadoFormComponent implements OnInit {
  public form: any;
  public controls: any;

  @Output() save: EventEmitter<any> = new EventEmitter();
  @Output() bindControls: EventEmitter<any> = new EventEmitter();
  @Input() title;

  estado_validation_messages = {
    'nome': [
      { type: 'required', message: 'Informe o nome do seu estado' },
      { type: 'pattern', message: 'O nome só pode conter letras' }
    ]
  }
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({});
    this.form.addControl('estadoId', new FormControl());
    this.form.addControl('nome', new FormControl(), Validators.required);

    this.controls = {
      nome: this.form.get('nome'),
      estadoId : this.form.get('estadoId')
    }
    this.bindControls.emit(this.controls);
  }

  public onSubmit() {
    if (this.validFormEntry()) {
      this.save.emit(this.controls);
    }
  }

  private validFormEntry(): boolean {
    return !this.controls.nome.hasError();
  }

}