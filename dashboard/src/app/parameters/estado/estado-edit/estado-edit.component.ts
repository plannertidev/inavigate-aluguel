import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ResponseDTO } from 'src/app/services/response.dto';
import { EstadoDataService } from '../estado.dataservice';
import { EstadoDTO } from '../estado.dto';

@Component({
  selector: 'app-estado-edit',
  templateUrl: './estado-edit.component.html',
  styleUrls: ['./estado-edit.component.scss']
})
export class EstadoEditComponent implements OnInit {
  private _estadoDTO: EstadoDTO;
  public controls: any;

  public newTitle:string = "Editar um Estado";

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private estadoDataService: EstadoDataService) { }

  ngOnInit(): void {
     this.route.params.subscribe(params => this.getEstado(params['id']));
  }

  public bindControls(controls) {
    this.controls = controls;
  }
  
  private getEstado(estadoId: string){
    this.estadoDataService.getById(estadoId).subscribe(response =>this.getEstadoCallback(response.data));
  }
  
  private getEstadoCallback(data){
    this._estadoDTO = data;
    if (this._estadoDTO != null) {
      this.controls.estadoId.setValue(this._estadoDTO.estadoId);
      this.controls.nome.setValue(this._estadoDTO.nome);
    }
  }

  private mapToDto(controls) : EstadoDTO {
    let estadoDTO: EstadoDTO = new EstadoDTO();
    estadoDTO.estadoId = controls.estadoId.value;
    estadoDTO.nome = controls.nome.value;
    return estadoDTO;
  }

  private createCallback(response: ResponseDTO){
    this.snackBar.open('Estado alterado com sucesso!', 'Success', { duration: 3000 });
    this.router.navigateByUrl('/estado-list');
  }

  public onSubmit(controls: any) {
    this.estadoDataService.update(this.mapToDto(controls)).subscribe( response => this.createCallback(response));
  }

}