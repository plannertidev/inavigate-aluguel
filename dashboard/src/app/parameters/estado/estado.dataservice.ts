import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { RequestService } from "src/app/services/request.service";
import { ResponseDTO } from "src/app/services/response.dto";
import { EstadoDTO } from "./estado.dto";

@Injectable()
export class EstadoDataService{
    constructor(private requestService: RequestService){
    }

    public getAll(): Observable<ResponseDTO> {
        return this.requestService.get("api/estado/getAll");
    }

    public getById(estadoId: string): Observable<ResponseDTO> {
        const params:any = [{key: "parametroJson",value: estadoId}];
        return this.requestService.get("api/estado/getById",  params);
    }

    public getByFilter(estadoDTO: EstadoDTO): Observable<ResponseDTO> {
        const params:any = [{key: "parametroJson", value: JSON.stringify(estadoDTO)}];
        return this.requestService.get("api/estado/getByFilter",  params);
    }

    public create(estadoDTO: EstadoDTO): Observable<ResponseDTO> {
        return this.requestService.post("api/estado/create",  estadoDTO);
    }

    public update(estadoDTO: EstadoDTO): Observable<ResponseDTO> {
        return this.requestService.post("api/estado/update", estadoDTO);
    }

    public delete(estadoDTO: EstadoDTO): Observable<ResponseDTO> {
        return this.requestService.post("api/estado/delete",  estadoDTO);
    }
}