import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PropriedadeEditComponent } from './propriedade-edit.component';

describe('PropriedadeEditComponent', () => {
  let component: PropriedadeEditComponent;
  let fixture: ComponentFixture<PropriedadeEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PropriedadeEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PropriedadeEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
