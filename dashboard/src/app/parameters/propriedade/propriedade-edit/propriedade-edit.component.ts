import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ResponseDTO } from 'src/app/services/response.dto';
import { PropriedadeDataService } from '../propriedade.dataservice';
import { PropriedadeDTO } from '../propriedade.dto';

@Component({
  selector: 'app-propriedade-edit',
  templateUrl: './propriedade-edit.component.html',
  styleUrls: ['./propriedade-edit.component.scss'],
  providers: [PropriedadeDataService]
})
export class PropriedadeEditComponent implements OnInit {
  private _propriedadeDTO: PropriedadeDTO;
  public controls: any;

  public newTitle:string = "Editar uma Propriedade";

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private propriedadeDataService: PropriedadeDataService) { }

  ngOnInit(): void {
     this.route.params.subscribe(params => this.getPropriedade(params['id']));

  }

  public bindControls(controls) {
    this.controls = controls;
  }
  
  private getPropriedade(propriedadeId: string){
    this.propriedadeDataService.getById(propriedadeId).subscribe(response =>this.getPropriedadeCallback(response.data));
  }
  
  private getPropriedadeCallback(data){
    console.log(data);
    this._propriedadeDTO = data;
    if (this._propriedadeDTO != null) {
      this.controls.propriedadeId.setValue(this._propriedadeDTO.propriedadeId);
      this.controls.descricao.setValue(this._propriedadeDTO.descricao);
      this.controls.tipo.setValue(this._propriedadeDTO.tipo);
      this.controls.valor.setValue(this._propriedadeDTO.valor);
      this.controls.estadoConservacao.setValue(this._propriedadeDTO.estadoConservacao);
     // this.controls.fabricanteId.setValue(this._propriedadeDTO.fabricanteId);
      this.controls.modeloId.setValue(this._propriedadeDTO.modeloId);
      this.controls.tipoCombustivel.setValue(this._propriedadeDTO.tipoCombustivel);
      this.controls.tamanho.setValue(this._propriedadeDTO.tamanho);
      this.controls.passageiroDia.setValue(this._propriedadeDTO.passageiroDia);
      this.controls.passageiroNoite.setValue(this._propriedadeDTO.passageiroNoite);
      this.controls.motor.setValue(this._propriedadeDTO.motor);
      this.controls.marca.setValue(this._propriedadeDTO.marca);
      this.controls.quantoJaRodou.setValue(this._propriedadeDTO.quantoJaRodou);
      this.controls.localizacao.setValue(this._propriedadeDTO.localizacao);
      this.controls.acessorioEquipamento.setValue(this._propriedadeDTO.acessorioEquipamento);
      this.controls.comunicacaoNavegacao.setValue(this._propriedadeDTO.comunicacaoNavegacao);
      this.controls.eletronico.setValue(this._propriedadeDTO.eletronico);
    }
  }
  private mapToDto(controls) : PropriedadeDTO {
    let propriedadeDTO: PropriedadeDTO = new PropriedadeDTO();
    propriedadeDTO.propriedadeId = controls.propriedadeId.value;
    propriedadeDTO.descricao = controls.descricao.value;
    propriedadeDTO.tipo = controls.tipo.value;
    propriedadeDTO.valor = controls.valor.value;
    propriedadeDTO.estadoConservacao = controls.estadoConservacao.value;
   // propriedadeDTO.fabricanteId = controls.fabricanteId.value;
    propriedadeDTO.modeloId = controls.modeloId.value;
    propriedadeDTO.tipoCombustivel = controls.tipoCombustivel.value;
    propriedadeDTO.tamanho = controls.tamanho  .value;
    propriedadeDTO.passageiroDia = controls.passageiroDia.value;
    propriedadeDTO.passageiroNoite = controls.passageiroNoite.value;
    propriedadeDTO.motor = controls.motor.value;
    propriedadeDTO.marca = controls.marca.value;
    propriedadeDTO.quantoJaRodou = controls.quantoJaRodou.value;
    propriedadeDTO.localizacao = controls.localizacao.value;
    propriedadeDTO.acessorioEquipamento = controls.acessorioEquipamento.value;
    propriedadeDTO.comunicacaoNavegacao = controls.comunicacaoNavegacao.value;
    propriedadeDTO.eletronico = controls.eletronico.value;
   return propriedadeDTO;
  }

  private createCallback(response: ResponseDTO){
    this.snackBar.open('Propriedade alterada com sucesso!', 'Success', { duration: 3000 });
    this.router.navigateByUrl('/propriedade-list');
  }

  public onSubmit(controls: any) {
    this.propriedadeDataService.update(this.mapToDto(controls)).subscribe( response => this.createCallback(response));
  }

}