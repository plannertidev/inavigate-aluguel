import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { RequestService } from "src/app/services/request.service";
import { ResponseDTO } from "src/app/services/response.dto";
import { PropriedadeDTO } from './propriedade.dto';

@Injectable()
export class PropriedadeDataService{
    constructor(private requestService: RequestService){
  }

  public getAll(): Observable<ResponseDTO>{
      return this.requestService.get('api/propriedade/getAll');
  }

  public getById(propriedadeId: string): Observable<ResponseDTO> {
    const params:any = [{key: "parametroJson",value: propriedadeId}];
    return this.requestService.get("api/propriedade/getById",  params);
}

public getByFilter(propriedadeDTO: PropriedadeDTO): Observable<ResponseDTO> {
    const params:any = [{key: "parametroJson", value: JSON.stringify(propriedadeDTO)}];
    return this.requestService.get("api/propriedade/getByFilter",  params);
}

public create(propriedadeDTO: PropriedadeDTO): Observable<ResponseDTO> {
    return this.requestService.post("api/propriedade/create",  propriedadeDTO);
}

public update(propriedadeDTO: PropriedadeDTO): Observable<ResponseDTO> {
    return this.requestService.post("api/propriedade/update", propriedadeDTO);
}

public delete(propriedadeDTO: PropriedadeDTO): Observable<ResponseDTO> {
    return this.requestService.post("api/propriedade/delete",  propriedadeDTO);
}

}
