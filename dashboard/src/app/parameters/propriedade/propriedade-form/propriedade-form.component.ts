import { Component,EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-propriedade-form',
  templateUrl: './propriedade-form.component.html',
  styleUrls: ['./propriedade-form.component.scss']
})
export class PropriedadeFormComponent implements OnInit {
  public form: any;
  public controls: any;

  @Output() save: EventEmitter<any> = new EventEmitter();
  @Output() bindControls: EventEmitter<any> = new EventEmitter();
  @Input() title;

  propriedade_validation_messages = {
    'descricao':[
      { type: 'required', message: 'Informe a descrição' },
      { type: 'minlength', message: 'Descrição precisa ter no mínimo 5 caracteres' },
      { type: 'maxlength', message: 'Descrição não pode ter mais do que 50 caracteres' },
      { type: 'pattern', message: 'A Descrição só pode conter letras e números' }
    ],
    'estadoConservacao':[
      { type: 'required', message: 'Informe o estado da sua propriedade' },
      { type: 'pattern', message: 'O Estado da sua propriedade só pode conter letras e números' }
    ],
    'modeloId':[
      { type: 'required', message: 'Informe o modelo' },
      { type: 'pattern', message: 'A Modelo só pode conter letras e números' }
    ],
    'tipoCombustivel':[
      { type: 'required', message: 'Informe o combustivel' },
      { type: 'pattern', message: 'A Combustivel só pode conter letras e números' }
    ],
    'tamanho':[
      { type: 'required', message: 'Informe o tamanho' },
      { type: 'pattern', message: 'A Tamanho só pode conter letras e números' }
    ],
    'passageiroDia':[
      { type: 'required', message: 'Informe a quantidade de passageiros dia' } ,
      { type: 'pattern', message: 'O passageiros dia só pode conter letras e números' }
    ],
    'passageiroNoite':[
      { type: 'required', message:'Informe a quantidade de passageiros noite'  },
      { type: 'pattern', message: 'O passageiros noite só pode conter letras e números' }
    ],
    'motor':[
      { type: 'required', message: 'Informe o motor' },
      { type: 'pattern', message: 'A motor só pode conter letras e números' }
    ],
    'marca':[
      { type: 'required', message: 'Informe a marca' },
      { type: 'pattern', message: 'A marca só pode conter letras e números' }
    ],
    'acessorioEquipamento':[
      { type: 'required', message: 'Informe os acessorios e equipamento' },
      { type: 'pattern', message: 'Acessorios e equipamento só pode conter letras e números' }
    ],
    'comunicacaoNavegacao':[
      { type: 'required', message: 'Informe as  comunicações e Navegações' },
      { type: 'pattern', message: 'Comunicações e Navegações só pode conter letras e números' }
    ],
    'eletronico':[
      { type: 'required', message: 'Informe os eletronico' },
      { type: 'pattern', message: 'Os eletronicos só pode conter letras e números' }
    ],
    'quantoJaRodou':[
      { type :'required', message: 'Informe quanto sua embarcação já rodou' },
    ],
    'localizacao':[
      { type :'required', message:'Informe a localização'}
    ]

  }

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({});
    this.form.addControl('propriedadeId', new FormControl());
    this.form.addControl('descricao', new FormControl(), Validators.required);
    this.form.addControl('tipo', new FormControl(), Validators.required);
    this.form.addControl('valor', new FormControl(), Validators.required);
    this.form.addControl('estadoConservacao', new FormControl(), Validators.required);
    this.form.addControl('modeloId', new FormControl(), Validators.required);
    this.form.addControl('tipoCombustivel', new FormControl(), Validators.required);
    this.form.addControl('tamanho', new FormControl(), Validators.required);
    this.form.addControl('passageiroDia', new FormControl(), Validators.required);
    this.form.addControl('passageiroNoite', new FormControl(), Validators.required);
    this.form.addControl('motor', new FormControl(), Validators.required);
    this.form.addControl('marca', new FormControl(), Validators.required);
    this.form.addControl('acessorioEquipamento', new FormControl(), Validators.required);
    this.form.addControl('comunicacaoNavegacao', new FormControl(), Validators.required);
    this.form.addControl('quantoJaRodou', new FormControl(), Validators.required);
    this.form.addControl('localizacao',new FormControl(), Validators.required);
    this.form.addControl('eletronico', new FormControl(), Validators.required);

    this.controls = {
      descricao: this.form.get('descricao'),
      propriedadeId: this.form.get('propriedadeId'),
      tipo: this.form.get('tipo'),
      valor: this.form.get('valor'),
      estadoConservacao: this.form.get('estadoConservacao'),
      modeloId: this.form.get('modeloId'),
      tipoCombustivel: this.form.get('tipoCombustivel'),
      tamanho: this.form.get('tamanho'),
      passageiroDia: this.form.get('passageiroDia'),
      passageiroNoite: this.form.get('passageiroNoite'),
      motor: this.form.get('motor'),
      marca: this.form.get('marca'),
      acessorioEquipamento: this.form.get('acessorioEquipamento'),
      comunicacaoNavegacao: this.form.get('comunicacaoNavegacao'),
      quantoJaRodou: this.form.get('quantoJaRodou'),
      localizacao: this.form.get('localizacao'),
      eletronico: this.form.get('eletronico'),

    }
    this.bindControls.emit(this.controls);
    
  }

  public onSubmit(){
    if(this.validFormEntry()){
      this.save.emit(this.controls);
    }
  }

  private validFormEntry(): boolean {
    return !this.controls.descricao.hasError();     
     
  }
}