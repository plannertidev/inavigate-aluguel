import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ResponseDTO } from 'src/app/services/response.dto';
import { PropriedadeDataService } from '../propriedade.dataservice';
import { PropriedadeDTO } from '../propriedade.dto';

@Component({
  selector: 'app-propriedade-create',
  templateUrl: './propriedade-create.component.html',
  styleUrls: ['./propriedade-create.component.scss'],
  providers: [PropriedadeDataService]
})
export class PropriedadeCreateComponent implements OnInit {

  constructor(
    private router: Router,
    private snackBar: MatSnackBar,
    private propriedadeDataService: PropriedadeDataService) { }

  ngOnInit(): void {
  }
  private mapToDto(controls) : PropriedadeDTO {
    let propriedadeDTO: PropriedadeDTO = new PropriedadeDTO();
    propriedadeDTO.descricao = controls.descricao.value;
    propriedadeDTO.acessorioEquipamento = controls.acessorioEquipamento.value;
    propriedadeDTO.tipoCombustivel = controls.tipoCombustivel.value;
    propriedadeDTO.comunicacaoNavegacao = controls.comunicacaoNavegacao.value;
    propriedadeDTO.eletronico = controls.eletronico.value;
    propriedadeDTO.estadoConservacao = controls.estadoConservacao.value;
    propriedadeDTO.marca = controls.marca.value;
    propriedadeDTO.modeloId = controls.modeloId.value;
    propriedadeDTO.motor = controls.motor.value;
    propriedadeDTO.quantoJaRodou = controls.quantoJaRodou.value;
    propriedadeDTO.localizacao = controls.localizacao.value;
    propriedadeDTO.passageiroDia = controls.passageiroDia.value;
    propriedadeDTO.passageiroNoite = controls.passageiroNoite.value
    propriedadeDTO.tamanho = controls.tamanho.value;
    propriedadeDTO.tipo = controls.tipo.value;
    propriedadeDTO.valor = controls.valor.value;
    return propriedadeDTO;
  }

  private createCallbackSuccess(response: ResponseDTO){
    this.snackBar.open('Parametro incluido com sucesso!', 'Success', { duration: 3000 });
    this.router.navigateByUrl('/propriedade-list');
  }

  public onSubmit(controls: any) {
    this.propriedadeDataService.create(this.mapToDto(controls)).subscribe( 
      response => this.createCallbackSuccess(response), error => this.createCallbackError(error));
  }
  
  private createCallbackError(error): any{
    this.snackBar.open(error.message, 'Error', { duration: 3000 });
  }

}
