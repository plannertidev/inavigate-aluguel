export class PropriedadeDTO{
    public  propriedadeId : string;
    public  descricao : string;
    public  tipo : number;
    public  valor : number; 
    public  estadoConservacao : string;
    public  fabricanteId : string;
    public  modeloId : string;
    public  tipoCombustivel : string;
    public  tamanho : number; 
    public  passageiroDia : number; 
    public  passageiroNoite : number; 
    public  motor : string;
    public  marca : string;
    public  quantoJaRodou : string;
    public  localizacao : string
    public  acessorioEquipamento : string;
    public  comunicacaoNavegacao : string;
    public  eletronico: string;

}