import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/core/confirmation-dialog/confirmation-dialog.component';
import { GridService } from 'src/app/services/grid.service';
import { PropriedadeDataService } from '../propriedade.dataservice';
import { PropriedadeDTO } from '../propriedade.dto';

@Component({
  selector: 'app-propriedade-list',
  templateUrl: './propriedade-list.component.html',
  styleUrls: ['./propriedade-list.component.scss'],
  providers: [GridService, PropriedadeDataService]
})
export class PropriedadeListComponent implements OnInit {
  public displayedColumns: string[] =
  [
    'descricao', 'action'
  ];
  
  form = this.formBuilder.group({
    descricao: ['']
  });

  controls = {
    descricao: this.form.get('descricao')
    }

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    public gridService: GridService<PropriedadeDTO>,
    private formBuilder: FormBuilder,
    public propriedadeDataService: PropriedadeDataService
  ) { }

  ngOnInit(): void {
    this.propriedadeDataService.getAll()
      .subscribe(response => this.atualizarGrid(response.data))
  }

  private atualizarGrid(data){
    this.gridService.setDataSource(data)
  }

  public onNewApplicationClick(){
    this.router.navigateByUrl('/propriedade-create');
  }

  public onEditAccount(element){
    this.router.navigate(['/propriedade-edit', element.propriedadeId]);
  }
  
  public onDeleteAccount(element){
  
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: 'Você confirma a exclusão desta Propriedade?'
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.propriedadeDataService.delete(element)
          .subscribe(
            () => { this.deleteCallback(); },
            error => {
              this.snackBar.open('Something went wrong.', 'Warning', { duration: 3000 });
            }
          );
      }
    });    
  }
  
  private deleteCallback() {
    this.propriedadeDataService.getByFilter(new PropriedadeDTO())
      .subscribe(response => this.gridService.setDataSource(response.data));
  
    this.snackBar.open('Propriedade excluida com sucesso!', 'Success', { duration: 3000 });
  }  

}
  