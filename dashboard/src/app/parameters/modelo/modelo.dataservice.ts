import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { RequestService } from "src/app/services/request.service";
import { ResponseDTO } from "src/app/services/response.dto";
import { ModeloDTO } from './modelo.dto';


@Injectable()
export class ModeloDataService {
    constructor(private requestService: RequestService) {
    }

    public getAll(): Observable<ResponseDTO> {
        return this.requestService.get("api/modelo/getAll");
    }

    public getById(modeloId: string): Observable<ResponseDTO> {
        const params:any = [{key: "parametroJson",value: modeloId}];
        return this.requestService.get("api/modelo/getById",  params);
    }

    public getByFilter(modeloDTO: ModeloDTO): Observable<ResponseDTO> {
        const params:any = [{key: "parametroJson", value: JSON.stringify(modeloDTO)}];
        return this.requestService.get("api/modelo/getByFilter",  params);
    }

    public create(modeloDTO: ModeloDTO): Observable<ResponseDTO> {
        return this.requestService.post("api/modelo/create",  modeloDTO);
    }

    public update(modeloDTO: ModeloDTO): Observable<ResponseDTO> {
        return this.requestService.post("api/modelo/update", modeloDTO);
    }

    public delete(modeloDTO: ModeloDTO): Observable<ResponseDTO> {
        return this.requestService.post("api/modelo/delete",  modeloDTO);
    }
}