import { Component,EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-modelo-form',
  templateUrl: './modelo-form.component.html',
  styleUrls: ['./modelo-form.component.scss']
})
export class ModeloFormComponent implements OnInit {
  public form: any;
  public controls: any;

  @Output() save: EventEmitter<any> = new EventEmitter();
  @Output() bindControls: EventEmitter<any> = new EventEmitter();
  @Input() title;
  modelo_validation_messages = {
    'descricao':[
      { type: 'required', message: 'Informe a descrição' },
      { type: 'minlength', message: 'Descrição precisa ter no mínimo 5 caracteres' },
      { type: 'maxlength', message: 'Descrição não pode ter mais do que 50 caracteres' },
      { type: 'pattern', message: 'A Descrição so pode conter letras e números' }
    ]
  }

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({});
    this.form.addControl('modeloId', new FormControl());
    this.form.addControl('descricao', new FormControl(), Validators.required);
    this.form.addControl('alturaBanheiro', new FormControl(), Validators.required);
    this.form.addControl('alturaCabine', new FormControl(), Validators.required);
    this.form.addControl('anguleVpopaGr', new FormControl(), Validators.required);
    this.form.addControl('banheiro', new FormControl(), Validators.required);
    this.form.addControl('boca', new FormControl(), Validators.required);
    this.form.addControl('cabine', new FormControl(), Validators.required);
    this.form.addControl('caladoMinMax', new FormControl(), Validators.required);
    this.form.addControl('capacidadeCarga', new FormControl(), Validators.required);
    this.form.addControl('casco', new FormControl(), Validators.required);
    this.form.addControl('comprimentoLOA', new FormControl(), Validators.required);
    this.form.addControl('comprimentoLWL', new FormControl(), Validators.required);
    this.form.addControl('inicioFimProducao', new FormControl(), Validators.required);
    this.form.addControl('motorDeSerie', new FormControl(), Validators.required);
    this.form.addControl('origem', new FormControl(), Validators.required);
    this.form.addControl('pesoLeve', new FormControl(), Validators.required);
    this.form.addControl('potenciaMaxima', new FormControl(), Validators.required);
    this.form.addControl('potenciaMinima', new FormControl(), Validators.required);
    this.form.addControl('projeto', new FormControl(), Validators.required);
    this.form.addControl('tanqueAgua', new FormControl(), Validators.required);
    this.form.addControl('tanqueCombustivel', new FormControl(), Validators.required);
    this.form.addControl('tripulantesDiaNoite', new FormControl(), Validators.required);
    this.form.addControl('velocidadeMaximaKm',new FormControl(), Validators.required);

    this.controls = {
      descricao: this.form.get('descricao'),
      modeloId: this.form.get('modeloId'),
      alturaBanheiro: this.form.get('alturaBanheiro'),
      alturaCabine: this.form.get('alturaCabine'),
      anguleVpopaGr: this.form.get('anguleVpopaGr'),
      banheiro: this.form.get('banheiro'),
      boca: this.form.get('boca'),
      cabine: this.form.get('cabine'),
      caladoMinMax: this.form.get('caladoMinMax'),
      capacidadeCarga: this.form.get('capacidadeCarga'),
      casco: this.form.get('casco'),
      comprimentoLOA: this.form.get('comprimentoLOA'),
      comprimentoLWL: this.form.get('comprimentoLWL'),
      inicioFimProducao: this.form.get('inicioFimProducao'),
      motorDeSerie: this.form.get('motorDeSerie'),
      origem: this.form.get('origem'),
      pesoLeve: this.form.get('pesoLeve'),
      potenciaMaxima: this.form.get('potenciaMaxima'),
      potenciaMinima: this.form.get('potenciaMinima'),
      projeto: this.form.get('projeto'),
      tanqueAgua: this.form.get('tanqueAgua'),
      tanqueCombustivel: this.form.get('tanqueCombustivel'),
      tripulantesDiaNoite: this.form.get('tripulantesDiaNoite'),
      velocidadeMaximaKm: this.form.get('velocidadeMaximaKm')
    }
    this.bindControls.emit(this.controls);
  }

  public onSubmit(){
    if(this.validFormEntry()){
      this.save.emit(this.controls);
    }
  }

  private validFormEntry(): boolean {
    return !this.controls.descricao.hasError();
  }
}