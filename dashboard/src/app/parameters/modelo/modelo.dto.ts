export class ModeloDTO{
    public modeloId : string;
    public descricao : string;
    public alturaBanheiro : string;
    public alturaCabine : string;
    public anguleVpopaGr : string;
    public banheiro : string;
    public boca : string;
    public cabine : string;
    public caladoMinMax : string;
    public capacidadeCarga : string;
    public casco : string;
    public comprimentoLOA : string;
    public comprimentoLWL : string;
    public inicioFimProducao : string;
    public motorDeSerie : string;
    public origem : string;
    public pesoLeve : string;
    
    public potenciaMaxima : string;
    public potenciaMinima : string;
    public projeto : string;
    public tanqueAgua : string;
    public tanqueCombustivel : string;
    public tripulantesDiaNoite : string;
    public velocidadeMaximaKm  : string;
}