import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/core/confirmation-dialog/confirmation-dialog.component';
import { GridService } from 'src/app/services/grid.service';
import { ModeloDataService } from '../modelo.dataservice';
import { ModeloDTO } from '../modelo.dto';

@Component({
  selector: 'app-modelo-list',
  templateUrl: './modelo-list.component.html',
  styleUrls: ['./modelo-list.component.scss'],
  providers: [GridService, ModeloDataService]
})
export class ModeloListComponent implements OnInit {
  public displayedColumns: string[] =
  [
    'descricao', 'action'
  ];
  
  form = this.formBuilder.group({
    descricao: ['']
  });

  controls = {
    descricao: this.form.get('descricao')
  }

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    public gridService: GridService<ModeloDTO>,
    private formBuilder: FormBuilder,
    public modeloDataService: ModeloDataService
  ) { }

  ngOnInit(): void {
    this.modeloDataService.getAll()
      .subscribe(response => this.atualizarGrid(response.data))
  }

  private atualizarGrid(data){
    this.gridService.setDataSource(data)
  }

  public onNewApplicationClick(){
    this.router.navigateByUrl('/modelo-create');
  }

  public onEditAccount(element){
    this.router.navigate(['/modelo-edit', element.modeloId]);
  }
  
  public onDeleteAccount(element){
  
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: 'Você confirma a exclusão deste Modelo?'
    });
  
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.modeloDataService.delete(element)
          .subscribe(
            () => { this.deleteCallback(); },
            error => {
              this.snackBar.open('Something went wrong.', 'Warning', { duration: 3000 });
            }
          );
      }
    });    
  }
  
  private deleteCallback() {
    this.modeloDataService.getByFilter(new ModeloDTO())
      .subscribe(response => this.gridService.setDataSource(response.data));
  
    this.snackBar.open('Modelo excluido com sucesso!', 'Success', { duration: 3000 });
  }  

}
  