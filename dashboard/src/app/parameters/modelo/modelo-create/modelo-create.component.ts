import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ResponseDTO } from 'src/app/services/response.dto';
import { ModeloDataService } from '../modelo.dataservice';
import { ModeloDTO } from '../modelo.dto';
@Component({
  selector: 'app-modelo-create',
  templateUrl: './modelo-create.component.html',
  styleUrls: ['./modelo-create.component.scss'],
  providers: [ModeloDataService]
})
export class ModeloCreateComponent implements OnInit {

  constructor(
    private router: Router,
    private snackBar: MatSnackBar,
    private modeloDataService: ModeloDataService) { }

  ngOnInit(): void {
  }

  private mapToDto(controls) : ModeloDTO {
    let modeloDTO: ModeloDTO = new ModeloDTO();
    modeloDTO.descricao = controls.descricao.value;
    modeloDTO.alturaBanheiro = controls.alturaBanheiro.value;
    modeloDTO.alturaCabine = controls.alturaCabine.value;
    modeloDTO.anguleVpopaGr = controls.anguleVpopaGr.value;
    modeloDTO.banheiro = controls.banheiro.value;
    modeloDTO.boca = controls.boca.value;
    modeloDTO.cabine = controls.cabine.value;
    modeloDTO.caladoMinMax = controls.caladoMinMax.value;
    modeloDTO.capacidadeCarga = controls.capacidadeCarga.value;
    modeloDTO.casco = controls.casco.value;
    modeloDTO.comprimentoLOA = controls.comprimentoLOA.value;
    modeloDTO.comprimentoLWL = controls.comprimentoLWL.value;
    modeloDTO.inicioFimProducao = controls.inicioFimProducao.value;
    modeloDTO.motorDeSerie = controls.motorDeSerie.value;
    modeloDTO.origem = controls.origem.value;
    modeloDTO.pesoLeve = controls.pesoLeve.value;
   return modeloDTO;
  }

  private createCallbackSuccess(response: ResponseDTO){
    this.snackBar.open('Parametro incluido com sucesso!', 'Success', { duration: 3000 });
    this.router.navigateByUrl('/modelo-list');
  }

  public onSubmit(controls: any) {
    this.modeloDataService.create(this.mapToDto(controls)).subscribe( 
      response => this.createCallbackSuccess(response), error => this.createCallbackError(error));
  }
  
  private createCallbackError(error): any{
    this.snackBar.open(error.message, 'Error', { duration: 3000 });
  }
}
