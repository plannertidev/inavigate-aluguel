import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ResponseDTO } from 'src/app/services/response.dto';
import { ModeloDataService } from '../modelo.dataservice';
import { ModeloDTO } from '../modelo.dto';

@Component({
  selector: 'app-modelo-edit',
  templateUrl: './modelo-edit.component.html',
  styleUrls: ['./modelo-edit.component.scss'],
  providers: [ModeloDataService]
})
export class ModeloEditComponent implements OnInit {
  private _modeloDTO: ModeloDTO;
  public controls: any;

  public newTitle:string = "Editar um Modelo";

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private modeloDataService: ModeloDataService) { }

  ngOnInit(): void {
     this.route.params.subscribe(params => this.getModelo(params['id']));
  }

  public bindControls(controls) {
    this.controls = controls;
  }
  
  private getModelo(modeloId: string){
    this.modeloDataService.getById(modeloId).subscribe(response =>this.getModeloCallback(response.data));
  }
  
  private getModeloCallback(data){
    this._modeloDTO = data;
    if (this._modeloDTO != null) {
      this.controls.modeloId.setValue(this._modeloDTO.modeloId);
      this.controls.descricao.setValue(this._modeloDTO.descricao);
      this.controls.alturaBanheiro.setValue(this._modeloDTO.alturaBanheiro);
      this.controls.alturaCabine.setValue(this._modeloDTO.alturaCabine);
      this.controls.anguleVpopaGr.setValue(this._modeloDTO.anguleVpopaGr);
      this.controls.banheiro.setValue(this._modeloDTO.banheiro);
      this.controls.boca.setValue(this._modeloDTO.boca);
      this.controls.cabine.setValue(this._modeloDTO.cabine);
      this.controls.caladoMinMax.setValue(this._modeloDTO.caladoMinMax);
      this.controls.capacidadeCarga.setValue(this._modeloDTO.capacidadeCarga);
      this.controls.casco.setValue(this._modeloDTO.casco);
      this.controls.comprimentoLOA.setValue(this._modeloDTO.comprimentoLOA);
      this.controls.comprimentoLWL.setValue(this._modeloDTO.comprimentoLWL);
      this.controls.inicioFimProducao.setValue(this._modeloDTO.inicioFimProducao);
      this.controls.motorDeSerie .setValue(this._modeloDTO.motorDeSerie );
      this.controls.origem.setValue(this._modeloDTO.origem);
      this.controls.pesoLeve.setValue(this._modeloDTO.pesoLeve);
      this.controls.potenciaMaxima .setValue(this._modeloDTO.potenciaMaxima );
      this.controls.potenciaMinima .setValue(this._modeloDTO.potenciaMinima );
      this.controls.projeto .setValue(this._modeloDTO.projeto );
      this.controls.tanqueAgua .setValue(this._modeloDTO.tanqueAgua );
      this.controls.tanqueCombustivel .setValue(this._modeloDTO.tanqueCombustivel );
      this.controls.tripulantesDiaNoite .setValue(this._modeloDTO.tripulantesDiaNoite );
      this.controls.velocidadeMaximaKm  .setValue(this._modeloDTO.velocidadeMaximaKm  );
    }
  }

  private mapToDto(controls) : ModeloDTO {
    let modeloDTO: ModeloDTO = new ModeloDTO();
    modeloDTO.modeloId = controls.modeloId.value;
    modeloDTO.alturaBanheiro = controls.alturaBanheiro.value;
    modeloDTO.alturaCabine = controls.alturaCabine.value;
    modeloDTO.anguleVpopaGr = controls.anguleVpopaGr.value;
    modeloDTO.banheiro = controls.banheiro.value;
    modeloDTO.boca = controls.boca.value;
    modeloDTO.cabine = controls.cabine.value;
    modeloDTO.caladoMinMax = controls.caladoMinMax.value;
    modeloDTO.capacidadeCarga = controls.capacidadeCarga.value;
    modeloDTO.casco = controls.casco.value;
    modeloDTO.comprimentoLOA = controls.comprimentoLOA.value;
    modeloDTO.comprimentoLWL = controls.comprimentoLWL.value;
    modeloDTO.inicioFimProducao = controls.inicioFimProducao.value;
    modeloDTO.motorDeSerie = controls.motorDeSerie.value;
    modeloDTO.descricao = controls.descricao.value;
    modeloDTO.origem = controls.origem.value;
    modeloDTO.pesoLeve = controls.pesoLeve.value;
    modeloDTO.potenciaMaxima = controls.potenciaMaxima.value;
    modeloDTO.potenciaMinima = controls.potenciaMinima.value;
    modeloDTO.projeto = controls.projeto.value;
    modeloDTO.tanqueAgua = controls.tanqueAgua.value;
    modeloDTO.tanqueCombustivel = controls.tanqueCombustivel.value;
    modeloDTO.tripulantesDiaNoite = controls.tripulantesDiaNoite.value;
    modeloDTO.velocidadeMaximaKm = controls.velocidadeMaximaKm .value;

    return modeloDTO;
  }

  private createCallback(response: ResponseDTO){
    this.snackBar.open('Modelo alterado com sucesso!', 'Success', { duration: 3000 });
    this.router.navigateByUrl('/modelo-list');
  }

  public onSubmit(controls: any) {
    this.modeloDataService.update(this.mapToDto(controls)).subscribe( response => this.createCallback(response));
  }

}