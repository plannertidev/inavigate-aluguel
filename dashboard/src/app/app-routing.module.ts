import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MarcaCreateComponent } from './parameters/marca/marca-create/marca-create.component';
import { MarcaEditComponent } from './parameters/marca/marca-edit/marca-edit.component';
import { MarcaListComponent } from './parameters/marca/marca-list/marca-list.component';
import { FabricanteCreateComponent } from './parameters/fabricante/fabricante-create/fabricante-create.component';
import { FabricanteEditComponent } from './parameters/fabricante/fabricante-edit/fabricante-edit.component';
import { FabricanteListComponent } from './parameters/fabricante/fabricante-list/fabricante-list.component';
import { TipoPropriedadeCreateComponent } from './parameters/tipo-propriedade/tipo-propriedade-create/tipo-propriedade-create.component';
import { TipoPropriedadeEditComponent } from './parameters/tipo-propriedade/tipo-propriedade-edit/tipo-propriedade-edit.component';
import { TipoPropriedadeListComponent } from './parameters/tipo-propriedade/tipo-propriedade-list/tipo-propriedade-list.component';
import { ModeloCreateComponent } from './parameters/modelo/modelo-create/modelo-create.component';
import { ModeloListComponent } from './parameters/modelo/modelo-list/modelo-list.component';
import { ModeloEditComponent } from './parameters/modelo/modelo-edit/modelo-edit.component';
import { PropriedadeListComponent } from './parameters/propriedade/propriedade-list/propriedade-list.component';
import { PropriedadeCreateComponent } from './parameters/propriedade/propriedade-create/propriedade-create.component';
import { PropriedadeEditComponent } from './parameters/propriedade/propriedade-edit/propriedade-edit.component';
import { CategoriaCreateComponent } from './parameters/categoria/categoria-create/categoria-create.component';
import { CategoriaListComponent } from './parameters/categoria/categoria-list/categoria-list.component';
import { CategoriaEditComponent } from './parameters/categoria/categoria-edit/categoria-edit.component';
import { EstadoListComponent } from './parameters/estado/estado-list/estado-list.component';
import { EstadoCreateComponent } from './parameters/estado/estado-create/estado-create.component';
import { EstadoEditComponent } from './parameters/estado/estado-edit/estado-edit.component';

const routes: Routes = [
  { path: 'fabricante-list', component: FabricanteListComponent },
  { path: 'fabricante-create', component: FabricanteCreateComponent},
  { path: 'fabricante-edit/:id', component: FabricanteEditComponent},
  //{ path: 'modalidade-edit/:id', component: ModalidadeEditComponent },

  { path: 'marca-list', component: MarcaListComponent},
  { path: 'marca-create', component: MarcaCreateComponent},
  { path: 'marca-edit/:id', component: MarcaEditComponent},

  { path: 'tipo-propriedade-list', component:TipoPropriedadeListComponent},
  { path: 'tipo-propriedade-create', component: TipoPropriedadeCreateComponent},
  { path: 'tipo-propriedade-edit/:id', component: TipoPropriedadeEditComponent},
  
  { path: 'modelo-list', component: ModeloListComponent},
  { path: 'modelo-create', component:ModeloCreateComponent},
  { path: 'modelo-edit/:id', component: ModeloEditComponent},
  
  { path: 'propriedade-list', component:PropriedadeListComponent},
  { path: 'propriedade-create', component: PropriedadeCreateComponent},
  { path: 'propriedade-edit/:id', component: PropriedadeEditComponent}, 

  { path: 'categoria-list', component:CategoriaListComponent},
  { path: 'categoria-create', component:CategoriaCreateComponent},
  { path: 'categoria-edit/:id', component: CategoriaEditComponent},

  { path: 'estado-list', component:EstadoListComponent},
  { path: 'estado-create', component:EstadoCreateComponent},
  { path: 'estado-edit/:id', component: EstadoEditComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
