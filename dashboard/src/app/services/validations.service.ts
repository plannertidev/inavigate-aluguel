export class ValidationsService{
    public isValid(controls){
        let result = true;

        controls.forEach(element => {
    
          if(element.hasError()) {
            result = false;
          }
         
        });
    
        return result;        
    }
}