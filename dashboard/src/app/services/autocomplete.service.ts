import { BehaviorSubject } from 'rxjs';
import { FormControl } from '@angular/forms';

export class AutocompleteService<T> {
    public DataSourceCollection: BehaviorSubject<T[]>;
    public FilteredDataSourceCollection: BehaviorSubject<T[]>;
    public AutocompleteFormControl: FormControl;
    public itemSelected: any;
    constructor(control: FormControl) {

        this.DataSourceCollection = new BehaviorSubject<T[]>([]);
        this.FilteredDataSourceCollection = new BehaviorSubject<T[]>([]);
        this.itemSelected = {};

        this.AutocompleteFormControl = control;
        this.AutocompleteFormControl.valueChanges.subscribe(x => this.setItemSelected(x) );
    }

    public setDataSource(data: any) {
        this.DataSourceCollection.next(data.data.result);
        this.FilteredDataSourceCollection.next(data.data.result);
    }

    public getDataSource(): any {
        return this.FilteredDataSourceCollection.value;
    }

    public setItemSelected(item: any): any {
        this.itemSelected = item;

        let tempArray: any[] = [item];
    }

    public getItemSelected(): T {
        return this.itemSelected;
    }
}
