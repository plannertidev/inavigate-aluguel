import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatTableModule } from '@angular/material/table';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FabricanteCreateComponent } from './parameters/fabricante/fabricante-create/fabricante-create.component';
import { FabricanteEditComponent } from './parameters/fabricante/fabricante-edit/fabricante-edit.component';
import { FabricanteFormComponent } from './parameters/fabricante/fabricante-form/fabricante-form.component';
import { FabricanteListComponent } from './parameters/fabricante/fabricante-list/fabricante-list.component';
import { PageHeaderComponent } from './layout/page-header/page-header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatListModule } from '@angular/material/list';
import { RequestService } from './services/request.service';
import {MarcaListComponent} from './parameters/marca/marca-list/marca-list.component';
import { MarcaCreateComponent } from './parameters/marca/marca-create/marca-create.component';
import { MarcaFormComponent } from './parameters/marca/marca-form/marca-form.component';
import { MarcaEditComponent } from './parameters/marca/marca-edit/marca-edit.component';
import { TipoPropriedadeCreateComponent } from './parameters/tipo-propriedade/tipo-propriedade-create/tipo-propriedade-create.component';
import { TipoPropriedadeFormComponent } from './parameters/tipo-propriedade/tipo-propriedade-form/tipo-propriedade-form.component';
import { TipoPropriedadeListComponent } from './parameters/tipo-propriedade/tipo-propriedade-list/tipo-propriedade-list.component';
import { TipoPropriedadeEditComponent } from './parameters/tipo-propriedade/tipo-propriedade-edit/tipo-propriedade-edit.component';
import { ModeloCreateComponent } from './parameters/modelo/modelo-create/modelo-create.component';
import { ModeloListComponent } from './parameters/modelo/modelo-list/modelo-list.component';
import { ModeloFormComponent } from './parameters/modelo/modelo-form/modelo-form.component';
import { ModeloEditComponent } from './parameters/modelo/modelo-edit/modelo-edit.component';
import { ConfirmationDialogComponent } from './core/confirmation-dialog/confirmation-dialog.component';
import { PropriedadeListComponent } from './parameters/propriedade/propriedade-list/propriedade-list.component';
import { PropriedadeCreateComponent } from './parameters/propriedade/propriedade-create/propriedade-create.component';
import { PropriedadeFormComponent } from './parameters/propriedade/propriedade-form/propriedade-form.component';
import { PropriedadeEditComponent } from './parameters/propriedade/propriedade-edit/propriedade-edit.component';
import { CategoriaCreateComponent } from './parameters/categoria/categoria-create/categoria-create.component';
import { CategoriaListComponent } from './parameters/categoria/categoria-list/categoria-list.component';
import { CategoriaEditComponent } from './parameters/categoria/categoria-edit/categoria-edit.component';
import { CategoriaFormComponent } from './parameters/categoria/categoria-form/categoria-form.component';
<<<<<<< HEAD
import { EstadoListComponent } from './parameters/estado/estado-list/estado-list.component';
import { EstadoEditComponent } from './parameters/estado/estado-edit/estado-edit.component';
import { EstadoFormComponent } from './parameters/estado/estado-form/estado-form.component';
import { EstadoCreateComponent } from './parameters/estado/estado-create/estado-create.component';
=======
import { CidadeEditComponent } from './parameters/cidade/cidade-edit/cidade-edit.component';
import { CidadeFormComponent } from './parameters/cidade/cidade-form/cidade-form.component';
import { CidadeListComponent } from './parameters/cidade/cidade-list/cidade-list.component';
import { CidadeCreateComponent } from './parameters/cidade/cidade-create/cidade-create.component';
>>>>>>> b631a5555a5e9211582c3a4a0b29d5223730ecec

@NgModule({
  declarations: [
    AppComponent,

    FabricanteCreateComponent,
    FabricanteEditComponent,
    FabricanteFormComponent,
    FabricanteListComponent,
    PageHeaderComponent,
    MarcaListComponent,
    MarcaCreateComponent,
    MarcaFormComponent,
    MarcaEditComponent,
    TipoPropriedadeCreateComponent,
    TipoPropriedadeFormComponent,
    TipoPropriedadeListComponent,
    TipoPropriedadeEditComponent,
    ModeloCreateComponent,
    ModeloListComponent,
    ModeloFormComponent,
    ModeloEditComponent,
    ConfirmationDialogComponent,
    PropriedadeListComponent,
    PropriedadeCreateComponent,
    PropriedadeFormComponent,
    PropriedadeEditComponent,
    CategoriaCreateComponent,
    CategoriaListComponent,
    CategoriaEditComponent,
    CategoriaFormComponent,
<<<<<<< HEAD
    EstadoListComponent,
    EstadoEditComponent,
    EstadoFormComponent,
    EstadoCreateComponent
=======
    CidadeEditComponent,
    CidadeFormComponent,
    CidadeListComponent,
    CidadeCreateComponent
>>>>>>> b631a5555a5e9211582c3a4a0b29d5223730ecec
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MatTableModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSnackBarModule,
    MatListModule,
    MatIconModule,
    MatInputModule,
    MatDialogModule,
    FormsModule,
  ],
  providers: [RequestService],
  bootstrap: [AppComponent]
})
export class AppModule { }
