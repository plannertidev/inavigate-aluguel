import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'dashboard';

  constructor(
    private  router: Router) {
    }

  public Fabricantelist() {
    this.router.navigateByUrl('/fabricante-list');
  }
  public Marcalist(){
    this.router.navigateByUrl('/marca-list');
  }
  public TipoPropriedadeList(){
    this.router.navigateByUrl('/tipo-propriedade-list');
  }
  public Modelolist(){
    this.router.navigateByUrl('/modelo-list');
  }
  public Propriedadelist(){
    this.router.navigateByUrl('propriedade-list');
  }
  public Categorialist(){
    this.router.navigateByUrl('/categoria-list');
  }
}
