using iNavigate.Application;
using iNavigate.Application.Proprietarios.Queries;
using iNavigate.Application.services;
using iNavigate.Application.TipoPropriedades.Queries;
using iNavigate.Domain.Events;
using iNavigate.Domain.Interfaces;
using iNavigate.Domain.Interfaces.visitante;
using iNavigate.Domain.Notifications;
using INavigate.Data.Context;
using INavigate.Data.Repositories;
using INavigate.Data.Repositories.visitante;
using iNavigateAPI.filters;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using System;

namespace iNavigateAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader());
            });

            services.AddSingleton(provider => Configuration);

            services.AddScoped<ApiExceptionFilter>();

            //  Mediator Pattern
            services.AddMediatR(typeof(Startup));
            services.AddMediatR(AppDomain.CurrentDomain.Load("iNavigate.Application"));
            services.AddMediatR(AppDomain.CurrentDomain.Load("iNavigate.Domain"));
            services.AddScoped<INotificationHandler<DomainNotification>, DomainNotificationHandler>();
            //services.AddScoped(typeof(IPipelineBehavior<,>), typeof(LoggingBehavior<,>));

            //Contexts
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddDbContext<iNavigateContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("iNavigate_connection"), 
                options => options.EnableRetryOnFailure()));

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            //  Queries
            services.AddScoped<IProprietarioQuery, ProprietarioQuery>();
            services.AddScoped<ITipoPropriedadeQuery, TipoPropriedadeQuery>();

            //  Repositories
            services.AddScoped<IFabricanteRepository, FabricanteRepository>();
            services.AddScoped<IMarcaRepository, MarcaRepository>();
            services.AddScoped<IBarcoRepository, PropriedadeRepository>();
            services.AddScoped<IProprietarioRepository, ProprietarioRepository>();
            services.AddScoped<ITipoPropriedadeRepository, TipoPropriedadeRepository>();
            services.AddScoped<IModeloRepository, ModeloRepository>();
            services.AddScoped<ICategoriaRepository, CategoriaRepository>();
            services.AddScoped<IAnuncioRepository, AnuncioRepository>();
            services.AddScoped<IEstadoRepository, EstadoRepository>();
            services.AddScoped<ICidadeRepository, CidadeRepository>();
            services.AddScoped<IMensagemRepository, MensagemRepository>();
            services.AddScoped<IVisitantePoliticaPrivacidadeRepository, VisitantePoliticaPrivacidadeRepository>();
            services.AddScoped<IReservaRepository, ReservaRepository>();
            services.AddScoped<IClienteRepository, ClienteRepository>();
            services.AddScoped<ITabelaPrecoRepository, TabelaPrecoRepository>();
            services.AddScoped<IDisponibilidadeRepository, DisponibilidadeRepository>();

            //  services
            services.AddScoped<IImageService, ImageService>();

            services.AddAuthentication("Bearer")
                .AddIdentityServerAuthentication("Bearer", options =>
                {
                    options.ApiName = "iNavigateAPI";
                    options.Authority = "https://localhost:44347/";
                });
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("CorsPolicy");

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
