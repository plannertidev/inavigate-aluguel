﻿using iNavigate.Application.Commands;
using iNavigate.Application.Queries;
using iNavigate.Domain.Events;
using iNavigateAPI.Controllers.Core;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace iNavigateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DisponibilidadeController : CoreController
    {
        public DisponibilidadeController(
            INotificationHandler<DomainNotification> notifications) : base(notifications)
        {
        }


        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create([FromBody] CreateDisponibilidadeCommand createDisponibilidadeCommand)
        {
            return Response(await Mediator.Send(createDisponibilidadeCommand));
        }

        [HttpPost]
        [Route("Update")]
        public async Task<IActionResult> Update([FromBody] UpdateDisponibilidadeCommand updateDisponibilidadeCommand)
        {
            return Response(await Mediator.Send(updateDisponibilidadeCommand));
        }


        [HttpPost]
        [Route("Delete")]
        public async Task<IActionResult> Delete([FromBody] DeleteDisponibilidadeCommand deleteDisponibilidadeCommand)
        {
            return Response(await Mediator.Send(deleteDisponibilidadeCommand));
        }

        [HttpGet]
        [Route("getAll")]
        public async Task<IActionResult> getAll()
        {
            GetAllDisponibilidadeQuery getAllDisponibilidadeQuery = new GetAllDisponibilidadeQuery();
            return Response(await Mediator.Send(getAllDisponibilidadeQuery));
        }

        [HttpGet]
        [Route("getById")]
        public async Task<IActionResult> getById(string parametroJson)
        {
            GetDisponibilidadeByIdQuery getDisponibilidadeByIdQuery = new GetDisponibilidadeByIdQuery();
            getDisponibilidadeByIdQuery.BarcoId = Guid.Parse(parametroJson);
            return Response(await Mediator.Send(getDisponibilidadeByIdQuery));
        }
        [HttpGet]
        [Route("getDisponibilidadePorBarco")]
        public async Task<IActionResult> getDisponibilidadePorBarco(string parametroJson)
        {
            GetDisponibilidadePorBarcoQuery getDisponibilidadePorBarcoQuery = new GetDisponibilidadePorBarcoQuery();
            getDisponibilidadePorBarcoQuery.BarcoId = Guid.Parse(parametroJson);
            return Response(await Mediator.Send(getDisponibilidadePorBarcoQuery));
        }
    }
}
