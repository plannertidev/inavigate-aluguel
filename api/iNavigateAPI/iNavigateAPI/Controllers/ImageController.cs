﻿using iNavigate.Application.Image;
using iNavigate.Application.services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace iNavigateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImageController : ControllerBase
    {
        private readonly IImageService _imageService;

        public ImageController(IImageService imageService)
        {
            _imageService = imageService;
        }

        [HttpPost]
        [Route("UploadImage")]
        public async Task<IActionResult> UploadImage(IFormFile file)
        {
            return new ObjectResult(await _imageService.UploadImage(file));
        }

        [HttpPost]
        [Route("Upload")]
        public async Task<IActionResult> Upload([FromBody]ImageViewModel image)
        {
            return new ObjectResult(await _imageService.UploadImage(image));
        }

        [HttpGet]
        [Route("getBoatImages")]
        public dynamic getBoatImages(string barcoId)
        {
            List<byte[]> imagesCollection = new List<byte[]>();

            string storagePath = @"\temp\inavigateImageStore\" + barcoId + "\\";
            if (Directory.Exists(storagePath))
            {
                string[] fileEntries = Directory.GetFiles(storagePath);
                foreach (string fileName in fileEntries)
                {
                    byte[] result = System.IO.File.ReadAllBytes(fileName);
                    imagesCollection.Add(result);
                }
            }

            return new { Images = imagesCollection };
        }

        [HttpGet]
        [Route("Download")]
        public dynamic Download(string filename)
        {
            filename = "8e91b4bc-0f94-43b8-a44d-dcdf281773ec.jpg";

            string storagePath = @"\temp\inavigateImageStore\7349cd13-4bb6-40c9-ae93-3d8642d03d7b\";

            FileStream image = null;

            try
            {
                if (System.IO.File.Exists(storagePath + filename))
                {
                    image = System.IO.File.OpenRead(storagePath + filename);

                    byte[] result = System.IO.File.ReadAllBytes(storagePath + filename);

                    return new { Image = result };
                }
                else
                {
                    image = System.IO.File.OpenRead(storagePath + "semFoto.jpg");
                }
            }
            catch (System.Exception ex)
            {
            }

            return File(image, "image/jpeg");
        }
    }
}
