﻿using iNavigate.Application.Commands;
using iNavigate.Application.Propriedades.Commands;
using iNavigate.Application.Propriedades.Queries;
using iNavigate.Application.Queries;
using iNavigate.Domain.Events;
using iNavigateAPI.Controllers.Core;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace iNavigateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BarcoController : CoreController
    {
        public BarcoController(
            INotificationHandler<DomainNotification> notifications) : base(notifications)
        {
        }

        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create([FromBody] CreateBarcoCommand createPropriedadeCommand)
        {
            return Response(await Mediator.Send(createPropriedadeCommand));
        }

        [HttpPost]
        [Route("Update")]
        public async Task<IActionResult> Update([FromBody] UpdateBarcoCommand updatePropriedadeCommand)
        {
            return Response(await Mediator.Send(updatePropriedadeCommand));
        }


        [HttpPost]
        [Route("Delete")]
        public async Task<IActionResult> Delete([FromBody] DeleteBarcoCommand deletePropriedadeCommand)
        {
            return Response(await Mediator.Send(deletePropriedadeCommand));
        }

        [HttpGet]
        [Route("getAll")]
        public async Task<IActionResult> getAll()
        {
            GetAllBarcoQuery getAllBarcoQuery = new GetAllBarcoQuery();
            return Response(await Mediator.Send(getAllBarcoQuery));
        }

        [HttpGet]
        [Route("getByFilter")]
        public async Task<IActionResult> getByFilter()
        {
            GetAllBarcoQuery getPropriedadeQuery = new GetAllBarcoQuery();
            return Response(await Mediator.Send(getPropriedadeQuery));

        }

        [HttpGet]
        [Route("getById")]
        public async Task<IActionResult> getById(string parametroJson)
        {
            GetBarcoByIdQuery getPropriedadeByIdQuery = new GetBarcoByIdQuery();
            getPropriedadeByIdQuery.BarcoId = Guid.Parse(parametroJson);
            return Response(await Mediator.Send(getPropriedadeByIdQuery));
        }

        [HttpGet]
        [Route("getByProprietario")]
        public async Task<IActionResult> getByProprietario(string parametroJson)
        {
            GetBarcoByProprietarioQuery getPropriedadeByProprietarioIdQuery = new GetBarcoByProprietarioQuery();
            getPropriedadeByProprietarioIdQuery.ProprietarioId = Guid.Parse(parametroJson);
            return Response(await Mediator.Send(getPropriedadeByProprietarioIdQuery));
        }

        [HttpPost]
        [Route("aprovar")]
        public async Task<IActionResult> aprovar([FromBody] AprovarBarcoCommand aprovarBarcoCommand)
        {
            return Response(await Mediator.Send(aprovarBarcoCommand));
        }
        

        [HttpGet]
        [Route("getBarcosAprovadosQuery")]
        public async Task<IActionResult> getBarcosAprovadosQuery()
        {
            GetBarcosAprovadosQuery getBarcosAprovadosQuery = new GetBarcosAprovadosQuery();
            return Response(await Mediator.Send(getBarcosAprovadosQuery));
        }

    }
}
