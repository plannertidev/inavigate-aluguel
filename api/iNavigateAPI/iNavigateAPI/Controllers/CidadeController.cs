﻿using iNavigate.Application.Commands;
using iNavigate.Application.Queries;
using iNavigate.Domain.Events;
using iNavigateAPI.Controllers.Core;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iNavigateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CidadeController : CoreController
    {
        public CidadeController(
                INotificationHandler<DomainNotification> notifications) : base(notifications)
        {
        }

        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create([FromBody] CreateCidadeCommand createCidadeCommand)
        {
            return Response(await Mediator.Send(createCidadeCommand));
        }


        [HttpPost]
        [Route("Update")]
        public async Task<IActionResult> Upadete([FromBody] UpdateCidadeCommand updateCidadeCommand)
        {
            return Response(await Mediator.Send(updateCidadeCommand));
        }

        [HttpPost]
        [Route("Delete")]
        public async Task<IActionResult> Delete([FromBody] DeleteCidadeCommand deleteCidadeCommand)
        {
            return Response(await Mediator.Send(deleteCidadeCommand));
        }

        [HttpGet]
        [Route("getAll")]
        public async Task<IActionResult> getAll()
        {
            GetCidadeQuery getCidadeQuery = new GetCidadeQuery();
            return Response(await Mediator.Send(getCidadeQuery));

        }

        [HttpGet]
        [Route("getByFilter")]
        public async Task<IActionResult> getByFilter()
        {
            GetCidadeQuery getCidadeQuery = new GetCidadeQuery();
            return Response(await Mediator.Send(getCidadeQuery));

        }

        [HttpGet]
        [Route("getById")]
        public async Task<IActionResult> getById(string parametroJson)
        {
            GetCidadeByIdQuery getCidadeByIdQuery = new GetCidadeByIdQuery();
            getCidadeByIdQuery.CidadeId = Guid.Parse(parametroJson);
            return Response(await Mediator.Send(getCidadeByIdQuery));

        }

    }
}
