﻿using iNavigate.Application.Queries;
using iNavigate.Application.MarcaDaPropriedades.Commands;
using iNavigate.Application.MarcaDaPropriedades.Queries;
using iNavigate.Domain.Events;
using iNavigateAPI.Controllers.Core;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace iNavigateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class MarcaController : CoreController
    {
        public MarcaController(
                INotificationHandler<DomainNotification> notifications) : base(notifications)
        {
        }

        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create([FromBody] CreateMarcaCommand createMarcaCommand)
        {
            return Response(await Mediator.Send(createMarcaCommand));           
        }

        [HttpPost]
        [Route("Update")]
        public async Task<IActionResult> Update([FromBody] UpdateMarcaCommand updateMarcaCommand)
        {
            return Response(await Mediator.Send(updateMarcaCommand));
        }

        [HttpPost]
        [Route("Delete")]
        public async Task<IActionResult> Delete([FromBody] DeleteMarcaCommand deleteMarcaCommand)
        {
            return Response(await Mediator.Send(deleteMarcaCommand));
        }

        [HttpGet]
        [Route("getAll")]
        public async Task<IActionResult> getAll()
        {
            GetMarcaQuery getMarcaQuery = new GetMarcaQuery();
            return Response(await Mediator.Send(getMarcaQuery));
        }

        [HttpGet]
        [Route("getByFilter")]
        public async Task<IActionResult> getByFilter()
        {
            GetMarcaQuery getMarcaQuery = new GetMarcaQuery();
            return Response(await Mediator.Send(getMarcaQuery));
        }

        [HttpGet]
        [Route("getById")]
        public async Task<IActionResult> getById(string parametroJson)
        {
            GetMarcaByIdQuery getMarcaByIdQuery = new GetMarcaByIdQuery();
            getMarcaByIdQuery.MarcaId = Guid.Parse(parametroJson);
            return Response(await Mediator.Send(getMarcaByIdQuery));
        }
    }
}
