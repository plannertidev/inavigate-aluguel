﻿using iNavigate.Application.Commands;
using iNavigate.Application.Queries;
using iNavigate.Domain.Events;
using iNavigateAPI.Controllers.Core;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace iNavigateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EstadoController : CoreController
    {
        public EstadoController(
                INotificationHandler<DomainNotification> notifications) : base(notifications)
        {
        }

        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create([FromBody] CreateEstadoCommand createEstadoCommand)
        {
            return Response(await Mediator.Send(createEstadoCommand));
        }


        [HttpPost]
        [Route("Update")]
        public async Task<IActionResult> Upadete([FromBody] UpdateEstadoCommand updateEstadoCommand)
        {
            return Response(await Mediator.Send(updateEstadoCommand));
        }

        [HttpPost]
        [Route("Delete")]
        public async Task<IActionResult> Delete([FromBody] DeleteEstadoCommand deleteEstadoCommand)
        {
            return Response(await Mediator.Send(deleteEstadoCommand));
        }

        [HttpGet]
        [Route("getAll")]
        public async Task<IActionResult> getAll()
        {
            GetEstadoQuery getEstadoQuery = new GetEstadoQuery();
            return Response(await Mediator.Send(getEstadoQuery));

        }

        [HttpGet]
        [Route("getByFilter")]
        public async Task<IActionResult> getByFilter()
        {
            GetEstadoQuery getEstadoQuery = new GetEstadoQuery();
            return Response(await Mediator.Send(getEstadoQuery));

        }

        [HttpGet]
        [Route("getById")]
        public async Task<IActionResult> getById(string parametroJson)
        {
            GetEstadoByIdQuery getEstadoByIdQuery = new GetEstadoByIdQuery();
            getEstadoByIdQuery.EstadoId = Guid.Parse(parametroJson);
            return Response(await Mediator.Send(getEstadoByIdQuery));

        }

    }
}