﻿using iNavigate.Domain.Events;
using iNavigate.Domain.Notifications;
using Microsoft.Extensions.DependencyInjection;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace iNavigateAPI.Controllers.Core
{
    [Route("api/[controller]")]
    [ApiController]
    public class CoreController : Controller
    {
        protected IMediator _mediator;

        protected IMediator Mediator => _mediator ?? (_mediator = HttpContext.RequestServices.GetService<IMediator>());

        private readonly DomainNotificationHandler _notifications;

        protected CoreController(INotificationHandler<DomainNotification> notifications)
        {
            _notifications = (DomainNotificationHandler)notifications;
        }

        protected new IActionResult Response(object result = null)
        {
            if (IsValidOperation())
            {
                return Ok(new
                {
                    success = true,
                    data = result
                });
            }

            return BadRequest(new
            {
                success = false,
                errors = _notifications.GetNotifications().Select(n => n.Value)
            });
        }
        
        protected bool IsValidOperation()
        {
            return (!_notifications.HasNotifications());
        }
    }
}

