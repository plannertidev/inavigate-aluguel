﻿using iNavigate.Application.Commands;
using iNavigate.Application.Proprietarios.Queries;
using iNavigate.Domain.Events;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace iNavigateAPI.Controllers.Core
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : CoreController
    {
        public AccountController(
              INotificationHandler<DomainNotification> notifications) : base(notifications)
        {
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> login([FromBody] LoginCommand loginCommand)
        {
            return Response(await Mediator.Send(loginCommand));
        }
    }
}
