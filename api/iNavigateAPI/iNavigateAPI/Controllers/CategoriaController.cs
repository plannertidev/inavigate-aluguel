﻿using iNavigate.Application.Commands;
using iNavigate.Application.Queries;
using iNavigate.Domain.Events;
using iNavigateAPI.Controllers.Core;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace iNavigateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriaController : CoreController
    {
        public CategoriaController(
                INotificationHandler<DomainNotification> notifications) : base(notifications)
        {
        }

        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create([FromBody] CreateCategoriaCommand createCategoriaCommand)
        {
            return Response(await Mediator.Send(createCategoriaCommand));
        }


        [HttpPost]
        [Route("Update")]
        public async Task<IActionResult> Upadete([FromBody] UpdateCategoriaCommand updateCategoriaCommand)
        {
            return Response(await Mediator.Send(updateCategoriaCommand));
        }

        [HttpPost]
        [Route("Delete")]
        public async Task<IActionResult> Delete([FromBody] DeleteCategoriaCommand deleteCategoriaCommand)
        {
            return Response(await Mediator.Send(deleteCategoriaCommand));
        }

        [HttpGet]
        [Route("getAll")]
        public async Task<IActionResult> getAll()
        {
            GetCategoriaQuery getCategoriaQuery = new GetCategoriaQuery();
            return Response(await Mediator.Send(getCategoriaQuery));

        }

        [HttpGet]
        [Route("getByFilter")]
        public async Task<IActionResult> getByFilter()
        {
            GetCategoriaQuery getCategoriaQuery = new GetCategoriaQuery();
            return Response(await Mediator.Send(getCategoriaQuery));

        }

        [HttpGet]
        [Route("getById")]
        public async Task<IActionResult> getById(string parametroJson)
        {
            GetCategoriaByIdQuery getCategoriaByIdQuery = new GetCategoriaByIdQuery();
            getCategoriaByIdQuery.CategoriaId = Guid.Parse(parametroJson);
            return Response(await Mediator.Send(getCategoriaByIdQuery));

        }

    }
}
