﻿using iNavigate.Application.Proprietarios.Commands;
using iNavigate.Application.Proprietarios.Queries;
using iNavigate.Domain.Events;
using iNavigateAPI.Controllers.Core;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace iNavigateAPI.Controllers
{
    public class ProprietarioController : CoreController
    {
        private readonly IProprietarioQuery _proprietarioQuery;
        public ProprietarioController(
              INotificationHandler<DomainNotification> notifications) : base(notifications)
        {
        }

        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create([FromBody] CreateProprietarioCommand insertProprietarioCommand)
        {
            return Response(await Mediator.Send(insertProprietarioCommand));
        }


        [HttpPost]
        [Route("Update")]
        public async Task<IActionResult> Update([FromBody] UpdateProprietarioCommand updateProprietarioCommand)
        {
            return Response(await Mediator.Send(updateProprietarioCommand));
        }


        [HttpPost]
        [Route("Delete")]
        public async Task<IActionResult> Delete([FromBody] DeleteProprietarioCommand deleteProprietarioCommand)
        {
            return Response(await Mediator.Send(deleteProprietarioCommand));
        }

        [HttpGet]
        [Route("getAll")]
        public async Task<IActionResult> getAll()
        {
            return Response(await _proprietarioQuery.getAll());
        }
    }
}
