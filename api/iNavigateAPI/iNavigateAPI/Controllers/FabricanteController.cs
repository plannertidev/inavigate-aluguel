﻿using iNavigate.Application.Commands;
using iNavigate.Application.Queires;
using iNavigate.Application.Queries;
using iNavigate.Domain.Events;
using iNavigateAPI.Controllers.Core;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace iNavigateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FabricanteController : CoreController
    {
        private readonly ILogger _logger;

        public FabricanteController(
             INotificationHandler<DomainNotification> notifications) : base(notifications)
        {
        }

        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> create([FromBody] CreateFabricanteCommand createFabricanteFabricanteCommand)
        {
            return Response(await Mediator.Send(createFabricanteFabricanteCommand));
        }

        [HttpPost]
        [Route("Update")]
        public async Task<IActionResult> Update([FromBody] UpdateFabricanteCommand updateFabricanteCommand)
        {
            return Response(await Mediator.Send(updateFabricanteCommand));
        }


        [HttpPost]
        [Route("Delete")]
        public async Task<IActionResult> Delete([FromBody] DeleteFabricanteCommand deleteFabricanteCommand)
        {
            return Response(await Mediator.Send(deleteFabricanteCommand));
        }

        [HttpGet]
        [Route("getAll")]
        public async Task<IActionResult> getAll(/*string paramentro*/)
        {
            GetFabricanteQuery getFabricantesQuery = new GetFabricanteQuery();
            return Response(await Mediator.Send(getFabricantesQuery));
        }

        [HttpGet]
        [Route("getByFilter")]
        public async Task<IActionResult> getByFilter(/*string paramentro*/)
        {
            GetFabricanteQuery getFabricantesQuery = new GetFabricanteQuery();
            return Response(await Mediator.Send(getFabricantesQuery));
        }

        [HttpGet]
        [Route("getById")]
        public async Task<IActionResult> getById(string parametroJson)
        {
            GetFabricanteById getFabricanteById = new GetFabricanteById();
            getFabricanteById.FabricanteId = Guid.Parse(parametroJson);
            return Response(await Mediator.Send(getFabricanteById));
        }
    }
}