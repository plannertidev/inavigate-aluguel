﻿using iNavigate.Application.Tools;
using iNavigate.Domain.Events;
using iNavigateAPI.Controllers.Core;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace iNavigateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ToolsController : CoreController
    {
        public ToolsController(
            INotificationHandler<DomainNotification> notifications) : base(notifications)
        {
        }

        [HttpGet]
        [Route("importarModelos")]
        public async Task<IActionResult> getAll()
        {
            ImportarModelosCommand getCategoriaQuery = new ImportarModelosCommand();
            return Response(await Mediator.Send(getCategoriaQuery));

        }
    }
}
