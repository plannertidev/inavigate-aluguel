﻿using iNavigate.Application.visitante.mensagenVisitante.Commands;
using iNavigate.Application.visitante.mensagenVisitante.Queries;
using iNavigate.Domain.Events;
using iNavigateAPI.Controllers.Core;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace iNavigateAPI.Controllers.visitante
{
    [Route("api/[controller]")]
    [ApiController]
    public class MensagemController : CoreController
    {
        public MensagemController(
            INotificationHandler<DomainNotification> notifications) : base(notifications)
        {
        }

        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create([FromBody] CreateMensagemCommand createMensagemCommand)
        {
            return Response(await Mediator.Send(createMensagemCommand));
        }

        [HttpPost]
        [Route("Update")]
        public async Task<IActionResult> Update([FromBody] UpdateMensagemCommand updateMensagemCommand)
        {
            return Response(await Mediator.Send(updateMensagemCommand));
        }


        [HttpPost]
        [Route("Delete")]
        public async Task<IActionResult> Delete([FromBody] DeleteMensagemCommand deleteMensagemCommand)
        {
            return Response(await Mediator.Send(deleteMensagemCommand));
        }

        [HttpGet]
        [Route("getAll")]
        public async Task<IActionResult> getAll()
        {
            GetAllMensagemQuery getAllMensagemQuery = new GetAllMensagemQuery();
            return Response(await Mediator.Send(getAllMensagemQuery));
        }

        [HttpGet]
        [Route("getByFilter")]
        public async Task<IActionResult> getByFilter()
        {
            GetAllMensagemQuery getMensagemQuery = new GetAllMensagemQuery();
            return Response(await Mediator.Send(getMensagemQuery));

        }

        [HttpGet]
        [Route("getById")]
        public async Task<IActionResult> getById(string parametroJson)
        {
            GetMensagemByIdQuery getMensagemByIdQuery = new GetMensagemByIdQuery();
            getMensagemByIdQuery.MensagemId = Guid.Parse(parametroJson);
            return Response(await Mediator.Send(getMensagemByIdQuery));
        }
    }
}