﻿using iNavigate.Application.visitante.reserva.Commands;
using iNavigate.Application.visitante.reserva.Queries;
using iNavigate.Domain.Events;
using iNavigateAPI.Controllers.Core;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace iNavigateAPI.Controllers.visitante
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReservaController : CoreController
    {
        public ReservaController(
             INotificationHandler<DomainNotification> notifications) : base(notifications)
        {
        }

        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create([FromBody] CreateReservaCommand createReservaCommand)
        {
            return Response(await Mediator.Send(createReservaCommand));
        }

        [HttpPost]
        [Route("Update")]
        public async Task<IActionResult> Update([FromBody] UpdateReservaCommand updateReservaCommand)
        {
            return Response(await Mediator.Send(updateReservaCommand));
        }


        [HttpPost]
        [Route("Delete")]
        public async Task<IActionResult> Delete([FromBody] DeleteReservaCommand deleteReservaCommand)
        {
            return Response(await Mediator.Send(deleteReservaCommand));
        }

        [HttpGet]
        [Route("getAll")]
        public async Task<IActionResult> getAll()
        {
            GetAllReservaQuery getAllReservaQuery = new GetAllReservaQuery();
            return Response(await Mediator.Send(getAllReservaQuery));
        }

        [HttpGet]
        [Route("getByFilter")]
        public async Task<IActionResult> getByFilter()
        {
            GetAllReservaQuery getReservaQuery = new GetAllReservaQuery();
            return Response(await Mediator.Send(getReservaQuery));

        }

        [HttpGet]
        [Route("getById")]
        public async Task<IActionResult> getById(string parametroJson)
        {
            GetReservaByIdQuery getReservaByIdQuery = new GetReservaByIdQuery();
            getReservaByIdQuery.ReservaId = Guid.Parse(parametroJson);
            return Response(await Mediator.Send(getReservaByIdQuery));
        }

        [HttpGet]
        [Route("getReservaPorBarco")]
        public async Task<IActionResult> getReservaPorBarco(string parametroJson)
        {
            GetReservaPorBarcoQuery getReservaPorBarcoQuery = new GetReservaPorBarcoQuery();
            getReservaPorBarcoQuery.BarcoId = Guid.Parse(parametroJson);
            return Response(await Mediator.Send(getReservaPorBarcoQuery));
        }


    }
}