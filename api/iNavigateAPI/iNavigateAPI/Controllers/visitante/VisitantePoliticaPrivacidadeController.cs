﻿using iNavigate.Application.visitante.visitantePoliticaPrivacidade.Commands;
using iNavigate.Application.visitante.visitantePoliticaPrivacidade.Queries;
using iNavigate.Domain.Events;
using iNavigateAPI.Controllers.Core;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iNavigateAPI.Controllers.visitante
{
    [Route("api/[controller]")]
    [ApiController]
    public class VisitantePoliticaPrivacidadeController : CoreController
    {
        public VisitantePoliticaPrivacidadeController(
            INotificationHandler<DomainNotification> notifications) : base(notifications)
        {
        }

        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create([FromBody] CreateVisitantePoliticaPrivacidadeCommand createVisitantePoliticaPrivacidadeCommand)
        {
            return Response(await Mediator.Send(createVisitantePoliticaPrivacidadeCommand));
        }

        [HttpPost]
        [Route("Update")]
        public async Task<IActionResult> Update([FromBody] UpdateVisitantePoliticaPrivacidadeCommand updateVisitantePoliticaPrivacidadeCommand)
        {
            return Response(await Mediator.Send(updateVisitantePoliticaPrivacidadeCommand));
        }


        [HttpPost]
        [Route("Delete")]
        public async Task<IActionResult> Delete([FromBody] DeleteVisitantePoliticaPrivacidadeCommand deleteVisitantePoliticaPrivacidadeCommand)
        {
            return Response(await Mediator.Send(deleteVisitantePoliticaPrivacidadeCommand));
        }

        [HttpGet]
        [Route("getAll")]
        public async Task<IActionResult> getAll()
        {
            GetAllVisitantePoliticaPrivacidadeQuery getAllVisitantePoliticaPrivacidadeQuery = new GetAllVisitantePoliticaPrivacidadeQuery();
            return Response(await Mediator.Send(getAllVisitantePoliticaPrivacidadeQuery));
        }

        [HttpGet]
        [Route("getByFilter")]
        public async Task<IActionResult> getByFilter()
        {
            GetAllVisitantePoliticaPrivacidadeQuery getVisitantePoliticaPrivacidadeQuery = new GetAllVisitantePoliticaPrivacidadeQuery();
            return Response(await Mediator.Send(getVisitantePoliticaPrivacidadeQuery));

        }

        [HttpGet]
        [Route("getById")]
        public async Task<IActionResult> getById(string parametroJson)
        {
            GetVisitantePoliticaPrivacidadeByIdQuery getVisitantePoliticaPrivacidadeByIdQuery = new GetVisitantePoliticaPrivacidadeByIdQuery();
            getVisitantePoliticaPrivacidadeByIdQuery.VisitantePoliticaPrivacidadeId = Guid.Parse(parametroJson);
            return Response(await Mediator.Send(getVisitantePoliticaPrivacidadeByIdQuery));
        }
    }
}
