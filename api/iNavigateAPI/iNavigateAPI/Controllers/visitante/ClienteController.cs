﻿using iNavigate.Application.visitante.cliente.Command;
using iNavigate.Application.visitante.cliente.Queries;
using iNavigate.Domain.Events;
using iNavigateAPI.Controllers.Core;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace iNavigateAPI.Controllers.visitante
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClienteController : CoreController
    {
        public ClienteController(
            INotificationHandler<DomainNotification> notifications) : base(notifications)
        {
        }

        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create([FromBody] CreateClienteCommand createClienteCommand)
        {
            return Response(await Mediator.Send(createClienteCommand));
        }

        [HttpPost]
        [Route("Update")]
        public async Task<IActionResult> Update([FromBody] UpdateClienteCommand updateClienteCommand)
        {
            return Response(await Mediator.Send(updateClienteCommand));
        }


        [HttpPost]
        [Route("Delete")]
        public async Task<IActionResult> Delete([FromBody] DeleteClienteCommand deleteClienteCommand)
        {
            return Response(await Mediator.Send(deleteClienteCommand));
        }

        [HttpGet]
        [Route("getAll")]
        public async Task<IActionResult> getAll()
        {
            GetAllClienteQuery getAllClienteQuery = new GetAllClienteQuery();
            return Response(await Mediator.Send(getAllClienteQuery));
        }

        [HttpGet]
        [Route("getByFilter")]
        public async Task<IActionResult> getByFilter()
        {
            GetAllClienteQuery getClienteQuery = new GetAllClienteQuery();
            return Response(await Mediator.Send(getClienteQuery));

        }

        [HttpGet]
        [Route("getById")]
        public async Task<IActionResult> getById(string parametroJson)
        {
            GetClienteByIdQuery getClienteByIdQuery = new GetClienteByIdQuery();
            getClienteByIdQuery.ClienteId = Guid.Parse(parametroJson);
            return Response(await Mediator.Send(getClienteByIdQuery));
        }
    }
}