﻿using iNavigate.Application.TipoPropriedades.Commands;
using iNavigate.Application.TipoPropriedades.Queries;
using iNavigate.Domain.Events;
using iNavigateAPI.Controllers.Core;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace iNavigateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoPropriedadeController : CoreController
    {
        public TipoPropriedadeController(
             INotificationHandler<DomainNotification> notifications) : base(notifications)
        {
        }

        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create([FromBody] CreateTipoPropriedadeCommand createTipoPropriedadeCommand)
        {
            return Response(await Mediator.Send(createTipoPropriedadeCommand));
        }

        [HttpPost]
        [Route("Update")]
        public async Task<IActionResult> Update([FromBody] UpdateTipoPropriedadeCommand updateTipoPropriedadeCommand)
        {
            return Response(await Mediator.Send(updateTipoPropriedadeCommand));
        }

        [HttpPost]
        [Route("Delete")]
        public async Task<IActionResult> Delete([FromBody] DeleteTipoPropriedadeCommand deleteTipoPropriedadeCommand)
        {
            return Response(await Mediator.Send(deleteTipoPropriedadeCommand));
        }

        [HttpGet]
        [Route("getAll")]
        public async Task<IActionResult> getAll()
        {
            GetTipoPropriedadeQuery getTipoPropriedadeQuery = new GetTipoPropriedadeQuery();
            return Response(await Mediator.Send(getTipoPropriedadeQuery));
        }

        [HttpGet]
        [Route("getFilter")]
        public async Task<IActionResult> getFilter()
        {
            GetTipoPropriedadeQuery getTipoPropriedadeQuery = new GetTipoPropriedadeQuery();
            return Response(await Mediator.Send(getTipoPropriedadeQuery));
        }

        [HttpGet]
        [Route("getById")]
        public async Task<IActionResult> getById(string parametroJson)
        {
            GetTipoPropriedadeByIdQuery getTipoPropriedadeByIdQuery = new GetTipoPropriedadeByIdQuery();
            getTipoPropriedadeByIdQuery.TipoPropriedadeId = Guid.Parse(parametroJson);
            return Response(await Mediator.Send(getTipoPropriedadeByIdQuery));
        }

    }
}
