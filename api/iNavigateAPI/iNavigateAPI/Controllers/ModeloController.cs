﻿using iNavigate.Application.ModeloDaPropriedades.Commands;
using iNavigate.Application.ModeloDaPropriedades.Queries;
using iNavigate.Domain.Events;
using iNavigateAPI.Controllers.Core;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace iNavigateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ModeloController : CoreController
    {
        public ModeloController(
                INotificationHandler<DomainNotification> notifications) : base(notifications)
        {
        }

        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create([FromBody] CreateModeloCommand createModeloCommand)
        {
            return Response(await Mediator.Send(createModeloCommand));
        }


        [HttpPost]
        [Route("Update")]
        public async Task<IActionResult> Upadete([FromBody] UpdateModeloCommand updateModeloCommand)
        {
            return Response(await Mediator.Send(updateModeloCommand));
        }

        [HttpPost]
        [Route("Delete")]
        public async Task<IActionResult> Delete([FromBody] DeleteModeloCommand deleteModeloCommand)
        {
            return Response(await Mediator.Send(deleteModeloCommand));
        }

        [HttpGet]
        [Route("getAll")]
        public async Task<IActionResult> getAll()
        {
            GetModeloQuery getModeloQuery = new GetModeloQuery();
            return Response(await Mediator.Send(getModeloQuery));

        }

        [HttpGet]
        [Route("getByFilter")]
        public async Task<IActionResult> getByFilter()
        {
            GetModeloQuery getModeloQuery = new GetModeloQuery();
            return Response(await Mediator.Send(getModeloQuery));
        }

        [HttpGet]
        [Route("getById")]
        public async Task<IActionResult> getById(string parametroJson)
        {
            GetModeloByIdQuery getModeloByIdQuery = new GetModeloByIdQuery();
            getModeloByIdQuery.ModeloId = Guid.Parse(parametroJson);
            return Response(await Mediator.Send(getModeloByIdQuery));

        }

    }
}
