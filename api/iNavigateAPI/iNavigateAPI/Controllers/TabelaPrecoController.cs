﻿using iNavigate.Application.Commands;
using iNavigate.Application.Queries;
using iNavigate.Domain.Events;
using iNavigateAPI.Controllers.Core;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iNavigateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TabelaPrecoController : CoreController
    {
        public TabelaPrecoController(
                INotificationHandler<DomainNotification> notifications) : base(notifications)
        {
        }

        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create([FromBody] CreateTabelaPrecoCommand createTabelaPrecoCommand)
        {
            return Response(await Mediator.Send(createTabelaPrecoCommand));
        }


        [HttpPost]
        [Route("Update")]
        public async Task<IActionResult> Upadete([FromBody] UpdateTabelaPrecoCommand updateTabelaPrecoCommand)
        {
            return Response(await Mediator.Send(updateTabelaPrecoCommand));
        }

        [HttpPost]
        [Route("Delete")]
        public async Task<IActionResult> Delete([FromBody] DeleteTabelaPrecoCommand deleteTabelaPrecoCommand)
        {
            return Response(await Mediator.Send(deleteTabelaPrecoCommand));
        }

        [HttpGet]
        [Route("getAll")]
        public async Task<IActionResult> getAll()
        {
            GetTabelaPrecoQuery getTabelaPrecoQuery = new GetTabelaPrecoQuery();
            return Response(await Mediator.Send(getTabelaPrecoQuery));

        }

        [HttpGet]
        [Route("getByFilter")]
        public async Task<IActionResult> getByFilter()
        {
            GetTabelaPrecoQuery getTabelaPrecoQuery = new GetTabelaPrecoQuery();
            return Response(await Mediator.Send(getTabelaPrecoQuery));

        }

        [HttpGet]
        [Route("getById")]
        public async Task<IActionResult> getById(string parametroJson)
        {
            GetTabelaPrecoByIdQuery getTabelaPrecoByIdQuery = new GetTabelaPrecoByIdQuery();
            getTabelaPrecoByIdQuery.TabelaPrecoId = Guid.Parse(parametroJson);
            return Response(await Mediator.Send(getTabelaPrecoByIdQuery));

        }

    }
}