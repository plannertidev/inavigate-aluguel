﻿using iNavigate.Application.ViewModel;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Queries
{
    public class GetBarcoByProprietarioQuery : IRequest<List<BarcoViewModel>>
    {
        public Guid ProprietarioId { get; set; }
    }

    public class GetPropriedadeByProprietarioQueryHandler : IRequestHandler<GetBarcoByProprietarioQuery, List<BarcoViewModel>>
    {
        private readonly iNavigateContext _context;

        public GetPropriedadeByProprietarioQueryHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<List<BarcoViewModel>> Handle(GetBarcoByProprietarioQuery request, CancellationToken cancellationToken)
        {
            List<BarcoViewModel> barcoViewModelcollection = new List<BarcoViewModel>();

            var records =
                from bar in _context.Barco
                join cat in _context.Categoria on bar.CategoriaId equals cat.CategoriaId
                join mod in _context.Modelo on bar.ModeloId equals mod.ModeloId
                join fab in _context.Fabricante on bar.FabricanteId equals fab.FabricanteId
                join disp in _context.Disponibilidade on bar.BarcoId equals disp.BarcoId
                where bar.ProprietarioId.Equals(request.ProprietarioId)
                select new { bar, cat, mod, fab, disp };

            foreach (var record in records.ToList())
            {
                BarcoViewModel barcoViewModel = MapBarcoToViewModel.map(record.bar, record.cat, record.mod, record.fab, record.disp);

                barcoViewModelcollection.Add(barcoViewModel);
            }

            return await Task.FromResult(barcoViewModelcollection);
        }
    }
}