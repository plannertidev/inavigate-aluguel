﻿using iNavigate.Application.ViewModel;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Queries
{
    public class GetBarcoByIdQuery : IRequest<BarcoViewModel>
    {
        public Guid BarcoId { get; set; }
    }

    public class GetPropriedadeByIdQueryHandler : IRequestHandler<GetBarcoByIdQuery, BarcoViewModel>
    {
        private readonly iNavigateContext _context;

        public GetPropriedadeByIdQueryHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<BarcoViewModel> Handle(GetBarcoByIdQuery request, CancellationToken cancellationToken)
        {
            BarcoViewModel barcoViewModel = new BarcoViewModel();

            var records =
                from bar in _context.Barco
                join cat in _context.Categoria on bar.CategoriaId equals cat.CategoriaId
                join mod in _context.Modelo on bar.ModeloId equals mod.ModeloId
                join fab in _context.Fabricante on bar.FabricanteId equals fab.FabricanteId
                join disp in _context.Disponibilidade on bar.BarcoId equals disp.BarcoId
                where bar.BarcoId.Equals(request.BarcoId)
                select new { bar, cat, mod, fab, disp };

            var record = records.FirstOrDefault();
            if (record != null)
            {
                barcoViewModel = MapBarcoToViewModel.map(record.bar, record.cat, record.mod, record.fab, record.disp);
            }

            return await Task.FromResult(barcoViewModel);
        }
    }
    
    
}
