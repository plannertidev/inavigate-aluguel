﻿using iNavigate.Application.ViewModel;
using INavigate.Data.Context;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Propriedades.Queries
{
    public class GetAllBarcoQuery : IRequest<List<BarcoViewModel>>
    {
    }

    public class GetBarcoQueryHandler : IRequestHandler<GetAllBarcoQuery, List<BarcoViewModel>>
    {
        private readonly iNavigateContext _context;

        public GetBarcoQueryHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<List<BarcoViewModel>> Handle(GetAllBarcoQuery request, CancellationToken cancellationToken)
        {
            var records =
                from bar in _context.Barco
                join cat in _context.Categoria on bar.CategoriaId equals cat.CategoriaId
                join mod in _context.Modelo on bar.ModeloId equals mod.ModeloId
                join fab in _context.Fabricante on bar.FabricanteId equals fab.FabricanteId
                join disp in _context.Disponibilidade on bar.BarcoId equals disp.BarcoId
                select new { bar, cat, mod, fab, disp };

            List<BarcoViewModel> PropriedadeViewModelsCollections = new List<BarcoViewModel>();
            foreach (var record in records.ToList())
            {
                BarcoViewModel barcoViewModel = MapBarcoToViewModel.map(record.bar, record.cat, record.mod, record.fab, record.disp);

                PropriedadeViewModelsCollections.Add(barcoViewModel);
            }

            return await Task.FromResult(PropriedadeViewModelsCollections);
        }
    }
}
