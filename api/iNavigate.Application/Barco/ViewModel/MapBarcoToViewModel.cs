﻿using iNavigate.Domain.Entites;

namespace iNavigate.Application.ViewModel
{
    public class MapBarcoToViewModel
    {
        public static BarcoViewModel map(Barco barco, Categoria categoria, Modelo modelo, Fabricante fabricante, Disponibilidade disponibilidade)
        {
            BarcoViewModel barcoViewModel = new BarcoViewModel();
            barcoViewModel.BarcoId = barco.BarcoId.ToString();
            barcoViewModel.Nome = barco.Nome;

            barcoViewModel.CategoriaId = barco.CategoriaId.ToString();
            barcoViewModel.CategoriaDescricao = categoria.Descricao;

            barcoViewModel.FabricanteId = barco.FabricanteId.ToString();
            barcoViewModel.FabricanteNome = fabricante.Nome;

            barcoViewModel.ModeloId = barco.ModeloId.ToString();
            barcoViewModel.ModeloDescricao = modelo.Descricao;

            barcoViewModel.TipoCombustivel = barco.TipoCombustivel;
            barcoViewModel.AnoFabricacao = barco.AnoFabricacao;
            barcoViewModel.UltimaReforma = barco.UltimaReforma;
            barcoViewModel.TripulacaoIncluidaNoPreco = barco.TripulacaoIncluidaNoPreco;
            barcoViewModel.Capacidade = barco.Capacidade;
            barcoViewModel.CapacidadePernoite = barco.CapacidadePernoite;
            barcoViewModel.NumeroDeCabines = barco.NumeroDeCabines;
            barcoViewModel.NumeroDeBanheiros = barco.NumeroDeBanheiros;
            barcoViewModel.Cumprimento = barco.Cumprimento;
            barcoViewModel.Boca = barco.Boca;
            barcoViewModel.Calado = barco.Calado;
            barcoViewModel.Potencia = barco.Potencia;
            barcoViewModel.VelocidadeCruzeiro = barco.VelocidadeCruzeiro;
            barcoViewModel.VelocidadeMaxima = barco.VelocidadeMaxima;
            barcoViewModel.ConsumoMedio = barco.ConsumoMedio;
            barcoViewModel.DepositoAguaDoce = barco.DepositoAguaDoce;
            barcoViewModel.DepositoCombustivel = barco.DepositoCombustivel;
            barcoViewModel.Status = barco.Status;
            barcoViewModel.EquipamentoConves = barco.EquipamentoConves;
            barcoViewModel.EquipamentoEletronico = barco.EquipamentoEletronico;
            barcoViewModel.OutrosEquipamentos = barco.OutrosEquipamentos;
            barcoViewModel.EntretenimentoAquatico = barco.EntretenimentoAquatico;
            barcoViewModel.Tripulacao = barco.Tripulacao;
            barcoViewModel.IdiomasFalado = barco.IdiomasFalado;

            barcoViewModel.Valor = disponibilidade.ValorDiaInteiro;

            barcoViewModel.TituloAnuncio = barco.TituloAnuncio;
            barcoViewModel.TipoAnuncio = barco.TipoAnuncio;

            if (barco.Status == (int)Barco.kdStatus.Pendente)
            {
                barcoViewModel.StatusDescription = "Aguardando aprovação";
            }
            else
            {
                if (barco.Status == (int)Barco.kdStatus.Aprovado)
                {
                    barcoViewModel.StatusDescription = "Aprovado";
                }
                else
                {
                    barcoViewModel.StatusDescription = "Reprovado";
                }
            }


            return barcoViewModel;
        }
    }
}
