﻿using System.Collections.Generic;

namespace iNavigate.Application.ViewModel
{
    public class BarcoViewModel
    {
        public BarcoViewModel () 
        {
            imagesCollection = new List<byte[]>();
        }

        public string BarcoId { get; set; }
        public string ProprietarioId { get; set; }
        public string Nome { get; set; }

        public string CategoriaId { get; set; }
        public string CategoriaDescricao { get; set; }
       
        public string FabricanteId { get; set; }
        public string ModeloId { get; set; }
        public string TipoCombustivel { get; set; }
        public string AnoFabricacao { get; set; }
        public string UltimaReforma { get; set; }
        public decimal TripulacaoIncluidaNoPreco { get; set; }
        public string Capacidade { get; set; }
        public string CapacidadePernoite { get; set; }
        public string NumeroDeCabines { get; set; }
        public string NumeroDeBanheiros { get; set; }
        public string Cumprimento { get; set; }
        public string Boca { get; set; }
        public string Calado { get; set; }
        public string Potencia { get; set; }
        public string VelocidadeCruzeiro { get; set; }
        public string VelocidadeMaxima { get; set; }
        public string ConsumoMedio { get; set; }
        public string DepositoAguaDoce { get; set; }
        public string DepositoCombustivel { get; set; }
        public decimal? Valor { get; set; }
        public string StatusDescription { get; set; }
        public string FabricanteNome { get; internal set; }
        public string ModeloDescricao { get; internal set; }
        public string EquipamentoConves { get; set; }
        public string EquipamentoEletronico { get; set; }
        public string OutrosEquipamentos { get; set; }
        public string EntretenimentoAquatico { get; set; }
        public string Tripulacao { get; set; }
        public string IdiomasFalado { get; set; }
        public decimal Status { get; set; }
        public string TituloAnuncio { get; set; }
        public string TipoAnuncio { get; set; }
        public List<byte[]> imagesCollection;
    }
}




