﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Propriedades.Commands
{
    public class DeleteBarcoCommand : IRequest<Unit>
    {
        public Guid PropriedadeId { get; set; }

        public class Handler : IRequestHandler<DeleteBarcoCommand, Unit>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IBarcoRepository  _propriedadeRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               IBarcoRepository propriedadeRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _propriedadeRepository = propriedadeRepository;
            }

            public async Task<Unit> Handle(DeleteBarcoCommand request, CancellationToken cancellationToken)
            {
                Barco propriedade = _propriedadeRepository.GetById(request.PropriedadeId);
                if (propriedade != null)
                {
                    _propriedadeRepository.Remove(propriedade.BarcoId);
                    _unitOfWork.Commit();
                }

                return Unit.Value;
            }
        }
    }
}
