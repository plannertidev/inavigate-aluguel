﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Commands
{
    public class AprovarBarcoCommand : IRequest<Barco>
    {
        public Guid BarcoId { get; set; }

        public class Handler : IRequestHandler<AprovarBarcoCommand, Barco>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IBarcoRepository _propriedadeRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               IBarcoRepository propriedadeRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _propriedadeRepository = propriedadeRepository;
            }

            public async Task<Barco> Handle(AprovarBarcoCommand request, CancellationToken cancellationToken)
            {
                Barco barco = _propriedadeRepository.GetById(request.BarcoId);

                if (barco != null)
                {
                    barco.Status = (decimal)Barco.kdStatus.Aprovado;
                    barco.LastModifiedBy = "TESTE";
                    barco.LastModified = DateTime.Now;

                    _propriedadeRepository.Update(barco);
                    _unitOfWork.Commit();
                }

                return await Task.FromResult(barco);
            }
        }
    }
}
