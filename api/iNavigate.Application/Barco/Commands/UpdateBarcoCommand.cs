﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Propriedades.Commands
{
    public class UpdateBarcoCommand : IRequest<Barco>
    {
        public Guid BarcoId { get; set; }
        public Guid ProprietarioId { get; set; }
        public string Nome { get; set; }
        public decimal Tipo { get; set; }
        public string FabricanteId { get; set; }
        public string ModeloId { get; set; }
        public string TipoCombustivel { get; set; }
        public string AnoFabricacao { get; set; }
        public string UltimaReforma { get; set; }
        public decimal TripulacaoIncluidaNoPreco { get; set; }
        public string Capacidade { get; set; }
        public string CapacidadePernoite { get; set; }
        public string NumeroDeCabines { get; set; }
        public string NumeroDeBanheiros { get; set; }
        public string Cumprimento { get; set; }
        public string Boca { get; set; }
        public string Calado { get; set; }
        public string Potencia { get; set; }
        public string VelocidadeCruzeiro { get; set; }
        public string VelocidadeMaxima { get; set; }
        public string ConsumoMedio { get; set; }
        public string DepositoAguaDoce { get; set; }
        public string DepositoCombustivel { get; set; }
        public decimal Status { get; set; }
        public string TituloAnuncio { get; set; }
        public string TipoAnuncio { get; set; }

        public class Handler : IRequestHandler<UpdateBarcoCommand, Barco>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IBarcoRepository _propriedadeRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               IBarcoRepository propriedadeRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _propriedadeRepository = propriedadeRepository;
            }

            public async Task<Barco> Handle(UpdateBarcoCommand request, CancellationToken cancellationToken)
            {
                Barco barco = _propriedadeRepository.GetById(request.BarcoId);

                if (barco != null)
                {
                   barco.Nome = request.Nome;
                   //barco.Tipo = request.Tipo;
                   barco.TipoCombustivel = request.TipoCombustivel;
                   barco.AnoFabricacao = request.AnoFabricacao;
                   barco.UltimaReforma = request.UltimaReforma;
                   barco.TripulacaoIncluidaNoPreco = request.TripulacaoIncluidaNoPreco;
                   barco.Capacidade = request.Capacidade;
                   barco.CapacidadePernoite = request.CapacidadePernoite;
                   barco.NumeroDeCabines = request.NumeroDeCabines;
                   barco.NumeroDeBanheiros = request.NumeroDeBanheiros;
                   barco.Cumprimento = request.Cumprimento;
                   barco.Boca = request.Boca;
                   barco.Calado = request.Calado;
                   barco.Potencia = request.Potencia;
                   barco.VelocidadeCruzeiro = request.VelocidadeCruzeiro;
                   barco.VelocidadeMaxima = request.VelocidadeMaxima;
                   barco.ConsumoMedio = request.ConsumoMedio;
                   barco.DepositoAguaDoce = request.DepositoAguaDoce;
                   barco.DepositoCombustivel = request.DepositoCombustivel;
                   barco.Valor = 0;
                   barco.Status = request.Status;
                   barco.TipoAnuncio = request.TipoAnuncio;
                   barco.TituloAnuncio = request.TituloAnuncio;
                   barco.LastModifiedBy = "TESTE";
                   barco.LastModified = DateTime.Now;

                    _propriedadeRepository.Update(barco);
                    _unitOfWork.Commit();
                }

                return await Task.FromResult(barco);
            }
        }
    }
}
