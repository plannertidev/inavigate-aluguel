﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Propriedades.Commands
{
    public class CreateBarcoCommand : IRequest<Barco>
    {
        public Guid BarcoId { get; set; }
        public Guid ProprietarioId { get; set; }
        public Guid CategoriaId { get; set; }
        public Guid FabricanteId { get; set; }
        public string Nome { get; set; }
        public Guid ModeloId { get; set; }
        public string TipoCombustivel { get; set; }
        public string AnoFabricacao { get; set; }
        public string UltimaReforma { get; set; }
        public decimal TripulacaoIncluidaNoPreco { get; set; }
        public string Capacidade { get; set; }
        public string CapacidadePernoite { get; set; }
        public string NumeroDeCabines { get; set; }
        public string NumeroDeBanheiros { get; set; }
        public string Cumprimento { get; set; }
        public string Boca { get; set; }
        public string Calado { get; set; }
        public string Potencia { get; set; }
        public string VelocidadeCruzeiro { get; set; }
        public string VelocidadeMaxima { get; set; }
        public string ConsumoMedio { get; set; }
        public string DepositoAguaDoce { get; set; }
        public string DepositoCombustivel { get; set; }
        public string equipamentoConves { get; set; }
        public string equipamentoEletronico { get; set; }
        public string outrosEquipamentos { get; set; }
        public string entretenimentoAquatico { get; set; }
        public string tripulacao { get; set; }
        public string idiomasFalado { get; set; }
        public decimal? Valor { get; set; }

        public class Handler : IRequestHandler<CreateBarcoCommand, Barco>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IBarcoRepository  _propriedadeRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               IBarcoRepository propriedadeRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _propriedadeRepository = propriedadeRepository;
            }

            public async Task<Barco> Handle(CreateBarcoCommand request, CancellationToken cancellationToken)
            {
                Guid barcoId = Guid.NewGuid();

                Barco barco = new Barco
                {
                    BarcoId = barcoId,
                    ProprietarioId = request.ProprietarioId,
                    Nome = request.Nome,
                    CategoriaId = request.CategoriaId,
                    FabricanteId = request.FabricanteId,
                    ModeloId = request.ModeloId,
                    TipoCombustivel = request.TipoCombustivel,
                    AnoFabricacao = request.AnoFabricacao,
                    UltimaReforma = request.UltimaReforma,
                    TripulacaoIncluidaNoPreco = request.TripulacaoIncluidaNoPreco,
                    Capacidade = request.Capacidade,
                    CapacidadePernoite = request.CapacidadePernoite,
                    NumeroDeCabines = request.NumeroDeCabines,
                    NumeroDeBanheiros = request.NumeroDeBanheiros,
                    Cumprimento = request.Cumprimento,
                    Boca = request.Boca,
                    Calado = request.Calado,
                    Potencia = request.Potencia,
                    VelocidadeCruzeiro = request.VelocidadeCruzeiro,
                    VelocidadeMaxima = request.VelocidadeMaxima,
                    ConsumoMedio = request.ConsumoMedio,
                    DepositoAguaDoce = request.DepositoAguaDoce,
                    DepositoCombustivel = request.DepositoCombustivel,
                    Valor = request.Valor,
                    Status = (int)Barco.kdStatus.Pendente,
                    EquipamentoConves = request.equipamentoConves,
                    EquipamentoEletronico = request.equipamentoEletronico,
                    OutrosEquipamentos = request.outrosEquipamentos,
                    EntretenimentoAquatico = request.entretenimentoAquatico,
                    Tripulacao = request.tripulacao,
                    IdiomasFalado = request.idiomasFalado,
                    CreatedBy = "TESTE",
                    Created = DateTime.Now
                };

                _propriedadeRepository.Add(barco);
                _unitOfWork.Commit();

                return await Task.FromResult(barco);
            }
        }
    }
}

