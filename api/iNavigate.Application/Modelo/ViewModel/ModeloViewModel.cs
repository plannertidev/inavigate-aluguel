﻿namespace iNavigate.Application.ModeloDaPropriedades.ViewModel
{
    public class ModeloViewModel
    {
        public string ModeloId { get; set; }
        public string FabricanteId { get; set; }
        public string FabricanteNome { get; set; }
        public string Descricao { get; set; }
        public string InicioFimProducao { get; set; }
        public string ComprimentoLOA { get; set; } //Lenght(LOA) 8,55 (m) / 28,04 (ft)
        public string ComprimentoLWL { get; set; } //Width(LWL) 7,40 (m) / 2427,20 (ft)
        public string Boca { get; set; } //Beam 3,32 (m) / 10,89 (ft)
        public string CaladoMin { get; set; } //Draft(Min.) /
        public string CaladoMax { get; set; } //Draft(Max.) 1,75 (m) / 574,00 (ft)
        public string PesoLeve { get; set; } //Weight(kg) 2850 (kg) / 6283,2 (lbs)
        public string Cabines { get; set; }
        public string Banheiros { get; set; }  //Cabins / Bath / 1
        public string AlturaBanheiro { get; set; }         //Bath height /
        public string AlturaCabine { get; set; } //Cabine height 1,78 (m) / 583,84 (ft)
        public string Capacidade { get; set; } //Passengers(day / night) 8 / 6
        public string CapacidadePernoite { get; set; }
        public string PotenciaMaxima { get; set; }//Min.Power 11,2 (kW) / 15 (hp)
        public string PotenciaMinima { get; set; }//Max. Power /
        public string TanqueAgua { get; set; } //Water Tank 150 (L) / 39,6 (gal)
        public string TanqueCombustivel { get; set; } //Fuel Tank /
        public string Origem { get; set; } //Origin country Brasil
        public string Casco { get; set; } //Hull Fiberglass (GRP)
        public string MotorDeSerie { get; set; } //Std.Engine
        public string VelocidadeMax { get; set; } //Std.Engine
    }
}
