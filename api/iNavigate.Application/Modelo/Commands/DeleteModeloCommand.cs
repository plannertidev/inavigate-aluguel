﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.ModeloDaPropriedades.Commands
{
    public class DeleteModeloCommand : IRequest<Unit>
    {
        public Guid ModeloId { get; set; }
        
        public class Handler : IRequestHandler<DeleteModeloCommand, Unit>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IModeloRepository _modeloRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               IModeloRepository modeloRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _modeloRepository = modeloRepository;
            }

            public async Task<Unit> Handle(DeleteModeloCommand request, CancellationToken cancellationToken)
            {
                Modelo modelo = _modeloRepository.GetById(request.ModeloId);
                if (modelo != null)
                {
                    _modeloRepository.Remove(modelo.ModeloId);
                    _unitOfWork.Commit();
                }

                return Unit.Value;
            }
        }
    }
}