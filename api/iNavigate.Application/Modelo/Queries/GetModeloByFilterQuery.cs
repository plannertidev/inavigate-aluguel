﻿using iNavigate.Domain.Entites;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Queries
{
    public class GetModeloByFilterQuery : IRequest<List<Modelo>>
    {
        public string Descricao { get; set; }
    }

    public class GetModeloByFilterQueryHandler : IRequestHandler<GetModeloByFilterQuery, List<Modelo>>
    {
        private readonly iNavigateContext _context;

        public GetModeloByFilterQueryHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<List<Modelo>> Handle(GetModeloByFilterQuery request, CancellationToken cancellationToken)
        {
            var records = _context.Modelo.Where(x => x.Descricao.Equals(request.Descricao)).ToList();

            return await Task.FromResult(records);
        }
    }

}
