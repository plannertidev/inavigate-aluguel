﻿using iNavigate.Application.ModeloDaPropriedades.ViewModel;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.ModeloDaPropriedades.Queries
{
    public class GetModeloByIdQuery : IRequest<ModeloViewModel>
    {
        public Guid ModeloId { get; set; }
    }

    public class GetModeloByIdQueryHandler : IRequestHandler<GetModeloByIdQuery, ModeloViewModel>
    {
        private readonly iNavigateContext _context;

        public GetModeloByIdQueryHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<ModeloViewModel> Handle(GetModeloByIdQuery request, CancellationToken cancellationToken)
        {
            ModeloViewModel modeloViewModel = null;

            var record =
                from mod in _context.Modelo
                join fab in _context.Fabricante on mod.FabricanteId equals fab.FabricanteId
                where mod.ModeloId.Equals(request.ModeloId)
                select new { Mod = mod, Fabr = fab };

            var modelo = record.FirstOrDefault();

            if (record.FirstOrDefault () != null)
            {
                modeloViewModel = new ModeloViewModel();
                modeloViewModel.ModeloId = modelo.Mod.ModeloId.ToString();
                modeloViewModel.FabricanteId = modelo.Mod.FabricanteId.ToString();
                modeloViewModel.FabricanteNome = modelo.Fabr.Nome;
                modeloViewModel.Descricao = modelo.Mod.Descricao;
                modeloViewModel.InicioFimProducao = modelo.Mod.InicioFimProducao;
                modeloViewModel.ComprimentoLOA = modelo.Mod.ComprimentoLOA;
                modeloViewModel.ComprimentoLWL = modelo.Mod.ComprimentoLWL;
                modeloViewModel.Boca = modelo.Mod.Boca;
                modeloViewModel.CaladoMin = modelo.Mod.CaladoMin;
                modeloViewModel.CaladoMax = modelo.Mod.CaladoMax;
                modeloViewModel.PesoLeve = modelo.Mod.PesoLeve;
                modeloViewModel.Cabines = modelo.Mod.Cabines;
                modeloViewModel.Banheiros = modelo.Mod.Banheiros;
                modeloViewModel.AlturaBanheiro = modelo.Mod.AlturaBanheiro;
                modeloViewModel.AlturaCabine = modelo.Mod.AlturaCabine;
                modeloViewModel.Capacidade = modelo.Mod.Capacidade;
                modeloViewModel.CapacidadePernoite = modelo.Mod.CapacidadePernoite;
                modeloViewModel.PotenciaMaxima = modelo.Mod.PotenciaMaxima;
                modeloViewModel.PotenciaMinima = modelo.Mod.PotenciaMinima;
                modeloViewModel.TanqueAgua = modelo.Mod.TanqueAgua;
                modeloViewModel.TanqueCombustivel = modelo.Mod.TanqueCombustivel;
                modeloViewModel.Origem = modelo.Mod.Origem;
                modeloViewModel.Casco = modelo.Mod.Casco;
                modeloViewModel.MotorDeSerie = modelo.Mod.MotorDeSerie;
                modeloViewModel.VelocidadeMax = modelo.Mod.VelocidadeMax;
            }

            return await Task.FromResult(modeloViewModel);
        }
    }
}
