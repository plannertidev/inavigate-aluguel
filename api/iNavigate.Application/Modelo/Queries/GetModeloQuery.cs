﻿using iNavigate.Application.ModeloDaPropriedades.ViewModel;
using iNavigate.Domain.Entites;
using INavigate.Data.Context;
using MediatR;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.ModeloDaPropriedades.Queries
{
    public class GetModeloQuery : IRequest<List<ModeloViewModel>>
    {
    }
    public class GetModeloHandler : IRequestHandler<GetModeloQuery, List<ModeloViewModel>>
    {
        private readonly iNavigateContext _context;

        public GetModeloHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<List<ModeloViewModel>> Handle(GetModeloQuery request, CancellationToken cancellationToken)
        {
            var records = from mod in _context.Modelo select mod;

            List<ModeloViewModel> ModeloViewModelCollection = new List<ModeloViewModel>();
            foreach (Modelo modelo in records.ToList())
            {
                ModeloViewModel modeloView = new ModeloViewModel();
                modeloView.ModeloId = modelo.ModeloId.ToString();
                modeloView.Descricao = modelo.Descricao;

                ModeloViewModelCollection.Add(modeloView);
            }

            return await Task.FromResult(ModeloViewModelCollection);
        }
    }
}
