﻿using INavigate.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace iNavigate.Application
{
    public class Query<TEntity> : IQuery<TEntity> where TEntity : class
    {
        protected readonly iNavigateContext Db;
        protected readonly DbSet<TEntity> DbSet;

        public Query(iNavigateContext context)
        {
            Db = context;
            DbSet = Db.Set<TEntity>();
        }

    }
}
