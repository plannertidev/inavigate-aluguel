﻿using iNavigate.Application.visitante.visitantePoliticaPrivacidade.ViewModel;
using iNavigate.Domain.Entites.visitante;
using INavigate.Data.Context;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.visitante.visitantePoliticaPrivacidade.Queries
{
    public class GetAllVisitantePoliticaPrivacidadeQuery : IRequest<List<VisitantePoliticaPrivacidadeViewModel>>
    {
    }

    public class GetAllVisitantePoliticaPrivacidadeHandler : IRequestHandler<GetAllVisitantePoliticaPrivacidadeQuery, List<VisitantePoliticaPrivacidadeViewModel>>
    {
        private readonly iNavigateContext _context;

        public GetAllVisitantePoliticaPrivacidadeHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<List<VisitantePoliticaPrivacidadeViewModel>> Handle(GetAllVisitantePoliticaPrivacidadeQuery request, CancellationToken cancellationToken)
        {
            var records = from vis in _context.VisitantePoliticaPrivacidade select vis;

            List<VisitantePoliticaPrivacidadeViewModel> VisitantePoliticaViewModelCollection = new List<VisitantePoliticaPrivacidadeViewModel>();
            foreach (VisitantePoliticaPrivacidade visitantePoliticaPrivacidade in records.ToList())
            {
                VisitantePoliticaPrivacidadeViewModel visitantePoliticaPrivacidadeViewModel = new VisitantePoliticaPrivacidadeViewModel();
                visitantePoliticaPrivacidadeViewModel.VisitantePoliticaPrivacidadeId = visitantePoliticaPrivacidade.VisitantePoliticaPrivacidadeId.ToString();
                visitantePoliticaPrivacidadeViewModel.Email = visitantePoliticaPrivacidade.Email;
                visitantePoliticaPrivacidadeViewModel.AceitouTermo = visitantePoliticaPrivacidade.AceitouTermo;
                visitantePoliticaPrivacidadeViewModel.PermiteEnvioInformacoes = visitantePoliticaPrivacidade.PermiteEnvioInformacoes;
                visitantePoliticaPrivacidadeViewModel.CreateBy = visitantePoliticaPrivacidade.CreatedBy;
                visitantePoliticaPrivacidadeViewModel.Created = visitantePoliticaPrivacidade.Created;
                visitantePoliticaPrivacidadeViewModel.LastModified = visitantePoliticaPrivacidade.LastModified;
                visitantePoliticaPrivacidadeViewModel.LastModifiedBy = visitantePoliticaPrivacidade.LastModifiedBy;

                VisitantePoliticaViewModelCollection.Add(visitantePoliticaPrivacidadeViewModel);
            }

            return await Task.FromResult(VisitantePoliticaViewModelCollection);
        }
    }

}