﻿using iNavigate.Application.visitante.visitantePoliticaPrivacidade.ViewModel;
using iNavigate.Domain.Entites.visitante;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.visitante.visitantePoliticaPrivacidade.Queries
{
    public class GetVisitantePoliticaPrivacidadeByIdQuery : IRequest<VisitantePoliticaPrivacidadeViewModel>
    {
        public Guid VisitantePoliticaPrivacidadeId { get; set; }
    }

    public class GetVisitantePloticaPrivacidadeByIdHandler : IRequestHandler<GetVisitantePoliticaPrivacidadeByIdQuery, VisitantePoliticaPrivacidadeViewModel>
    {
        private readonly iNavigateContext _context;

        public GetVisitantePloticaPrivacidadeByIdHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<VisitantePoliticaPrivacidadeViewModel> Handle(GetVisitantePoliticaPrivacidadeByIdQuery request, CancellationToken cancellationToken)
        {
            VisitantePoliticaPrivacidadeViewModel visitantePoliticaPrivacidadeViewModel = null;

            var records = from vis in _context.VisitantePoliticaPrivacidade where vis.VisitantePoliticaPrivacidadeId.Equals(request.VisitantePoliticaPrivacidadeId) select vis;
            VisitantePoliticaPrivacidade visitantePoliticaPrivacidade = records.FirstOrDefault();

            if (visitantePoliticaPrivacidade != null)
            {
                visitantePoliticaPrivacidadeViewModel = new VisitantePoliticaPrivacidadeViewModel();

                visitantePoliticaPrivacidadeViewModel.VisitantePoliticaPrivacidadeId = visitantePoliticaPrivacidade.VisitantePoliticaPrivacidadeId.ToString();
                visitantePoliticaPrivacidadeViewModel.Email = visitantePoliticaPrivacidade.Email;
                visitantePoliticaPrivacidadeViewModel.AceitouTermo = visitantePoliticaPrivacidade.AceitouTermo;
                visitantePoliticaPrivacidadeViewModel.PermiteEnvioInformacoes = visitantePoliticaPrivacidade.PermiteEnvioInformacoes;
                visitantePoliticaPrivacidadeViewModel.CreateBy = visitantePoliticaPrivacidade.CreatedBy;
                visitantePoliticaPrivacidadeViewModel.Created = visitantePoliticaPrivacidade.Created;
                visitantePoliticaPrivacidadeViewModel.LastModified = visitantePoliticaPrivacidade.LastModified;
                visitantePoliticaPrivacidadeViewModel.LastModifiedBy = visitantePoliticaPrivacidade.LastModifiedBy;
            }

            return await Task.FromResult(visitantePoliticaPrivacidadeViewModel);
        }
    }

}
