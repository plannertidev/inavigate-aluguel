﻿using iNavigate.Domain.Core;
using System;

namespace iNavigate.Application.visitante.visitantePoliticaPrivacidade.ViewModel
{
    public class VisitantePoliticaPrivacidadeViewModel 
    {
        public string VisitantePoliticaPrivacidadeId { get; set; }
        public string Email { get; set; }
        public int AceitouTermo { get; set; }
        public int PermiteEnvioInformacoes { get; set; }
       
        public string CreateBy { get; set; }

        public DateTime Created { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModified { get; set; }
    }
}
