﻿using iNavigate.Domain.Entites.visitante;
using iNavigate.Domain.Interfaces.visitante;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.visitante.visitantePoliticaPrivacidade.Commands
{
    public class DeleteVisitantePoliticaPrivacidadeCommand : IRequest<Unit>
    {
        public Guid VisitantePoliticaPrivacidadeId { get; set; }

        public class Handler : IRequestHandler<DeleteVisitantePoliticaPrivacidadeCommand, Unit>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IVisitantePoliticaPrivacidadeRepository _visitantePoliticaPrivacidadeRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               IVisitantePoliticaPrivacidadeRepository visitantePoliticaPrivacidadeRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _visitantePoliticaPrivacidadeRepository = visitantePoliticaPrivacidadeRepository;
            }

            public async Task<Unit> Handle(DeleteVisitantePoliticaPrivacidadeCommand request, CancellationToken cancellationToken)
            {
                VisitantePoliticaPrivacidade visitantePoliticaPrivacidade = _visitantePoliticaPrivacidadeRepository.GetById(request.VisitantePoliticaPrivacidadeId);
                if (visitantePoliticaPrivacidade != null)
                {
                    _visitantePoliticaPrivacidadeRepository.Remove(visitantePoliticaPrivacidade.VisitantePoliticaPrivacidadeId);
                    _unitOfWork.Commit();
                }

                return Unit.Value;
            }
        }
    }
}
