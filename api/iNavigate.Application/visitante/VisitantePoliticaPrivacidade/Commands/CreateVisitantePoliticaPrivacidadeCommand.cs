﻿using iNavigate.Domain.Entites.visitante;
using iNavigate.Domain.Interfaces.visitante;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.visitante.visitantePoliticaPrivacidade.Commands
{
    public class CreateVisitantePoliticaPrivacidadeCommand : IRequest<VisitantePoliticaPrivacidade>
    {
        public Guid VisitantePoliticaPrivacidadeId { get; set; }
        public string Email { get; set; }
        public int AceitouTermo { get; set; }
        public int PermiteEnvioInformacoes { get; set; }

        public class Handler : IRequestHandler<CreateVisitantePoliticaPrivacidadeCommand, VisitantePoliticaPrivacidade>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IVisitantePoliticaPrivacidadeRepository _visitantePoliticaPrivacidadeRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               IVisitantePoliticaPrivacidadeRepository visitantePoliticaPrivacidadeRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _visitantePoliticaPrivacidadeRepository = visitantePoliticaPrivacidadeRepository;
            }

            public async Task<VisitantePoliticaPrivacidade> Handle(CreateVisitantePoliticaPrivacidadeCommand request, CancellationToken cancellationToken)
            {
                Guid visitantePoliticaPrivacidadeId = Guid.NewGuid();

                VisitantePoliticaPrivacidade visitantePoliticaPrivacidade = new VisitantePoliticaPrivacidade
                {
                    VisitantePoliticaPrivacidadeId = visitantePoliticaPrivacidadeId,
                    Email = request.Email,
                    AceitouTermo = request.AceitouTermo,
                    PermiteEnvioInformacoes = request.PermiteEnvioInformacoes,
                  
                    CreatedBy = "TESTE",
                    Created = DateTime.Now
                };

                _visitantePoliticaPrivacidadeRepository.Add(visitantePoliticaPrivacidade);
                _unitOfWork.Commit();

                return await Task.FromResult(visitantePoliticaPrivacidade);
            }
        }
    }
}
