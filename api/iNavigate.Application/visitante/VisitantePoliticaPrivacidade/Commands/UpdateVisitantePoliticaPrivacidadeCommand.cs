﻿using iNavigate.Domain.Entites.visitante;
using iNavigate.Domain.Interfaces.visitante;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.visitante.visitantePoliticaPrivacidade.Commands
{
    public class UpdateVisitantePoliticaPrivacidadeCommand : IRequest<VisitantePoliticaPrivacidade>
    {
        public Guid VisitantePoliticaPrivacidadeId { get; set; }
        public string Email { get; set; }
        public int AceitouTermo { get; set; }
        public int PermiteEnvioInformacoes { get; set; }

        public class Handler : IRequestHandler<UpdateVisitantePoliticaPrivacidadeCommand, VisitantePoliticaPrivacidade>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IVisitantePoliticaPrivacidadeRepository _visitantePoliticaPrivacidadeRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
                IVisitantePoliticaPrivacidadeRepository visitantePoliticaPrivacidadeRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _visitantePoliticaPrivacidadeRepository = visitantePoliticaPrivacidadeRepository;
            }

            public async Task<VisitantePoliticaPrivacidade> Handle(UpdateVisitantePoliticaPrivacidadeCommand request, CancellationToken cancellationToken)
            {
                VisitantePoliticaPrivacidade visitantePoliticaPrivacidade = _visitantePoliticaPrivacidadeRepository.GetById(request.VisitantePoliticaPrivacidadeId);

                if (visitantePoliticaPrivacidade != null)
                {
                    visitantePoliticaPrivacidade.Email = request.Email;
                    visitantePoliticaPrivacidade.AceitouTermo = request.AceitouTermo;
                    visitantePoliticaPrivacidade.PermiteEnvioInformacoes = request.PermiteEnvioInformacoes;

                    visitantePoliticaPrivacidade.LastModifiedBy = "TESTE";
                    visitantePoliticaPrivacidade.LastModified = DateTime.Now;

                    _visitantePoliticaPrivacidadeRepository.Update(visitantePoliticaPrivacidade);
                    _unitOfWork.Commit();
                }

                return await Task.FromResult(visitantePoliticaPrivacidade);
            }
        }
    }
}