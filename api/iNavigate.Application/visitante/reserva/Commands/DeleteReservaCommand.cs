﻿using iNavigate.Domain.Entites.visitante;
using iNavigate.Domain.Interfaces.visitante;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.visitante.reserva.Commands
{
    public class DeleteReservaCommand : IRequest<Unit>
    {
        public Guid ReservaId { get; set; }

        public class Handler : IRequestHandler<DeleteReservaCommand, Unit>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IReservaRepository _reservaRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               IReservaRepository reservaRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _reservaRepository = reservaRepository;
            }

            public async Task<Unit> Handle(DeleteReservaCommand request, CancellationToken cancellationToken)
            {
                Reserva reserva = _reservaRepository.GetById(request.ReservaId);
                if (reserva != null)
                {
                    _reservaRepository.Remove(reserva.ReservaId);
                    _unitOfWork.Commit();
                }

                return Unit.Value;
            }
        }
    }
}