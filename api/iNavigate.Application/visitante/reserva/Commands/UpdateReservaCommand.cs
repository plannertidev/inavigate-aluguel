﻿using iNavigate.Domain.Entites.visitante;
using iNavigate.Domain.Interfaces.visitante;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.visitante.reserva.Commands
{
    public class UpdateReservaCommand : IRequest<Reserva>
    {
        public Guid ReservaId { get; set; }
        public Guid BarcoId { get; set; }
        public Guid ClienteId { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fim { get; set; } 
        public int Tipo { get; set; }
        public int Status { get; set; }

        public class Handler : IRequestHandler<UpdateReservaCommand, Reserva>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IReservaRepository _reservaRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
                 IReservaRepository reservaRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _reservaRepository = reservaRepository;
            }

            public async Task<Reserva> Handle(UpdateReservaCommand request, CancellationToken cancellationToken)
            {
                Reserva reserva = _reservaRepository.GetById(request.ReservaId);

                if (reserva != null)
                {
                    reserva.Inicio = request.Inicio;
                    reserva.Fim = request.Fim;
                    reserva.ClienteId = request.ClienteId;
                    reserva.BarcoId = request.BarcoId;
                    reserva.Tipo = request.Tipo;
                    reserva.Status = request.Status;
                    
                    reserva.LastModified = DateTime.Now;

                    _reservaRepository.Update(reserva);
                    try
                    {
                        _unitOfWork.Commit();
                    }
                    catch (Exception e)
                    {

                        throw;
                    }
                   
                }

                return await Task.FromResult(reserva);
            }
        }
    }
}