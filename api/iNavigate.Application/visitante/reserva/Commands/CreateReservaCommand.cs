﻿using iNavigate.Domain.Entites.visitante;
using iNavigate.Domain.Interfaces.visitante;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Linq;
using System.Threading.Tasks;

namespace iNavigate.Application.visitante.reserva.Commands
{
    public class CreateReservaCommand : IRequest<Reserva>
    {
        public Guid ClienteId { get; set; }
        public Guid BarcoId { get; set; }
        public string Inicio { get; set; }
        public string Fim { get; set; }
        public int Tipo { get; set; }
        public int Status { get; set; }

        public class Handler : IRequestHandler<CreateReservaCommand, Reserva>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IReservaRepository _reservaRepository;
            private readonly iNavigateContext _context;

            public Handler(
                iNavigateContext context,
               IMediator mediator,
               IUnitOfWork unitOfWork,
               IReservaRepository reservaRepository)
            {
                _context = context;
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _reservaRepository = reservaRepository;
            }

            public async Task<Reserva> Handle(CreateReservaCommand request, CancellationToken cancellationToken)
            {
                var records = from res in _context.Reserva
                              where request.BarcoId.Equals(res.BarcoId)
                              select res;

                var dados = records.ToList().Where(x => x.Inicio >= DateTime.Parse(request.Inicio) && x.Fim <= DateTime.Parse(request.Inicio) ||
                    x.Inicio <= DateTime.Parse(request.Fim) && x.Fim >= DateTime.Parse(request.Inicio));
                Reserva reserva;

                if(dados.ToList().Count > 0)
                {
                    reserva = null;
                }
                else
                {
                    Guid reservaId = Guid.NewGuid();

                     reserva = new Reserva
                     {
                        ReservaId = reservaId,
                        Inicio = DateTime.Parse(request.Inicio),
                        Fim = DateTime.Parse(request.Fim),
                        ClienteId = request.ClienteId,
                        BarcoId = request.BarcoId,
                        Tipo = request.Tipo,
                        Status = request.Status,

                        Created = DateTime.Now
                     };

                    _reservaRepository.Add(reserva);
                    _unitOfWork.Commit();
                }
                
                return await Task.FromResult(reserva);
            }
        }
    }
}