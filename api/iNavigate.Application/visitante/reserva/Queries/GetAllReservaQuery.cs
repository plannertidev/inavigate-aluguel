﻿using iNavigate.Application.ViewModel;
using iNavigate.Application.visitante.reserva.ViewModel;
using iNavigate.Domain.Entites.visitante;
using INavigate.Data.Context;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.visitante.reserva.Queries
{
    public class GetAllReservaQuery : IRequest<List<ReservaViewModel>>
    {
    }

    public class GetAllReservaHandler : IRequestHandler<GetAllReservaQuery, List<ReservaViewModel>>
    {
        private readonly iNavigateContext _context;

        public GetAllReservaHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<List<ReservaViewModel>> Handle(GetAllReservaQuery request, CancellationToken cancellationToken)
        {
            var records = from res in _context.Reserva
                          join bar in _context.Barco on res.BarcoId equals bar.BarcoId
                          select new { res, bar };

            List<ReservaViewModel> ReservaViewModelCollection = new List<ReservaViewModel>();
            foreach (var record in records.ToList())
            {
                ReservaViewModel reservaViewModel = MapReservaToViewModel.map(record.res, record.bar);
                ReservaViewModelCollection.Add(reservaViewModel);
            }

            return await Task.FromResult(ReservaViewModelCollection);
        }
    }
}