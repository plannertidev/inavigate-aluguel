﻿using iNavigate.Application.visitante.reserva.ViewModel;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace iNavigate.Application.visitante.reserva.Queries
{
   public class GetReservaPorBarcoQuery : IRequest<List<ReservaViewModel>> 
    {
        public Guid BarcoId { get; set; }
    }

    public class GetDisponibilidadePorBarcoQueryHandler : IRequestHandler<GetReservaPorBarcoQuery, List<ReservaViewModel>>
    {
        private readonly iNavigateContext _context;

        public GetDisponibilidadePorBarcoQueryHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<List<ReservaViewModel>> Handle(GetReservaPorBarcoQuery request, CancellationToken cancellationToken)
        {
            var records = from res in _context.Reserva
                          join bar in _context.Barco on res.BarcoId equals bar.BarcoId
                          where res.BarcoId.Equals(request.BarcoId) 
                          select new { res, bar };
           
            List<ReservaViewModel> ReservaViewModelsCollections = new List<ReservaViewModel>();
            foreach (var record in records.ToList())
            {
                ReservaViewModel reservaViewModel = new ReservaViewModel();

                reservaViewModel.ReservaId = record.res.ReservaId.ToString();
                reservaViewModel.Inicio = record.res.Inicio;
                reservaViewModel.Fim = record.res.Fim;
                reservaViewModel.BarcoId = record.res.BarcoId.ToString();
                reservaViewModel.BarcoDescription = record.bar.Nome;
                reservaViewModel.Tipo = record.res.Tipo;
                reservaViewModel.Status = record.res.Status;
                reservaViewModel.CreateBy = record.res.CreatedBy;
                reservaViewModel.Created = record.res.Created;
                reservaViewModel.LastModified = record.res.LastModified;
                reservaViewModel.LastModifiedBy = record.res.LastModifiedBy;

                ReservaViewModelsCollections.Add(reservaViewModel);
            }

            return await Task.FromResult(ReservaViewModelsCollections);
        }
    }
}