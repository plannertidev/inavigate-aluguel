﻿using iNavigate.Application.visitante.reserva.ViewModel;
using iNavigate.Domain.Entites.visitante;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.visitante.reserva.Queries
{
    public class GetReservaByIdQuery : IRequest<ReservaViewModel>
    {
        public Guid ReservaId { get; set; }
    }

    public class GetReservaByIdHandler : IRequestHandler<GetReservaByIdQuery, ReservaViewModel>
    {
        private readonly iNavigateContext _context;

        public GetReservaByIdHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<ReservaViewModel> Handle(GetReservaByIdQuery request, CancellationToken cancellationToken)
        {
            ReservaViewModel reservaViewModel = null;

            var records = from res in _context.Reserva where res.ReservaId.Equals(request.ReservaId) select res;
            Reserva reserva = records.FirstOrDefault();

            if (reserva != null)
            {
                reservaViewModel = new ReservaViewModel();

                reservaViewModel.ReservaId = reserva.ReservaId.ToString();
                reservaViewModel.Inicio = reserva.Inicio;
                reservaViewModel.Fim = reserva.Fim;
                reservaViewModel.Tipo = reserva.Tipo;
                reservaViewModel.Status = reserva.Status;
                reservaViewModel.CreateBy = reserva.CreatedBy;
                reservaViewModel.Created = reserva.Created;
                reservaViewModel.LastModified = reserva.LastModified;
                reservaViewModel.LastModifiedBy = reserva.LastModifiedBy;
            }

            return await Task.FromResult(reservaViewModel);
        }
    }
}