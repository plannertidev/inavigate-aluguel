﻿using System;

namespace iNavigate.Application.visitante.reserva.ViewModel
{
    public  class ReservaViewModel
    {
        public string ReservaId { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fim { get; set; }
        public int Tipo { get; set; }
        public string TipoDescription { get; set; }
        public int Status { get; set; }
        public string StatusDescription { get; set; }
        public string CreateBy { get; set; }
        public DateTime Created { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModified { get; set; }
        public string BarcoDescription { get; set; }
        public string BarcoId { get; set; }
    }
}
