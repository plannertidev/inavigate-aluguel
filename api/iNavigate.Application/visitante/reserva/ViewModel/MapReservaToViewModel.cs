﻿using iNavigate.Application.visitante.reserva.ViewModel;
using iNavigate.Domain.Entites;
using iNavigate.Domain.Entites.visitante;

namespace iNavigate.Application.ViewModel
{
    public class MapReservaToViewModel
    {
        public static ReservaViewModel map(Reserva reserva, Barco barco)
        {
            ReservaViewModel reservaViewModel = new ReservaViewModel();
            reservaViewModel.BarcoId = barco.BarcoId.ToString();
            reservaViewModel.BarcoDescription = barco.Nome;
            reservaViewModel.ReservaId = reserva.ReservaId.ToString();
            reservaViewModel.Inicio = reserva.Inicio;
            reservaViewModel.Fim = reserva.Fim;
            reservaViewModel.Tipo = reserva.Tipo;
            reservaViewModel.TipoDescription = Reserva.GetDescription((Reserva.KdTipo)reservaViewModel.Tipo);
            reservaViewModel.Status = reserva.Status;
            reservaViewModel.StatusDescription = Reserva.GetDescription((Reserva.KdStatus)reservaViewModel.Status);
            reservaViewModel.CreateBy = reserva.CreatedBy;
            reservaViewModel.Created = reserva.Created;
            reservaViewModel.LastModified = reserva.LastModified;
            reservaViewModel.LastModifiedBy = reserva.LastModifiedBy;

            return reservaViewModel;
        }
    }
}
