﻿using iNavigate.Domain.Entites.visitante;
using iNavigate.Domain.Interfaces.visitante;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.visitante.mensagenVisitante.Commands
{
    public class UpdateMensagemCommand : IRequest<Mensagem>
    {
        public Guid MensagemId { get; set; }
        public DateTime DataPretendida { get; set; }
        public decimal NumeroDias { get; set; }
        public int IncluirMarinheiro { get; set; }
        public decimal NumeroPassageiros { get; set; }
        public string Remetente { get; set; }
        public string RemetenteEmail { get; set; }
        public string RemetenteTelefone { get; set; }
        public string Assunto { get; set; }
        public int Status { get; set; }


        public class Handler : IRequestHandler<UpdateMensagemCommand, Mensagem>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IMensagemRepository _mensagemRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
                 IMensagemRepository mensagemRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _mensagemRepository = mensagemRepository;
            }

            public async Task<Mensagem> Handle(UpdateMensagemCommand request, CancellationToken cancellationToken)
            {
                Mensagem mensagem = _mensagemRepository.GetById(request.MensagemId);

                if (mensagem != null)
                {
                    mensagem.DataPretendida = DateTime.Today;
                    mensagem.QuantidadeDias = request.NumeroDias;
                    mensagem.IncluirMarinheiro = request.IncluirMarinheiro;
                    mensagem.NumeroPassageiros = request.NumeroPassageiros;
                    mensagem.RemetenteNome = request.Remetente;
                    mensagem.RemetenteEmail = request.RemetenteEmail;
                    mensagem.RemetenteTelefone = request.RemetenteTelefone;
                    mensagem.Status = request.Status;
                    
                    mensagem.RemetenteEmail = request.RemetenteEmail;
                    mensagem.LastModifiedBy = "TESTE";
                    mensagem.LastModified = DateTime.Now;

                    _mensagemRepository.Update(mensagem);
                    _unitOfWork.Commit();
                }

                return await Task.FromResult(mensagem);
            }
        }
    }
}
