﻿using iNavigate.Domain.Entites.visitante;
using iNavigate.Domain.Interfaces.visitante;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.visitante.mensagenVisitante.Commands
{
    public class DeleteMensagemCommand : IRequest<Unit>
    {
        public Guid MensagemId { get; set; }

        public class Handler : IRequestHandler<DeleteMensagemCommand, Unit>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IMensagemRepository _mensagemRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               IMensagemRepository mensagemRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _mensagemRepository = mensagemRepository;
            }

            public async Task<Unit> Handle(DeleteMensagemCommand request, CancellationToken cancellationToken)
            {
                Mensagem mensagem = _mensagemRepository.GetById(request.MensagemId);
                if (mensagem != null)
                {
                    _mensagemRepository.Remove(mensagem.MensagemId);
                    _unitOfWork.Commit();
                }

                return Unit.Value;
            }
        }
    }
}