﻿using iNavigate.Domain.Entites.visitante;
using iNavigate.Domain.Interfaces.visitante;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.visitante.mensagenVisitante.Commands
{
    public class CreateMensagemCommand : IRequest<Mensagem>
    {
        public string DataPretendida { get; set; }
        public decimal NumeroDias { get; set; }
        public decimal IncluirMarinheiro { get; set; }
        public decimal NumeroPassageiros { get; set; }
        public string RemetenteNome { get; set; }
        public string RemetenteEmail { get; set; }
        public string RemetenteTelefone { get; set; }
        public string Assunto { get; set; }
        public int Status { get; set; }

        public class Handler : IRequestHandler<CreateMensagemCommand, Mensagem>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IMensagemRepository _mensagemRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               IMensagemRepository mensagemRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _mensagemRepository = mensagemRepository;
            }

            public async Task<Mensagem> Handle(CreateMensagemCommand request, CancellationToken cancellationToken)
            {
                Guid mensagemId = Guid.NewGuid();
                int resultIncluirMarinheiro;

                if (request.IncluirMarinheiro.Equals(1))
                {
                    resultIncluirMarinheiro = (int)Mensagem.kdIncluirMarinheiro.Sim;
                }
                else
                {
                    resultIncluirMarinheiro = (int)Mensagem.kdIncluirMarinheiro.Não;
                }

                Mensagem mensagem = new Mensagem
                {
                    MensagemId = mensagemId,
                    DataPretendida = DateTime.Parse(request.DataPretendida),
                    QuantidadeDias = request.NumeroDias,
                    IncluirMarinheiro = resultIncluirMarinheiro,
                    NumeroPassageiros = request.NumeroPassageiros,
                    RemetenteNome = request.RemetenteNome,
                    RemetenteEmail = request.RemetenteEmail,
                    RemetenteTelefone = request.RemetenteTelefone,
                    Assunto = request.Assunto,
                    Status = request.Status,

                    CreatedBy = "TESTE",
                    Created = DateTime.Now
                };

                _mensagemRepository.Add(mensagem);
                _unitOfWork.Commit();

                return await Task.FromResult(mensagem);
            }
        }
    }
}
