﻿using iNavigate.Application.visitante.mensagenVisitante.ViewModel;
using iNavigate.Domain.Entites.visitante;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.visitante.mensagenVisitante.Queries
{
    public class GetMensagemByIdQuery : IRequest<MensagemViewModel>
    {
        public Guid MensagemId { get; set; }
    }

    public class GetMensagemByIdHandler : IRequestHandler<GetMensagemByIdQuery, MensagemViewModel>
    {
        private readonly iNavigateContext _context;

        public GetMensagemByIdHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<MensagemViewModel> Handle(GetMensagemByIdQuery request, CancellationToken cancellationToken)
        {
            MensagemViewModel mensagemViewModel = null;

            var records = from mes in _context.Mensagem where mes.MensagemId.Equals(request.MensagemId) select mes;
            Mensagem mensagem = records.FirstOrDefault();

            if (mensagem != null)
            {
                mensagemViewModel = new MensagemViewModel();

                mensagemViewModel.MensagemId = mensagem.MensagemId.ToString();
                mensagemViewModel.DataLocacao = mensagem.DataPretendida;
                mensagemViewModel.NumeroDias = mensagem.QuantidadeDias;
                mensagemViewModel.IncluirMarinheiro = mensagem.IncluirMarinheiro;
                mensagemViewModel.NumeroPassageiros = mensagem.NumeroPassageiros;
                mensagemViewModel.RemetenteNome = mensagem.RemetenteNome;
                mensagemViewModel.RemetenteEmail = mensagem.RemetenteEmail;
                mensagemViewModel.RemetenteTelefone = mensagem.RemetenteTelefone;
                mensagemViewModel.Assunto = mensagem.Assunto;
                mensagemViewModel.Status = mensagem.Status;
                mensagemViewModel.CreateBy = mensagem.CreatedBy;
                mensagemViewModel.Created = mensagem.Created;
                mensagemViewModel.LastModified = mensagem.LastModified;
                mensagemViewModel.LastModifiedBy = mensagem.LastModifiedBy;
            }

            return await Task.FromResult(mensagemViewModel);
        }
    }
}
