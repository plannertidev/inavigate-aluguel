﻿using iNavigate.Application.visitante.mensagenVisitante.ViewModel;
using iNavigate.Domain.Entites.visitante;
using INavigate.Data.Context;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.visitante.mensagenVisitante.Queries
{
    public class GetAllMensagemQuery : IRequest<List<MensagemViewModel>>
    {
    }

    public class GetAllMensagemHandler : IRequestHandler<GetAllMensagemQuery, List<MensagemViewModel>>
    {
        private readonly iNavigateContext _context;

        public GetAllMensagemHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<List<MensagemViewModel>> Handle(GetAllMensagemQuery request, CancellationToken cancellationToken)
        {
            var records = from mes in _context.Mensagem select mes;

            List<MensagemViewModel> MensagemViewModelCollection = new List<MensagemViewModel>();
            foreach (Mensagem mensagem in records.ToList())
            {
                MensagemViewModel mensagemeViewModel = new MensagemViewModel();
                mensagemeViewModel.MensagemId = mensagem.MensagemId.ToString();
                mensagemeViewModel.DataLocacao = mensagem.DataPretendida;
                mensagemeViewModel.NumeroDias = mensagem.QuantidadeDias;
                mensagemeViewModel.IncluirMarinheiro = mensagem.IncluirMarinheiro;
                mensagemeViewModel.NumeroPassageiros = mensagem.NumeroPassageiros;
                mensagemeViewModel.RemetenteNome = mensagem.RemetenteNome;
                mensagemeViewModel.RemetenteEmail = mensagem.RemetenteEmail;
                mensagemeViewModel.RemetenteTelefone = mensagem.RemetenteTelefone;
                mensagemeViewModel.Assunto = mensagem.Assunto;
                mensagemeViewModel.Status = mensagem.Status;
                mensagemeViewModel.CreateBy = mensagem.CreatedBy;
                mensagemeViewModel.Created = mensagem.Created;
                mensagemeViewModel.LastModified = mensagem.LastModified;
                mensagemeViewModel.LastModifiedBy = mensagem.LastModifiedBy;

                MensagemViewModelCollection.Add(mensagemeViewModel);
            }

            return await Task.FromResult(MensagemViewModelCollection);
        }
    }

}