﻿using System;

namespace iNavigate.Application.visitante.mensagenVisitante.ViewModel
{
    public class MensagemViewModel
    {
        public string MensagemId { get; set; }
        public DateTime DataLocacao { get; set; }
        public decimal NumeroDias { get; set; }
        public int IncluirMarinheiro { get; set; }
        public decimal NumeroPassageiros { get; set; }
        public string RemetenteNome { get; set; }
        public string RemetenteEmail { get; set; }
        public string RemetenteTelefone { get; set; }
        public string Assunto { get; set; }
        public int Status { get; set; }

        public string CreateBy { get; set; }

        public DateTime Created { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModified { get; set; }
    }
}
