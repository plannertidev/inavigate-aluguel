﻿using iNavigate.Application.visitante.cliente.ViewModel;
using iNavigate.Domain.Entites.visitante;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.visitante.cliente.Queries
{
    public class GetClienteByIdQuery : IRequest<ClienteViewModel>
    {
        public Guid ClienteId { get; set; }
    }

    public class GetClienteByIdHandler : IRequestHandler<GetClienteByIdQuery, ClienteViewModel>
    {
        private readonly iNavigateContext _context;

        public GetClienteByIdHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<ClienteViewModel> Handle(GetClienteByIdQuery request, CancellationToken cancellationToken)
        {
            ClienteViewModel clienteViewModel = null;

            var records = from cli in _context.Cliente where cli.ClienteId.Equals(request.ClienteId) select cli;
            Cliente cliente = records.FirstOrDefault();

            if (cliente != null)
            {
                clienteViewModel = new ClienteViewModel();

                clienteViewModel.ClienteId = cliente.ClienteId.ToString();
                clienteViewModel.Nome = cliente.Nome;
                clienteViewModel.Email = cliente.Email;
                clienteViewModel.Telefone = cliente.Telefone;
                
                clienteViewModel.CreateBy = cliente.CreatedBy;
                clienteViewModel.Created = cliente.Created;
                clienteViewModel.LastModified = cliente.LastModified;
                clienteViewModel.LastModifiedBy = cliente.LastModifiedBy;
            }

            return await Task.FromResult(clienteViewModel);
        }
    }
}