﻿using iNavigate.Application.visitante.cliente.ViewModel;
using iNavigate.Domain.Entites.visitante;
using INavigate.Data.Context;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.visitante.cliente.Queries
{
    public class GetAllClienteQuery : IRequest<List<ClienteViewModel>>
    {
    }

    public class GetAllClienteHandler : IRequestHandler<GetAllClienteQuery, List<ClienteViewModel>>
    {
        private readonly iNavigateContext _context;

        public GetAllClienteHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<List<ClienteViewModel>> Handle(GetAllClienteQuery request, CancellationToken cancellationToken)
        {
            var records = from cli in _context.Cliente select cli;

            List<ClienteViewModel> ClienteViewModelCollection = new List<ClienteViewModel>();
            foreach (Cliente cliente in records.ToList())
            {
                ClienteViewModel clienteViewModel = new ClienteViewModel();
                clienteViewModel.ClienteId = cliente.ClienteId.ToString();
                clienteViewModel.Nome = cliente.Nome;
                clienteViewModel.Email = cliente.Email;
                clienteViewModel.Telefone = cliente.Telefone;
                
                clienteViewModel.CreateBy = cliente.CreatedBy;
                clienteViewModel.Created = cliente.Created;
                clienteViewModel.LastModified = cliente.LastModified;
                clienteViewModel.LastModifiedBy = cliente.LastModifiedBy;

                ClienteViewModelCollection.Add(clienteViewModel);
            }

            return await Task.FromResult(ClienteViewModelCollection);
        }
    }
}