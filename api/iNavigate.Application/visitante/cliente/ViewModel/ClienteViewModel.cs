﻿using System;

namespace iNavigate.Application.visitante.cliente.ViewModel
{
    public class ClienteViewModel
    {
            public string ClienteId { get; set; }
            public string Nome { get; set; }
            public string Email { get; set; }
            public string Telefone { get; set; }

            public string CreateBy { get; set; }

            public DateTime Created { get; set; }

            public string LastModifiedBy { get; set; }

            public DateTime? LastModified { get; set; }
    }
}
