﻿using iNavigate.Domain.Entites.visitante;
using iNavigate.Domain.Interfaces.visitante;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.visitante.cliente.Command
{
    public class DeleteClienteCommand : IRequest<Unit>
    {
        public Guid ClienteId { get; set; }

        public class Handler : IRequestHandler<DeleteClienteCommand, Unit>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IClienteRepository _clienteRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               IClienteRepository clienteRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _clienteRepository = clienteRepository;
            }

            public async Task<Unit> Handle(DeleteClienteCommand request, CancellationToken cancellationToken)
            {
                Cliente cliente = _clienteRepository.GetById(request.ClienteId);
                if (cliente != null)
                {
                    _clienteRepository.Remove(cliente.ClienteId);
                    _unitOfWork.Commit();
                }

                return Unit.Value;
            }
        }
    }
}