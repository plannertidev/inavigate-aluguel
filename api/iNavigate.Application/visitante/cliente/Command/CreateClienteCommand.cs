﻿using iNavigate.Domain.Entites.visitante;
using iNavigate.Domain.Interfaces.visitante;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.visitante.cliente.Command
{
    public class CreateClienteCommand : IRequest<Cliente>
    {
        public Guid ClienteId { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }

        public class Handler : IRequestHandler<CreateClienteCommand, Cliente>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IClienteRepository _clienteRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               IClienteRepository clienteRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _clienteRepository = clienteRepository;
            }

            public async Task<Cliente> Handle(CreateClienteCommand request, CancellationToken cancellationToken)
            {
                Guid clienteId = Guid.NewGuid();

                Cliente cliente = new Cliente
                {
                    ClienteId = clienteId,
                    Nome = request.Nome,
                    Email = request.Email,
                    Telefone = request.Telefone,
                    
                    CreatedBy = "TESTE",
                    Created = DateTime.Now
                };

                _clienteRepository.Add(cliente);
                _unitOfWork.Commit();

                return await Task.FromResult(cliente);
            }
        }
    }
}