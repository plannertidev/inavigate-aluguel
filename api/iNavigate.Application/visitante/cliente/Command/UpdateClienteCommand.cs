﻿using iNavigate.Domain.Entites.visitante;
using iNavigate.Domain.Interfaces.visitante;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.visitante.cliente.Command
{
    public class UpdateClienteCommand : IRequest<Cliente>
    {
        public Guid ClienteId { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }

        public class Handler : IRequestHandler<UpdateClienteCommand, Cliente>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IClienteRepository _clienteRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
                 IClienteRepository clienteRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _clienteRepository = clienteRepository;
            }

            public async Task<Cliente> Handle(UpdateClienteCommand request, CancellationToken cancellationToken)
            {
                Cliente cliente = _clienteRepository.GetById(request.ClienteId);

                if (cliente != null)
                {
                    cliente.Nome = request.Nome;
                    cliente.Email = request.Email;
                    cliente.Telefone = request.Telefone;
                    
                    cliente.LastModifiedBy = "TESTE";
                    cliente.LastModified = DateTime.Now;

                    _clienteRepository.Update(cliente);
                    _unitOfWork.Commit();
                }

                return await Task.FromResult(cliente);
            }
        }
    }
}