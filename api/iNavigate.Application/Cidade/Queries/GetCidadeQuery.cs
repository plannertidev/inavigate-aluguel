﻿using iNavigate.Application.ViewModel;
using iNavigate.Domain.Entites;
using INavigate.Data.Context;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Queries
{
    public class GetCidadeQuery : IRequest<List<CidadeViewModel>>
    {
    }
    public class GetCidadeHandler : IRequestHandler<GetCidadeQuery, List<CidadeViewModel>>
    {
        private readonly iNavigateContext _context;

        public GetCidadeHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<List<CidadeViewModel>> Handle(GetCidadeQuery request, CancellationToken cancellationToken)
        {
            var records = from cid in _context.Cidade select cid;

            List<CidadeViewModel> CidadeViewModelCollection = new List<CidadeViewModel>();
            foreach (Cidade cidade in records.ToList())
            {
                CidadeViewModel cidadeView = new CidadeViewModel();
                cidadeView.CidadeId = cidade.CidadeId.ToString();
                cidadeView.EstadoId = cidade.EstadoId.ToString();
                cidadeView.Nome = cidade.Nome;
            
                cidadeView.Creator = cidade.Creator;
                cidadeView.Created = cidade.Created;
                cidadeView.Changer = cidade.Changer;
                cidadeView.Changed = cidade.Changed;

                CidadeViewModelCollection.Add(cidadeView);
            }

            return await Task.FromResult(CidadeViewModelCollection);
        }

    }
}

