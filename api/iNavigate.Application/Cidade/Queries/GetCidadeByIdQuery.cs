﻿using iNavigate.Domain.Entites;
using INavigate.Data.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Queries
{
    public class GetCidadeByIdQuery : IRequest<Cidade>
    {
        public Guid CidadeId { get; set; }
    }

    public class GetCidadeByIdQueryHandler : IRequestHandler<GetCidadeByIdQuery, Cidade>
    {
        private readonly iNavigateContext _context;

        public GetCidadeByIdQueryHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<Cidade> Handle(GetCidadeByIdQuery request, CancellationToken cancellationToken)
        {
            var records = await _context.Cidade.Where(x => x.CidadeId.Equals(request.CidadeId))
                    .FirstOrDefaultAsync(cancellationToken);

            return await Task.FromResult(records);
        }
    }
}