﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Commands
{
   public class CreateCidadeCommand : IRequest<Guid>
    {
        public Guid CidadeId { get; set; }
        public string Nome { get; set; }
        public Guid EstadoId { get; set; }

        public string Creator { get; set; }
        public DateTime? Created { get; set; }
        public string Changer { get; set; }
        public DateTime? Changed { get; set; }

        public class Handler : IRequestHandler<CreateCidadeCommand,Guid>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly ICidadeRepository _cidadeRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               ICidadeRepository cidadeRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _cidadeRepository = cidadeRepository;
            }

            public async Task<Guid> Handle(CreateCidadeCommand request, CancellationToken cancellationToken)
            {
                Guid result = Guid.NewGuid();

                Cidade cidade = new Cidade
                {
                    CidadeId = result,
                    EstadoId = result,
                    Nome = request.Nome,
                    Creator = request.Creator,
                    Created = request.Created,
                    Changer = request.Changer,
                    Changed = request.Changed

                };

                _cidadeRepository.Add(cidade);
                try
                {
                    _unitOfWork.Commit();
                }
                catch (Exception e)
                {

                    throw;
                }


                return await Task.FromResult(result);
            }
        }
    }
 }