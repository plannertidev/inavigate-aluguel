﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Commands
{
    public class UpdateCidadeCommand : IRequest<Unit>
    {
        public Guid CidadeId { get; set; }
        public string Nome { get; set; }
        public Guid EstadoId { get; set; }

        public string Creator { get; set; }
        public DateTime? Created { get; set; }
        public string Changer { get; set; }
        public DateTime? Changed { get; set; }

        public class Handler : IRequestHandler<UpdateCidadeCommand, Unit>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly ICidadeRepository _cidadeRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               ICidadeRepository cidadeRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _cidadeRepository = cidadeRepository;
            }

            public async Task<Unit> Handle(UpdateCidadeCommand request, CancellationToken cancellationToken)
            {
                Cidade cidade = _cidadeRepository.GetById(request.CidadeId);
                if (cidade != null)
                {
                    cidade.EstadoId = request.EstadoId;
                    cidade.Nome = request.Nome;
                    cidade.Creator = request.Creator;
                    cidade.Created = request.Created;
                    cidade.Changer = request.Changer;
                    cidade.Changed = request.Changed;

                    _cidadeRepository.Update(cidade);
                    _unitOfWork.Commit();
                }

                return Unit.Value;
            }
        }
    }
}
