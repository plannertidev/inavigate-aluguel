﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Commands
{
    public class DeleteCidadeCommand : IRequest<Unit>
    {
        public Guid CidadeId { get; set; }

        public class Handler : IRequestHandler<DeleteCidadeCommand, Unit>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly ICidadeRepository _cidadeRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               ICidadeRepository cidadeRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _cidadeRepository = cidadeRepository;
            }

            public async Task<Unit> Handle(DeleteCidadeCommand request, CancellationToken cancellationToken)
            {
                Cidade cidade = _cidadeRepository.GetById(request.CidadeId);
                if (cidade != null)
                {
                    _cidadeRepository.Remove(cidade.CidadeId);
                    _unitOfWork.Commit();
                }

                return Unit.Value;
            }
        }
    }


}
