﻿using iNavigate.Application.Image;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace iNavigate.Application.services
{
    public interface IImageService
    {
        Task<string> UploadImage(IFormFile file);
        Task<string> UploadImage(ImageViewModel file);
        
    }
}
