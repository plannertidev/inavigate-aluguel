﻿using iNavigate.Application.Image;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Threading.Tasks;

namespace iNavigate.Application.services
{
    public class ImageService : IImageService
    {
        public async Task<string> UploadImage(IFormFile file)
        {
            if (CheckIfImageFile(file))
            {
                return await WriteFile(file);
            }

            return "Invalid image file";
        }

        public async Task<string> UploadImage(ImageViewModel file)
        {
            var path = @"\temp\inavigateImageStore\" + file.Folder;

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            string base64 = file.File.Replace("data:image/jpeg;base64,", "");
            byte[] bytes = Convert.FromBase64String(base64);

            //var extension = "." + file.FileName.Split('.')[file.FileName.Split('.').Length - 1];
            string fileName = Guid.NewGuid().ToString() + ".jpg";//extension; 
            await File.WriteAllBytesAsync(path + "\\" + fileName, bytes);

            return null;
        }

        private bool CheckIfImageFile(IFormFile file)
        {
            byte[] fileBytes;
            using (var ms = new MemoryStream())
            {
                file.CopyTo(ms);
                fileBytes = ms.ToArray();
            }

            return WriterHelper.GetImageFormat(fileBytes) != WriterHelper.ImageFormat.unknown;
        }

        public async Task<string> WriteFile(IFormFile file)
        {
            string fileName;
            try
            {
                var extension = "." + file.FileName.Split('.')[file.FileName.Split('.').Length - 1];
                fileName = Guid.NewGuid().ToString() + extension; //Create a new Name 
                                                                  //for the file due to security reasons.
                                                                  //var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\images", fileName);
                var path = @"\temp\inavigateImageStore\" + file.FileName;

                using (var bits = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(bits);
                }
            }
            catch (Exception e)
            {
                return e.Message;
            }

            return fileName;
        }

    }
}
