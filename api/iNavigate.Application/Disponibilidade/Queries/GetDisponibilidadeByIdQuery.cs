﻿using iNavigate.Application.ViewModel;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace iNavigate.Application.Queries
{
    public class GetDisponibilidadeByIdQuery : IRequest<DisponibilidadeViewModel>
    {
        public Guid BarcoId { get; set; }
    }

    public class GetDisponibilidadeByIdQueryHandler : IRequestHandler<GetDisponibilidadeByIdQuery, DisponibilidadeViewModel>
    {
        private readonly iNavigateContext _context;

        public GetDisponibilidadeByIdQueryHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<DisponibilidadeViewModel> Handle(GetDisponibilidadeByIdQuery request, CancellationToken cancellationToken)
        {
            DisponibilidadeViewModel disponibilidadeViewModel = new DisponibilidadeViewModel();

            var records = from disp in _context.Disponibilidade where disp.BarcoId.Equals(request.BarcoId) select disp;
            var record = records.FirstOrDefault();

            if (record != null)
            {
                disponibilidadeViewModel.DisponibilidadeId = record.DisponibilidadeId;
                disponibilidadeViewModel.BarcoId = record.BarcoId;
                disponibilidadeViewModel.Cor = record.Cor;
                disponibilidadeViewModel.Nome = record.Nome;
                disponibilidadeViewModel.Inicio = record.Inicio;
                disponibilidadeViewModel.Fim = record.Fim;
                disponibilidadeViewModel.MinimoDias = record.MinimoDias;
                disponibilidadeViewModel.ValorMeioDia = record.ValorMeioDia;
                disponibilidadeViewModel.ValorDiaInteiro = record.ValorDiaInteiro;
                disponibilidadeViewModel.ValorSemana = record.ValorSemana;

                disponibilidadeViewModel.Created = record.Created;
                disponibilidadeViewModel.CreatedBy = record.CreatedBy;
                disponibilidadeViewModel.LastModified = record.LastModified;
                disponibilidadeViewModel.LastModifiedBy = record.LastModifiedBy;
            }

            return await Task.FromResult(disponibilidadeViewModel);
        }
    }
}
