﻿using iNavigate.Application.ViewModel;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace iNavigate.Application.Queries
{
    public class GetDisponibilidadePorBarcoQuery : IRequest<List<DisponibilidadeViewModel>>
    {
        public Guid BarcoId { get; set; }
    }

    public class GetDisponibilidadePorBarcoQueryHandler : IRequestHandler<GetDisponibilidadePorBarcoQuery, List<DisponibilidadeViewModel>>
    {
        private readonly iNavigateContext _context;

        public GetDisponibilidadePorBarcoQueryHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<List<DisponibilidadeViewModel>> Handle(GetDisponibilidadePorBarcoQuery request, CancellationToken cancellationToken)
        {
            var records = from disp in _context.Disponibilidade
                          join bar in _context.Barco on disp.BarcoId equals bar.BarcoId                          
                          where disp.BarcoId.Equals(request.BarcoId)
                          select new { disp, bar };

            List<DisponibilidadeViewModel> DisponibilidadeViewModelsCollections = new List<DisponibilidadeViewModel>();
            foreach (var record in records.ToList())
            {
                DisponibilidadeViewModel disponibilidadeViewModel = new DisponibilidadeViewModel();

                disponibilidadeViewModel.DisponibilidadeId = record.disp.DisponibilidadeId;
                disponibilidadeViewModel.BarcoId = record.disp.BarcoId;
                disponibilidadeViewModel.Nome = record.disp.Nome;
                disponibilidadeViewModel.Cor = record.disp.Cor;
                disponibilidadeViewModel.Inicio = record.disp.Inicio;
                disponibilidadeViewModel.Fim = record.disp.Fim;
                disponibilidadeViewModel.ValorMeioDia = record.disp.ValorMeioDia;
                disponibilidadeViewModel.ValorDiaInteiro = record.disp.ValorDiaInteiro;
                disponibilidadeViewModel.ValorSemana = record.disp.ValorSemana;
                disponibilidadeViewModel.MinimoDias = record.disp.MinimoDias;
                disponibilidadeViewModel.BarcoDescription = record.bar.Nome;

                disponibilidadeViewModel.Created = record.disp.Created;
                disponibilidadeViewModel.CreatedBy = record.disp.CreatedBy;
                disponibilidadeViewModel.LastModified = record.disp.LastModified;
                disponibilidadeViewModel.LastModifiedBy = record.disp.LastModifiedBy;

                DisponibilidadeViewModelsCollections.Add(disponibilidadeViewModel);
            }

            return await Task.FromResult(DisponibilidadeViewModelsCollections);
        }
    }
}
