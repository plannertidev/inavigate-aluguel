﻿using iNavigate.Application.ViewModel;
using INavigate.Data.Context;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace iNavigate.Application.Queries
{
    public class GetAllDisponibilidadeQuery : IRequest<List<DisponibilidadeViewModel>>
    {
    }

    public class GetDisponibilidadeQueryHandler : IRequestHandler<GetAllDisponibilidadeQuery, List<DisponibilidadeViewModel>>
    {
        private readonly iNavigateContext _context;

        public GetDisponibilidadeQueryHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<List<DisponibilidadeViewModel>> Handle(GetAllDisponibilidadeQuery request, CancellationToken cancellationToken)
        {
            var records = from disp in _context.Disponibilidade select disp;
                

            List<DisponibilidadeViewModel> DisponibilidadeViewModelsCollections = new List<DisponibilidadeViewModel>();
            foreach (var record in records.ToList())
            {
                DisponibilidadeViewModel disponibilidadeViewModel = new DisponibilidadeViewModel();
                
                disponibilidadeViewModel.DisponibilidadeId = record.DisponibilidadeId;
                disponibilidadeViewModel.BarcoId = record.BarcoId;
                disponibilidadeViewModel.Nome = record.Nome;
                disponibilidadeViewModel.Cor = record.Cor;
                disponibilidadeViewModel.Inicio = record.Inicio;
                disponibilidadeViewModel.Fim = record.Fim;
                disponibilidadeViewModel.ValorMeioDia = record.ValorMeioDia;
                disponibilidadeViewModel.ValorDiaInteiro = record.ValorDiaInteiro;
                disponibilidadeViewModel.ValorSemana = record.ValorSemana;
                disponibilidadeViewModel.MinimoDias = record.MinimoDias;

                disponibilidadeViewModel.Created = record.Created;
                disponibilidadeViewModel.CreatedBy = record.CreatedBy;
                disponibilidadeViewModel.LastModified = record.LastModified;
                disponibilidadeViewModel.LastModifiedBy = record.LastModifiedBy;

                DisponibilidadeViewModelsCollections.Add(disponibilidadeViewModel);
            }

            return await Task.FromResult(DisponibilidadeViewModelsCollections);
        }
    }
}
