﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Commands
{
    public class UpdateDisponibilidadeCommand : IRequest<Disponibilidade>
    {
        public Guid DisponibilidadeId { get; set; }
        public Guid BarcoId { get; set; }
        public string Cor { get; set; }
        public string Nome { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fim { get; set; }
        public int MinimoDias { get; set; }
        public int ValorMeioDia { get; set; }
        public int ValorDiaInteiro { get; set; }
        public int ValorSemana { get; set; }

        public class Handler : IRequestHandler<UpdateDisponibilidadeCommand, Disponibilidade>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IDisponibilidadeRepository _disponibilidadeRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               IDisponibilidadeRepository disponibilidadeRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _disponibilidadeRepository = disponibilidadeRepository;
            }

            public async Task<Disponibilidade> Handle(UpdateDisponibilidadeCommand request, CancellationToken cancellationToken)
            {
                Disponibilidade disponibilidade = _disponibilidadeRepository.GetById(request.DisponibilidadeId);

                if (disponibilidade != null)
                {
                    disponibilidade.DisponibilidadeId = request.DisponibilidadeId;
                    disponibilidade.BarcoId = request.BarcoId;
                    disponibilidade.Nome = request.Nome;
                    disponibilidade.Cor = request.Cor;
                    disponibilidade.Inicio = request.Inicio;
                    disponibilidade.Fim = request.Fim;
                    disponibilidade.MinimoDias = request.MinimoDias;
                    disponibilidade.ValorMeioDia = request.ValorMeioDia;
                    disponibilidade.ValorDiaInteiro = request.ValorDiaInteiro;
                    disponibilidade.ValorSemana = request.ValorSemana;

                    disponibilidade.LastModifiedBy = "TESTE";
                    disponibilidade.LastModified = DateTime.Now;

                    _disponibilidadeRepository.Update(disponibilidade);
                    _unitOfWork.Commit();
                }

                return await Task.FromResult(disponibilidade);
            }
        }
    }
}
