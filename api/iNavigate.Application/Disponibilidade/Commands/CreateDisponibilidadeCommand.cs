﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Commands
{
    public class CreateDisponibilidadeCommand : IRequest<Disponibilidade>
    {
        public Guid DisponibilidadeId { get; set; }
        public Guid BarcoId { get; set; }
        public string Cor { get; set; }
        public string Nome { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fim { get; set; }
        public int MinimoDias { get; set; }
        public int ValorMeioDia { get; set; }
        public int ValorDiaInteiro { get; set; }
        public int ValorSemana { get; set; }


        public class Handler : IRequestHandler<CreateDisponibilidadeCommand, Disponibilidade>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IDisponibilidadeRepository _disponibilidadeRepository;
            private readonly iNavigateContext _context;
            public Handler(
                iNavigateContext context,
               IMediator mediator,
               IUnitOfWork unitOfWork,
               IDisponibilidadeRepository disponibilidadeRepository)
            {
                _context = context;
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _disponibilidadeRepository = disponibilidadeRepository;
            }
           
            public async Task<Disponibilidade> Handle(CreateDisponibilidadeCommand request, CancellationToken cancellationToken)
            {

                var records = from disp in _context.Disponibilidade where request.BarcoId.Equals(disp.BarcoId) 
                              select disp;

                var dados = records.ToList().Where(x => x.Inicio >= request.Inicio && x.Fim <= request.Inicio ||
                    x.Inicio <= request.Fim && x.Fim >= request.Inicio);
                Disponibilidade disponibilidade;

                if (dados.ToList().Count > 0)
                {
                    
                    disponibilidade = null;
                }
                else
                {
                    
                    Guid disponibilidadeId = Guid.NewGuid();

                   
                    disponibilidade = new Disponibilidade
                    {

                        DisponibilidadeId = disponibilidadeId,
                        BarcoId = request.BarcoId,
                        Nome = request.Nome,
                        Cor = request.Cor,
                        Inicio = request.Inicio,
                        Fim = request.Fim,
                        MinimoDias = request.MinimoDias,
                        ValorMeioDia = request.ValorMeioDia,
                        ValorDiaInteiro = request.ValorDiaInteiro,
                        ValorSemana = request.ValorSemana,

                        CreatedBy = "TESTE",
                        Created = DateTime.Now
                    };
                    
                    _disponibilidadeRepository.Add(disponibilidade);
                    _unitOfWork.Commit();
                }

                return await Task.FromResult(disponibilidade);
               
            }
        }
        
    }
}
