﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Commands
{
    public class DeleteDisponibilidadeCommand : IRequest<Unit>
    {
        public Guid DisponibilidadeId { get; set; }

        public class Handler : IRequestHandler<DeleteDisponibilidadeCommand, Unit>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IDisponibilidadeRepository _disponibilidadeRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               IDisponibilidadeRepository disponibilidadeRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _disponibilidadeRepository = disponibilidadeRepository;
            }

            public async Task<Unit> Handle(DeleteDisponibilidadeCommand request, CancellationToken cancellationToken)
            {
                Disponibilidade disponibilidade = _disponibilidadeRepository.GetById(request.DisponibilidadeId);
                if (disponibilidade != null)
                {
                    _disponibilidadeRepository.Remove(disponibilidade.DisponibilidadeId);
                    _unitOfWork.Commit();
                }

                return Unit.Value;
            }
        }
    }
}
