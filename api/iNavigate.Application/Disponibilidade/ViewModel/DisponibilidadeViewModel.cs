﻿using System;

namespace iNavigate.Application.ViewModel
{
    public class DisponibilidadeViewModel
    {
        public Guid DisponibilidadeId { get; set; }
        public Guid BarcoId { get; set; }
        public string Cor { get; set; }
        public string Nome { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fim { get; set; }
        public decimal MinimoDias { get; set; }
        public decimal ValorMeioDia { get; set; }
        public decimal ValorDiaInteiro { get; set; }
        public decimal ValorSemana { get; set; }
        public string BarcoDescription { get; set; }

        public string CreatedBy { get; set; }
        public DateTime Created { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModified { get; set; }

    }
}
