﻿using iNavigate.Domain.Entites;
using INavigate.Data.Context;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace iNavigate.Application.TipoPropriedades.Queries
{
   public class TipoPropriedadeQuery : Query<TipoPropriedade>, ITipoPropriedadeQuery
    {
        public TipoPropriedadeQuery(iNavigateContext context) : base(context)
        {
        }
        public Task<IEnumerable<TipoPropriedade>> getAll()
        {
            throw new System.NotImplementedException();
        }
    }
}
