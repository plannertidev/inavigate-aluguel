﻿using iNavigate.Application.TipoPropriedades.ViewModel;
using iNavigate.Domain.Entites;
using INavigate.Data.Context;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.TipoPropriedades.Queries
{
    public class GetTipoPropriedadeQuery : IRequest<List<TipoPropriedadeViewModel>>
    {
    }

    public class GetTipoPropriedadeQueryHandler : IRequestHandler<GetTipoPropriedadeQuery, List<TipoPropriedadeViewModel>>
    {
        private readonly iNavigateContext _context;

        public GetTipoPropriedadeQueryHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<List<TipoPropriedadeViewModel>> Handle(GetTipoPropriedadeQuery request, CancellationToken cancellationToken)
        {
            var records = from tipo in _context.TipoPropriedade select tipo;

            List<TipoPropriedadeViewModel> TipoPropriedadeViewModelsCollections = new List<TipoPropriedadeViewModel>();
            foreach (TipoPropriedade  tipoPropriedade in records.ToList())
            {
                TipoPropriedadeViewModel tipoPropriedadeView = new TipoPropriedadeViewModel();
                tipoPropriedadeView.TipoPropriedadeId = tipoPropriedade.TipoPropriedadeId.ToString();
                tipoPropriedadeView.Descricao = tipoPropriedade.Descricao;

                TipoPropriedadeViewModelsCollections.Add(tipoPropriedadeView);
            }


            return await Task.FromResult(TipoPropriedadeViewModelsCollections);
        }
    }
}
