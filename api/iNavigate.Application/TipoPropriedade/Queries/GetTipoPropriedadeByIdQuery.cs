﻿using iNavigate.Domain.Entites;
using INavigate.Data.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.TipoPropriedades.Queries
{
    public class GetTipoPropriedadeByIdQuery : IRequest<TipoPropriedade>
    {
        public Guid TipoPropriedadeId { get; set; }
    }

    public class GetTipoPropriedadeHandler : IRequestHandler<GetTipoPropriedadeByIdQuery, TipoPropriedade>
    {
        private readonly iNavigateContext _context;

        public GetTipoPropriedadeHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<TipoPropriedade> Handle(GetTipoPropriedadeByIdQuery request, CancellationToken cancellationToken)
        {
            var records = await _context.TipoPropriedade.Where(x => x.TipoPropriedadeId.Equals(request.TipoPropriedadeId))
                  .FirstOrDefaultAsync(cancellationToken);
            return await Task.FromResult(records);

        }
    }
}
