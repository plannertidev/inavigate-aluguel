﻿using iNavigate.Domain.Entites;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace iNavigate.Application.TipoPropriedades.Queries
{
    public interface ITipoPropriedadeQuery
    {
        Task<IEnumerable<TipoPropriedade>> getAll();
    }
}
