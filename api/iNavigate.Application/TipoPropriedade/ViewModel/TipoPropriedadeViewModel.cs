﻿namespace iNavigate.Application.TipoPropriedades.ViewModel
{
    public class TipoPropriedadeViewModel
    {
        public string TipoPropriedadeId { get; set; }
        public string Descricao { get; set; }
    }
}
