﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.TipoPropriedades.Commands
{
    public class UpdateTipoPropriedadeCommand : IRequest<Unit>
    {
        public Guid TipoPropriedadeId { get; set; }
        public string Descricao { get; set; }

        public class Handler : IRequestHandler<UpdateTipoPropriedadeCommand, Unit>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly ITipoPropriedadeRepository _tipoPropriedadeRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               ITipoPropriedadeRepository tipoPropriedadeRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _tipoPropriedadeRepository = tipoPropriedadeRepository;
            }

            public async Task<Unit> Handle(UpdateTipoPropriedadeCommand request, CancellationToken cancellationToken)
            {
                TipoPropriedade tipoPropriedade = _tipoPropriedadeRepository.GetById(request.TipoPropriedadeId);
                if (tipoPropriedade != null)
                {
                    tipoPropriedade.Descricao = request.Descricao;

                    _tipoPropriedadeRepository.Update(tipoPropriedade);
                    _unitOfWork.Commit();
                }

                return Unit.Value;
            }
        }
    }
}
