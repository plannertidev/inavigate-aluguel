﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.TipoPropriedades.Commands
{
    public class DeleteTipoPropriedadeCommand : IRequest<Unit>
    {
        public Guid TipoPropriedadeId { get; set; }


        public class Handler : IRequestHandler<DeleteTipoPropriedadeCommand, Unit>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly ITipoPropriedadeRepository _tipoPropriedadeRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               ITipoPropriedadeRepository tipoPropriedadeRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _tipoPropriedadeRepository = tipoPropriedadeRepository;
            }

            public async Task<Unit> Handle(DeleteTipoPropriedadeCommand request, CancellationToken cancellationToken)
            {
                TipoPropriedade tipoPropriedade = _tipoPropriedadeRepository.GetById(request.TipoPropriedadeId);
                if (tipoPropriedade != null)
                {
                    _tipoPropriedadeRepository.Remove(tipoPropriedade.TipoPropriedadeId);
                    _unitOfWork.Commit();
                }

                return Unit.Value;
            }

        }
    }
}
