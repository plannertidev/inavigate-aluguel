﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.TipoPropriedades.Commands
{
    public class CreateTipoPropriedadeCommand : IRequest<Guid>
    {
        public Guid TipoPropriedadeId { get; set; }
        public string Descricao { get; set; }

        public class Handler : IRequestHandler<CreateTipoPropriedadeCommand, Guid>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly ITipoPropriedadeRepository _tipoPropriedadeRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               ITipoPropriedadeRepository tipoPropriedadeRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _tipoPropriedadeRepository = tipoPropriedadeRepository;
            }

            public async Task<Guid> Handle(CreateTipoPropriedadeCommand request, CancellationToken cancellationToken)
            {
                Guid result = Guid.NewGuid();

                TipoPropriedade tipoPropriedade = new TipoPropriedade
                {
                    TipoPropriedadeId = result,
                    Descricao = request.Descricao,

                };

                _tipoPropriedadeRepository.Add(tipoPropriedade);

                try
                {
                    _unitOfWork.Commit();
                }
                catch (Exception e)
                {

                    throw;
                }
             

                return await Task.FromResult(result);
            }
        }
    }
}
