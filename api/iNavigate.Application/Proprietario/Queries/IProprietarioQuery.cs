﻿using iNavigate.Domain.Entites;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace iNavigate.Application.Proprietarios.Queries
{
   public interface IProprietarioQuery
    {
        Task<IEnumerable<Proprietario>> getAll();
    }
}
