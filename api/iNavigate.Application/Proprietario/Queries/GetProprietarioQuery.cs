﻿using iNavigate.Domain.Entites;
using INavigate.Data.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Proprietarios.Queries
{
    public class GetProprietarioQuery : IRequest<List<Proprietario>>
    {
    }

    public class GetProprietarioQueryHandler : IRequestHandler<GetProprietarioQuery,List<Proprietario>>
    {
        private readonly iNavigateContext _context;

        public GetProprietarioQueryHandler(iNavigateContext context)
        {
            _context = context;
        }
        public async Task<List<Proprietario>> Handle(GetProprietarioQuery request, CancellationToken cancellationToken)
        {
            var records = await _context.Proprietario
                    .ToListAsync(cancellationToken);


            return await Task.FromResult(records);
        }

    }
}
