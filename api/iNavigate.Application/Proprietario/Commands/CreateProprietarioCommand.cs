﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Proprietarios.Commands
{
    public class CreateProprietarioCommand : IRequest<Guid>
    {
        public Guid ProprietarioId { get; set; }
        public string Email { get; set; }
        public string Nome { get; set; }
        public string Endereco { get; set; }
        public string Senha { get; set; }
        public string CEP { get; set; }
        public string Estado { get; set; }
        public string Cidade { get; set; }
        public DateTime DataNascimento { get; set; }
        public string Celular { get; set; }
        public string TelefoneSecundario { get; set; }
        public string Sexo { get; set; }
        public string CPF { get; set; }

        public class Handler : IRequestHandler<CreateProprietarioCommand, Guid>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IProprietarioRepository _proprietarioRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               IProprietarioRepository proprietarioRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _proprietarioRepository = proprietarioRepository;
            }

            public async Task<Guid> Handle(CreateProprietarioCommand request, CancellationToken cancellationToken)
            {
                Guid result = Guid.NewGuid();

                Proprietario proprietario = new Proprietario
                {
                    ProprietarioId = result,
                    Email = request.Email,
                    Nome= request.Nome,
                    Senha = request.Senha,
                    Celular = request.Celular,
                    CreatedBy = string.Empty,
                    Created = DateTime.Now
                };

                _proprietarioRepository.Add(proprietario);
                _unitOfWork.Commit();

                return await Task.FromResult(result);
            }
        }


    }
}
