﻿using iNavigate.Application.Proprietarios.ViewModel;
using iNavigate.Domain.Entites;
using INavigate.Data.Context;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Commands
{
    public class LoginCommand : IRequest<ProprietarioViewModel>
    {
        public string Email { get; set; }
        public string Senha { get; set; }
    }

    public class Handler : IRequestHandler<LoginCommand, ProprietarioViewModel>
    {
        private readonly iNavigateContext _context;

        public Handler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<ProprietarioViewModel> Handle(LoginCommand request, CancellationToken cancellationToken)
        {
            ProprietarioViewModel proprietarioViewModel = null;

            var record = from pro in _context.Proprietario 
                         where pro.Email.Equals(request.Email) && pro.Senha.Equals(request.Senha)
                         select pro;

            Proprietario proprietario = record.FirstOrDefault();

            if (proprietario != null)
            {
                proprietarioViewModel = new ProprietarioViewModel();
                proprietarioViewModel.ProprietarioId = proprietario.ProprietarioId.ToString();
                proprietarioViewModel.Email = proprietario.Email;
                proprietarioViewModel.Nome = proprietario.Nome;
                //proprietarioViewModel.Sobrenome = proprietario.Sobrenome;
                //proprietarioViewModel.Password = proprietario.Password;
                //proprietarioViewModel.Endereco = proprietario.Endereco;
                //proprietarioViewModel.CEP = proprietario.CEP;
                //proprietarioViewModel.Estado = proprietario.Estado;
                //proprietarioViewModel.Cidade = requproprietarioest.Cidade;
                //proprietarioViewModel.DataNascimento = proprietario.DataNascimento;
                //proprietarioViewModel.TelefonePrincipal = proprietario.TelefonePrincipal;
                //proprietarioViewModel.TelefoneSecundario = proprietario.TelefoneSecundario;
                //proprietarioViewModel.Sexo = proprietario.Sexo;
                //proprietarioViewModel.CPF = proprietario.CPF;
            }

            return await Task.FromResult(proprietarioViewModel);
        }
    }
}
