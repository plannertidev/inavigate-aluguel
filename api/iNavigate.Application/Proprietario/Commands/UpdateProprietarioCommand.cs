﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Proprietarios.Commands
{
    public class UpdateProprietarioCommand : IRequest<Unit>
    {
        public Guid ProprietarioId { get; set; }
        public string Email { get; set; }
        public string Nome { get; set; }
        public string Sobrenome { get; set; }
        public string Endereco { get; set; }
        public string Password { get; set; }
        public string CEP { get; set; }
        public string Estado { get; set; }
        public string Cidade { get; set; }
        public DateTime DataNascimento { get; set; }
        public string TelefonePrincipal { get; set; }
        public string TelefoneSecundario { get; set; }
        public string Sexo { get; set; }
        public string CPF { get; set; }

        public class Handler : IRequestHandler<UpdateProprietarioCommand, Unit>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IProprietarioRepository _proprietarioRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               IProprietarioRepository proprietarioRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _proprietarioRepository = proprietarioRepository;
            }

            public async Task<Unit> Handle(UpdateProprietarioCommand request, CancellationToken cancellationToken)
            {
                bool result = false;

                Proprietario proprietario = _proprietarioRepository.GetById(request.ProprietarioId);
                if (proprietario != null)
                {
                    proprietario.Email = request.Email;
                    proprietario.Nome = request.Nome;
                    proprietario.Senha = request.Password;
                    proprietario.Celular = request.TelefonePrincipal;

                    _proprietarioRepository.Update(proprietario);
                    _unitOfWork.Commit();
                }

                return Unit.Value;
            }
        }
    }
}
