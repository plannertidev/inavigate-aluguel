﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Proprietarios.Commands
{
    public class DeleteProprietarioCommand : IRequest<Unit>
    {
        public Guid ProprietarioId { get; set; }

        public class Handler : IRequestHandler<DeleteProprietarioCommand, Unit>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IProprietarioRepository _proprietarioRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               IProprietarioRepository proprietarioRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _proprietarioRepository = proprietarioRepository;
            }

            public async Task<Unit> Handle(DeleteProprietarioCommand request, CancellationToken cancellationToken)
            {
                Proprietario proprietario = _proprietarioRepository.GetById(request.ProprietarioId);
                if (proprietario != null)
                {
                    _proprietarioRepository.Remove(proprietario.ProprietarioId);
                    _unitOfWork.Commit();
                }

                return Unit.Value;
            }
        }
    }
}
