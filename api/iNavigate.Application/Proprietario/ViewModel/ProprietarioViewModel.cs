﻿using System;

namespace iNavigate.Application.Proprietarios.ViewModel
{
    public class ProprietarioViewModel
    {
        public string ProprietarioId { get; set; }
        public string Email { get; set; }
        public string Nome { get; set; }
        public string Sobrenome { get; set; }
        public string Password { get; set; }
        public string Endereco { get; set; }
        public string CEP { get; set; }
        public string Estado { get; set; }
        public string Cidade { get; set; }
        public DateTime DataNascimento { get; set; }
        public string TelefonePrincipal { get; set; }
        public string TelefoneSecundario { get; set; }
        public string Sexo { get; set; }
        public string CPF { get; set; }
    }
}
