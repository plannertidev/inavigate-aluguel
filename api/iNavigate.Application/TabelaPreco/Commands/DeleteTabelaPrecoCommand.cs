﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Commands
{
    public class DeleteTabelaPrecoCommand : IRequest<Unit>
    {
        public Guid TabelaPrecoId { get; set; }

        public class Handler : IRequestHandler<DeleteTabelaPrecoCommand, Unit>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly ITabelaPrecoRepository _tabelaPrecoRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               ITabelaPrecoRepository tabelaPrecoRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _tabelaPrecoRepository = tabelaPrecoRepository;
            }

            public async Task<Unit> Handle(DeleteTabelaPrecoCommand request, CancellationToken cancellationToken)
            {
                TabelaPreco tabelaPreco = _tabelaPrecoRepository.GetById(request.TabelaPrecoId);
                if (tabelaPreco != null)
                {
                    _tabelaPrecoRepository.Remove(tabelaPreco.TabelaPrecoId);
                    _unitOfWork.Commit();
                }

                return Unit.Value;
            }
        }
    }
}