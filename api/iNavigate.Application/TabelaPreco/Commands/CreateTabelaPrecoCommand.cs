﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Commands
{
    public class CreateTabelaPrecoCommand : IRequest<TabelaPreco>
    {
        public Guid TabelaPrecoId { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fim { get; set; }
        public decimal ValorMeioPeriodo { get; set; }
        public decimal ValorDiaria { get; set; }


        public class Handler : IRequestHandler<CreateTabelaPrecoCommand, TabelaPreco>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly ITabelaPrecoRepository _tabelaPrecoRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               ITabelaPrecoRepository tabelaPrecoRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _tabelaPrecoRepository = tabelaPrecoRepository;
            }

            public async Task<TabelaPreco> Handle(CreateTabelaPrecoCommand request, CancellationToken cancellationToken)
            {
                Guid tabelaPrecoId = Guid.NewGuid();

                TabelaPreco tabelaPreco = new TabelaPreco
                {
                    TabelaPrecoId = tabelaPrecoId,
                    Inicio = request.Inicio,
                    Fim = request.Fim,
                    ValorMeioPeriodo = request.ValorMeioPeriodo,
                    ValorDiaria = request.ValorDiaria,

                    CreatedBy = "TESTE",
                    Created = DateTime.Now
                };

                _tabelaPrecoRepository.Add(tabelaPreco);
                _unitOfWork.Commit();

                return await Task.FromResult(tabelaPreco);
            }
        }
    }
}