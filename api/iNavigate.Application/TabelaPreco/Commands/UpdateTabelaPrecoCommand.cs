﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Commands
{
    public class UpdateTabelaPrecoCommand : IRequest<TabelaPreco>
    {
        public Guid TabelaPrecoId { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fim { get; set; }
        public decimal ValorMeioPeriodo { get; set; }
        public decimal ValorDiaria { get; set; }

        
        public class Handler : IRequestHandler<UpdateTabelaPrecoCommand, TabelaPreco>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly ITabelaPrecoRepository _tabelaPrecoRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               ITabelaPrecoRepository tabelaPrecoRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _tabelaPrecoRepository = tabelaPrecoRepository;
            }

            public async Task<TabelaPreco> Handle(UpdateTabelaPrecoCommand request, CancellationToken cancellationToken)
            {
                TabelaPreco tabelaPreco = _tabelaPrecoRepository.GetById(request.TabelaPrecoId);

                if (tabelaPreco != null)
                {
                   tabelaPreco.Inicio = request.Inicio;
                   tabelaPreco.Fim = request.Fim;
                   tabelaPreco.ValorMeioPeriodo = request.ValorMeioPeriodo;
                    tabelaPreco.ValorDiaria = request.ValorDiaria;

                    tabelaPreco.LastModifiedBy = "TESTE";
                    tabelaPreco.LastModified = DateTime.Now;

                    _tabelaPrecoRepository.Update(tabelaPreco);
                    _unitOfWork.Commit();
                }

                return await Task.FromResult(tabelaPreco);
            }
        }
    }
}