﻿using System;

namespace iNavigate.Application.ViewModel
{
    public class TabelaPrecoViewModel
    {
        public string TabelaPrecoId { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fim { get; set; }
        public decimal ValorMeioPeriodo { get; set; }
        public decimal ValorDiaria { get; set; }

        public string CreateBy { get; set; }

        public DateTime Created { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModified { get; set; }
    }
}
