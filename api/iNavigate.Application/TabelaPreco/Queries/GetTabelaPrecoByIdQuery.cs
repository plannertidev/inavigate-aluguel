﻿using iNavigate.Application.ViewModel;
using iNavigate.Domain.Entites;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Queries
{
    public class GetTabelaPrecoByIdQuery : IRequest<TabelaPrecoViewModel>
    {
        public Guid TabelaPrecoId { get; set; }
    }

    public class GetTabelaPrecoByIdHandler : IRequestHandler<GetTabelaPrecoByIdQuery, TabelaPrecoViewModel>
    {
        private readonly iNavigateContext _context;

        public GetTabelaPrecoByIdHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<TabelaPrecoViewModel> Handle(GetTabelaPrecoByIdQuery request, CancellationToken cancellationToken)
        {
            TabelaPrecoViewModel tabelaPrecoViewModel = null;

            var records = from tpreco in _context.TabelaPreco where tpreco.TabelaPrecoId.Equals(request.TabelaPrecoId) select tpreco;
            TabelaPreco tabelaPreco = records.FirstOrDefault();

            if (tabelaPreco != null)
            {
                tabelaPrecoViewModel = new TabelaPrecoViewModel();

                tabelaPrecoViewModel.TabelaPrecoId = tabelaPreco.TabelaPrecoId.ToString();
                tabelaPrecoViewModel.Inicio = tabelaPreco.Inicio;
                tabelaPrecoViewModel.Fim = tabelaPreco.Fim;
                tabelaPrecoViewModel.ValorMeioPeriodo = tabelaPreco.ValorMeioPeriodo;
                tabelaPrecoViewModel.ValorDiaria = tabelaPreco.ValorDiaria;

                tabelaPrecoViewModel.CreateBy = tabelaPreco.CreatedBy;
                tabelaPrecoViewModel.Created = tabelaPreco.Created;
                tabelaPrecoViewModel.LastModified = tabelaPreco.LastModified;
                tabelaPrecoViewModel.LastModifiedBy = tabelaPreco.LastModifiedBy;
            }

            return await Task.FromResult(tabelaPrecoViewModel);
        }
    }
}