﻿using iNavigate.Application.ViewModel;
using iNavigate.Domain.Entites;
using INavigate.Data.Context;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Queries
{
    public class GetAllTabelaPrecoQuery : IRequest<List<TabelaPrecoViewModel>>
    {
    }

    public class GetAllTabelaPrecoHandler : IRequestHandler<GetAllTabelaPrecoQuery, List<TabelaPrecoViewModel>>
    {
        private readonly iNavigateContext _context;

        public GetAllTabelaPrecoHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<List<TabelaPrecoViewModel>> Handle(GetAllTabelaPrecoQuery request, CancellationToken cancellationToken)
        {
            var records = from tpreco in _context.TabelaPreco select tpreco;

            List<TabelaPrecoViewModel> TabelaPrecoViewModelCollection = new List<TabelaPrecoViewModel>();
            foreach (TabelaPreco tabelaPreco in records.ToList())
            {
                TabelaPrecoViewModel tabelaPrecoViewModel = new TabelaPrecoViewModel();
                tabelaPrecoViewModel.TabelaPrecoId = tabelaPreco.TabelaPrecoId.ToString();
                tabelaPrecoViewModel.Inicio = tabelaPreco.Inicio;
                tabelaPrecoViewModel.Fim = tabelaPreco.Fim;
                tabelaPrecoViewModel.ValorMeioPeriodo = tabelaPreco.ValorMeioPeriodo;
                tabelaPrecoViewModel.ValorDiaria = tabelaPreco.ValorDiaria;

                tabelaPrecoViewModel.CreateBy = tabelaPreco.CreatedBy;
                tabelaPrecoViewModel.Created = tabelaPreco.Created;
                tabelaPrecoViewModel.LastModified = tabelaPreco.LastModified;
                tabelaPrecoViewModel.LastModifiedBy = tabelaPreco.LastModifiedBy;

                TabelaPrecoViewModelCollection.Add(tabelaPrecoViewModel);
            }

            return await Task.FromResult(TabelaPrecoViewModelCollection);
        }
    }
}