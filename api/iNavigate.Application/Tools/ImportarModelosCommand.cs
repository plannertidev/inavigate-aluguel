﻿using iNavigate.Application.Queries;
using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using MediatR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Tools
{
    public class ImportarModelosCommand : IRequest<Modelo>
    {
        public class Handler : IRequestHandler<ImportarModelosCommand, Modelo>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IModeloRepository _modeloRepository;
            private readonly IFabricanteRepository _fabricanteRepository;
            private readonly ICategoriaRepository _categoriaRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               IModeloRepository modeloRepository,
               IFabricanteRepository fabricanteRepository,
               ICategoriaRepository categoriaRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _modeloRepository = modeloRepository;
                _fabricanteRepository = fabricanteRepository;
                _categoriaRepository = categoriaRepository;
            }

            public async Task<Modelo> Handle(ImportarModelosCommand request, CancellationToken cancellationToken)
            {
                Modelo modelo = new Modelo();

                int contador = 0;

                string root = @"C:\Temp\modelos de barcos";

                string[] fileEntries = Directory.GetFiles(root);
                foreach (string fileName in fileEntries)
                {
                    var data = ParsePdf(fileName);
                    var lines = data.Split("\n");
                    string categoriaName = extractCategoriaName(lines);

                    GetCategoriaByFilterQuery getCategoriaByFilter = new GetCategoriaByFilterQuery();
                    getCategoriaByFilter.descricao = categoriaName;
                    List<Categoria> categoriaCollection = await _mediator.Send(getCategoriaByFilter);

                    if (categoriaCollection.FirstOrDefault() == null)
                    {
                        Categoria categoria = new Categoria();
                        categoria.CategoriaId = Guid.NewGuid();
                        categoria.Descricao = categoriaName;
                        _categoriaRepository.Add(categoria);
                    }

                    Guid fabricanteId = Guid.Empty;
                    string fabricanteName = extractFabricanteName(lines);
                    GetFabricanteByFilterQuery getFabricanteByFilterQuery = new GetFabricanteByFilterQuery();
                    getFabricanteByFilterQuery.nome = fabricanteName;
                    List<Fabricante> fabricantesCollection = await _mediator.Send(getFabricanteByFilterQuery);
                    if (fabricantesCollection.FirstOrDefault() == null)
                    {
                        fabricanteId = Guid.NewGuid();
                        Fabricante fabricante = new Fabricante();
                        fabricante.FabricanteId = fabricanteId;
                        fabricante.Nome = fabricanteName;
                        _fabricanteRepository.Add(fabricante);
                    }

                    string modeloDescricao = extractModelo("Descricao", lines);
                    GetModeloByFilterQuery getModeloByFilterQuery = new GetModeloByFilterQuery();
                    getModeloByFilterQuery.Descricao = modeloDescricao;
                    List<Modelo> modelosCollection = await _mediator.Send(getModeloByFilterQuery);
                    if (modelosCollection.FirstOrDefault() == null)
                    {
                        modelo = new Modelo();
                        modelo.FabricanteId = fabricanteId;
                        modelo.Descricao = extractModelo("Descricao", lines);
                        modelo.AlturaBanheiro = extractModelo("AlturaBanheiro", lines);
                        modelo.AlturaCabine = extractModelo("AlturaCabine", lines);
                        modelo.Banheiros = extractModelo("Banheiros", lines);
                        modelo.Boca = extractModelo("Boca", lines);
                        modelo.Cabines = extractModelo("Cabines", lines);
                        modelo.CaladoMin = extractModelo("CaladoMin", lines);
                        modelo.CaladoMax = extractModelo("CaladoMax", lines);
                        modelo.Capacidade = extractModelo("Capacidade", lines);
                        modelo.CapacidadePernoite = extractModelo("CapacidadePernoite", lines);
                        modelo.Casco = extractModelo("Casco", lines);
                        modelo.ComprimentoLOA = extractModelo("ComprimentoLOA", lines);
                        modelo.ComprimentoLWL = extractModelo("ComprimentoLWL", lines);
                        modelo.InicioFimProducao = extractModelo("InicioFimProducao", lines);
                        modelo.MotorDeSerie = extractModelo("MotorDeSerie", lines);
                        modelo.Origem = extractModelo("Origem", lines);
                        modelo.PesoLeve = extractModelo("PesoLeve", lines);
                        modelo.PotenciaMaxima = extractModelo("PotenciaMaxima", lines);
                        modelo.PotenciaMinima = extractModelo("PotenciaMinima", lines);
                        modelo.TanqueAgua = extractModelo("TanqueAgua", lines);
                        modelo.TanqueCombustivel = extractModelo("TanqueCombustivel", lines);
                        modelo.VelocidadeMax = extractModelo("VelocidadeMaximaKm", lines);
                        _modeloRepository.Add(modelo);
                        contador++;
                    }


                    if (contador > 500)
                    {
                        break;
                    }
                }

                _unitOfWork.Commit();

                return await Task.FromResult(modelo);
            }

            private string extractModelo(string v, string[] lines)
            {
                if (v.Equals("Descricao")) { return lines[0].Trim(); }

                if (v.Equals("AlturaBanheiro")) { return lines[13].Replace("Bath height /", "").Trim(); }

                if (v.Equals("AlturaCabine")) { return lines[12].Replace("Cabine height", "").Trim(); }

                if (v.Equals("AnguleVpopaGr")) { return lines[12].Replace("Cabine height", "").Trim(); }

                if (v.Equals("InicioFimProducao")) { return lines[4].Replace("(Start / End) Production", "").Trim(); }

                if (v.Equals("Banheiros")) 
                {
                    string text = lines[11].Replace("Cabins / Bath", "");
                    var contente = text.Split("/");
                    string banheiros = string.Empty;
                    if (contente.Length > 1)
                    {
                        banheiros = contente[1];
                    }
                    return banheiros; 
                }

                if (v.Equals("Boca")) { return lines[7].Replace("Beam", "").Trim(); }
                
                if (v.Equals("Cabines")) 
                {
                    string text = lines[11].Replace("Cabins / Bath", "");
                    var contente = text.Split("/");
                    string banheiros = string.Empty;
                    if (contente.Length > 0)
                    {
                        banheiros = contente[0];
                    }
                    return banheiros;
                }

                if (v.Equals("CaladoMin")) 
                {
                    return lines[8].Replace("Draft (Min.)", "").Trim();
                } 

                if (v.Equals("CaladoMax")) 
                {
                    return lines[9].Replace("Draft (Max.)", "").Trim();
                }

                if (v.Equals("Capacidade")) 
                {
                    string text = lines[14].Replace("Passengers (day / night)", "");
                    var contente = text.Split("/");
                    string value = string.Empty;
                    if (contente.Length > 0)
                    {
                        value = contente[0];
                    }
                    return value;
                }

                if (v.Equals("CapacidadePernoite"))
                {
                    string text = lines[14].Replace("Passengers (day / night)", "");
                    var contente = text.Split("/");
                    string value = string.Empty;
                    if (contente.Length > 1)
                    {
                        value = contente[1];
                    }
                    return value;
                }

                if (v.Equals("TanqueAgua")) { return lines[16].Replace("Water Tank", "").Trim(); }
                if (v.Equals("TanqueCombustivel")) { return lines[15].Replace("Fuel Tank", "").Trim(); }
                if (v.Equals("PotenciaMaxima")) { return lines[17].Replace("Min. Power", "").Trim(); }
                if (v.Equals("PotenciaMinima")) { return lines[18].Replace("Max. Power", "").Trim(); }
                if (v.Equals("MotorDeSerie")) { return lines[19].Replace("Std. Engine", "").Trim(); }
                if (v.Equals("ComprimentoLOA")) { return lines[5].Replace("Lenght (LOA)", "").Trim(); }
                if (v.Equals("ComprimentoLWL")) { return lines[6].Replace("Width (LWL)", "").Trim(); }
                if (v.Equals("PesoLeve")) { return lines[10].Replace("Weight (kg)", "").Trim(); }
                if (v.Equals("Projeto")) { return string.Empty; }
                if (v.Equals("CapacidadeCarga")) { return string.Empty; } //<===
                if (v.Equals("VelocidadeMaximaKm")) { return string.Empty; }

                int lineCaso = 21;
                int lineOrigem = 22;
                if (!lines[lineCaso].StartsWith("Hull"))
                {
                    lineCaso++;
                    lineOrigem++;
                }
                if (v.Equals("Casco")) { return lines[lineCaso].Replace("Hull", "").Trim(); }
                if (v.Equals("Origem")) { return lines[lineOrigem].Replace("Origin country", "").Trim(); }

                return string.Empty;
            }

            private string extractFabricanteName(string[] lines)
            {
                return lines[2].Replace("Manufacturer", "").Trim();
            }

            private string extractCategoriaName(string[] lines)
            {
                return lines[1].Replace("Category", "").Trim();
            }

            public string ParsePdf(string fileName)
            {
                if (!File.Exists(fileName))
                    throw new FileNotFoundException(fileName);
                using (PdfReader reader = new PdfReader(fileName))
                {
                    StringBuilder sb = new StringBuilder();

                    ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                    for (int page = 0; page < reader.NumberOfPages; page++)
                    {
                        string text = PdfTextExtractor.GetTextFromPage(reader, page + 1, strategy);
                        if (!string.IsNullOrWhiteSpace(text))
                        {
                            sb.Append(Encoding.UTF8.GetString(ASCIIEncoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(text))));
                        }
                    }

                    return sb.ToString();
                }
            }
        }
    }
}

