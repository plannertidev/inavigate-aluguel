﻿using iNavigate.Application.MarcaDaPropriedades.ViewModel;
using iNavigate.Domain.Entites;
using INavigate.Data.Context;
using MediatR;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.MarcaDaPropriedades.Queries
{
    public class GetMarcaQuery : IRequest<List<MarcaViewModel>>
    {
    }
    public class GetMarcaHandler : IRequestHandler<GetMarcaQuery, List<MarcaViewModel>>
    {
        private readonly iNavigateContext _context;

        public GetMarcaHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<List<MarcaViewModel>> Handle(GetMarcaQuery request, CancellationToken cancellationToken)
        {
            var records = from marc in _context.Marca select marc;

            List<MarcaViewModel> MarcaViewModelCollection = new List<MarcaViewModel>();
            foreach (Marca marca in records.ToList())
            {
                MarcaViewModel marcaView = new MarcaViewModel();
                marcaView.MarcaId = marca.MarcaId.ToString();
                marcaView.Descricao = marca.Descricao;
                marcaView.Creator = marca.Creator;
                marcaView.Created = marca.Created;
                marcaView.Changer = marca.Changer;
                marcaView.Changed = marca.Changed;
                MarcaViewModelCollection.Add(marcaView);
            }

            return await Task.FromResult(MarcaViewModelCollection);
        }
    }
}
