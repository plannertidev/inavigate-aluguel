﻿using iNavigate.Domain.Entites;
using INavigate.Data.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Queries
{
   public  class GetMarcaByIdQuery : IRequest<Marca>
    {

        public Guid MarcaId { get; set; }
    }

    public class GetMarcaByIdQueryHandler : IRequestHandler<GetMarcaByIdQuery, Marca>
    {
        private readonly iNavigateContext _context;

        public GetMarcaByIdQueryHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<Marca> Handle(GetMarcaByIdQuery request, CancellationToken cancellationToken)
        {
            var records = await _context.Marca.Where(x => x.MarcaId.Equals(request.MarcaId))
                    .FirstOrDefaultAsync(cancellationToken);

            return await Task.FromResult(records);
        }
    }
}
