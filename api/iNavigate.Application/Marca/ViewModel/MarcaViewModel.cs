﻿using System;

namespace iNavigate.Application.MarcaDaPropriedades.ViewModel
{
    public class MarcaViewModel
    {
        public string MarcaId { get; set; }
        public string Descricao { get; set; }
        public string Creator { get; set; }
        public DateTime? Created { get; set; }
        public string Changer { get; set; }
        public DateTime? Changed { get; set; }
    }
}
