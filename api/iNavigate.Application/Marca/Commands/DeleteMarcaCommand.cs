﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.MarcaDaPropriedades.Commands
{
    public class DeleteMarcaCommand : IRequest<Unit>
    {
        public Guid MarcaId { get; set; }
      
        public class Handler : IRequestHandler<DeleteMarcaCommand, Unit>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IMarcaRepository _marcaRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               IMarcaRepository marcaRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _marcaRepository = marcaRepository;
            }

            public async Task<Unit> Handle(DeleteMarcaCommand request, CancellationToken cancellationToken)
            {
                Marca marca = _marcaRepository.GetById(request.MarcaId);
                if (marca != null)
                {
                    _marcaRepository.Remove(marca.MarcaId);
                    _unitOfWork.Commit();
                }

                return Unit.Value;
            }
        }
    }

}
