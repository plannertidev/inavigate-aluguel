﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.MarcaDaPropriedades.Commands
{
    public class UpdateMarcaCommand : IRequest<Unit>
    {

        public Guid MarcaId { get; set; }
        public string Descricao { get; set; }
        public string Creator { get; set; }
        public DateTime Created { get; set; }
        public string Changer { get; set; }
        public DateTime Changed { get; set; }

        public class Handler : IRequestHandler<UpdateMarcaCommand, Unit>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IMarcaRepository _marcaRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               IMarcaRepository marcaRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _marcaRepository = marcaRepository;
            }

            public async Task<Unit> Handle(UpdateMarcaCommand request, CancellationToken cancellationToken)
            {
                Marca marca = _marcaRepository.GetById(request.MarcaId);
                if (marca != null)
                {
                    marca.Descricao = request.Descricao;
                    marca.Changer = request.Changer;
                    marca.Changed = DateTime.Now;
                    _marcaRepository.Update(marca);
                    _unitOfWork.Commit();
                }

                return Unit.Value;
            }
        }
    }
}
