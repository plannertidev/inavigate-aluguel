﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.MarcaDaPropriedades.Commands
{
    public class CreateMarcaCommand : IRequest<Guid>
    {
        public Guid MarcaId { get; set; }
        public string Descricao { get; set; }
        public string Creator { get; set; }
        public DateTime? Created { get; set; }
        public string Changer { get; set; }
        public DateTime? Changed { get; set; }

        public class Handler : IRequestHandler<CreateMarcaCommand, Guid>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IMarcaRepository _marcaRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               IMarcaRepository marcaRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _marcaRepository = marcaRepository;
            }

            public async Task<Guid> Handle(CreateMarcaCommand request, CancellationToken cancellationToken)
            {
                Guid result = Guid.NewGuid();

                Marca marca = new Marca
                {
                    MarcaId = result,
                    Descricao = request.Descricao,
                    Creator = request.Creator,
                    Created = DateTime.Now,

                };

                _marcaRepository.Add(marca);

                try
                {
                    _unitOfWork.Commit();
                }
                catch (Exception e)
                {

                    throw;
                }
                
                return await Task.FromResult(result);
            }
        }
    }
}
