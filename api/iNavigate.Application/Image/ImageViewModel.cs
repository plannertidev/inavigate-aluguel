﻿namespace iNavigate.Application.Image
{
    public class ImageViewModel
    {
        public string Folder { get; set; }
        public string FileName { get; set; }
        public string File { get; set; }
    }
}
