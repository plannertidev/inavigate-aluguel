﻿using System;

namespace iNavigate.Application.ViewModel
{
    public class FabricanteViewModel
    {
        public string FabricanteId { get; set; }
        public string Nome { get; set; }
        public string Creator { get; set; }
        public DateTime? Created { get; set; }
        public string Changer { get; set; }
        public DateTime? Changed { get; set; }
    }
}