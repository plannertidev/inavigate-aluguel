﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Commands
{
    public class UpdateFabricanteCommand : IRequest<Unit>
    {
        public Guid FabricanteId { get; set; }
        public string Nome { get; set; }
        public string Creator { get; set; }
        public DateTime Created { get; set; }
        public string Changer { get; set; }
        public DateTime Changed { get; set; }

        public class Handler : IRequestHandler<UpdateFabricanteCommand, Unit>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IFabricanteRepository _fabricanteDaPropriedadeRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               IFabricanteRepository fabricanteDaPropriedadeRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _fabricanteDaPropriedadeRepository = fabricanteDaPropriedadeRepository;
            }

            public async Task<Unit> Handle(UpdateFabricanteCommand request, CancellationToken cancellationToken)
            {
                Fabricante fabricanteDaPropriedade = _fabricanteDaPropriedadeRepository.GetById(request.FabricanteId);
                if (fabricanteDaPropriedade != null)
                {
                    fabricanteDaPropriedade.Nome = request.Nome;
                    fabricanteDaPropriedade.Changer = request.Changer;
                    fabricanteDaPropriedade.Changed = DateTime.Now;
                    _fabricanteDaPropriedadeRepository.Update(fabricanteDaPropriedade);
                    _unitOfWork.Commit();
                }

                return Unit.Value;
            }
        }
    }
}
