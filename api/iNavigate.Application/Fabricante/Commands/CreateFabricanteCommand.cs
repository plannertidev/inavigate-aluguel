﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Commands
{
    public class CreateFabricanteCommand : IRequest<Domain.Entites.Fabricante>
    {
        public Guid FabricantePropriedadeId { get; set; }
        public string Nome { get; set; }
        public string Creator { get; set; }
        public DateTime? Created { get; set; }
        public string Changer { get; set; }
        public DateTime? Changed { get; set; }

        public class Handler : IRequestHandler<CreateFabricanteCommand, Domain.Entites.Fabricante>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IFabricanteRepository _fabricantePropriedadeRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               IFabricanteRepository fabricanteDaPropriedadeRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _fabricantePropriedadeRepository = fabricanteDaPropriedadeRepository;
            }

            public async Task<Domain.Entites.Fabricante> Handle(CreateFabricanteCommand request, CancellationToken cancellationToken)
            {
                Guid fabricanteId = Guid.NewGuid();

                Domain.Entites.Fabricante fabricante = new Domain.Entites.Fabricante
                {
                    FabricanteId = fabricanteId,
                    Nome = request.Nome,
                    Creator = request.Creator,
                    Created = DateTime.Now

                };

                _fabricantePropriedadeRepository.Add(fabricante);

                try
                {
                    _unitOfWork.Commit();
                }
                catch (Exception meuerro)
                {

                    throw;
                }
                

                return await Task.FromResult(fabricante);
            }
        }

    }

}
