﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Commands
{
    public class DeleteFabricanteCommand : IRequest<Unit>
    {
        public Guid FabricanteId { get; set; }

        public class Handler : IRequestHandler<DeleteFabricanteCommand, Unit>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IFabricanteRepository _fabricanteDaPropriedadeRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               IFabricanteRepository fabricanteDaPropriedadeRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _fabricanteDaPropriedadeRepository = fabricanteDaPropriedadeRepository;
            }

            public async Task<Unit> Handle(DeleteFabricanteCommand request, CancellationToken cancellationToken)
            {
                Fabricante fabricante = _fabricanteDaPropriedadeRepository.GetById(request.FabricanteId);
                if (fabricante != null)
                {
                    _fabricanteDaPropriedadeRepository.Remove(fabricante.FabricanteId);
                    _unitOfWork.Commit();
                }

                return Unit.Value;
            }

        }
    }
}


