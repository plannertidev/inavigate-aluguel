﻿using iNavigate.Application.ViewModel;
using iNavigate.Domain.Entites;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Queries
{
    public class GetFabricanteById : IRequest<FabricanteViewModel>
    {
        public Guid FabricanteId { get; set; }
    }

    public class GetFabricanteByIdHandler : IRequestHandler<GetFabricanteById, FabricanteViewModel>
    {
        private readonly iNavigateContext _context;

        public GetFabricanteByIdHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<FabricanteViewModel> Handle(GetFabricanteById request, CancellationToken cancellationToken)
        {
            FabricanteViewModel fabricanteView = null;

            var records = from fab in _context.Fabricante where fab.FabricanteId.Equals(request.FabricanteId) select fab;
            Fabricante fabricante = records.FirstOrDefault();

            if (fabricante != null)
            {
                fabricanteView = new FabricanteViewModel();

                fabricanteView.FabricanteId = fabricante.FabricanteId.ToString();
                fabricanteView.Nome = fabricante.Nome;
                fabricanteView.Creator = fabricante.Creator;
                fabricanteView.Created = fabricante.Created;
                fabricanteView.Changer = fabricante.Changer;
                fabricanteView.Changed = fabricante.Changed;
            }

            return await Task.FromResult(fabricanteView);
        }
    }

}
