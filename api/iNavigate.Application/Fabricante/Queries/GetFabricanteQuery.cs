﻿using iNavigate.Application.ViewModel;
using iNavigate.Domain.Entites;
using INavigate.Data.Context;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Queires
{
    public class GetFabricanteQuery : IRequest<List<FabricanteViewModel>>
    {
    }

    public class GetFabricantesHandler : IRequestHandler<GetFabricanteQuery, List<FabricanteViewModel>>
    {
        private readonly iNavigateContext _context;

        public GetFabricantesHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<List<FabricanteViewModel>> Handle(GetFabricanteQuery request, CancellationToken cancellationToken)
        {
            var records = from fab in _context.Fabricante select fab;

            List<FabricanteViewModel> FabricanteViewModelCollection = new List<FabricanteViewModel>();
            foreach (Fabricante fabricante in records.ToList())
            {
                FabricanteViewModel fabricanteView = new FabricanteViewModel();
                fabricanteView.FabricanteId = fabricante.FabricanteId.ToString();
                fabricanteView.Nome = fabricante.Nome;
                fabricanteView.Creator = fabricante.Creator;
                fabricanteView.Created = fabricante.Created;
                fabricanteView.Changer = fabricante.Changer;
                fabricanteView.Changed = fabricante.Changed;
                FabricanteViewModelCollection.Add(fabricanteView);
            }

            return await Task.FromResult(FabricanteViewModelCollection);
        }
    }

}
