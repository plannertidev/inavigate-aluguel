﻿using iNavigate.Domain.Entites;
using INavigate.Data.Context;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Queries
{
    public class GetFabricanteByFilterQuery : IRequest<List<Fabricante>>
    {
        public string nome { get; set; }
    }

    public class GetFabricanteByFilterQueryHandler : IRequestHandler<GetFabricanteByFilterQuery, List<Fabricante>>
    {
        private readonly iNavigateContext _context;

        public GetFabricanteByFilterQueryHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<List<Fabricante>> Handle(GetFabricanteByFilterQuery request, CancellationToken cancellationToken)
        {
            var records = _context.Fabricante.Where(x => x.Nome.Equals(request.nome)).ToList();

            return await Task.FromResult(records);
        }
    }
}
