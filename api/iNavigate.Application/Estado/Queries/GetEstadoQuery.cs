﻿using iNavigate.Application.ViewModel;
using iNavigate.Domain.Entites;
using INavigate.Data.Context;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace iNavigate.Application.Queries
{
    public class GetEstadoQuery : IRequest<List<EstadoViewModel>>
    {
    }
    public class GetAEstadoHandler : IRequestHandler<GetEstadoQuery, List<EstadoViewModel>>
    {
        private readonly iNavigateContext _context;

        public GetAEstadoHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<List<EstadoViewModel>> Handle(GetEstadoQuery request, CancellationToken cancellationToken)
        {
            var records = from esta in _context.Estado select esta;

            List<EstadoViewModel> EstadoViewModelCollection = new List<EstadoViewModel>();
            foreach (Estado estado in records.ToList())
            {
                EstadoViewModel estadoView = new EstadoViewModel();
                estadoView.EstadoId = estado.EstadoId.ToString();
                estadoView.Nome = estado.Nome;

                estadoView.Creator = estado.Creator;
                estadoView.Created = estado.Created;
                estadoView.Changer = estado.Changer;
                estadoView.Changed = estado.Changed;

                EstadoViewModelCollection.Add(estadoView);
            }

            return await Task.FromResult(EstadoViewModelCollection);
        }

    }
}