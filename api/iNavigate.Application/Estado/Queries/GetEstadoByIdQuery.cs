﻿using iNavigate.Domain.Entites;
using INavigate.Data.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Queries
{
    public class GetEstadoByIdQuery : IRequest<Estado>
    {
        public Guid EstadoId { get; set; }
    }

    public class GetEstadoByIdQueryHandler : IRequestHandler<GetEstadoByIdQuery, Estado>
    {
        private readonly iNavigateContext _context;

        public GetEstadoByIdQueryHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<Estado> Handle(GetEstadoByIdQuery request, CancellationToken cancellationToken)
        {
            var records = await _context.Estado.Where(x => x.EstadoId.Equals(request.EstadoId))
                    .FirstOrDefaultAsync(cancellationToken);

            return await Task.FromResult(records);
        }
    }
}
