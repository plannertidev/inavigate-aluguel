﻿using System;

namespace iNavigate.Application.ViewModel
{
    public class EstadoViewModel
    {
        public string EstadoId { get; set; }
        public string Nome { get; set; }

        public string Creator { get; set; }
        public DateTime? Created { get; set; }
        public string Changer { get; set; }
        public DateTime? Changed { get; set; }
    }
}
