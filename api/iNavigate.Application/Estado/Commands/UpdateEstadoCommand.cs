﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Commands
{
    public class UpdateEstadoCommand : IRequest<Unit>
    {
        public Guid EstadoId { get; set; }
        public string Nome { get; set; }
        
        public string Creator { get; set; }
        public DateTime? Created { get; set; }
        public string Changer { get; set; }
        public DateTime? Changed { get; set; }

        public class Handler : IRequestHandler<UpdateEstadoCommand, Unit>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IEstadoRepository _estadoRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               IEstadoRepository estadoRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _estadoRepository = estadoRepository;
            }

            public async Task<Unit> Handle(UpdateEstadoCommand request, CancellationToken cancellationToken)
            {
                Estado estado = _estadoRepository.GetById(request.EstadoId);
                if (estado != null)
                {
                   
                    estado.Nome = request.Nome;
                    
                    estado.Creator = request.Creator;
                    estado.Created = request.Created;
                    estado.Changer = request.Changer;
                    estado.Changed = request.Changed;

                    _estadoRepository.Update(estado);
                    _unitOfWork.Commit();
                }

                return Unit.Value;
            }
        }
    }
}