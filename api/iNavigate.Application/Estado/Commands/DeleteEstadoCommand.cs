﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Commands
{
    public class DeleteEstadoCommand : IRequest<Unit>
    {
        public Guid EstadoId { get; set; }

        public class Handler : IRequestHandler<DeleteEstadoCommand, Unit>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IEstadoRepository _estadoRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               IEstadoRepository estadoRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _estadoRepository = estadoRepository;
            }

            public async Task<Unit> Handle(DeleteEstadoCommand request, CancellationToken cancellationToken)
            {
                Estado estado = _estadoRepository.GetById(request.EstadoId);
                if (estado != null)
                {
                    _estadoRepository.Remove(estado.EstadoId);
                    _unitOfWork.Commit();
                }

                return Unit.Value;
            }
        }
    }
}
