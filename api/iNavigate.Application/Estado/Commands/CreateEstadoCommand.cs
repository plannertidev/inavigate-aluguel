﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Commands
{
    public class CreateEstadoCommand : IRequest<Guid>
    {
        public Guid EstadoId { get; set; }
        public string Nome { get; set; }

        public string Creator { get; set; }
        public DateTime? Created { get; set; }
        public string Changer { get; set; }
        public DateTime? Changed { get; set; }


        public class Handler : IRequestHandler<CreateEstadoCommand, Guid>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IEstadoRepository _estadoRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               IEstadoRepository estadoRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _estadoRepository = estadoRepository;
            }

            public async Task<Guid> Handle(CreateEstadoCommand request, CancellationToken cancellationToken)
            {
                Guid result = Guid.NewGuid();

                Estado estado = new Estado
                {
                    EstadoId = result,
                    Nome = request.Nome,

                    Creator = request.Creator,
                    Created = request.Created,
                    Changer = request.Changer,
                    Changed = request.Changed

                };

                _estadoRepository.Add(estado);
                try
                {
                    _unitOfWork.Commit();
                }
                catch (Exception e)
                {

                    throw;
                }


                return await Task.FromResult(result);
            }
        }
    }
}