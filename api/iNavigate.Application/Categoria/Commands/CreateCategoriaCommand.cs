﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Commands
{
    public class CreateCategoriaCommand : IRequest<Guid>
    {
        public Guid CategoriaId { get; set; }
        public string Descricao { get; set; }
        public string Creator { get; set; }
        public DateTime? Created { get; set; }
        public string Changer { get; set; }
        public DateTime? Changed { get; set; }

        public class Handler : IRequestHandler<CreateCategoriaCommand, Guid>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly ICategoriaRepository _categoriaRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               ICategoriaRepository categoriaRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _categoriaRepository = categoriaRepository;
            }

            public async Task<Guid> Handle(CreateCategoriaCommand request, CancellationToken cancellationToken)
            {
                Guid result = Guid.NewGuid();

                Categoria categoria = new Categoria
                {
                    CategoriaId = result,
                    Descricao = request.Descricao,
                    Creator = request.Creator,
                    Created = request.Created,
                    Changer = request.Changer,
                    Changed = request.Changed

                };

                _categoriaRepository.Add(categoria);
                try
                {
                    _unitOfWork.Commit();
                }
                catch (Exception e)
                {

                    throw;
                }


                return await Task.FromResult(result);
            }
        }
    }
}
