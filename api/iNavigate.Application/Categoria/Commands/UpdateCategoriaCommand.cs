﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Commands
{
    public class UpdateCategoriaCommand : IRequest<Unit>
    {
        public Guid CategoriaId { get; set; }
        public string Descricao { get; set; }
        public string Creator { get; set; }
        public DateTime? Created { get; set; }
        public string Changer { get; set; }
        public DateTime? Changed { get; set; }

        public class Handler : IRequestHandler<UpdateCategoriaCommand, Unit>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly ICategoriaRepository _categoriaRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               ICategoriaRepository categoriaRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _categoriaRepository = categoriaRepository;
            }

            public async Task<Unit> Handle(UpdateCategoriaCommand request, CancellationToken cancellationToken)
            {
                Categoria categoria = _categoriaRepository.GetById(request.CategoriaId);
                if (categoria != null)
                {
                    categoria.Descricao = request.Descricao;
                    categoria.Creator = request.Creator;
                    categoria.Created = request.Created;
                    categoria.Changer = request.Changer;
                    categoria.Changed = request.Changed;

                    _categoriaRepository.Update(categoria);
                    _unitOfWork.Commit();
                }

                return Unit.Value;
            }
        }
    }
}
