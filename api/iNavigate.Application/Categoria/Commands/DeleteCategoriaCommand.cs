﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Commands
{
    public class DeleteCategoriaCommand : IRequest<Unit>
    {
        public Guid CategoriaId { get; set; }

        public class Handler : IRequestHandler<DeleteCategoriaCommand, Unit>
        {
            private readonly IMediator _mediator;
            private readonly IUnitOfWork _unitOfWork;
            private readonly ICategoriaRepository _categoriaRepository;

            public Handler(
               IMediator mediator,
               IUnitOfWork unitOfWork,
               ICategoriaRepository categoriaRepository)
            {
                _mediator = mediator;
                _unitOfWork = unitOfWork;
                _categoriaRepository = categoriaRepository;
            }

            public async Task<Unit> Handle(DeleteCategoriaCommand request, CancellationToken cancellationToken)
            {
                Categoria categoria = _categoriaRepository.GetById(request.CategoriaId);
                if (categoria != null)
                {
                    _categoriaRepository.Remove(categoria.CategoriaId);
                    _unitOfWork.Commit();
                }

                return Unit.Value;
            }
        }
    }
    
    
}
