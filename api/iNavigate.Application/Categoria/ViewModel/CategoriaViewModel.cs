﻿using System;

namespace iNavigate.Application.ViewModel
{
    public class CategoriaViewModel
    {
        public string CategoriaId { get; set; }
        public string Descricao { get; set; }
        public string Creator { get; set; }
        public DateTime? Created { get; set; }
        public string Changer { get; set; }
        public DateTime? Changed { get; set; }
    }
}
