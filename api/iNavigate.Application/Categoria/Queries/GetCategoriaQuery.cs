﻿using iNavigate.Application.ViewModel;
using iNavigate.Domain.Entites;
using INavigate.Data.Context;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Queries
{
    public class GetCategoriaQuery : IRequest<List<CategoriaViewModel>>
    {
    }
    public class GetCategoriaHandler : IRequestHandler<GetCategoriaQuery, List<CategoriaViewModel>>
    {
        private readonly iNavigateContext _context;

        public GetCategoriaHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<List<CategoriaViewModel>> Handle(GetCategoriaQuery request, CancellationToken cancellationToken)
        {
            var records = from cat in _context.Categoria select cat;

            List<CategoriaViewModel> CategoriaViewModelCollection = new List<CategoriaViewModel>();
            foreach (Categoria categoria in records.ToList())
            {
                CategoriaViewModel categoriaView = new CategoriaViewModel();
                categoriaView.CategoriaId = categoria.CategoriaId.ToString();
                categoriaView.Descricao = categoria.Descricao;
                categoriaView.Creator = categoria.Creator;
                categoriaView.Created = categoria.Created;
                categoriaView.Changer = categoria.Changer;
                categoriaView.Changed = categoria.Changed;

                CategoriaViewModelCollection.Add(categoriaView);
            }

            return await Task.FromResult(CategoriaViewModelCollection);
        }

    }
}
