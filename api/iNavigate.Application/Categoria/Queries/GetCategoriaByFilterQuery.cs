﻿using iNavigate.Domain.Entites;
using INavigate.Data.Context;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Queries
{
    public class GetCategoriaByFilterQuery : IRequest<List<Categoria>>
    {
        public string descricao { get; set; }
    }

    public class GetCategoriaByFilterQueryHandler : IRequestHandler<GetCategoriaByFilterQuery, List<Categoria>>
    {
        private readonly iNavigateContext _context;

        public GetCategoriaByFilterQueryHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<List<Categoria>> Handle(GetCategoriaByFilterQuery request, CancellationToken cancellationToken)
        {
            var records = _context.Categoria.Where(x => x.Descricao.Equals(request.descricao)).ToList();

            return await Task.FromResult(records);
        }
    }
}
