﻿using iNavigate.Domain.Entites;
using INavigate.Data.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iNavigate.Application.Queries
{
    public class GetCategoriaByIdQuery : IRequest<Categoria>
    {
        public Guid CategoriaId { get; set; }
    }

    public class GetCategoriaByIdQueryHandler : IRequestHandler<GetCategoriaByIdQuery, Categoria>
    {
        private readonly iNavigateContext _context;

        public GetCategoriaByIdQueryHandler(iNavigateContext context)
        {
            _context = context;
        }

        public async Task<Categoria> Handle(GetCategoriaByIdQuery request, CancellationToken cancellationToken)
        {
            var records = await _context.Categoria.Where(x => x.CategoriaId.Equals(request.CategoriaId))
                    .FirstOrDefaultAsync(cancellationToken);

            return await Task.FromResult(records);
        }
    }
}
