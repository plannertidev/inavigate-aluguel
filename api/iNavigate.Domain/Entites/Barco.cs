﻿using iNavigate.Domain.Core;
using System;
using System.ComponentModel;

namespace iNavigate.Domain.Entites
{
    public class Barco : AuditableEntity
    {
        public Guid BarcoId { get; set; }
        public Guid ProprietarioId { get; set; }
        public Guid CategoriaId { get; set; }
        public Guid FabricanteId { get; set; }
        public Guid ModeloId { get; set; }
        public string Nome { get; set; }
        public string TipoCombustivel { get; set; }
        public string AnoFabricacao { get; set; }
        public string UltimaReforma { get; set; }
        public decimal TripulacaoIncluidaNoPreco { get; set; }
        public string Capacidade { get; set; }
        public string CapacidadePernoite { get; set; }
        public string NumeroDeCabines { get; set; }
        public string NumeroDeBanheiros { get; set; }
        public string Cumprimento { get; set; }
        public string Boca { get; set; }
        public string Calado { get; set; }
        public string Potencia { get; set; }
        public string VelocidadeCruzeiro { get; set; }
        public string VelocidadeMaxima { get; set; }
        public string ConsumoMedio { get; set; }
        public string DepositoAguaDoce { get; set; }
        public string DepositoCombustivel { get; set; }
        public decimal? Valor { get; set; }
        public decimal Status { get; set; }

        public string EquipamentoConves { get; set; }
        public string EquipamentoEletronico { get; set; }
        public string OutrosEquipamentos { get; set; }
        public string EntretenimentoAquatico { get; set; }
        public string Tripulacao { get; set; }
        public string IdiomasFalado { get; set; }

        public string TituloAnuncio { get; set; }
        public string TipoAnuncio { get; set; }
      

        public enum kdStatus
        {
            [Description("Pendente Aprovação")]
            Pendente,
            [Description("Aprovado")]
            Aprovado
        }

        public enum kdEquipamentoConves
        {
            [Description("Capota Dodger")]
            CapotaDodger,
            [Description("Bimini")]
            Bimini,
            [Description("Vela de Regata")]
            VelaRegata,
            [Description("Sala de cinema exterior")]
            SalaCinemaExterior,
            [Description("Propulsor de proa")]
            PropulsorProa,
            [Description("Flaps")]
            Flaps,
            [Description("Vela principal com enrolamento no mastro")]
            VelaPrincipalEnrolamentoMastro,
            [Description("Bar exterior")]
            BarExterior,
            [Description("Guinchos elétricos")]
            GuinchosEletricos,
            [Description("Âncora elétrica")]
            AncoraEletrica,
            [Description("Corda de segurança")]
            CordaSegurança,
            [Description("Estabilizador de embarcação na ancoragem")]
            EstabilizadorEmbarcacaoAncoragem,
            [Description("Churrasqueira")]
            Churrasqueira,
            [Description("Vela Spinnaker")]
            VelaSpinnaker,
            [Description("Acessibilidade para cadeiras de rodas")]
            AcessibilidadeCadeirasRodas,
            [Description("Estabilizador de embarcação na navegação")]
            EstabilizadorEmbarcacaoNavegacao,
            [Description("Plataforma de mergulho")]
            PlataformaMergulho,
            [Description("Vela Genaker")]
            VelaGenaker,
            [Description("Jacuzzi no convés")]
            JacuzziConves
        }

        public enum kdEquipamentoEletronico
        {
            [Description("Piloto automático")]
            PilotoAutomatico,
            [Description("TV")]
            TV,
            [Description("VHF")]
            VHF,
            [Description("TV em cada cabine")]
            TVCadaCabine,
            [Description("Telefone por satélite")]
            TelefoneSatelite,
            [Description("Video game")]
            VideoGame,
            [Description("Gerador")]
            Gerador,
            [Description("Auto-falantes no cockpit")]
            AutoFalantesCockpit,
            [Description("Conexão iPod")]
            ConexaoiPod,
            [Description("Tomada 220V")]
            Tomada220V,
            [Description("TV no salão")]
            TVSalao,
            [Description("MP4")]
            MP4,
            [Description("GPS Plotter")]
            GPSPlotter,
            [Description("Sonda (eco-sonoro)")]
            SondaEcoSonoro,
            [Description("WiFi a bordo")]
            WiFiBordo,
            [Description("IPS")]
            IPS,
            [Description("DVD player")]
            DVDPlayer,
            [Description("Inversor")]
            Inversor,
            [Description("Anemômetro")]
            Anemometro,
            [Description("CD")]
            CD,
            [Description("Sistema de painel de energia solar")]
            SistemaPainelEnergiaSolar,
            [Description("Radar")]
            Radar,
            [Description("Odômetro")]
            Odometro,
            [Description("GPS Plotter no cockpit")]
            GPSPlotterCockpit,
            [Description("Computador")]
            Computador,
            [Description("Sistema de purificador de água")]
            SistemaPurificadorAgua,
        }

        public enum kdOutrosEquipamentos
        {
            [Description("Água quente")]
            AguaQuente,
            [Description("Mesa no cockpit")]
            MesaCockpit,
            [Description("Congelador")]
            Congelador,
            [Description("Motor de popa")]
            MotorPopa,
            [Description("A/C em cada cabine")]
            ACCabine,
            [Description("Micro-ondas")]
            MicroOndas,
            [Description("Convés de teca")]
            ConvesTeca,
            [Description("Aquecedor no salão")]
            AquecedorSalao,
            [Description("Material de cozinha")]
            MaterialCozinha,
            [Description("A/C no salão")]
            ACSalao,
            [Description("Bote auxiliar")]
            BoteAuxiliar,
            [Description("Academia")]
            Academia,
            [Description("Cofre")]
            Cofre,
            [Description("Fogão a gás")]
            FogaoGas,
            [Description("Aquecedor em cada cabine")]
            AquecedorCadaCabine,
            [Description("Colete salva-vidas")]
            ColeteSalvaVidas,
            [Description("Aquecedor")]
            Aquecedor,
            [Description("Cooler")]
            Cooler,
            [Description("Geladeira elétrica")]
            GeladeiraEletrica,
            [Description("Equipamento de segurança")]
            EquipamentoSegurança,
            [Description("Forno")]
            Forno,
            [Description("Louças")]
            Loucas,
            [Description("Cartas Náuticas")]
            CartasNauticas,
            [Description("Elevador")]
            Elevador,
            [Description("Rádio Beacons Indicador de Posição de Emergência (EPIRB)")]
            RadioBeacons,
            [Description("Chuveiro no convés")]
            ChuveiroConves,
            [Description("Ar condicionado")]
            ArCondicionado,
            [Description("USB/AUX")]
            USBAUX,
            [Description("Serviço de Transfer")]
            ServicoTransfer
        }

        public enum kdEntretenimentoAquatico
        {
            [Description("Jet ski")]
            JetSki,
            [Description("Seabob")]
            Seabob,
            [Description("Equipamento de snorkel")]
            EquipamentoSnorkel,
            [Description("Caiaque")]
            Caiaque,
            [Description("Boia Banana")]
            BoiaBanana,
            [Description("Equipamento de mergulho")]
            EquipamentoMergulho,
            [Description("Equipamento de pesca")]
            EquipamentoPesca,
            [Description("Skis aquáticos")]
            SkisAquaticos,
            [Description("Skurfer")]
            Skurfer,
            [Description("Stand up paddle")]
            StandPaddle,
            [Description("Windsurf")]
            Windsurf,
            [Description("Boia Donut")]
            BoiaDonut,
            [Description("Bicicleta")]
            Bicicleta,
        }

        public enum kdTripulação
        {
            [Description("Marinheiro")]
            Marinheiro,
            [Description("Segunda comissária de bordo")]
            SegundaComissariaBordo,
            [Description("Primeiro oficial")]
            PrimeiroOficial,
            [Description("Terceira comissária de bordo")]
            TerceiraComissariaBordo,
            [Description("Segundo oficial")]
            SegundoOficial,
            [Description("Engenheiro chefe")]
            EngenheiroChefe,
            [Description("Chefe")]
            Chefe,
            [Description("Auxiliar de marinheiro")]
            AuxiliarMarinheiro,
            [Description("Comissária de bordo chefe")]
            ComissáriaBordoChefe,
            [Description("Segundo auxiliar de marinheiro")]
            SegundoAuxiliarMarinheiro
        }

        public enum kdIdiomasFalado
        {
            [Description("Português")]
            Portugues,
            [Description("Croata")]
            Croata,
            [Description("Inglês")]
            Ingles,
            [Description("Russo")]
            Russo,
            [Description("Espanhol")]
            Espanhol,
            [Description("Chinês")]
            Chines,
            [Description("Francês")]
            Frances,
            [Description("Grego")]
            Grego,
            [Description("Italiano")]
            Italiano,
            [Description("Árabe")]
            Arabe,
            [Description("Alemão")]
            Alemao,
            [Description("Turco")]
            Turco,
            [Description("Holandês")]
            Holandes
        }
    }
}

