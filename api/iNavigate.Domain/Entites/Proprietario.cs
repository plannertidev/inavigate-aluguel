﻿using iNavigate.Domain.Core;
using System;

namespace iNavigate.Domain.Entites
{
   public class Proprietario : AuditableEntity
    {
        public Guid ProprietarioId { get; set; }
        public string Email { get; set; }
        public string Nome { get; set; }
        public string Senha { get; set; }
        public string Celular { get; set; }
    }
}
