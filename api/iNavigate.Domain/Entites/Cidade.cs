﻿using System;

namespace iNavigate.Domain.Entites
{
    public class Cidade
    {
        public Guid CidadeId { get; set; }
        public Guid EstadoId { get; set;}
        public string Nome { get; set; }

        public string Creator { get; set; }
        public DateTime? Created { get; set; }
        public string Changer { get; set; }
        public DateTime? Changed { get; set; }
    }
}
