﻿using System;

namespace iNavigate.Domain.Entites
{
    public class Marca
    {
        public Guid MarcaId { get; set; }
        public string Descricao { get; set; }
        public string Creator { get; set; }
        public DateTime? Created { get; set; }
        public string Changer { get; set; }
        public DateTime? Changed { get; set; }
    }
}
