﻿using iNavigate.Domain.Core;
using System;

namespace iNavigate.Domain.Entites
{
    public class TabelaPreco: AuditableEntity
    {
        public Guid TabelaPrecoId { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fim { get; set; }
        public decimal ValorMeioPeriodo { get; set; }
        public decimal ValorDiaria { get; set; }
    }
}
