﻿using System;

namespace iNavigate.Domain.Entites
{
    public class ModeloEmbarcacaoPropriedade
    {
        public Guid ModeloEmbarcacaoPropriedadeId { get; set; }
        public string Descricao { get; set; }
    }
}
