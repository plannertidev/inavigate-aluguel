﻿using iNavigate.Domain.Core;
using System;

namespace iNavigate.Domain.Entites
{
    public class Disponibilidade : AuditableEntity
    {
        public Guid DisponibilidadeId { get; set; }
        public Guid BarcoId { get; set; }
        public string Cor { get; set; }
        public string Nome { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fim { get; set; }
        public decimal MinimoDias { get; set; }
        public decimal ValorMeioDia { get; set; }
        public decimal ValorDiaInteiro { get; set; }
        public decimal ValorSemana { get; set; }
    }
}
