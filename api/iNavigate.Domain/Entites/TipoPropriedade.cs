﻿using System;

namespace iNavigate.Domain.Entites
{
    public class TipoPropriedade
    {
        public Guid TipoPropriedadeId { get; set; }
        public string Descricao { get; set; }
    }
}
