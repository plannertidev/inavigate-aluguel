﻿using iNavigate.Domain.Core;
using System;
using System.ComponentModel;

namespace iNavigate.Domain.Entites.visitante
{
    public class Mensagem : AuditableEntity
    {
        public Guid MensagemId { get; set; }
        public DateTime DataPretendida { get; set; }
        public decimal QuantidadeDias { get; set; }
        public int IncluirMarinheiro { get; set; }
        public decimal NumeroPassageiros { get; set; }
        public string RemetenteNome { get; set; }
        public string RemetenteEmail { get; set; }
        public string RemetenteTelefone { get; set; }
        public string Assunto { get; set; }
        public int Status { get; set; }


        public enum kdIncluirMarinheiro
        {
            [Description("Sim")]
            Sim = 1,
            [Description("Não")]
            Não = 0
        }
    }
}
