﻿using iNavigate.Domain.Core;
using System;
using System.ComponentModel;
using System.Reflection;

namespace iNavigate.Domain.Entites.visitante
{
    public class Reserva : AuditableEntity
    {
        public Guid ReservaId { get; set; }
        public Guid ClienteId { get; set; }
        public Guid BarcoId { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fim { get; set; }
        public int Tipo { get; set; }
        public int Status { get; set; }

        public enum KdTipo
        {
            [Description("Reserva")]
            Reserva = 0,
            [Description("Bloqueio")]
            Bloqueio = 1
        }

        public enum KdStatus
        {
            [Description("Pendente")]
            Reserva = 0,
            [Description("Confirmada")]
            Confirmada = 1,
            [Description("Cancelada")]
            Cancelada = 2
        }

        public static string GetDescription(System.Enum en)
        {
            Type type = en.GetType();



            MemberInfo[] memInfo = type.GetMember(en.ToString());



            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);



                if (attrs != null && attrs.Length > 0)
                {
                    return ((DescriptionAttribute)attrs[0]).Description;
                }
            }



            return en.ToString();
        }
    }
    

}