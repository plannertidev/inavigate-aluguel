﻿using iNavigate.Domain.Core;
using System;

namespace iNavigate.Domain.Entites.visitante
{
    public  class VisitantePoliticaPrivacidade : AuditableEntity
    {
        public Guid VisitantePoliticaPrivacidadeId { get; set; }
        public string Email { get; set; }
        public int AceitouTermo { get; set; }
        public int PermiteEnvioInformacoes { get; set; }
    }
}
