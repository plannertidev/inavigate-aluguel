﻿using iNavigate.Domain.Core;
using System;

namespace iNavigate.Domain.Entites.visitante
{
    public class Cliente: AuditableEntity
    {
        public Guid ClienteId { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }

    }
}
