﻿using System;

namespace iNavigate.Domain.Entites
{
    public class Anuncio
    {
        public Guid AnuncioId { get; set; }
        public Guid ProprietarioId { get; set; }
        public Guid PropriedadeId { get; set; }
        public string TipoAnuncio { get; set; }
        public string Valor { get; set; }
        public DateTime? Data { get; set; }

        public string Localizacao { get; set; }
        public string Cidade { get; set; }
        public string Fabricante { get; set; }
        public string Modelo { get; set; }
        public string Combustivel { get; set; }
        public string KmBarco { get; set; }
        public string AluguelBarco { get; set; }
        public string Endereco { get; set; }
        public string Nome { get; set; }
        public string Sobrenome { get; set; }
        public string Cpf { get; set; }
        public string Estado { get; set; }
        public DateTime? DataNascimento { get; set; }
        public string Sexo { get; set; }
        public string Cep { get; set; }
        public string TelefonePrincipal { get; set; }
        public string TelefoneSecundario { get; set; }

        public string Creator { get; set; }
        public DateTime? Created { get; set; }
        public string Changer { get; set; }
        public DateTime? Changed { get; set; }


        public enum KDTipoAnuncio
        {

        }
    }
}
