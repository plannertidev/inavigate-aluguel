﻿using iNavigate.Domain.Core;
using iNavigate.Domain.Entites;

namespace iNavigate.Domain.Interfaces
{
    public interface IBarcoRepository : IRepository<Barco>
    {
    }
}
