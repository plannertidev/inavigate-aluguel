﻿using iNavigate.Domain.Core;
using iNavigate.Domain.Entites.visitante;

namespace iNavigate.Domain.Interfaces.visitante
{
    public interface IClienteRepository : IRepository<Cliente>
    {
    }
}