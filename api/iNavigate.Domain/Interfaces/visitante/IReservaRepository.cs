﻿using iNavigate.Domain.Core;
using iNavigate.Domain.Entites.visitante;

namespace iNavigate.Domain.Interfaces.visitante
{
    public  interface IReservaRepository : IRepository<Reserva>
    {
    }
}