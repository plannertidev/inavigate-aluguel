﻿using System;

namespace iNavigate.Domain.Core
{
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        void Add(TEntity obj);
        TEntity GetById(Guid id);
        void Update(TEntity obj);
        void Remove(Guid id);
        int SaveChanges();
    }
  
}
