﻿using System;

namespace iNavigate.Domain.Core
{
    public abstract class AuditableEntity
    {
        public string CreatedBy { get; set; }

        public DateTime Created { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime ? LastModified { get; set; }
    }
}
