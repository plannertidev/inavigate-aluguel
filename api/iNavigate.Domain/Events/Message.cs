﻿using MediatR;
using System;

namespace iNavigate.Domain.Events
{
    public class Message : IRequest<bool>
    {
        public string MessageType { get; private set; }
        public Guid AggregateId { get; private set; }

        protected Message()
        {
            MessageType = GetType().Name;
        }
    }
}
