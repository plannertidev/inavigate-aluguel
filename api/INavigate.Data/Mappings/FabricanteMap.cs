﻿using iNavigate.Domain.Entites;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace INavigate.Data.Mappings
{
    public class FabricanteMap : IEntityTypeConfiguration<Fabricante>
    {
        public void Configure(EntityTypeBuilder<Fabricante> builder)
        {
            builder.ToTable("FABRICANTE");
            builder.HasKey(x => x.FabricanteId);
            builder.Property(c => c.FabricanteId).HasColumnName("FabricanteId");
        }

    }
}
