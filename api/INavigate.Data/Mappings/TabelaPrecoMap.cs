﻿using iNavigate.Domain.Entites;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace INavigate.Data.Mappings
{
    public class TabelaPrecoMap : IEntityTypeConfiguration<TabelaPreco>
    {
        public void Configure(EntityTypeBuilder<TabelaPreco> builder)
        {
            builder.ToTable("TABELAPRECO");
            builder.HasKey(x => x.TabelaPrecoId);
            builder.Property(c => c.TabelaPrecoId).HasColumnName("TabelaPrecoId");
        }
    }
}
