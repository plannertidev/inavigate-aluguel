﻿using iNavigate.Domain.Entites;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace INavigate.Data.Mappings
{
    public class EstadoMap : IEntityTypeConfiguration<Estado>
    {
        public void Configure(EntityTypeBuilder<Estado> builder)
        {
            builder.ToTable("ESTADO");
            builder.HasKey(x => x.EstadoId);
            builder.Property(c => c.EstadoId).HasColumnName("EstadoId");
        }

    }
}
