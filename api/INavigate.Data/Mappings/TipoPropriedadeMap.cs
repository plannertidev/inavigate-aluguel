﻿using iNavigate.Domain.Entites;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace INavigate.Data.Mappings
{
    public class TipoPropriedadeMap : IEntityTypeConfiguration<TipoPropriedade>
    {
        public void Configure(EntityTypeBuilder<TipoPropriedade> builder)
        {
            builder.ToTable("TIPOPROPRIEDADE");
            builder.HasKey(x => x.TipoPropriedadeId);
            builder.Property(c => c.TipoPropriedadeId).HasColumnName("TipoPropriedadeId");
        }
    }
}
