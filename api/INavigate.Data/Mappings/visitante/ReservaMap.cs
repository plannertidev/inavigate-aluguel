﻿using iNavigate.Domain.Entites.visitante;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace INavigate.Data.Mappings.visitante
{
    public class ReservaMap : IEntityTypeConfiguration<Reserva>
    {
        public void Configure(EntityTypeBuilder<Reserva> builder)
        {
            builder.ToTable("RESERVA");
            builder.HasKey(x => x.ReservaId);
            builder.Property(c => c.ReservaId).HasColumnName("ReservaId");
        }
    }
}