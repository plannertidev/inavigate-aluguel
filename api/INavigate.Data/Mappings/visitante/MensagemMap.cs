﻿using iNavigate.Domain.Entites.visitante;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace INavigate.Data.Mappings.visitante
{
    public class MensagemMap : IEntityTypeConfiguration<Mensagem>
    {
        public void Configure(EntityTypeBuilder<Mensagem> builder)
        {
            builder.ToTable("MENSAGEM");
            builder.HasKey(x => x.MensagemId);
            builder.Property(c => c.MensagemId).HasColumnName("MensagemId");
        }

    }
}
