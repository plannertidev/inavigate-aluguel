﻿using iNavigate.Domain.Entites.visitante;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace INavigate.Data.Mappings.visitante
{
    public class VisitantePoliticaPrivacidadeMap : IEntityTypeConfiguration<VisitantePoliticaPrivacidade>
    {
        public void Configure(EntityTypeBuilder<VisitantePoliticaPrivacidade> builder)
        {
            builder.ToTable("VISITANTEPOLITICAPRIVACIDADE");
            builder.HasKey(x => x.VisitantePoliticaPrivacidadeId);
            builder.Property(c => c.VisitantePoliticaPrivacidadeId).HasColumnName("VisitantePoliticaPrivacidadeId");
        }

    }
    
}
