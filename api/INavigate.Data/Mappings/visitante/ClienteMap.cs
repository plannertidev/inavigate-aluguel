﻿using iNavigate.Domain.Entites.visitante;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace INavigate.Data.Mappings.visitante
{
    public class ClienteMap : IEntityTypeConfiguration<Cliente>
    {
        public void Configure(EntityTypeBuilder<Cliente>builder)
        {
            builder.ToTable("CLIENTE");
            builder.HasKey(x => x.ClienteId);
            builder.Property(c => c.ClienteId).HasColumnName("ClienteId");
        }
    }
}