﻿using iNavigate.Domain.Entites;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace INavigate.Data.Mappings
{
    public class CidadeMap : IEntityTypeConfiguration<Cidade>
    {
        public void Configure(EntityTypeBuilder<Cidade> builder)
        {
            builder.ToTable("CIDADE");
            builder.HasKey(x => x.CidadeId);
            builder.Property(c => c.CidadeId).HasColumnName("CidadeId");
        }

    }
}
