﻿using iNavigate.Domain.Entites;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace INavigate.Data.Mappings
{
    public class BarcoMap : IEntityTypeConfiguration<Barco>
    {
        public void Configure(EntityTypeBuilder<Barco> builder)
        {
            builder.ToTable("BARCO");
            builder.HasKey(x => x.BarcoId);
            builder.Property(c => c.BarcoId).HasColumnName("BarcoId");
        }
    }
}
