﻿using iNavigate.Domain.Entites;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace INavigate.Data.Mappings
{
    public class DisponibilidadeMap : IEntityTypeConfiguration<Disponibilidade>
    {
        public void Configure(EntityTypeBuilder<Disponibilidade> builder)
        {
            builder.ToTable("DISPONIBILIDADE");
            builder.HasKey(x => x.DisponibilidadeId);
            builder.Property(c => c.DisponibilidadeId).HasColumnName("DisponibilidadeId");
        }
    }
}
