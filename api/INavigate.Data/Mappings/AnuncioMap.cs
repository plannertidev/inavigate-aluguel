﻿using iNavigate.Domain.Entites;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace INavigate.Data.Mappings
{
    public class AnuncioMap : IEntityTypeConfiguration<Anuncio>
    {
        public void Configure(EntityTypeBuilder<Anuncio> builder)
        {
            builder.ToTable("ANUNCIO");
            builder.HasKey(x => x.AnuncioId);
            builder.Property(c => c.AnuncioId).HasColumnName("AnuncioId");
        }

    }
}
