﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using INavigate.Data.Core;

namespace INavigate.Data.Repositories
{
    public class DisponibilidadeRepository : Repository<Disponibilidade>, IDisponibilidadeRepository
    {
        public DisponibilidadeRepository(iNavigateContext context)
           : base(context)
        {

        }
    }
}
