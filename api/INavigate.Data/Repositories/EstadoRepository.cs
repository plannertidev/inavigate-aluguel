﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using INavigate.Data.Core;

namespace INavigate.Data.Repositories
{
    public class EstadoRepository : Repository<Estado>, IEstadoRepository
    {
        public EstadoRepository(iNavigateContext context)
            : base(context)
        {

        }
    }
}
