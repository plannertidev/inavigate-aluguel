﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using INavigate.Data.Core;

namespace INavigate.Data.Repositories
{
    public class PropriedadeRepository : Repository<Barco>, IBarcoRepository
    {
        public PropriedadeRepository(iNavigateContext context)
            :base(context)
        {

        }

    }
} 
