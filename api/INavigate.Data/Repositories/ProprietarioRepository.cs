﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using INavigate.Data.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace INavigate.Data.Repositories
{
   public class ProprietarioRepository : Repository<Proprietario>, IProprietarioRepository
    {
        public ProprietarioRepository(iNavigateContext context)
            : base(context)
        {

        }
    }
}
