﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using INavigate.Data.Core;

namespace INavigate.Data.Repositories
{
    public class TipoPropriedadeRepository: Repository<TipoPropriedade>, ITipoPropriedadeRepository
    {
        public TipoPropriedadeRepository(iNavigateContext context)
           : base(context)
        {

        }
    }
}
