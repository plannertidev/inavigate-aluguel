﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using INavigate.Data.Core;

namespace INavigate.Data.Repositories
{
    public class TabelaPrecoRepository: Repository<TabelaPreco>, ITabelaPrecoRepository
    {
        public TabelaPrecoRepository(iNavigateContext context)
           : base(context)
        {

        }
    }
}
