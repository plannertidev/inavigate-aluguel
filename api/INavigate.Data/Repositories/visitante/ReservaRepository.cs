﻿using iNavigate.Domain.Entites.visitante;
using iNavigate.Domain.Interfaces.visitante;
using INavigate.Data.Context;
using INavigate.Data.Core;

namespace INavigate.Data.Repositories.visitante
{
    public class ReservaRepository : Repository<Reserva>, IReservaRepository
    {
        public ReservaRepository(iNavigateContext context)
            : base(context)
        {

        }
    }
}