﻿using iNavigate.Domain.Entites.visitante;
using iNavigate.Domain.Interfaces.visitante;
using INavigate.Data.Context;
using INavigate.Data.Core;

namespace INavigate.Data.Repositories.visitante
{
    public  class ClienteRepository : Repository<Cliente>, IClienteRepository
    {
        public ClienteRepository(iNavigateContext context)
            : base(context)
        {

        }
    }
}
