﻿using iNavigate.Domain.Entites.visitante;
using iNavigate.Domain.Interfaces.visitante;
using INavigate.Data.Context;
using INavigate.Data.Core;

namespace INavigate.Data.Repositories.visitante
{
    public class VisitantePoliticaPrivacidadeRepository : Repository<VisitantePoliticaPrivacidade>, IVisitantePoliticaPrivacidadeRepository
    {
        public VisitantePoliticaPrivacidadeRepository(iNavigateContext context)
            : base(context)
        {

        }
    }
}
