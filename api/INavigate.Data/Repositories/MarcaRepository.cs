﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using INavigate.Data.Core;

namespace INavigate.Data.Repositories
{
    public class MarcaRepository : Repository<Marca>, IMarcaRepository
    {
        public MarcaRepository(iNavigateContext context)
                : base(context)
        {

        }
    }
}
