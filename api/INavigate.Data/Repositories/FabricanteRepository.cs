﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Interfaces;
using INavigate.Data.Context;
using INavigate.Data.Core;

namespace INavigate.Data.Repositories
{
    public class FabricanteRepository : Repository<Fabricante>, IFabricanteRepository
    {
        public FabricanteRepository(iNavigateContext context)
            : base(context)
        {

        }
    }
}