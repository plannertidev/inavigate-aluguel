﻿using iNavigate.Domain.Entites;
using iNavigate.Domain.Entites.visitante;
using INavigate.Data.Mappings;
using Microsoft.EntityFrameworkCore;

namespace INavigate.Data.Context
{
    public class iNavigateContext : DbContext, IDbContext
    {
        public DbSet<Fabricante> Fabricante { get; set; }
        public DbSet<Marca> Marca { get; set; }
        public DbSet<Modelo> Modelo { get; set; }
        public DbSet<Anuncio> Anuncio { get; set; }
        public DbSet<Barco> Barco { get; set; }
        public DbSet<Proprietario> Proprietario { get; set; }
        public DbSet<TipoPropriedade> TipoPropriedade { get; set; }
        public DbSet<Categoria> Categoria { get; set; }
        public DbSet<Estado> Estado { get; set; }
        public DbSet<Cidade> Cidade { get; set; }
        public DbSet<VisitantePoliticaPrivacidade> VisitantePoliticaPrivacidade { get; set; }
        public DbSet<Mensagem> Mensagem { get; set; }
        public DbSet<Cliente> Cliente { get; set; }
        public DbSet<Reserva> Reserva { get; set; }
        public DbSet<TabelaPreco> TabelaPreco { get; set; }
        public DbSet<Disponibilidade> Disponibilidade { get; set; }


        public iNavigateContext(DbContextOptions<iNavigateContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new FabricanteMap());
            modelBuilder.ApplyConfiguration(new AnuncioMap());
            modelBuilder.ApplyConfiguration(new DisponibilidadeMap());
            //modelBuilder.ApplyConfiguration(new PropriedadeMap());

            base.OnModelCreating(modelBuilder);
        }
    }
}
