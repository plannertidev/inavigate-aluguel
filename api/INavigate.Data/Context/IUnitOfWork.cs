﻿using System;

namespace INavigate.Data.Context
{
    public interface IUnitOfWork : IDisposable
    {
        bool Commit();
    }
}
