﻿namespace INavigate.Data.Context
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly iNavigateContext _context;

        public UnitOfWork(iNavigateContext context)
        {
            _context = context;
        }

        public bool Commit()
        {
            return _context.SaveChanges() > 0;
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
