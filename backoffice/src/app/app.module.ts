import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './layout/navigation/navigation/navigation.component';
import { BarcoListComponent } from './parametros/barco/barco-list/barco-list.component';
import { RequestService } from './core/services/request.service';
import { HttpClientModule } from '@angular/common/http';
import { BarcoCreateComponent } from './parametros/barco/barco-create/barco-create.component';
import { BarcoFormComponent } from './parametros/barco/barco-form/barco-form.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BarcoEditComponent } from './parametros/barco/barco-edit/barco-edit.component';
import { LoginComponent } from './processos/login/login/login.component';
import { UserContextService } from './core/services/user-context-service';
import { HomeComponent } from './layout/home/home/home.component';
import { NotificationService } from './core/services/notification.service';
import { CdkStepperModule} from '@angular/cdk/stepper';
import { ImageUploadComponent } from './core/image-upload/image-upload/image-upload.component';
import { StepperComponent } from './layout/stepper/stepper/stepper.component';
import { DashboardComponent } from './processos/dashboard/dashboard/dashboard.component';
import { AutocompleteModule } from './core/autocomplete/autocomplete.module';
import { OverlayModule } from '@angular/cdk/overlay';
import { FilterPipe } from './core/autocomplete/filter.pipe';
import { PainelDeMensagensComponent } from './processos/mensagem/painel-de-mensagens/painel-de-mensagens.component';
import { PainelDeReservaComponent } from './processos/reserva/painel-de-reserva/painel-de-reserva.component';
import { BloqueioBarcoComponent } from './processos/reserva/reservar-barco/reservar-barco.component';
import { PlannerDatePickerModule } from './core/dates/date-picker/planner-date-picker.module';
import { PlannerRangeDatePickerComponent } from './core/dates/planner-range-date-picker/planner-range-date-picker.component';
import { PlannerCalendarComponent } from './core/dates/calendar/planner-calendar/planner-calendar.component';
import { PlannerMonthComponent } from './core/dates/calendar/planner-month/planner-month.component';
import { GerenciarTemporadaComponent } from './processos/gerenciar-temporada/gerenciar-temporada.component';
import { PlannerDatePickerDirective } from './core/dates/date-picker/planner-date-picker-directive';
import { PlannerDatePickerComponent } from './core/dates/date-picker/planner-date-picker.component';
import { TemporadaBarcoComponent } from './processos/temporada-barco/temporada-barco.component';
import { ColorPickerComponent } from './core/color-picker/color-picker.component';
import { RegistrarComponent } from './parametros/proprietario/registrar/registrar.component';
import { TextoAnuncioComponent } from './parametros/barco/barco-texto-anuncio/barco-texto-anuncio.component';

@NgModule({
  declarations: [
    AppComponent,
    FilterPipe,
    NavigationComponent,
    BarcoListComponent,
    BarcoCreateComponent,
    BarcoFormComponent,
    BarcoEditComponent,
    LoginComponent,
    HomeComponent,
    ImageUploadComponent,
    StepperComponent,
    DashboardComponent,
    PainelDeMensagensComponent,
    PainelDeReservaComponent,
    BloqueioBarcoComponent,
    PlannerRangeDatePickerComponent,
    PlannerCalendarComponent,
    PlannerMonthComponent,
    GerenciarTemporadaComponent,
    PlannerDatePickerDirective,
    PlannerDatePickerComponent,
    TemporadaBarcoComponent,
    ColorPickerComponent,
    RegistrarComponent,
    TextoAnuncioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    CdkStepperModule,
    AutocompleteModule, 
    OverlayModule,
    PlannerDatePickerModule,
  ],
  providers: [
    RequestService, 
    NotificationService, 
    UserContextService
  ],
  bootstrap: [AppComponent],
  entryComponents: [PlannerDatePickerComponent],
})
export class AppModule { }
