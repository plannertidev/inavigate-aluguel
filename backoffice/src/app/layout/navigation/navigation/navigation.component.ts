import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faCartPlus, faCoffee, faComment, faHotel, faInbox, faNotesMedical, faShip, faUserCog } from '@fortawesome/free-solid-svg-icons';
import { UserContextService } from 'src/app/core/services/user-context-service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  isLoged = false;

  faCog = faUserCog;
  faShip = faShip;
  faCart = faCartPlus;
  faReservas = faHotel;
  faMensagens = faComment;
  faOpinioes = faInbox;

  constructor(
    private router: Router,
    public userContextService: UserContextService,) { }

  ngOnInit(): void {
  }

  listarBarcos(): void {
    this.router.navigateByUrl('/barco-list');
  }

  showDashboard(): void {
    this.router.navigateByUrl('/dashboard');
  }

  painelMensagens = () => {
    this.router.navigateByUrl('/painel-mensagens');
  }

  painelReservas = () => {
    this.router.navigateByUrl('/painel-reserva');
  }

  criarDisponibilidade = () => {
    this.router.navigateByUrl('/gerenciar-temporada');
  }

  cadastrarBarco = () => {
    this.router.navigateByUrl('/registrar');
  }
}
