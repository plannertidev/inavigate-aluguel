import { OverlayRef } from '@angular/cdk/overlay';
import { AfterViewInit, ElementRef } from '@angular/core';
import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-planner-date-picker',
  templateUrl: './planner-date-picker.component.html',
  styleUrls: ['./planner-date-picker.component.scss']
})
export class PlannerDatePickerComponent implements OnInit, AfterViewInit {

  @ViewChild('datepickerdiv', {static:false}) datePickerDiv: ElementRef;

  public date = moment();
  public daysCollection;
  public dateForm: FormGroup;
  
  public overlayRef: OverlayRef;

  public datePickedSubject:Subject<any> = new Subject();

  constructor(
    private formBuilder: FormBuilder){
    this.initDateForm();
  }

  public ngOnInit(): void {
    this.daysCollection = this.createCalendar(this.date);
  }

  ngAfterViewInit(): void {
  }

  public initDateForm(){
    return this.dateForm = this.formBuilder.group({
      dateFrom:[null, Validators.required],
      dateTo:[null, Validators.required],
    });
  }

  public createCalendar = (month) => {
    let firstDay = moment(month).startOf('M');
    let days = Array.apply(null,{ length: month.daysInMonth()})
      .map (Number.call, Number)
      .map((n) => {
        return moment(firstDay).add(n, 'd');
      })

    for(let n = 0; n < firstDay.weekday();n++){
      days.unshift(null);
    }

    return days;
  }

  public todayCheck(day) {
    if (!day){
      return false;
    }
    return moment().format('L') === day.format('L');
  }

  public nextMonth = () => {
    this.date.add(1, 'M')
    this.daysCollection = this.createCalendar(this.date);
  }

  public previousMonth = () => {
    this.date.subtract(1, 'M')
    this.daysCollection = this.createCalendar(this.date);
  }

  public selectedDate(day) {
    console.log(day);
    let dayFormatted = day.format('DD/MM/YYYY');
    if (this.dateForm.valid) {
      this.dateForm.setValue({ dateFrom: null, dateTo: null})
      return;
    }

    if (!this.dateForm.get('dateFrom').value) {
      this.dateForm.get('dateFrom').patchValue(dayFormatted);
    } else {
      this.dateForm.get('dateTo').patchValue(dayFormatted);
    }

    this.datePickedSubject.next(dayFormatted);
    this.close();
  }

  public isSelected = (day) => {
    if(!day) {
      return false;
    }

    let dateFrom = moment(this.dateForm.value.dateFrom, 'DD/MM/YYYY');
    let dateTo = moment(this.dateForm.value.dateTo, 'DD/MM/YYYY');
    if(this.dateForm.value) {
      return dateFrom.isSameOrBefore(day) && dateTo.isSameOrAfter(day);
    }

    if (this.dateForm.get('dateFrom').valid) {
      return dateFrom.isSame(day);
    }
  }

  close = () => {
    this.overlayRef.detach();
  }
  
  @HostListener('mouseout')
  hide() {  
    console.log("mouseout");
  }

  @HostListener('mouseenter')
  show(){
    console.log("mouseenter");
  }

  onClose() {
    this.overlayRef.detach();
  }
}
