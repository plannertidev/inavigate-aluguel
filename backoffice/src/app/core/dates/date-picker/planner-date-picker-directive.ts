import { OverlayRef, Overlay, OverlayPositionBuilder, OverlayConfig } from "@angular/cdk/overlay";
import { ComponentPortal } from "@angular/cdk/portal";
import { Directive, OnInit, Input, HostListener, ComponentRef, ElementRef } from "@angular/core";
import { FormControl } from "@angular/forms";
import { PlannerDatePickerComponent } from "./planner-date-picker.component";

@Directive({ selector: '[plannerDatePicker]' })
export class PlannerDatePickerDirective implements OnInit {

  //@Input("plannerDatePicker") control: ElementRef;
  @Input("plannerDatePicker") control: FormControl;

  private overlayRef: OverlayRef;

  constructor(private overlayPositionBuilder: OverlayPositionBuilder,
    private elementRef: ElementRef,
    private overlay: Overlay) {}
    
  ngOnInit(): void {
    const positionStrategy = this.overlayPositionBuilder
      .flexibleConnectedTo(this.elementRef)
      .withPositions([{
        originX: 'start',
        originY: 'bottom',
        overlayX: 'start',
        overlayY: 'top',
      }]);

    let config = new OverlayConfig({positionStrategy: positionStrategy, hasBackdrop: true, backdropClass: 'cdk-overlay-transparent-backdrop'});      
    this.overlayRef = this.overlay.create(config);

    this.overlayRef.backdropClick().subscribe( () => { this.overlayRef.detach();} );
  }

  @HostListener('focus')
  show() {
    // Create tooltip portal
    const tooltipPortal = new ComponentPortal(PlannerDatePickerComponent);

    // Attach tooltip portal to overlay
    const tooltipRef: ComponentRef<PlannerDatePickerComponent> = this.overlayRef.attach(tooltipPortal);
      
    tooltipRef.instance.overlayRef = this.overlayRef;

    tooltipRef.instance.datePickedSubject.subscribe(data => {
      this.control.setValue(data);
      this.elementRef.nativeElement.value = data;
    });
   }

  @HostListener('click')
  hide() {  
  }



}