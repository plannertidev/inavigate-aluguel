import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlannerRangeDatePickerComponent } from './planner-range-date-picker.component';

describe('PlannerRangeDatePickerComponent', () => {
  let component: PlannerRangeDatePickerComponent;
  let fixture: ComponentFixture<PlannerRangeDatePickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlannerRangeDatePickerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlannerRangeDatePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
