import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlannerMonthComponent } from './planner-month.component';

describe('PlannerMonthComponent', () => {
  let component: PlannerMonthComponent;
  let fixture: ComponentFixture<PlannerMonthComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlannerMonthComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlannerMonthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
