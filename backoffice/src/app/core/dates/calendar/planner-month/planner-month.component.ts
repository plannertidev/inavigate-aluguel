import { Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';
import { TemporadaDataService } from 'src/app/processos/temporada-barco/temporada.dataservice';
import { ReservaDataService } from 'src/app/processos/reserva/reserva.dataservice';
import { DateHelper } from '../../date-helper';

@Component({
  selector: 'app-planner-month',
  templateUrl: './planner-month.component.html',
  styleUrls: ['./planner-month.component.scss'],
  providers: [TemporadaDataService, ReservaDataService]
})
export class PlannerMonthComponent implements OnInit {

  @Input() public barcoId:string = "";
  @Input() public year:string = "2021"; //DateHelper.year(moment()); 
  @Input() public month:string = "04";//DateHelper.month(moment());  
  
  public currentDate:any;

  public daysCollection1;
  
  constructor(
    private disponibilidadeDataService: TemporadaDataService) { 
  }

  ngOnInit(): void {
    this.currentDate = moment(this.year + "-" + this.month +  "-" + "01", 'YYYY/MM/DD')
    this.daysCollection1 = DateHelper.createCalendar(this.currentDate);
  }

  clickDay = (day:any) => {}

  displayDayV2 = (day:any) => {
    return DateHelper.displayDayV2(day);
  }
}
