import { Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';
import { DateHelper } from '../../date-helper';

@Component({
  selector: 'app-planner-calendar',
  templateUrl: './planner-calendar.component.html',
  styleUrls: ['./planner-calendar.component.scss']
})
export class PlannerCalendarComponent implements OnInit {

  @Input() barcoId: string;

  constructor() { }

  ngOnInit(): void {
  }

  currentYear = () => {
    return DateHelper.year(moment());
  }
}
