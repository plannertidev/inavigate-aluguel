import * as moment from "moment";

export class DateHelper {

  private static locksCollection = [];
  private static seasonCollection = [];

  public static createCalendar = (month) => {
    let firstDay = moment(month).startOf('M');
    let days = Array.apply(null, { length: month.daysInMonth() })
      .map(Number.call, Number)
      .map((n) => {
        return moment(firstDay).add(n, 'd');
      })

    for (let n = 0; n < firstDay.weekday(); n++) {
     days.unshift(null);
    }

    return days;
  }

  public static day = (date) => {
    return moment(date).format("D");
  }

  public static month = (date) => {
    return moment(date).format("M");
  }

  public static year = (date) => {
    return moment(date).format("YYYY");
  }

  public static displayDayV2 = (day) => {
    let result = "";

    if (DateHelper.available(day)) {
      result = "available ";
    }

    if (DateHelper.isLocked(day)) {
      result = "blocked ";
    } else {

      if (day === null) {
        result = "inactive ";
      }
    }
    return result;
  }

  public todayCheck(day) {
    if (!day) {
      return false;
    }
    return moment().format('L') === day.format('L');
  }

  private static isLocked(day) {

    let result = false;

    this.locksCollection.forEach(function (entry) {

      if (entry.start.isSameOrBefore(day) && entry.end.isSameOrAfter(day)) {
        result = true;
      }
    });

    return result;
  }

  private static available(day) {

    let result = false;

    if (day) {
      this.seasonCollection.forEach(function (entry) {

        const inicio = moment(entry.inicio, 'YYYY-MM-DD');  //Date.parse(entry.inicio);
        const fim = moment(entry.fim, 'YYYY-MM-DD');  //Date.parse(entry.fim);
  
        if (day >= inicio && day <= fim) {
          result = true;
        }
      });
    }

    return result;
  }

  public static addSeason(disponibiliade: any) {
    DateHelper.seasonCollection.push(disponibiliade);
  }

  public static clearSeasons() { DateHelper.seasonCollection = []}
}