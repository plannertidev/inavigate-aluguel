export class Helper {

    static isEmptyOrNull(object:any) {
        return object === null || object === undefined || object === '';
    }
}