import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-color-picker',
  templateUrl: './color-picker.component.html',
  styleUrls: ['./color-picker.component.scss']
})
export class ColorPickerComponent implements OnInit {

  selectedColor = '';
  colors:any;

  public colorPickedSubject: Subject<any> = new Subject();

  constructor() { }

  ngOnInit(): void {
    this.colors = [
      {
        name: 'yellow',
        value: '#ffff00'
      },
      {
        name: 'red',
        value: '#ff3300'
      },
      {
        name: 'blue',
        value: '#0000ff'
      },
      {
        name: 'green',
        value: '#00FF00'
      },
      {
        name: 'white',
        value: '#FFF'
      },
      {
        name: 'black',
        value: '#000'
      }
    ];
  }

  onChange(value){
    this.selectedColor = value;
    this.colorPickedSubject.next(this.selectedColor);
  }
}
