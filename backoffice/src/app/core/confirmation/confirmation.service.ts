import { Injectable, ComponentRef, ComponentFactoryResolver, ApplicationRef, Injector, EmbeddedViewRef } from "@angular/core";
import { Observable } from "rxjs";
import { ConfirmationComponent } from "./confirmation.component";
import { DialogModule } from "./dialog.module";

@Injectable({
    providedIn: DialogModule,
  })
  export class DialogService {
    dialogComponentRef: ComponentRef<ConfirmationComponent>
  
    constructor(
      private componentFactoryResolver: ComponentFactoryResolver,
      private appRef: ApplicationRef,
      private injector: Injector
    ) {}
  
    appendDialogComponentToBody(message: string) {
        const componentFactory = this.componentFactoryResolver.resolveComponentFactory(ConfirmationComponent);
        const componentRef = componentFactory.create(this.injector);
        this.appRef.attachView(componentRef.hostView);

        const domElem = (componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
        document.body.appendChild(domElem);

        this.dialogComponentRef = componentRef;
        this.dialogComponentRef.instance.message = message;
    }

    private removeDialogComponentFromBody() {
        this.appRef.detachView(this.dialogComponentRef.hostView);
        this.dialogComponentRef.destroy();
      }    

      public open(message: string) {
        this.appendDialogComponentToBody(message);
    }

    public afterClosed(): Observable<boolean>  {
      return this.dialogComponentRef.instance.observable;
    }    
  }