import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {
  
  @ViewChild('modaldiv', { static: false }) modaldiv: ElementRef;

  public message:string;
  public observable: BehaviorSubject<boolean>;

  constructor(
    private renderer: Renderer2) { 
      this.observable = new BehaviorSubject<boolean>(false);
    }

  ngOnInit(): void {
  }

  public afterClosed(value: boolean): Observable<boolean>  {
    this.renderer.removeClass(this.modaldiv.nativeElement,"is-active");
    this.observable.next(value);
    return this.observable;
  }
}
