import { Component, Input, OnInit } from "@angular/core";
import { UploaderService } from "./image-service";
import {
  faUpload
} from '@fortawesome/free-solid-svg-icons';
import { ImageDTO } from "./image-dto";

@Component({
  selector: "app-image-upload",
  templateUrl: "./image-upload.component.html",
  styleUrls: ["./image-upload.component.scss"],
  providers: [UploaderService]
})
export class ImageUploadComponent implements OnInit {
  @Input()
  folderName: string;

  progress: number;
  infoMessage: any;
  isUploading: boolean = false;
  file: File;

  upload = faUpload;

  imageCollection: [string | ArrayBuffer];
  fileCollection: [string |ArrayBuffer];

  imageUrl: string | ArrayBuffer =
    "https://bulma.io/images/placeholders/480x480.png";
  fileName: string = "No file selected";

  constructor(private uploader: UploaderService) {}

  ngOnInit() {
    this.uploader.progressSource.subscribe(progress => {
      this.progress = progress;
    });
  }

  onChange(file: File) {
    if (file) {
      this.fileName = file.name;
      this.file = file;

      const reader = new FileReader();
      reader.readAsDataURL(file);

      reader.onload = event => {
        this.imageUrl = reader.result;

        if (this.imageCollection == null) {
          this.imageCollection = [this.imageUrl];
          this.fileCollection = [reader.result]
        } else {
          this.imageCollection.push(this.imageUrl);
          this.fileCollection.push(reader.result);
        }

      };
    }
  }

  teste = (parm) => {}

  onUpload() {

    this.fileCollection.forEach( (file) => { 

    this.infoMessage = null;
    this.progress = 0;
    this.isUploading = true;
    
    let imageDTO: ImageDTO = new ImageDTO();
    imageDTO.folder = this.folderName;
    imageDTO.file = file;

    this.uploader.uploadImage(imageDTO).subscribe(message => {
      this.isUploading = false;
      this.infoMessage = message;
    });

    } );
  }
}