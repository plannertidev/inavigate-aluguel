export class ImageDTO {
    folder: string;
    fileName: string;
    file: string | ArrayBuffer;
}