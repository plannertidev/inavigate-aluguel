import { Injectable } from "@angular/core";

@Injectable()
export class UserContextService {
    public _isLoged = false;

    private _userId: string;

    public login = () => {this._isLoged = true}
    public logoff = () => {this._isLoged = false}
    public isLoged = () => {return this._isLoged}
    
    public get = () => {return this._userId}
    public set = (userId:string) => {this._userId = userId}
}
