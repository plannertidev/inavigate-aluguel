export class DisponibilidadeDTO{
    public disponibilidadeId:string;
    public barcoId: string;
    public cor : string;
    public nome: string;
    public inicio: Date;
    public fim: Date;
    public minimoDias: number;
    public valorMeioDia: number;
    public valorDiaInteiro: number;
    public  valorSemana: number;
}