import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { RequestService } from "src/app/core/services/request.service";
import { ResponseDTO } from "src/app/core/services/response.dto";
import { DisponibilidadeDTO } from "./temporada.dto";

@Injectable()
export class TemporadaDataService {
    constructor(private requestService: RequestService) {
    }

    public getAll(): Observable<ResponseDTO> {
        return this.requestService.getApi('api/disponibilidade/getAll');
    }

    public getDisponibilidadePorBarco(barcoId: string): Observable<ResponseDTO> {
        const params: any = [{ key: "parametroJson", value: barcoId }];
        return this.requestService.getApi("api/disponibilidade/getDisponibilidadePorBarco", params);
    }

    public getById(barcoId: string): Observable<ResponseDTO> {
        const params: any = [{ key: "parametroJson", value: barcoId }];
        return this.requestService.getApi("api/disponibilidade/getById", params);
    }

    public create(disponibilidadeDTO: DisponibilidadeDTO): Observable<ResponseDTO> {
        return this.requestService.postApi("api/disponibilidade/create", disponibilidadeDTO);
    }

    public update(disponibilidadeDTO: DisponibilidadeDTO): Observable<ResponseDTO> {
        return this.requestService.postApi("api/disponibilidade/update", disponibilidadeDTO);
    }

    public delete(disponibilidadeDTO: DisponibilidadeDTO): Observable<ResponseDTO> {
        return this.requestService.postApi("api/disponibilidade/delete", disponibilidadeDTO);
    }

}
