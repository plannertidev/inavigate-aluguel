import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { TemporadaDataService } from './temporada.dataservice';
import { DisponibilidadeDTO } from './temporada.dto';
import { PlannerRangeDatePickerComponent } from 'src/app/core/dates/planner-range-date-picker/planner-range-date-picker.component';
import { ResponseDTO } from 'src/app/core/services/response.dto';
import { NotificationService } from 'src/app/core/services/notification.service';
import { GridService } from 'src/app/core/services/grid.service';
import { ColorPickerComponent } from 'src/app/core/color-picker/color-picker.component';

@Component({
  selector: 'app-temporada-barco',
  templateUrl: './temporada-barco.component.html',
  styleUrls: ['./temporada-barco.component.scss'],
  providers: [TemporadaDataService, NotificationService, GridService, PlannerRangeDatePickerComponent]
})
export class TemporadaBarcoComponent implements OnInit {
  @Input() barcoId: string;
  @ViewChild(ColorPickerComponent, { static: true }) public colorPickerComponent: ColorPickerComponent;
  @ViewChild(PlannerRangeDatePickerComponent, { static: true }) public plannerRangeDatePickerComponent: PlannerRangeDatePickerComponent;
  public dadosTemporadaForm: FormGroup;
  public controls: any;
  public dadosInformados = [];

  disponibilidade_validation_messages = {
    'valor_dia': [
      { type: 'required', message: 'Informe o valor do dia inteiro.' }
    ],
    'valor_semana': [
      { type: 'required', message: 'Informe o valor da Semana.' }
    ],
    'cor': [
      { type: 'required', message: 'Informe a cor.' }
    ],
    'nome': [
      { type: 'required', message: 'Informe o nome da temporada.' },
      { type: 'maxlength', message: 'Nome não pode ter mais do que 50 caracteres' }
    ],
    'inicio': [
      { type: 'required', message: 'Informe a data inicial.' }
    ],
    'fim': [
      { type: 'required', message: 'Informe a data final.' }
    ],
    'min_dias': [
      { type: 'required', message: 'Informe o mínimo de dias.' }
    ],
  }
  constructor(
    private formBuilder: FormBuilder,
    private notificationService: NotificationService,
    public gridService: GridService<DisponibilidadeDTO>,
    public testeLimpar: PlannerRangeDatePickerComponent,
    private disponibilidadeDataService: TemporadaDataService) {

    this.dadosTemporadaForm = this.formBuilder.group({
      cor: ['', Validators.required],
      nome: ['', Validators.required],
      inicio: ['', Validators.required],
      fim: ['', Validators.required],
      min_dias: ['', Validators.required],
      valor_meio_dia: ['', Validators.required],
      valor_dia: ['', Validators.required],
      valor_semana: ['', Validators.required]
    });

    this.controls = {
      cor: this.dadosTemporadaForm.get('cor'),
      nome: this.dadosTemporadaForm.get('nome'),
      inicio: this.dadosTemporadaForm.get('inicio'),
      fim: this.dadosTemporadaForm.get('fim'),
      min_dias: this.dadosTemporadaForm.get('min_dias'),
      valor_meio_dia: this.dadosTemporadaForm.get('valor_meio_dia'),
      valor_dia: this.dadosTemporadaForm.get('valor_dia'),
      valor_semana: this.dadosTemporadaForm.get('valor_semana')
    }
  }

  ngOnInit(): void {
    this.colorPickerComponent.colorPickedSubject.subscribe(color => {
      this.controls.cor.setValue(color);
    })

    this.disponibilidadeDataService.getDisponibilidadePorBarco(this.barcoId).subscribe(response => this.atualizarGrid(response.data));
  }

  atualizarGrid(data) {
    this.gridService.setDataSource(data);
  }

  adicionarTemporada(controls) {

    if (this.dataEntryIsValid()) {
      let disponibilidadeDTO: DisponibilidadeDTO = new DisponibilidadeDTO();
      disponibilidadeDTO = this.maptoDTO(controls);

      this.disponibilidadeDataService.create(disponibilidadeDTO).subscribe(
        response => this.createCallbackSuccess(response), error => this.createCallbackError(error));
    }

    this.testeLimpar.limparDatas();
  }

  excluirTemporada(data) {
    let disponibilidadeDTO: DisponibilidadeDTO = new DisponibilidadeDTO();
    disponibilidadeDTO.disponibilidadeId = data.disponibilidadeId;
    this.disponibilidadeDataService.delete(disponibilidadeDTO).subscribe(
      response => this.deleteCallbackSuccess(response), error => this.deleteCallbackError(error)
    );
  }

  maptoDTO(controls): DisponibilidadeDTO {
    let disponibilidadeDTO: DisponibilidadeDTO = new DisponibilidadeDTO();
    disponibilidadeDTO.barcoId = this.barcoId;
    disponibilidadeDTO.cor = this.controls.cor.value;
    disponibilidadeDTO.nome = controls.nome.value;
    disponibilidadeDTO.inicio = new Date(controls.inicio.value.substring(6, 10), Number.parseInt(controls.inicio.value.substring(3, 5)) - 1, controls.inicio.value.substring(0, 2));
    disponibilidadeDTO.fim = new Date(controls.fim.value.substring(6, 10), Number.parseInt(controls.fim.value.substring(3, 5)) - 1, controls.fim.value.substring(0, 2));
    disponibilidadeDTO.minimoDias = controls.min_dias.value;
    disponibilidadeDTO.valorMeioDia = controls.valor_meio_dia.value;
    disponibilidadeDTO.valorDiaInteiro = controls.valor_dia.value;
    disponibilidadeDTO.valorSemana = controls.valor_semana.value;

    return disponibilidadeDTO;
  }

  private createCallbackSuccess(response: ResponseDTO) {
    if (response.data == null) {
      this.notificationService.error('Período já reservado!');
    } else {
      this.notificationService.success('Disponibilidade incluida com sucesso!');
      this.disponibilidadeDataService.getDisponibilidadePorBarco(response.data.barcoId).subscribe(response => this.atualizarGrid(response.data));
    }
  }

  private createCallbackError(error): any {
    this.notificationService.error(error.message);
  }

  private deleteCallbackSuccess(response: ResponseDTO) {
    this.disponibilidadeDataService.getDisponibilidadePorBarco(this.barcoId).subscribe(response => this.atualizarGrid(response.data));
    this.notificationService.success('Disponibilidade excluída com sucesso!');
  }

  private deleteCallbackError(error): any {
    this.notificationService.error(error.message);
  }

  private dataEntryIsValid(): boolean {

    let result = true;
    const dataInicio = new Date(this.controls.inicio.value.substring(6, 10), Number.parseInt(this.controls.inicio.value.substring(3, 5)) - 1, this.controls.inicio.value.substring(0, 2));
    const dataFim = new Date(this.controls.fim.value.substring(6, 10), Number.parseInt(this.controls.fim.value.substring(3, 5)) - 1, this.controls.fim.value.substring(0, 2));
    if (dataInicio.getTime() > dataFim.getTime()) {
      this.notificationService.error('Informe um periodo válido!');
    }

    return result;
  }
}