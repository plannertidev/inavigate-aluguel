import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TemporadaBarcoComponent } from './temporada-barco.component';

describe('DisponibilidadeBarcoComponent', () => {
  let component: TemporadaBarcoComponent;
  let fixture: ComponentFixture<TemporadaBarcoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TemporadaBarcoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TemporadaBarcoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
