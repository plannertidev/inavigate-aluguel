import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GerenciarTemporadaComponent } from './gerenciar-temporada.component';

describe('CriarDisponibilidadeComponent', () => {
  let component: GerenciarTemporadaComponent;
  let fixture: ComponentFixture<GerenciarTemporadaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GerenciarTemporadaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GerenciarTemporadaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
