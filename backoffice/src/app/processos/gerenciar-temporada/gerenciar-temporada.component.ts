import { Component, Input, OnInit } from '@angular/core';
import { GridService } from 'src/app/core/services/grid.service';
import { ActivatedRoute } from '@angular/router';
import { BarcoDataService } from 'src/app/parametros/barco/barco-dataservice';
import { TemporadaDataService } from '../temporada-barco/temporada.dataservice';
import { DateHelper } from 'src/app/core/dates/date-helper';
import { ReservaDTO } from '../reserva/reserva.dto';
import { ReservaDataService } from '../reserva/reserva.dataservice';


@Component({
  selector: 'app-criar-disponibilidade',
  templateUrl: './gerenciar-temporada.component.html',
  styleUrls: ['./gerenciar-temporada.component.scss'],
  providers: [GridService,BarcoDataService, TemporadaDataService, ReservaDataService ]
})
export class GerenciarTemporadaComponent implements OnInit {

  @Input() barcoId = null;
  public barco: string;

  constructor(
    private _activatedRouted: ActivatedRoute,
    private _barcoDataService : BarcoDataService,
    private _TemporadaDataService: TemporadaDataService,
    private _reservaDataService: ReservaDataService,
    public gridService: GridService<ReservaDTO>) { }

  ngOnInit(): void {
    this._activatedRouted.params.subscribe(params => this.barcoId = (params['id']));
    this._barcoDataService.getById(this.barcoId).subscribe(result => {
      this.GetNome(result.data);
      this.loadSeasons();
    });
    this._reservaDataService.getById(this.barcoId)
    {
      this.loadLocks();
    }
  }
  public GetNome(result)
  {
    this.barco = result.nome;
  }
  
  private loadSeasons() {
    DateHelper.clearSeasons();
    this._TemporadaDataService.getDisponibilidadePorBarco(this.barcoId)
    .subscribe(response =>
      {
        console.log(response)
        response.data.forEach(element => {
          DateHelper.addSeason(element);
        });
      });    
  }

  private loadLocks() {
    DateHelper.clearSeasons();
    this._reservaDataService.getReservaPorBarco(this.barcoId)
    .subscribe(response =>
      {
        console.log(response)
        response.data.forEach(element => {
          DateHelper.addSeason(element);
        });
      });    
  }
}
