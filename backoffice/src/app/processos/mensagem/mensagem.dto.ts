export class MensagemDTO {
    mensagemId: string;
    dataPretendida: Date;
    quantidadeDias:number;
    incluirMarinheiro:boolean;
    numeroDePassageiros:number;
    remetenteNome:string;
    remetenteEmail:string;
    remetenteTelefone:string;
    assunto:string;
    status:number;
    created: Date;
}

