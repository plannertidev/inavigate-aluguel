import { Component, OnInit } from '@angular/core';
import { GridService } from 'src/app/core/services/grid.service';
import { MensagemDataService } from '../mensagem.dataservice';
import { MensagemDTO } from '../mensagem.dto';

@Component({
  selector: 'app-painel-de-mensagens',
  templateUrl: './painel-de-mensagens.component.html',
  styleUrls: ['./painel-de-mensagens.component.scss'],
  providers: [MensagemDataService, GridService ]
})
export class PainelDeMensagensComponent implements OnInit {

  constructor(public gridService: GridService<MensagemDTO>,
              private mensagemDataService: MensagemDataService) { }

  ngOnInit(): void {
    this.mensagemDataService.getAll().subscribe(response => this.atualizarGrid(response.data));
  }

  private atualizarGrid(data) {
    this.gridService.setDataSource(data)
  }

}
