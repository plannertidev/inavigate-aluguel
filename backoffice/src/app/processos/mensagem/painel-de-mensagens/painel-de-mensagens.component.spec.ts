import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PainelDeMensagensComponent } from './painel-de-mensagens.component';

describe('PainelDeMensagensComponent', () => {
  let component: PainelDeMensagensComponent;
  let fixture: ComponentFixture<PainelDeMensagensComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PainelDeMensagensComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PainelDeMensagensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
