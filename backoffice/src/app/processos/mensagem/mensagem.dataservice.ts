import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { RequestService } from "src/app/core/services/request.service";
import { ResponseDTO } from "src/app/core/services/response.dto";
import { MensagemDTO } from "./mensagem.dto";

@Injectable()
export class MensagemDataService {
    constructor(private requestService: RequestService) {
    }

    public getAll(): Observable<ResponseDTO> {
        return this.requestService.getApi("api/mensagem/getAll");
    }

    public getById(mensagemId: string): Observable<ResponseDTO> {
        const params:any = [{key: "parametroJson",value: mensagemId}];
        return this.requestService.getApi("api/mensagem/getById",  params);
    }

    public getByFilter(mensagemDTO: MensagemDTO): Observable<ResponseDTO> {
        const params:any = [{key: "parametroJson", value: JSON.stringify(mensagemDTO)}];
        return this.requestService.getApi("api/mensagem/getByFilter",  params);
    }

    public create(mensagemDTO: MensagemDTO): Observable<ResponseDTO> {
        return this.requestService.postApi("api/mensagem/create",  mensagemDTO);
    }

    public update(mensagemDTO: MensagemDTO): Observable<ResponseDTO> {
        return this.requestService.post("api/mensagem/update", mensagemDTO);
    }

    public delete(mensagemDTO: MensagemDTO): Observable<ResponseDTO> {
        return this.requestService.post("api/mensagem/delete",  mensagemDTO);
    }

    public login(mensagemDTO: MensagemDTO): Observable<ResponseDTO> {
        return this.requestService.postApi("api/account/login",  mensagemDTO);
    }

}