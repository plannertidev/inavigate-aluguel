import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BloqueioBarcoComponent } from './reservar-barco.component';

describe('BloqueioBarcoComponent', () => {
  let component: BloqueioBarcoComponent;
  let fixture: ComponentFixture<BloqueioBarcoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BloqueioBarcoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BloqueioBarcoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
