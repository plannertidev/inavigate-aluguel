import { AfterViewInit } from '@angular/core';
import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { DialogService } from 'src/app/core/confirmation/confirmation.service';
import { PlannerRangeDatePickerComponent } from 'src/app/core/dates/planner-range-date-picker/planner-range-date-picker.component';
import { GridService } from 'src/app/core/services/grid.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { ResponseDTO } from 'src/app/core/services/response.dto';
import { ReservaDataService } from 'src/app/processos/reserva/reserva.dataservice';
import { ReservaDTO } from 'src/app/processos/reserva/reserva.dto';

@Component({
  selector: 'app-reservar-barco',
  templateUrl: './reservar-barco.component.html',
  styleUrls: ['./reservar-barco.component.scss'],
  providers:[GridService, ReservaDataService,DialogService, NotificationService]
})
export class BloqueioBarcoComponent implements OnInit, AfterViewInit {
 public bloqueioForm: FormGroup;
 public controls: any;
 public now = new Date();
 // barcoId : string ;
 public bloqueios = [];
 
 @Input() barcoId;
 @ViewChild(PlannerRangeDatePickerComponent, { static: true }) public plannerRangeDatePickerComponent: PlannerRangeDatePickerComponent;

  constructor(
    private formBuilder: FormBuilder,
    private activatedRouted: ActivatedRoute,
    private reservaDataService: ReservaDataService,
    private notificationService: NotificationService,
    public gridService: GridService<ReservaDTO>) { 

      this.bloqueioForm = this.formBuilder.group({
        barcoId:[''],
        reservaId:[''],
        clienteId: ['', Validators.required],
        inicio:['' ,Validators.required],
        fim:['',Validators.required],
        tipo:[0 ,Validators.required]
      });
        this.controls = {
          reservaId: this.bloqueioForm.get('reservaId'),
          barcoId: this.bloqueioForm.get('barcoId'),
          clienteId: this.bloqueioForm.get('clienteId'),
          inicio: this.bloqueioForm.get('inicio'),
          fim: this.bloqueioForm.get('fim'),
          tipo: this.bloqueioForm.get('tipo')
        }
    }

  ngOnInit(): void {
    this.activatedRouted.params.subscribe(params => this.barcoId = params['id']);
    this.reservaDataService.getReservaPorBarco(this.barcoId).subscribe(response => this.atualizarGrid(response.data));
  }
 ngAfterViewInit(): void {
    // this.plannerRangeDatePickerComponent.date1PickedSubject.subscribe( data => {
      // this.bloqueioForm.get("inicio").patchValue(data);
     //})

   //this.plannerRangeDatePickerComponent.date2PickedSubject.subscribe( data => {
    //   this.bloqueioForm.get("date2").patchValue(data);
     //})
  }


   public bloqueio(element): ReservaDTO{
       let reservaDTO: ReservaDTO = new ReservaDTO();
        reservaDTO.barcoId = this.barcoId;
        reservaDTO.clienteId = "00000000-0000-0000-0000-000000000000";
        reservaDTO.inicio = this.controls.inicio.value;
        reservaDTO.fim = this.controls.fim.value;
        reservaDTO.tipo = 1;
        reservaDTO.status = 1;
      
        this.bloqueioBarco(reservaDTO);
        console.log(reservaDTO);
        return reservaDTO;
     }
     
    public bloqueioBarco(element){
      const dataInicio = element.inicio;
       const dataFim = element.fim;
      if( dataInicio > dataFim)
      {
        this.notificationService.error('Informe uma data valida para fazer a sua reserva!');
        
      }
      else{
              this.reservaDataService.create(element)
                .subscribe(
                  response => { this.createCallback(response),this.mapToArray(element) },
                  error => {
                    this.notificationService.error('Something went wrong.');
                }
                );
      }
    }

    private createCallback(response : ResponseDTO) {
      if(response.data == null)
      {
        this.notificationService.error('Barco já bloqueado nesse mesmo periodo!');
      }
      else
      {
        this.notificationService.success('Barco bloqueado!');
     this.reservaDataService.getReservaPorBarco(this.barcoId).subscribe(response => this.atualizarGrid(response.data));
      }
     
    } 
    public mapToArray(bloqueio){
      this.bloqueios.push({
        barcoId : this.barcoId,
        clienteId : "00000000-0000-0000-0000-000000000000",
        inicio : bloqueio.inicio,
        fim : bloqueio.fim,
        tipo: 1,
        status : 2
      });
    }

    public excluirBloqueio(dado){
      let reservaDTO: ReservaDTO = new ReservaDTO();
      reservaDTO.reservaId = dado.reservaId;
       this.reservaDataService.delete(reservaDTO).subscribe(
        response => this.deleteCallbackSuccess(response), error => this.deleteCallbackError(error)
      );
    }
    private deleteCallbackSuccess(response: ResponseDTO){
      this.notificationService.success('Bloqueio  excluída com sucesso!');
      this.reservaDataService.getReservaPorBarco(this.barcoId).subscribe(response => this.atualizarGrid(response.data));
    }
    
    private deleteCallbackError(error): any{
      this.notificationService.error(error.message);
    }

    private atualizarGrid(data) {
      this.gridService.setDataSource(data)
    }
}