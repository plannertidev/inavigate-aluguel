import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PainelDeReservaComponent } from './painel-de-reserva.component';

describe('PainelDeReservaComponent', () => {
  let component: PainelDeReservaComponent;
  let fixture: ComponentFixture<PainelDeReservaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PainelDeReservaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PainelDeReservaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
