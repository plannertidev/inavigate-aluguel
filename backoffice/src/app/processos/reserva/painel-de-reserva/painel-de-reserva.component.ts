import { Component, OnInit } from '@angular/core';
import { DialogService } from 'src/app/core/confirmation/confirmation.service';
import { GridService } from 'src/app/core/services/grid.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { ReservaDataService } from '../reserva.dataservice';
import { ReservaDTO } from '../reserva.dto';
import { ResponseDTO } from 'src/app/core/services/response.dto';
import { DisponibilidadeDTO } from 'src/app/processos/temporada-barco/temporada.dto';
import { TemporadaDataService } from 'src/app/processos/temporada-barco/temporada.dataservice';
@Component({
  selector: 'app-painel-de-reserva',
  templateUrl: './painel-de-reserva.component.html',
  styleUrls: ['./painel-de-reserva.component.scss'],
  providers: [ReservaDataService, GridService, DialogService, NotificationService,TemporadaDataService]
})
export class PainelDeReservaComponent implements OnInit {
  
  
  constructor(public gridService: GridService<ReservaDTO>,
             private dialogService : DialogService,
             private notificationService: NotificationService,
             private disponibilidadeDataService: TemporadaDataService,
             private reservaDataService: ReservaDataService) { }


  ngOnInit(): void {
    this.reservaDataService.getAll().subscribe(response => this.atualizarGrid(response.data));
  }
  private atualizarGrid(data) {
    this.gridService.setDataSource(data)
  }
  public create(element){
    this.dialogService.open("Confirmar sua Reserva?");
    this.dialogService.afterClosed().subscribe(
      result => {
        if (result){
          //element.tipo = 1;
          element.status = 1;
          this.reservaDataService.create(this.mapToDTOUpdate(element))
            .subscribe(
              () => { this.createCallback();this.createDisponibilidade(element); },
              error => {
                this.notificationService.error('Something went wrong.');
              }
            );
             // console.log(this.mapToDTODisponibilidade(element));
        }
       
      });
  }
  private createCallback() {
    this.reservaDataService.getAll().subscribe(response => this.atualizarGrid(response.data));
    this.notificationService.success('Reserva feita com sucesso!');
  } 
 
  public cancelarReserva(element){
    this.dialogService.open("Cancelar sua Reserva?");
    this.dialogService.afterClosed().subscribe(
      result => {
        if (result){
          element.tipo = 1;
          element.status = 2;
          this.reservaDataService.update(this.mapToDTOUpdate(element))
            .subscribe(
              () => { this.updateCallback(); },
              error => {
                this.notificationService.error('Something went wrong.');
              }
            );

        }
      });
    
  }
  private updateCallback() {
    this.reservaDataService.getAll().subscribe(response => this.atualizarGrid(response.data));
    this.notificationService.success('Reserva cancelada!');
  } 
  private createDisponibilidade(element){
    this.disponibilidadeDataService.create(this.mapToDTODisponibilidade(element))
    .subscribe(
      response => this.createCallbackSuccessDisponibilidade(response), error => this.createCallbackErrorDisponibilidade(error));;
  }

  private mapToDTOUpdate(element): ReservaDTO{
    let reservaDTO: ReservaDTO = new ReservaDTO();
    reservaDTO.reservaId = element.reservaId;
    reservaDTO.barcoId = element.barcoId;
    reservaDTO.clienteId = element.clienteId ="7A029DE9-39AC-4853-8FF5-2450A681A398";
    reservaDTO.inicio = element.inicio;
    reservaDTO.fim = element.fim;
    reservaDTO.tipo = element.tipo;
    reservaDTO.status = element.status;
   
    return reservaDTO;

  }
  private mapToDTODisponibilidade(element): DisponibilidadeDTO{
    let disponibilidadeDTO: DisponibilidadeDTO = new DisponibilidadeDTO();
    disponibilidadeDTO.barcoId = element.barcoId;
    disponibilidadeDTO.inicio = element.inicio;
    disponibilidadeDTO.fim = element.fim;
    console.log(disponibilidadeDTO);
    
    return disponibilidadeDTO;
  }

  private createCallbackSuccessDisponibilidade(response: ResponseDTO){
    this.notificationService.success('Disponibilidade incluida com sucesso!');
  }
  
  private createCallbackErrorDisponibilidade(error): any{
    this.notificationService.error(error.message);
  }
}