import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { RequestService } from "src/app/core/services/request.service";
import { ResponseDTO } from "src/app/core/services/response.dto";
import { ReservaDTO } from "./reserva.dto";

@Injectable()
export class ReservaDataService {
    constructor(private requestService: RequestService) {
    }

    public getAll(): Observable<ResponseDTO> {
        return this.requestService.getApi("api/reserva/getAll");
    }
    public getReservaPorBarco(barcoId: string): Observable<ResponseDTO> {
        const params: any = [{ key: "parametroJson", value: barcoId }];
        return this.requestService.getApi("api/reserva/getReservaPorBarco", params);
    }

    public getById(reservaId: string): Observable<ResponseDTO> {
        const params:any = [{key: "parametroJson",value: reservaId}];
        return this.requestService.getApi("api/reserva/getById",  params);
    }

    public getByFilter(reservaDTO: ReservaDTO): Observable<ResponseDTO> {
        const params:any = [{key: "parametroJson", value: JSON.stringify(reservaDTO)}];
        return this.requestService.getApi("api/reserva/getByFilter",  params);
    }

    public create(reservaDTO: ReservaDTO): Observable<ResponseDTO> {
        return this.requestService.postApi("api/reserva/create",  reservaDTO);
    }

    public update(reservaDTO: ReservaDTO): Observable<ResponseDTO> {
        return this.requestService.postApi("api/reserva/update", reservaDTO);
    }

    public delete(reservaDTO: ReservaDTO): Observable<ResponseDTO> {
        return this.requestService.postApi("api/reserva/delete",  reservaDTO);
    }

    public login(reservaDTO: ReservaDTO): Observable<ResponseDTO> {
        return this.requestService.postApi("api/account/login",  reservaDTO);
    }

}