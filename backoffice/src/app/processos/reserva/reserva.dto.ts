export class ReservaDTO {
    reservaId : string;
    clienteId : string;
    barcoId: string;
    inicio : Date;
    fim : Date;
    tipo : number;
    status: number;
    created: Date;
}