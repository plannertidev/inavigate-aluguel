import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NotificationService } from 'src/app/core/services/notification.service';
import { RequestService } from 'src/app/core/services/request.service';
import { ResponseDTO } from 'src/app/core/services/response.dto';
import { faPhone, faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { ProprietarioDataService } from '../proprietario-dataservice';
import { ProprietarioDTO } from '../proprietario-dto';
import { UserContextService } from 'src/app/core/services/user-context-service';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.scss'],
  providers: [ProprietarioDataService, RequestService ]
})
export class RegistrarComponent implements OnInit {

  faPhone = faPhone;
  faEnvelope = faEnvelope;

  public registerForm: FormGroup;
  controls: any;

  constructor(
    private router: Router, 
    private formBuilder: FormBuilder,
    private proprietarioDataService: ProprietarioDataService,
    private notificationService: NotificationService,
    private userContextService: UserContextService) {
   }

  ngOnInit(): void {

    this.registerForm = this.formBuilder.group({
      nome: ['', Validators.required],
      celular: ['', Validators.required],
      email: ['', Validators.required],
      senha: ['', Validators.required ],
      senhaConfirmacao: ['', Validators.required],
    });

    this.controls = {
      nome: this.registerForm.get('nome'),
      celular: this.registerForm.get('celular'),
      email: this.registerForm.get('email'),
      senha: this.registerForm.get('senha'),
      senhaConfirmacao: this.registerForm.get('senhaConfirmacao')
    }    
  }

  public gotoHome():any{
    this.router.navigateByUrl('/home-component');    
  }

  private mapToDto(controls) : ProprietarioDTO {
    let proprietarioDTO: ProprietarioDTO = new ProprietarioDTO();
    proprietarioDTO.nome = controls.nome.value;
    proprietarioDTO.celular = controls.celular.value;
    proprietarioDTO.email = controls.email.value;
    proprietarioDTO.senha = controls.senha.value;
    return proprietarioDTO;
  }

  public verificarCampos(controls) : boolean{
    
    let camposValidos: boolean = true;

    //Celular

    let celularRegex : RegExp= /^\([0-9]{2}\) [0-9]?[0-9]{4}-[0-9]{4}$/;

    // if (controls.celular.value !== '' && (controls.celular.value.length < 9 || !celularRegex.test(controls.celular.value))) {
    //   camposValidos = false;
    //   this.notificationService.error('Error: Formato de celular invalido! (XX) 9XXXX-XXXX');
    // }     

    //Email

    let emailRegex : RegExp = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

    if (controls.email.value !== '' && (controls.email.value.length <= 5 || !emailRegex.test(controls.email.value))) {
      camposValidos = false;
      this.notificationService.error('Error: Formato de email invalido! ');
    } 

    if (controls.email.value === '') {
      camposValidos = false;
      this.notificationService.error('Error: O email nao pode ficar em branco ');
    } 

    //Senha

    if (controls.senha.value !== '' && (controls.senha.value.length < 3 )) {
      camposValidos = false;
      this.notificationService.error("Error: A senha deve possuir mais de 3 caracteres ");
    } 

    if (controls.senha.value === '') {
      camposValidos = false;
      this.notificationService.error('Error: A senha nao pode ficar em branco ');
    } 

    if (controls.senha.value !== controls.senhaConfirmacao.value) {
      camposValidos = false;
      this.notificationService.error('Error: As senhas devem ser iguais! ');
    }     

    return camposValidos;
  }

  public registrar():any{
    if (this.verificarCampos(this.registerForm.controls)) {
      this.proprietarioDataService.create(this.mapToDto(this.registerForm.controls)).subscribe( 
        response => this.createCallbackSuccess(response), error => this.createCallbackError(error));
    } 
  }
  
  private createCallbackSuccess(response: ResponseDTO){
    this.userContextService.login();
    this.userContextService.set(response.data);
    this.router.navigateByUrl('/home');
  } 
  
  private createCallbackError(error): any{
  }

  public loginProprietario = () =>{}
}
