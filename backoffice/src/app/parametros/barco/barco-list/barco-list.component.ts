import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DialogService } from 'src/app/core/confirmation/confirmation.service';
import { GridService } from 'src/app/core/services/grid.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { UserContextService } from 'src/app/core/services/user-context-service';
import { BarcoDataService } from '../barco-dataservice';
import { BarcoDTO } from '../barco-dto';

@Component({
  selector: 'app-barco-list',
  templateUrl: './barco-list.component.html',
  styleUrls: ['./barco-list.component.scss'],
  providers: [GridService, BarcoDataService, DialogService, NotificationService]
})
export class BarcoListComponent implements OnInit {

  constructor(
    private router: Router,
    public gridService: GridService<BarcoDTO>,
    public barcoDataService: BarcoDataService,
    private dialogService : DialogService,
    private notificationService: NotificationService,
    private userContextService: UserContextService) {}

  ngOnInit(): void {
    this.barcoDataService.getByProprietario(this.userContextService).subscribe(response => this.atualizarGrid(response.data));
  }

  public create() {
    this.router.navigateByUrl('/barco-create');
  }

  public edit(barco){
    this.router.navigate(['/barco-edit', barco.barcoId]);
  }
  
  public bloqueio(barco){
    this.router.navigate(['/bloqueio-barco', barco.barcoId]);
  }

  public delete(element){
    this.dialogService.open("Confirma exclusão do Barco?");
    this.dialogService.afterClosed().subscribe(
      result => {
        if (result){

          this.barcoDataService.delete(element)
            .subscribe(
              () => { this.deleteCallback(); },
              error => {
                this.notificationService.error('Something went wrong.');
              }
            );

        }
      });
  }

  private deleteCallback() {
    this.barcoDataService.getAll().subscribe(response => this.atualizarGrid(response.data));
    this.notificationService.success('Barco excluido com sucesso!');
  } 

  private atualizarGrid(data) {
    this.gridService.setDataSource(data)
  }

  public gerenciarCalendarios(data) {
    this.router.navigate(['/gerenciar-temporada', data.barcoId]);
  }

  public anuncio(data){
    this.router.navigate(['barco-texto-anuncio',data.barcoId]);
  }
}
