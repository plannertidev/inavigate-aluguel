import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TextoAnuncioComponent } from './barco-texto-anuncio.component';

describe('TextoAnuncioComponent', () => {
  let component: TextoAnuncioComponent;
  let fixture: ComponentFixture<TextoAnuncioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TextoAnuncioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TextoAnuncioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
