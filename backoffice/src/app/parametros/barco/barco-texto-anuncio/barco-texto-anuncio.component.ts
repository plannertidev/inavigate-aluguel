import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GridService } from 'src/app/core/services/grid.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { ResponseDTO } from 'src/app/core/services/response.dto';
import { UserContextService } from 'src/app/core/services/user-context-service';
import { BarcoDataService } from '../barco-dataservice';
import { BarcoDTO } from '../barco-dto';

@Component({
  selector: 'app-texto-anuncio',
  templateUrl: './barco-texto-anuncio.component.html',
  styleUrls: ['./barco-texto-anuncio.component.scss'],
  providers:[BarcoDataService,GridService]
})
export class TextoAnuncioComponent implements OnInit {
  public dadosAnunciosForm: FormGroup;
  public controls: any;
  public dadosBarco: BarcoDTO;
  public tituloAnuncio : string;
  public tipoAnuncio : string;

  @Input() barcoId: string;
  public barco: string;
  
  constructor(
    private formBuilder: FormBuilder,
    private barcoDataService: BarcoDataService,
    private router : Router,
    private activatedRouted: ActivatedRoute,
    private notificationService: NotificationService,
    private userContextService: UserContextService,
    public gridService: GridService<BarcoDTO> ) {

      this.dadosAnunciosForm = this.formBuilder.group({
        barcoId:[''],
        tituloAnuncio:['', Validators.required],
        tipoAnuncio: ['', Validators.required]
      });

      this.controls = {
        barcoId: this.dadosAnunciosForm.get('barcoId'),
        tituloAnuncio: this.dadosAnunciosForm.get('tituloAnuncio'),
        tipoAnuncio: this.dadosAnunciosForm.get('tipoAnuncio')
      }
     }

  ngOnInit(): void {
    this.activatedRouted.params.subscribe(params => this.barcoId = params['id']);
    this.barcoDataService.getById(this.barcoId).subscribe(response => this.atualizarGrid(response.data));
  }

  public salvar(): BarcoDTO{
    let barcoDTO: BarcoDTO = new BarcoDTO();
    barcoDTO.barcoId = this.barcoId;
    barcoDTO.tipoAnuncio = this.controls.tipoAnuncio.value;
    barcoDTO.tituloAnuncio = this.controls.tituloAnuncio.value;
    barcoDTO.anoFabricacao = this.dadosBarco.anoFabricacao;
    barcoDTO.nome = this.dadosBarco.nome;
    barcoDTO.categoriaId = this.dadosBarco.categoriaId;
    barcoDTO.fabricanteId = this.dadosBarco.fabricanteId;
    barcoDTO.proprietarioId =  this.userContextService.get();
    barcoDTO.modeloId = this.dadosBarco.modeloId;
    barcoDTO.tipoCombustivel = this.dadosBarco.tipoCombustivel;
    barcoDTO.ultimaReforma  = this.dadosBarco.ultimaReforma;
    barcoDTO.tripulacaoIncluidaNoPreco = this.dadosBarco.tripulacaoIncluidaNoPreco;
    barcoDTO.capacidade = this.dadosBarco.capacidade;
    barcoDTO.capacidadePernoite = this.dadosBarco.capacidadePernoite;
    barcoDTO.numeroDeCabines = this.dadosBarco.numeroDeCabines;
    barcoDTO.numeroDeBanheiros = this.dadosBarco.numeroDeBanheiros;
    barcoDTO.cumprimento  = this.dadosBarco.cumprimento;
    barcoDTO.boca = this.dadosBarco.boca;
    barcoDTO.calado = this.dadosBarco.calado;
    barcoDTO.potencia = this.dadosBarco.potencia;
    barcoDTO.velocidadeCruzeiro = this.dadosBarco.velocidadeCruzeiro;
    barcoDTO.velocidadeMaxima = this.dadosBarco.velocidadeMaxima;
    barcoDTO.consumoMedio = this.dadosBarco.consumoMedio;
    barcoDTO.depositoAguaDoce = this.dadosBarco.depositoAguaDoce;
    barcoDTO.depositoCombustivel = this.dadosBarco.depositoCombustivel;
    barcoDTO.equipamentoConves = this.dadosBarco.equipamentoConves;
    barcoDTO.equipamentoEletronico = this.dadosBarco.equipamentoEletronico;
    barcoDTO.outrosEquipamentos = this.dadosBarco.outrosEquipamentos;
    barcoDTO.entretenimentoAquatico = this.dadosBarco.entretenimentoAquatico;
    barcoDTO.tripulacao = this.dadosBarco.tripulacao;
    barcoDTO.idiomasFalado = this.dadosBarco.idiomasFalado;
   
    this.barcoDataService.update(barcoDTO).subscribe(
      response => this.updateCallbackSuccess(response), error => this.updateCallbackError(error));
   
    return barcoDTO;
  }
  private atualizarGrid(data)
  {
    this.barco = data.nome;
    this.controls.tituloAnuncio.setValue( data.tituloAnuncio);
    this.controls.tipoAnuncio.setValue(data.tipoAnuncio);
    this.dadosBarco = data;
    this.tipoAnuncio = data.tipoAnuncio;
    this.tituloAnuncio = data.tituloAnuncio;
    this.gridService.setDataSource(data)
  }

  private updateCallbackSuccess(response: ResponseDTO) {
    this.barcoDataService.getById(response.data.barcoId).subscribe(response => this.atualizarGrid(response.data));
    this.notificationService.success('Anúncio feito com sucesso!');
    this.router.navigate(['/barco-list']);
  }

  private updateCallbackError(error): any {
    this.notificationService.error(error.message);
  }
}
