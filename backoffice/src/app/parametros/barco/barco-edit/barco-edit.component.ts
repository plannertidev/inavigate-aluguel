import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {
  faUser,
  faAddressBook,
  faCreditCard,
  faChevronLeft,
  faChevronRight,
  faSave,
  faBan,
} from '@fortawesome/free-solid-svg-icons';
import { BarcoDataService } from '../barco-dataservice';
import { BarcoFormComponent } from '../barco-form/barco-form.component';

@Component({
  selector: 'app-barco-edit',
  templateUrl: './barco-edit.component.html',
  styleUrls: ['./barco-edit.component.scss'],
  providers: [BarcoDataService ]
})
export class BarcoEditComponent implements OnInit {

  @Input()
  isLinear = true;

  @Input()
  isEditable = true;

  frmValues: object = {};

  faPerson = faUser;
  faAddressBook = faAddressBook;
  faCreditCard = faCreditCard;
  faChevronLeft = faChevronLeft;
  faChevronRight = faChevronRight;
  faSave = faSave;
  faBan = faBan;

  @ViewChild(BarcoFormComponent, { static: true }) public bancoFormComponent: BarcoFormComponent;
  public bancoForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private activatedRouted: ActivatedRoute,
              private barcoDataService: BarcoDataService) {
  }
  
  ngOnInit(): void {
    this.activatedRouted.params.subscribe(params => this.getBarco(params['id']));
    this.bancoForm = this.bancoFormComponent.createFormGroup();
  }
  
  private getBarco(barcoId: string){
    this.barcoDataService.getById(barcoId).subscribe(response =>this.getBarcoCallback(response.data));
  }

  private getBarcoCallback(data){
    console.log(data);

    if (data != null) {
      this.bancoForm.controls.barcoId.setValue(data.barcoId);

      this.bancoForm.controls.categoriaId.setValue(data.categoriaId);
      this.bancoForm.controls.categoriaDescricao.setValue(data.categoriaDescricao);

      this.bancoForm.controls.modeloId.setValue(data.modeloId);
      this.bancoForm.controls.modeloDescricao.setValue(data.modeloDescricao);

      this.bancoForm.controls.fabricanteId.setValue(data.fabricanteId);
      this.bancoForm.controls.fabricanteNome.setValue(data.fabricanteNome);

      this.bancoForm.controls.tripulacaoIncluidaNoPreco.setValue(data.tripulacaoIncluidaNoPreco);

      this.bancoForm.controls.nome.setValue(data.nome);
      this.bancoForm.controls.tipoCombustivel.setValue(data.tipoCombustivel);
      this.bancoForm.controls.capacidade.setValue(data.capacidade);
      this.bancoForm.controls.capacidadePernoite.setValue(data.capacidadePernoite);
      this.bancoForm.controls.anoFabricacao.setValue(data.anoFabricacao);
      this.bancoForm.controls.boca.setValue(data.boca);
      this.bancoForm.controls.potencia.setValue(data.potencia);
      this.bancoForm.controls.calado.setValue(data.calado);
      this.bancoForm.controls.cumprimento.setValue(data.cumprimento);
      this.bancoForm.controls.ultimaReforma.setValue(data.ultimaReforma);  
      this.bancoForm.controls.depositoAguaDoce.setValue(data.depositoAguaDoce);
      this.bancoForm.controls.depositoCombustivel.setValue(data.depositoCombustivel);;
      this.bancoForm.controls.consumoMedio.setValue(data.consumoMedio);
      this.bancoForm.controls.modeloId.setValue(data.modeloId);
      this.bancoForm.controls.velocidadeMaxima.setValue(data.velocidadeMaxima);
      this.bancoForm.controls.numeroDeCabines.setValue(data.numeroDeCabines);
      this.bancoForm.controls.numeroDeBanheiros.setValue(data.numeroDeBanheiros);

      //  Equipamento Conves
      const equipamentoConvesArray = data.equipamentoConves.split(";");
      this.bancoForm.controls.equipamentoConves_churrasqueira.setValue(equipamentoConvesArray[3] === '1' ? true : false);
      this.bancoForm.controls.equipamentoConves_plataformaMergulho.setValue(equipamentoConvesArray[4] === '1' ? true : false);
      this.bancoForm.controls.equipamentoConves_bimini.setValue(equipamentoConvesArray[5] === '1' ? true : false);
      this.bancoForm.controls.equipamentoConves_flaps.setValue(equipamentoConvesArray[6] === '1' ? true : false);
      this.bancoForm.controls.equipamentoConves_ancoraEletrica.setValue(equipamentoConvesArray[7] === '1' ? true : false);
      this.bancoForm.controls.equipamentoConves_velaSpinnaker.setValue(equipamentoConvesArray[8] === '1' ? true : false);
      this.bancoForm.controls.equipamentoConves_velaGenaker.setValue(equipamentoConvesArray[9] === '1' ? true : false);
      this.bancoForm.controls.equipamentoConves_velaRegata.setValue(equipamentoConvesArray[10] === '1' ? true : false);
      this.bancoForm.controls.equipamentoConves_velaPrincipalEnrolamentoMastro.setValue(equipamentoConvesArray[11] === '1' ? true : false);
      this.bancoForm.controls.equipamentoConves_cordaSegurança.setValue(equipamentoConvesArray[12] === '1' ? true : false);
      this.bancoForm.controls.equipamentoConves_acessibilidadeCadeirasRodas.setValue(equipamentoConvesArray[13] === '1' ? true : false);
      this.bancoForm.controls.equipamentoConves_jacuzziConves.setValue(equipamentoConvesArray[14] === '1' ? true : false);
      this.bancoForm.controls.equipamentoConves_salaCinemaExterior.setValue(equipamentoConvesArray[15] === '1' ? true : false);
      this.bancoForm.controls.equipamentoConves_barExterior.setValue(equipamentoConvesArray[16] === '1' ? true : false);
      this.bancoForm.controls.equipamentoConves_estabilizadorEmbarcacaoAncoragem.setValue(equipamentoConvesArray[17] === '1' ? true : false);
      this.bancoForm.controls.equipamentoConves_estabilizadorEmbarcacaoNavegacao.setValue(equipamentoConvesArray[18] === '1' ? true : false);

      //  Equipamento Eletronico
      const equipamentoEletronicoArray = data.equipamentoEletronico.split(";");
      this.bancoForm.controls.equipamentoEletronico_pilotoAutomatico.setValue(equipamentoEletronicoArray[0] === '1' ? true : false);
      this.bancoForm.controls.equipamentoEletronico_gerador.setValue(equipamentoEletronicoArray[1] === '1' ? true : false);
      this.bancoForm.controls.equipamentoEletronico_MP4.setValue(equipamentoEletronicoArray[2] === '1' ? true : false);
      this.bancoForm.controls.equipamentoEletronico_DVDPlayer.setValue(equipamentoEletronicoArray[3] === '1' ? true : false);
      this.bancoForm.controls.equipamentoEletronico_radar.setValue(equipamentoEletronicoArray[4] === '1' ? true : false);
      this.bancoForm.controls.equipamentoEletronico_TV.setValue(equipamentoEletronicoArray[5] === '1' ? true : false);
      this.bancoForm.controls.equipamentoEletronico_autoFalantesCockpit.setValue(equipamentoEletronicoArray[6] === '1' ? true : false);
      this.bancoForm.controls.equipamentoEletronico_GPSPlotter.setValue(equipamentoEletronicoArray[7] === '1' ? true : false);
      this.bancoForm.controls.equipamentoEletronico_inversor.setValue(equipamentoEletronicoArray[8] === '1' ? true : false);
      this.bancoForm.controls.equipamentoEletronico_odometro.setValue(equipamentoEletronicoArray[9] === '1' ? true : false);
      this.bancoForm.controls.equipamentoEletronico_VHF.setValue(equipamentoEletronicoArray[10] === '1' ? true : false);
      this.bancoForm.controls.equipamentoEletronico_conexaoiPod.setValue(equipamentoEletronicoArray[11] === '1' ? true : false);
      this.bancoForm.controls.equipamentoEletronico_sondaEcoSonoro.setValue(equipamentoEletronicoArray[12] === '1' ? true : false);
      this.bancoForm.controls.equipamentoEletronico_anemometro.setValue(equipamentoEletronicoArray[13] === '1' ? true : false);
      this.bancoForm.controls.equipamentoEletronico_GPSPlotterCockpit.setValue(equipamentoEletronicoArray[14] === '1' ? true : false);
      this.bancoForm.controls.equipamentoEletronico_TVCadaCabine.setValue(equipamentoEletronicoArray[15] === '1' ? true : false);
      this.bancoForm.controls.equipamentoEletronico_tomada220V.setValue(equipamentoEletronicoArray[16] === '1' ? true : false);
      this.bancoForm.controls.equipamentoEletronico_WiFiBordo.setValue(equipamentoEletronicoArray[17] === '1' ? true : false);
      this.bancoForm.controls.equipamentoEletronico_CD.setValue(equipamentoEletronicoArray[18] === '1' ? true : false);
      this.bancoForm.controls.equipamentoEletronico_computador.setValue(equipamentoEletronicoArray[19] === '1' ? true : false);
      this.bancoForm.controls.equipamentoEletronico_telefoneSatelite.setValue(equipamentoEletronicoArray[20] === '1' ? true : false);
      this.bancoForm.controls.equipamentoEletronico_TVSalao.setValue(equipamentoEletronicoArray[21] === '1' ? true : false);
      this.bancoForm.controls.equipamentoEletronico_IPS.setValue(equipamentoEletronicoArray[22] === '1' ? true : false);
      this.bancoForm.controls.equipamentoEletronico_sistemaPainelEnergiaSolar.setValue(equipamentoEletronicoArray[23] === '1' ? true : false);
      this.bancoForm.controls.equipamentoEletronico_sistemaPurificadorAgua.setValue(equipamentoEletronicoArray[24] === '1' ? true : false);
      this.bancoForm.controls.equipamentoEletronico_videoGame.setValue(equipamentoEletronicoArray[25] === '1' ? true : false);

      //  Outros Equipamentos 
      const outrosEquipamentosArray = data.outrosEquipamentos.split(";");
      this.bancoForm.controls.outrosEquipamentos_aguaQuente.setValue(outrosEquipamentosArray[0] === '1' ? true : false);
      this.bancoForm.controls.outrosEquipamentos_boteAuxiliar.setValue(outrosEquipamentosArray[1] === '1' ? true : false);
      this.bancoForm.controls.outrosEquipamentos_microOndas.setValue(outrosEquipamentosArray[2] === '1' ? true : false);
      this.bancoForm.controls.outrosEquipamentos_coleteSalvaVidas.setValue(outrosEquipamentosArray[3] === '1' ? true : false);
      this.bancoForm.controls.outrosEquipamentos_forno.setValue(outrosEquipamentosArray[4] === '1' ? true : false);
      this.bancoForm.controls.outrosEquipamentos_chuveiroConves.setValue(outrosEquipamentosArray[5] === '1' ? true : false);
      this.bancoForm.controls.outrosEquipamentos_mesaCockpit.setValue(outrosEquipamentosArray[6] === '1' ? true : false);
      this.bancoForm.controls.outrosEquipamentos_convesTeca.setValue(outrosEquipamentosArray[7] === '1' ? true : false);
      this.bancoForm.controls.outrosEquipamentos_academia.setValue(outrosEquipamentosArray[8] === '1' ? true : false);
      this.bancoForm.controls.outrosEquipamentos_aquecedor.setValue(outrosEquipamentosArray[9] === '1' ? true : false);
      this.bancoForm.controls.outrosEquipamentos_loucas.setValue(outrosEquipamentosArray[10] === '1' ? true : false);
      this.bancoForm.controls.outrosEquipamentos_arCondicionado.setValue(outrosEquipamentosArray[11] === '1' ? true : false);
      this.bancoForm.controls.outrosEquipamentos_congelador.setValue(outrosEquipamentosArray[12] === '1' ? true : false);
      this.bancoForm.controls.outrosEquipamentos_aquecedorSalao.setValue(outrosEquipamentosArray[13] === '1' ? true : false);
      this.bancoForm.controls.outrosEquipamentos_cofre.setValue(outrosEquipamentosArray[14] === '1' ? true : false);
      this.bancoForm.controls.outrosEquipamentos_cooler.setValue(outrosEquipamentosArray[15] === '1' ? true : false);
      this.bancoForm.controls.outrosEquipamentos_cartasNauticas.setValue(outrosEquipamentosArray[16] === '1' ? true : false);
      this.bancoForm.controls.outrosEquipamentos_USBAUX.setValue(outrosEquipamentosArray[17] === '1' ? true : false);
      this.bancoForm.controls.outrosEquipametos_motorPopa.setValue(outrosEquipamentosArray[18] === '1' ? true : false);
      this.bancoForm.controls.outrosEquipamentos_materialCozinha.setValue(outrosEquipamentosArray[19] === '1' ? true : false);
      this.bancoForm.controls.outrosEquipamentos_fogaoGas.setValue(outrosEquipamentosArray[20] === '1' ? true : false);
      this.bancoForm.controls.outrosEquipamentos_geladeiraEletrica.setValue(outrosEquipamentosArray[21] === '1' ? true : false);
      this.bancoForm.controls.outrosEquipamentos_elevador.setValue(outrosEquipamentosArray[22] === '1' ? true : false);
      this.bancoForm.controls.outrosEquipamentos_servicoTransfer.setValue(outrosEquipamentosArray[23] === '1' ? true : false);
      this.bancoForm.controls.outrosEquipamentos_ACCabine.setValue(outrosEquipamentosArray[24] === '1' ? true : false);
      this.bancoForm.controls.outrosEquipamentos_ACSalao.setValue(outrosEquipamentosArray[25] === '1' ? true : false);
      this.bancoForm.controls.outrosEquipamentos_aquecedorCadaCabine.setValue(outrosEquipamentosArray[26] === '1' ? true : false);
      this.bancoForm.controls.outrosEquipamentos_equipamentoSeguranca.setValue(outrosEquipamentosArray[27] === '1' ? true : false);
      this.bancoForm.controls.outrosEquipamentos_radioBeacons.setValue(outrosEquipamentosArray[28] === '1' ? true : false);

      //  Tripulaçao
      const entretenimentoAquaticoArray = data.entretenimentoAquatico.split(";");
      this.bancoForm.controls.entretenimentoAquatico_jetSki.setValue(entretenimentoAquaticoArray[0] === '1' ? true : false);
      this.bancoForm.controls.entretenimentoAquatico_caiaque.setValue(entretenimentoAquaticoArray[1] === '1' ? true : false);
      this.bancoForm.controls.entretenimentoAquatico_equipamentoPesca.setValue(entretenimentoAquaticoArray[2] === '1' ? true : false);
      this.bancoForm.controls.entretenimentoAquatico_standPaddle.setValue(entretenimentoAquaticoArray[3] === '1' ? true : false);
      this.bancoForm.controls.entretenimentoAquatico_boiaDonut.setValue(entretenimentoAquaticoArray[4] === '1' ? true : false);
      this.bancoForm.controls.entretenimentoAquatico_seabob.setValue(entretenimentoAquaticoArray[5] === '1' ? true : false);
      this.bancoForm.controls.entretenimentoAquatico_boiaBanana.setValue(entretenimentoAquaticoArray[6] === '1' ? true : false);
      this.bancoForm.controls.entretenimentoAquatico_skisAquaticos.setValue(entretenimentoAquaticoArray[7] === '1' ? true : false);
      this.bancoForm.controls.entretenimentoAquatico_windsurf.setValue(entretenimentoAquaticoArray[8] === '1' ? true : false);
      this.bancoForm.controls.entretenimentoAquatico_bicicleta.setValue(entretenimentoAquaticoArray[9] === '1' ? true : false);
      this.bancoForm.controls.entretenimentoAquatico_equipamentoSnorkel.setValue(entretenimentoAquaticoArray[10] === '1' ? true : false);
      this.bancoForm.controls.entretenimentoAquatico_equipamentoMergulho.setValue(entretenimentoAquaticoArray[11] === '1' ? true : false);
      this.bancoForm.controls.entretenimentoAquatico_skurfer.setValue(entretenimentoAquaticoArray[12] === '1' ? true : false);

      //  Tripulaçao
      const tripulacaoArray = data.tripulacao.split(";");
      this.bancoForm.controls.tripulacao_marinheiro.setValue(tripulacaoArray[0] === '1' ? true : false);
      this.bancoForm.controls.tripulacao_primeiroOficial.setValue(tripulacaoArray[1] === '1' ? true : false);
      this.bancoForm.controls.tripulacao_segundoOficial.setValue(tripulacaoArray[2] === '1' ? true : false);
      this.bancoForm.controls.tripulacao_chefe.setValue(tripulacaoArray[3] === '1' ? true : false);
      this.bancoForm.controls.tripulacao_comissáriaBordoChefe.setValue(tripulacaoArray[4] === '1' ? true : false);
      this.bancoForm.controls.tripulacao_segundaComissariaBordo.setValue(tripulacaoArray[5] === '1' ? true : false);
      this.bancoForm.controls.tripulacao_terceiraComissariaBordo.setValue(tripulacaoArray[6] === '1' ? true : false);
      this.bancoForm.controls.tripulacao_engenheiroChefe.setValue(tripulacaoArray[7] === '1' ? true : false);
      this.bancoForm.controls.tripulacao_auxiliarMarinheiro.setValue(tripulacaoArray[8] === '1' ? true : false);
      this.bancoForm.controls.tripulacao_segundoAuxiliarMarinheiro.setValue(tripulacaoArray[9] === '1' ? true : false);

      //  Idiomas falados
      const idiomasFaladoArray = data.idiomasFalado.split(";");
      this.bancoForm.controls.idiomasFalado_portugues.setValue(idiomasFaladoArray[0] === '1' ? true : false);
      this.bancoForm.controls.idiomasFalado_ingles.setValue(idiomasFaladoArray[1] === '1' ? true : false);
      this.bancoForm.controls.idiomasFalado_espanhol.setValue(idiomasFaladoArray[2] === '1' ? true : false);
      this.bancoForm.controls.idiomasFalado_frances.setValue(idiomasFaladoArray[3] === '1' ? true : false);
      this.bancoForm.controls.idiomasFalado_italiano.setValue(idiomasFaladoArray[4] === '1' ? true : false);
      this.bancoForm.controls.idiomasFalado_alemao.setValue(idiomasFaladoArray[5] === '1' ? true : false);
      this.bancoForm.controls.idiomasFalado_holandes.setValue(idiomasFaladoArray[6] === '1' ? true : false);
      this.bancoForm.controls.idiomasFalado_croata.setValue(idiomasFaladoArray[7] === '1' ? true : false);
      this.bancoForm.controls.idiomasFalado_russo.setValue(idiomasFaladoArray[8] === '1' ? true : false);
      this.bancoForm.controls.idiomasFalado_chines.setValue(idiomasFaladoArray[9] === '1' ? true : false);
      this.bancoForm.controls.idiomasFalado_grego.setValue(idiomasFaladoArray[10] === '1' ? true : false);
      this.bancoForm.controls.idiomasFalado_arabe.setValue(idiomasFaladoArray[11] === '1' ? true : false);
      this.bancoForm.controls.idiomasFalado_turco.setValue(idiomasFaladoArray[12] === '1' ? true : false);
    }
  }

  submit(): void {
  }
}
