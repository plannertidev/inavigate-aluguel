import { CdkStepper } from '@angular/cdk/stepper';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators,FormGroup } from '@angular/forms';

import {
  faChevronLeft,
  faChevronRight,
  faSave,
  faImages,
  faShip,
  faCogs,
  faSun
} from '@fortawesome/free-solid-svg-icons';
import { Subject } from 'rxjs';
import { Helper } from 'src/app/core/helper';
import { CategoriaDataService } from '../../categoria/categoria.dataservice';
import { FabricanteDataService } from '../../fabricante/fabricante.dataservice';
import { ModeloDataService } from '../../modelo/modelo.dataservice';

@Component({
  selector: 'app-barco-form',
  templateUrl: './barco-form.component.html',
  styleUrls: ['./barco-form.component.scss'],
  providers: [FabricanteDataService, ModeloDataService, CategoriaDataService ]
})
export class BarcoFormComponent implements OnInit {
 
  faPerson = faShip;
  faAddressBook = faCogs;
  faImages = faImages;
  faChevronLeft = faChevronLeft;
  faChevronRight = faChevronRight;
  faSave = faSave;
  faBan = faImages;
  faSun = faSun;

  barcoFormSubject:Subject<any> = new Subject();
  modeloSelectedSubject:Subject<any> = new Subject();
  
  public controls: any;
  public barcoForm : FormGroup;

  optionsFabricante = [];
  optionsModelo = [];
  optionsCategoria = [];

  constructor(
    private formBuilder: FormBuilder,
    private fabricanteDataService: FabricanteDataService,
    private modeloDataService: ModeloDataService,
    private categoriaDataService: CategoriaDataService) {
      
    this.barcoForm = this.formBuilder.group({
      barcoId:[''],
      nome:['',Validators.required],
      categoriaId:['',Validators.required],
      categoriaDescricao:['',Validators.required],
      fabricanteId:['',Validators.required],
      fabricanteNome:['',Validators.required],
      modeloId:['',Validators.required],
      modeloDescricao:['',Validators.required],
      tipoCombustivel:['',Validators.required],
      anoFabricacao:['',Validators.required],
      ultimaReforma:['',Validators.required],
      tripulacaoIncluidaNoPreco:['',Validators.required],
      capacidade:['',Validators.required],
      capacidadePernoite:['',Validators.required],
      numeroDeCabines:['',Validators.required],
      numeroDeBanheiros:['',Validators.required],
      cumprimento:['',Validators.required],
      boca:['',Validators.required],
      calado:['',Validators.required],
      potencia:['',Validators.required],
      velocidadeCruzeiro:['',Validators.required],
      velocidadeMaxima:['',Validators.required],
      consumoMedio:['',Validators.required],
      depositoAguaDoce:['',Validators.required],
      depositoCombustivel:['',Validators.required],
      equipamentoConves_capotaDodger:['',Validators.required],
      equipamentoConves_bimini:['',Validators.required],
      equipamentoConves_velaRegata:['',Validators.required],
      equipamentoConves_salaCinemaExterior:['',Validators.required],
      equipamentoConves_propulsorProa:['',Validators.required],
      equipamentoConves_flaps:['', Validators.required],
      equipamentoConves_velaPrincipalEnrolamentoMastro:['', Validators.required],
      equipamentoConves_barExterior:['', Validators.required],
      equipamentoConves_guinchosEletricos:['',Validators.required],
      equipamentoConves_ancoraEletrica:['',Validators.required],
      equipamentoConves_cordaSegurança:['',Validators.required],
      equipamentoConves_estabilizadorEmbarcacaoAncoragem:['',Validators.required],
      equipamentoConves_churrasqueira:['',Validators.required],
      equipamentoConves_velaSpinnaker:['',Validators.required],
      equipamentoConves_acessibilidadeCadeirasRodas:['',Validators.required],
      equipamentoConves_estabilizadorEmbarcacaoNavegacao:['',Validators.required],
      equipamentoConves_plataformaMergulho:['',Validators.required],
      equipamentoConves_velaGenaker:['',Validators.required],
      equipamentoConves_jacuzziConves:['',Validators.required],
      equipamentoEletronico_pilotoAutomatico:['',Validators.required],
      equipamentoEletronico_TV:['',Validators.required],
      equipamentoEletronico_VHF:['',Validators.required],
      equipamentoEletronico_TVCadaCabine:['',Validators.required],
      equipamentoEletronico_telefoneSatelite:['',Validators.required],
      equipamentoEletronico_videoGame:['',Validators.required],
      equipamentoEletronico_gerador:['',Validators.required],
      equipamentoEletronico_autoFalantesCockpit:['',Validators.required],
      equipamentoEletronico_conexaoiPod:['',Validators.required],
      equipamentoEletronico_tomada220V:['',Validators.required],
      equipamentoEletronico_TVSalao:['',Validators.required],
      equipamentoEletronico_MP4:['',Validators.required],
      equipamentoEletronico_GPSPlotter:['',Validators.required],
      equipamentoEletronico_sondaEcoSonoro:['',Validators.required],
      equipamentoEletronico_WiFiBordo:['',Validators.required],
      equipamentoEletronico_IPS:['',Validators.required],
      equipamentoEletronico_DVDPlayer:['',Validators.required],
      equipamentoEletronico_inversor:['',Validators.required],
      equipamentoEletronico_anemometro:['',Validators.required],
      equipamentoEletronico_CD:['',Validators.required],
      equipamentoEletronico_sistemaPainelEnergiaSolar:['',Validators.required],
      equipamentoEletronico_radar:['',Validators.required],
      equipamentoEletronico_odometro:['',Validators.required],
      equipamentoEletronico_GPSPlotterCockpit:['',Validators.required],
      equipamentoEletronico_computador:['',Validators.required],
      equipamentoEletronico_sistemaPurificadorAgua:['',Validators.required],
      outrosEquipamentos_aguaQuente:['',Validators.required],
      outrosEquipamentos_mesaCockpit:['',Validators.required],
      outrosEquipamentos_congelador:['',Validators.required],
      outrosEquipametos_motorPopa:['',Validators.required],
      outrosEquipamentos_ACCabine:['',Validators.required],
      outrosEquipamentos_microOndas:['',Validators.required],
      outrosEquipamentos_convesTeca:['',Validators.required],
      outrosEquipamentos_aquecedorSalao:['',Validators.required],
      outrosEquipamentos_materialCozinha:['',Validators.required],
      outrosEquipamentos_ACSalao:['',Validators.required],
      outrosEquipamentos_boteAuxiliar:['',Validators.required],
      outrosEquipamentos_academia:['',Validators.required],
      outrosEquipamentos_cofre:['',Validators.required],
      outrosEquipamentos_fogaoGas:['',Validators.required],
      outrosEquipamentos_aquecedorCadaCabine:['',Validators.required],
      outrosEquipamentos_coleteSalvaVidas:['',Validators.required],
      outrosEquipamentos_aquecedor:['',Validators.required],
      outrosEquipamentos_cooler:['',Validators.required],
      outrosEquipamentos_geladeiraEletrica:['',Validators.required],
      outrosEquipamentos_equipamentoSeguranca:['',Validators.required],
      outrosEquipamentos_forno:['',Validators.required],
      outrosEquipamentos_loucas:['',Validators.required],
      outrosEquipamentos_cartasNauticas:['',Validators.required],
      outrosEquipamentos_elevador:['',Validators.required],
      outrosEquipamentos_radioBeacons:['',Validators.required],
      outrosEquipamentos_chuveiroConves:['',Validators.required],
      outrosEquipamentos_arCondicionado:['',Validators.required],
      outrosEquipamentos_USBAUX:['',Validators.required],
      outrosEquipamentos_servicoTransfer:['',Validators.required],
      entretenimentoAquatico_jetSki:['',Validators.required],
      entretenimentoAquatico_seabob:['',Validators.required],
      entretenimentoAquatico_equipamentoSnorkel:['',Validators.required],
      entretenimentoAquatico_caiaque:['',Validators.required],
      entretenimentoAquatico_boiaBanana:['',Validators.required],
      entretenimentoAquatico_equipamentoMergulho:['',Validators.required],
      entretenimentoAquatico_equipamentoPesca:['',Validators.required],
      entretenimentoAquatico_skisAquaticos:['',Validators.required],
      entretenimentoAquatico_skurfer:['',Validators.required],
      entretenimentoAquatico_standPaddle:['',Validators.required],
      entretenimentoAquatico_windsurf:['',Validators.required],
      entretenimentoAquatico_boiaDonut:['',Validators.required],
      entretenimentoAquatico_bicicleta:['',Validators.required],
      tripulacao_marinheiro:['',Validators.required],
      tripulacao_segundaComissariaBordo:['',Validators.required],
      tripulacao_primeiroOficial:['',Validators.required],
      tripulacao_terceiraComissariaBordo:['',Validators.required],
      tripulacao_segundoOficial:['',Validators.required],
      tripulacao_engenheiroChefe:['',Validators.required],
      tripulacao_chefe:['',Validators.required],
      tripulacao_auxiliarMarinheiro:['',Validators.required],
      tripulacao_comissáriaBordoChefe:['',Validators.required],
      tripulacao_segundoAuxiliarMarinheiro:['',Validators.required],
      idiomasFalado_portugues:['',Validators.required],
      idiomasFalado_croata:['',Validators.required],
      idiomasFalado_ingles:['',Validators.required],
      idiomasFalado_russo:['',Validators.required],
      idiomasFalado_espanhol:['',Validators.required],
      idiomasFalado_chines:['',Validators.required],
      idiomasFalado_frances:['',Validators.required],
      idiomasFalado_grego:['',Validators.required],
      idiomasFalado_italiano:['',Validators.required],
      idiomasFalado_arabe:['',Validators.required],
      idiomasFalado_alemao:['',Validators.required],
      idiomasFalado_turco:['',Validators.required],
      idiomasFalado_holandes:['',Validators.required]

    });

    this.controls = {
      barcoId: this.barcoForm.get('barcoId'),
      nome: this.barcoForm.get('nome'),
      categoriaId: this.barcoForm.get('categoriaId'),
      categoriaDescricao: this.barcoForm.get('categoriaDescricao'),
      fabricanteId: this.barcoForm.get('fabricanteId'),
      fabricanteNome: this.barcoForm.get('fabricanteNome'),
      modeloId: this.barcoForm.get('modeloId'),
      modeloDescricao: this.barcoForm.get('modeloDescricao'),
      anoFabricacao: this.barcoForm.get('anoFabricacao'),
      ultimaReforma: this.barcoForm.get('ultimaReforma'),
      tripulacaoIncluidaNoPreco: this.barcoForm.get('tripulacaoIncluidaNoPreco'),
      tipoCombustivel: this.barcoForm.get('tipoCombustivel'),
      capacidade: this.barcoForm.get('capacidade'),
      capacidadePernoite: this.barcoForm.get('capacidadePernoite'),
      numeroDeCabines: this.barcoForm.get('numeroDeCabines'),
      numeroDeBanheiros: this.barcoForm.get('numeroDeBanheiros'),
      cumprimento: this.barcoForm.get('cumprimento'),
      boca: this.barcoForm.get('boca'),
      calado: this.barcoForm.get('calado'),
      potencia: this.barcoForm.get('potencia'),
      velocidadeCruzeiro: this.barcoForm.get('velocidadeCruzeiro'),
      velocidadeMaxima: this.barcoForm.get('velocidadeMaxima'),
      consumoMedio: this.barcoForm.get('consumoMedio'),
      depositoAguaDoce: this.barcoForm.get('depositoAguaDoce'),
      depositoCombustivel: this.barcoForm.get('depositoCombustivel'),
      equipamentoConves_capotaDodger: this.barcoForm.get('equipamentoConves_capotaDodger'),
      equipamentoConves_bimini: this.barcoForm.get('equipamentoConves_bimini'),
      equipamentoConves_velaRegata: this.barcoForm.get('equipamentoConves_velaRegata'),
      equipamentoConves_salaCinemaExterior: this.barcoForm.get('equipamentoConves_salaCinemaExterior'),
      equipamentoConves_propulsorProa: this.barcoForm.get('equipamentoConves_propulsorProa'),
      equipamentoConves_flaps: this.barcoForm.get('equipamentoConves_flaps'),
      equipamentoConves_velaPrincipalEnrolamentoMastro: this.barcoForm.get('equipamentoConves_velaPrincipalEnrolamentoMastro'),
      equipamentoConves_barExterior: this.barcoForm.get('equipamentoConves_barExterior'),
      equipamentoConves_guinchosEletricos: this.barcoForm.get('equipamentoConves_guinchosEletricos'),
      equipamentoConves_ancoraEletrica: this.barcoForm.get('equipamentoConves_ancoraEletrica'),
      equipamentoConves_cordaSegurança: this.barcoForm.get('equipamentoConves_cordaSegurança'),
      equipamentoConves_estabilizadorEmbarcacaoAncoragem: this.barcoForm.get('equipamentoConves_estabilizadorEmbarcacaoAncoragem'),
      equipamentoConves_churrasqueira: this.barcoForm.get('equipamentoConves_churrasqueira'),
      equipamentoConves_velaSpinnaker: this.barcoForm.get('equipamentoConves_velaSpinnaker'),
      equipamentoConves_acessibilidadeCadeirasRodas: this.barcoForm.get('equipamentoConves_acessibilidadeCadeirasRodas'),
      equipamentoConves_estabilizadorEmbarcacaoNavegacao: this.barcoForm.get('equipamentoConves_estabilizadorEmbarcacaoNavegacao'),
      equipamentoConves_plataformaMergulho: this.barcoForm.get('equipamentoConves_plataformaMergulho'),
      equipamentoConves_velaGenaker: this.barcoForm.get('equipamentoConves_velaGenaker'),
      equipamentoConves_jacuzziConves: this.barcoForm.get('equipamentoConves_jacuzziConves'),
      equipamentoEletronico_pilotoAutomatico: this.barcoForm.get('equipamentoEletronico_pilotoAutomatico'),
      equipamentoEletronico_TV: this.barcoForm.get('equipamentoEletronico_TV'),
      equipamentoEletronico_VHF: this.barcoForm.get('equipamentoEletronico_VHF'),
      equipamentoEletronico_TVCadaCabine: this.barcoForm.get('equipamentoEletronico_TVCadaCabine'),
      equipamentoEletronico_telefoneSatelite: this.barcoForm.get('equipamentoEletronico_telefoneSatelite'),
      equipamentoEletronico_videoGame: this.barcoForm.get('equipamentoEletronico_videoGame'),
      equipamentoEletronico_gerador: this.barcoForm.get('equipamentoEletronico_gerador'),
      equipamentoEletronico_autoFalantesCockpit: this.barcoForm.get('equipamentoEletronico_autoFalantesCockpit'),
      equipamentoEletronico_conexaoiPod: this.barcoForm.get('equipamentoEletronico_conexaoiPod'),
      equipamentoEletronico_tomada220V: this.barcoForm.get('equipamentoEletronico_tomada220V'),
      equipamentoEletronico_TVSalao: this.barcoForm.get('equipamentoEletronico_TVSalao'),
      equipamentoEletronico_MP4: this.barcoForm.get('equipamentoEletronico_MP4'),
      equipamentoEletronico_GPSPlotter: this.barcoForm.get('equipamentoEletronico_GPSPlotter'),
      equipamentoEletronico_sondaEcoSonoro: this.barcoForm.get('equipamentoEletronico_sondaEcoSonoro'),
      equipamentoEletronico_WiFiBordo: this.barcoForm.get('equipamentoEletronico_WiFiBordo'),
      equipamentoEletronico_IPS: this.barcoForm.get('equipamentoEletronico_IPS'),
      equipamentoEletronico_DVDPlayer: this.barcoForm.get('equipamentoEletronico_DVDPlayer'),
      equipamentoEletronico_inversor: this.barcoForm.get('equipamentoEletronico_inversor'),
      equipamentoEletronico_anemometro: this.barcoForm.get('equipamentoEletronico_anemometro'),
      equipamentoEletronico_CD: this.barcoForm.get('equipamentoEletronico_CD'),
      equipamentoEletronico_sistemaPainelEnergiaSolar: this.barcoForm.get('equipamentoEletronico_sistemaPainelEnergiaSolar'),
      equipamentoEletronico_radar: this.barcoForm.get('equipamentoEletronico_radar'),
      equipamentoEletronico_odometro: this.barcoForm.get('equipamentoEletronico_odometro'),
      equipamentoEletronico_GPSPlotterCockpit: this.barcoForm.get('equipamentoEletronico_GPSPlotterCockpit'),
      equipamentoEletronico_computador: this.barcoForm.get('equipamentoEletronico_computador'),
      equipamentoEletronico_sistemaPurificadorAgua: this.barcoForm.get('equipamentoEletronico_sistemaPurificadorAgua'),
      outrosEquipamentos_aguaQuente: this.barcoForm.get('outrosEquipamentos_aguaQuente'),
      outrosEquipamentos_mesaCockpit: this.barcoForm.get('outrosEquipamentos_mesaCockpit'),
      outrosEquipamentos_congelador: this.barcoForm.get('outrosEquipamentos_congelador'),
      outrosEquipametos_motorPopa: this.barcoForm.get('outrosEquipametos_motorPopa'),
      outrosEquipamentos_ACCabine: this.barcoForm.get('outrosEquipamentos_ACCabine'),
      outrosEquipamentos_microOndas: this.barcoForm.get('outrosEquipamentos_microOndas'),
      outrosEquipamentos_convesTeca: this.barcoForm.get('outrosEquipamentos_convesTeca'),
      outrosEquipamentos_aquecedorSalao: this.barcoForm.get('outrosEquipamentos_aquecedorSalao'),
      outrosEquipamentos_materialCozinha: this.barcoForm.get('outrosEquipamentos_materialCozinha'),
      outrosEquipamentos_ACSalao: this.barcoForm.get('outrosEquipamentos_ACSalao'),
      outrosEquipamentos_boteAuxiliar: this.barcoForm.get('outrosEquipamentos_boteAuxiliar'),
      outrosEquipamentos_academia: this.barcoForm.get('outrosEquipamentos_academia'),
      outrosEquipamentos_cofre: this.barcoForm.get('outrosEquipamentos_cofre'),
      outrosEquipamentos_fogaoGas: this.barcoForm.get('outrosEquipamentos_fogaoGas'),
      outrosEquipamentos_aquecedorCadaCabine: this.barcoForm.get('outrosEquipamentos_aquecedorCadaCabine'),
      outrosEquipamentos_coleteSalvaVidas: this.barcoForm.get('outrosEquipamentos_coleteSalvaVidas'),
      outrosEquipamentos_aquecedor: this.barcoForm.get('outrosEquipamentos_aquecedor'),
      outrosEquipamentos_cooler: this.barcoForm.get('outrosEquipamentos_cooler'),
      outrosEquipamentos_geladeiraEletrica: this.barcoForm.get('outrosEquipamentos_geladeiraEletrica'),
      outrosEquipamentos_equipamentoSeguranca: this.barcoForm.get('outrosEquipamentos_equipamentoSeguranca'),
      outrosEquipamentos_forno: this.barcoForm.get('outrosEquipamentos_forno'),
      outrosEquipamentos_loucas: this.barcoForm.get('outrosEquipamentos_loucas'),
      outrosEquipamentos_cartasNauticas: this.barcoForm.get('outrosEquipamentos_cartasNauticas'),
      outrosEquipamentos_elevador: this.barcoForm.get('outrosEquipamentos_elevador'),
      outrosEquipamentos_radioBeacons: this.barcoForm.get('outrosEquipamentos_radioBeacons'),
      outrosEquipamentos_chuveiroConves: this.barcoForm.get('outrosEquipamentos_chuveiroConves'),
      outrosEquipamentos_arCondicionado: this.barcoForm.get('outrosEquipamentos_arCondicionado'),
      outrosEquipamentos_USBAUX: this.barcoForm.get('outrosEquipamentos_USBAUX'),
      outrosEquipamentos_servicoTransfer: this.barcoForm.get('outrosEquipamentos_servicoTransfer'),
      entretenimentoAquatico_jetSki: this.barcoForm.get('entretenimentoAquatico_jetSki'),
      entretenimentoAquatico_seabob: this.barcoForm.get('entretenimentoAquatico_seabob'),
      entretenimentoAquatico_equipamentoSnorkel: this.barcoForm.get('entretenimentoAquatico_equipamentoSnorkel'),
      entretenimentoAquatico_caiaque: this.barcoForm.get('entretenimentoAquatico_caiaque'),
      entretenimentoAquatico_boiaBanana: this.barcoForm.get('entretenimentoAquatico_boiaBanana'),
      entretenimentoAquatico_equipamentoMergulho: this.barcoForm.get('entretenimentoAquatico_equipamentoMergulho'),
      entretenimentoAquatico_equipamentoPesca: this.barcoForm.get('entretenimentoAquatico_equipamentoPesca'),
      entretenimentoAquatico_skisAquaticos: this.barcoForm.get('entretenimentoAquatico_skisAquaticos'),
      entretenimentoAquatico_skurfer: this.barcoForm.get('entretenimentoAquatico_skurfer'),
      entretenimentoAquatico_standPaddle: this.barcoForm.get('entretenimentoAquatico_standPaddle'),
      entretenimentoAquatico_windsurf: this.barcoForm.get('entretenimentoAquatico_windsurf'),
      entretenimentoAquatico_boiaDonut: this.barcoForm.get('entretenimentoAquatico_boiaDonut'),
      entretenimentoAquatico_bicicleta: this.barcoForm.get('entretenimentoAquatico_bicicleta'),
      tripulacao_marinheiro: this.barcoForm.get('tripulacao_marinheiro'),
      tripulacao_segundaComissariaBordo: this.barcoForm.get('tripulacao_segundaComissariaBordo'),
      tripulacao_primeiroOficial: this.barcoForm.get('tripulacao_primeiroOficial'),
      tripulacao_terceiraComissariaBordo: this.barcoForm.get('tripulacao_terceiraComissariaBordo'),
      tripulacao_segundoOficial: this.barcoForm.get('tripulacao_segundoOficial'),
      tripulacao_engenheiroChefe: this.barcoForm.get('tripulacao_engenheiroChefe'),
      tripulacao_chefe: this.barcoForm.get('tripulacao_chefe'),
      tripulacao_auxiliarMarinheiro: this.barcoForm.get('tripulacao_auxiliarMarinheiro'),
      tripulacao_comissáriaBordoChefe: this.barcoForm.get('tripulacao_comissáriaBordoChefe'),
      tripulacao_segundoAuxiliarMarinheiro: this.barcoForm.get('tripulacao_segundoAuxiliarMarinheiro'),
      idiomasFalado_portugues: this.barcoForm.get('idiomasFalado_portugues'),
      idiomasFalado_croata: this.barcoForm.get('idiomasFalado_croata'),
      idiomasFalado_ingles: this.barcoForm.get('idiomasFalado_ingles'),
      idiomasFalado_russo: this.barcoForm.get('idiomasFalado_russo'),
      idiomasFalado_espanhol: this.barcoForm.get('idiomasFalado_espanhol'),
      idiomasFalado_chines: this.barcoForm.get('idiomasFalado_chines'),
      idiomasFalado_frances: this.barcoForm.get('idiomasFalado_frances'),
      idiomasFalado_grego: this.barcoForm.get('idiomasFalado_grego'),
      idiomasFalado_italiano: this.barcoForm.get('idiomasFalado_italiano'),
      idiomasFalado_arabe: this.barcoForm.get('idiomasFalado_arabe'),
      idiomasFalado_alemao: this.barcoForm.get('idiomasFalado_alemao'),
      idiomasFalado_turco: this.barcoForm.get('idiomasFalado_turco'),
      idiomasFalado_holandes: this.barcoForm.get('idiomasFalado_holandes'),
    }

   }

  ngOnInit(): void {

    this.fabricanteDataService.getAll().subscribe(
      (response) => {  
        response.data.forEach(element => {
          this.optionsFabricante.push( { id: element.fabricanteId , label: element.nome} );
        });
      });

      this.modeloDataService.getAll().subscribe(
        (response) => {  
          response.data.forEach(element => {
            this.optionsModelo.push( { id: element.modeloId , label: element.descricao} );
          });
        });

        this.categoriaDataService.getAll().subscribe(
          (response) => {  
            response.data.forEach(element => {
              this.optionsCategoria.push( { id: element.categoriaId , label: element.descricao} );
            });
          });
  
    this.teste();
  }

  createFormGroup(): FormGroup{
    return this.barcoForm;
  }

  validadeAbaBarco = (cdkStepper: CdkStepper) => {
    if (!this.abaBarcoIsValid()) {
      cdkStepper.selectedIndex = 0;
    }
  }

  abaBarcoIsValid = () => {  
    let result = true;

    if ( Helper.isEmptyOrNull( this.controls.modeloDescricao.value)) {
      this.controls.modeloDescricao.markAsTouched();
      result = false;
    }

    if ( Helper.isEmptyOrNull( this.controls.fabricanteNome.value)) {
      this.controls.fabricanteNome.markAsTouched();
      result = false;
    }

    if ( Helper.isEmptyOrNull( this.controls.categoriaDescricao.value)) {
      this.controls.categoriaDescricao.markAsTouched();
      result = false;
    }

    return result;
  }

  onSubmit = () => {  this.barcoFormSubject.next(this.controls);}

  teste = () => {
    // this.controls.nome.setValue("TIETA");
    // this.controls.fabricanteNome.setValue("Royal Falcon Fleet");
    // this.controls.fabricanteId.setValue("Royal Falcon Fleet");
    // this.controls.tipo.setValue(1);
    // this.controls.tipoCombustivel.setValue("Gasolina");
    // this.controls.modeloId.setValue("");
    // this.controls.modeloDescricao.setValue("Royal Falcon");
    // this.controls.anoFabricacao.setValue("2019");
    // this.controls.ultimaReforma.setValue("2020");
    // this.controls.tripulacaoIncluidaPreco.setValue("SIM");
    // this.controls.capacidade.setValue("10");
    // this.controls.capacidadePernoite.setValue("5");
    // this.controls.numeroCabines.setValue("1");
    // this.controls.numeroBanheiros.setValue("4");
    // this.controls.cumprimento.setValue("12,50m");
    // this.controls.boca.setValue("Juniores");
    // this.controls.calado.setValue("41 fit");
    // this.controls.potencia.setValue("2300 hp");
    // this.controls.velocidadeCruzeiro.setValue("15 knots");
    // this.controls.velocidadeMaxima.setValue("35 knots");
    // this.controls.consumoMedio.setValue(" 20 lit km");
    // this.controls.depositoAguaDoce.setValue("1");
    // this.controls.depositoCombustivel.setValue("2");
    // this.controls.equipamentoConves_bimini.setValue(1);
  }

  saveBoat = () => {
    if (this.formValid()) {
      this.barcoFormSubject.next(this.controls);
    }
  }

  formValid = () => {
    return true;
  }

  onSelectedFabricante = (option) => {
    this.controls.fabricanteId.value = option.id;
  }

  onModeloSelected = (option) => {
    this.controls.modeloId.value = option.id;
    this.modeloSelectedSubject.next(option.id);
  }

  onCategoriaSelected = (option) => {
    this.controls.categoriaId.value = option.id;
  }
}
