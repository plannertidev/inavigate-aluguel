export class BarcoDTO{
    public barcoId : string;
    public nome : string;
    public categoriaId : number;
    public fabricanteId : string;
    public proprietarioId:string;
    public modeloId : string;
    public tipoCombustivel : string; 
    public anoFabricacao: string;
    public ultimaReforma: string;
    public tripulacaoIncluidaNoPreco: string;
    public capacidade: string
    public capacidadePernoite: string;
    public numeroDeCabines: number;
    public numeroDeBanheiros: number;
    public cumprimento: number;
    public boca: string;
    public calado: string;
    public potencia: string;
    public velocidadeCruzeiro: number;
    public velocidadeMaxima: number;
    public consumoMedio: number;
    public depositoAguaDoce: number;
    public depositoCombustivel: number;

    public equipamentoConves: string;
    public equipamentoEletronico: string;
    public outrosEquipamentos : string;
    public entretenimentoAquatico: string;
    public tripulacao: string;
    public idiomasFalado: string;

    public tituloAnuncio: string;
    public tipoAnuncio: string;
}

export enum kdEquipamentoConves {
    CapotaDodger,
    Bimini,
    VelaRegata,
    SalaCinemaExterior,
    PropulsorProa,
    Flaps,
    VelaPrincipalEnrolamentoMastro,
    BarExterior,
    GuinchosEletricos,
    AncoraEletrica,
    CordaSegurança,
    EstabilizadorEmbarcacaoAncoragem,
    Churrasqueira,
    VelaSpinnaker,
    AcessibilidadeCadeirasRodas,
    EstabilizadorEmbarcacaoNavegacao,
    PlataformaMergulho,
    VelaGenaker,
    JacuzziConves
}   
export enum kdEquipamentoEletronico{
    PilotoAutomatico,
    TV,
    VHF,
    TVCadaCabine,
    TelefoneSatelite,
    VideoGame,
    Gerador,
    AutoFalantesCockpit,
    ConexaoiPod,
    Tomada220V,
    TVSalao,
    MP4,
    GPSPlotter,
    SondaEcoSonoro,
    WiFiBordo,
    IPS,
    DVDPlayer,
    Inversor,
    Anemometro,
    CD,
    SistemaPainelEnergiaSolar,
    Radar,
    Odometro,
    GPSPlotterCockpit,
    Computador,
    SistemaPurificadorAgua
}
export enum kdOutrosEquipamentos{
    AguaQuente,
    MesaCockpit,
    Congelador,
    MotorPopa,
    ACCabine,
    MicroOndas,
    ConvesTeca,
    AquecedorSalao,
    MaterialCozinha,
    ACSalao,
    BoteAuxiliar,
    Academia,
    Cofre,
    FogaoGas,
    AquecedorCadaCabine,
    ColeteSalvaVidas,
    Aquecedor,
    Cooler,
    GeladeiraEletrica,
    EquipamentoSeguranca,
    Forno,
    Loucas,
    CartasNauticas,
    Elevador,
    RadioBeacons,
    ChuveiroConves,
    ArCondicionado,
    USBAUX,
    ServicoTransfer,
}

export enum kdEntretenimentoAquatico{
    JetSki,
    Seabob,
    EquipamentoSnorkel,
    Caiaque,
    BoiaBanana,
    EquipamentoMergulho,
    EquipamentoPesca,
    SkisAquaticos,
    Skurfer,
    StandPaddle,
    Windsurf,
    BoiaDonut,
    Bicicleta
}

export enum kdTripulacao{
    Marinheiro,
    SegundaComissariaBordo,
    PrimeiroOficial,
    TerceiraComissariaBordo,
    SegundoOficial,
    EngenheiroChefe,
    Chefe,
    AuxiliarMarinheiro,
    ComissáriaBordoChefe,
    SegundoAuxiliarMarinheiro
}

export enum kdIdiomasFalado{
    Portugues,
    Croata,
    Ingles,
    Russo,
    Espanhol,
    Chines,
    Frances,
    Grego,
    Italiano,
    Arabe,
    Alemao,
    Turco,
    Holandes
}