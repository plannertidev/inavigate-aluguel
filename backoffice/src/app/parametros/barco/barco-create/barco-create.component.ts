import { Component, OnInit,ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NotificationService } from 'src/app/core/services/notification.service';
import { ResponseDTO } from 'src/app/core/services/response.dto';
import { BarcoFormComponent } from '../barco-form/barco-form.component';
import { BarcoDataService } from '../barco-dataservice';
import { BarcoDTO } from '../barco-dto'; 
import { faSave} from '@fortawesome/free-solid-svg-icons';
import { ModeloDataService } from '../../modelo/modelo.dataservice';
import { ModeloDTO } from '../../modelo/modelo.dto';
import { UserContextService } from 'src/app/core/services/user-context-service';


@Component({
  selector: 'app-barco-create',
  templateUrl: './barco-create.component.html',
  styleUrls: ['./barco-create.component.scss'],
  providers: [BarcoDataService, ModeloDataService]
})
export class BarcoCreateComponent implements OnInit {
  faSave = faSave;

  @ViewChild(BarcoFormComponent, { static: true }) public bancoFormComponent: BarcoFormComponent;
  public bancoForm: FormGroup;

  constructor( 
    private router: Router,
    private notificationService: NotificationService,
    private barcoDataService: BarcoDataService,
    private modeloDataService: ModeloDataService,
    private userContextService: UserContextService) { }

  ngOnInit(): void {
    this.bancoForm = this.bancoFormComponent.createFormGroup();
    this.bancoFormComponent.barcoFormSubject.subscribe(controls => this.onSubmit(controls))
    this.bancoFormComponent.modeloSelectedSubject.subscribe(controls => this.omModeloSelected(controls))
  }

  private mapToDto(controls) : BarcoDTO{
    let barcoDTO: BarcoDTO = new BarcoDTO()
    barcoDTO.nome = controls.nome.value;
    barcoDTO.categoriaId = controls.categoriaId.value;
    barcoDTO.fabricanteId = controls.fabricanteId.value;
    barcoDTO.proprietarioId = this.userContextService.get();
    barcoDTO.modeloId = controls.modeloId.value;
    barcoDTO.tipoCombustivel = controls.tipoCombustivel.value;
    barcoDTO.anoFabricacao = controls.anoFabricacao.value;
    barcoDTO.ultimaReforma = controls.ultimaReforma.value;
    barcoDTO.tipoCombustivel = controls.tipoCombustivel.value;
    barcoDTO.tripulacaoIncluidaNoPreco = controls.tripulacaoIncluidaNoPreco.value;
    barcoDTO.capacidade = controls.capacidade.value;
    barcoDTO.capacidadePernoite = controls.capacidadePernoite.value;
    barcoDTO.numeroDeCabines = controls.numeroDeCabines.value;
    barcoDTO.numeroDeBanheiros = controls.numeroDeBanheiros.value;
    barcoDTO.cumprimento = controls.cumprimento.value;
    barcoDTO.boca = controls.boca.value;
    barcoDTO.calado = controls.calado.value;
    barcoDTO.potencia = controls.potencia.value;
    barcoDTO.nome = controls.nome.value;
    barcoDTO.velocidadeCruzeiro = controls.velocidadeCruzeiro.value;
    barcoDTO.velocidadeMaxima = controls.velocidadeMaxima.value;
    barcoDTO.consumoMedio = controls.consumoMedio.value;
    barcoDTO.depositoAguaDoce = controls.depositoAguaDoce.value;
    barcoDTO.depositoCombustivel = controls.depositoCombustivel.value;

    barcoDTO.equipamentoConves =
    this.fromCheckbox(controls.equipamentoConves_capotaDodger.value) +
    this.fromCheckbox(controls.equipamentoConves_propulsorProa.value) +
    this.fromCheckbox(controls.equipamentoConves_guinchosEletricos.value) +
    this.fromCheckbox(controls.equipamentoConves_churrasqueira.value) +
    this.fromCheckbox(controls.equipamentoConves_plataformaMergulho.value) +
    this.fromCheckbox(controls.equipamentoConves_bimini.value) +
    this.fromCheckbox(controls.equipamentoConves_flaps.value) +
    this.fromCheckbox(controls.equipamentoConves_ancoraEletrica.value) +
    this.fromCheckbox(controls.equipamentoConves_velaSpinnaker.value) +
    this.fromCheckbox(controls.equipamentoConves_velaGenaker.value) +
    this.fromCheckbox(controls.equipamentoConves_velaRegata.value) +
    this.fromCheckbox(controls.equipamentoConves_velaPrincipalEnrolamentoMastro.value) +
    this.fromCheckbox(controls.equipamentoConves_cordaSegurança.value) +
    this.fromCheckbox(controls.equipamentoConves_acessibilidadeCadeirasRodas.value) +
    this.fromCheckbox(controls.equipamentoConves_jacuzziConves.value) +
    this.fromCheckbox(controls.equipamentoConves_salaCinemaExterior.value) +
    this.fromCheckbox(controls.equipamentoConves_barExterior.value) +
    this.fromCheckbox(controls.equipamentoConves_estabilizadorEmbarcacaoAncoragem.value) +
    this.fromCheckbox(controls.equipamentoConves_estabilizadorEmbarcacaoNavegacao.value);

    barcoDTO.equipamentoEletronico =
    this.fromCheckbox(controls.equipamentoEletronico_pilotoAutomatico.value) +
    this.fromCheckbox(controls.equipamentoEletronico_gerador.value) +
    this.fromCheckbox(controls.equipamentoEletronico_MP4.value) +
    this.fromCheckbox(controls.equipamentoEletronico_DVDPlayer.value) +
    this.fromCheckbox(controls.equipamentoEletronico_radar.value) +
    this.fromCheckbox(controls.equipamentoEletronico_TV.value) +
    this.fromCheckbox(controls.equipamentoEletronico_autoFalantesCockpit.value) +
    this.fromCheckbox(controls.equipamentoEletronico_GPSPlotter.value) +
    this.fromCheckbox(controls.equipamentoEletronico_inversor.value) + 
    this.fromCheckbox(controls.equipamentoEletronico_odometro.value) +
    this.fromCheckbox(controls.equipamentoEletronico_VHF.value) +
    this.fromCheckbox(controls.equipamentoEletronico_conexaoiPod.value) +
    this.fromCheckbox(controls.equipamentoEletronico_sondaEcoSonoro.value) +
    this.fromCheckbox(controls.equipamentoEletronico_anemometro.value) +
    this.fromCheckbox(controls.equipamentoEletronico_GPSPlotterCockpit.value) +
    this.fromCheckbox(controls.equipamentoEletronico_TVCadaCabine.value) +
    this.fromCheckbox(controls.equipamentoEletronico_tomada220V.value) +
    this.fromCheckbox(controls.equipamentoEletronico_WiFiBordo.value) +
    this.fromCheckbox(controls.equipamentoEletronico_CD.value) +
    this.fromCheckbox(controls.equipamentoEletronico_computador.value) +
    this.fromCheckbox(controls.equipamentoEletronico_telefoneSatelite.value) +
    this.fromCheckbox(controls.equipamentoEletronico_TVSalao.value) +
    this.fromCheckbox(controls.equipamentoEletronico_IPS.value) +
    this.fromCheckbox(controls.equipamentoEletronico_sistemaPainelEnergiaSolar.value) + 
    this.fromCheckbox(controls.equipamentoEletronico_sistemaPurificadorAgua.value) +
    this.fromCheckbox(controls.equipamentoEletronico_videoGame.value);

    barcoDTO.outrosEquipamentos = 
    this.fromCheckbox(controls.outrosEquipamentos_aguaQuente.value) +
    this.fromCheckbox(controls.outrosEquipamentos_microOndas.value) +
    this.fromCheckbox(controls.outrosEquipamentos_boteAuxiliar.value) +
    this.fromCheckbox(controls.outrosEquipamentos_coleteSalvaVidas.value) +
    this.fromCheckbox(controls.outrosEquipamentos_forno.value) +
    this.fromCheckbox(controls.outrosEquipamentos_chuveiroConves.value) +
    this.fromCheckbox(controls.outrosEquipamentos_mesaCockpit.value) +
    this.fromCheckbox(controls.outrosEquipamentos_convesTeca.value) +
    this.fromCheckbox(controls.outrosEquipamentos_academia.value) +
    this.fromCheckbox(controls.outrosEquipamentos_aquecedor.value) +
    this.fromCheckbox(controls.outrosEquipamentos_loucas.value) +
    this.fromCheckbox(controls.outrosEquipamentos_arCondicionado.value) +
    this.fromCheckbox(controls.outrosEquipamentos_congelador.value) +
    this.fromCheckbox(controls.outrosEquipamentos_aquecedorSalao.value) +
    this.fromCheckbox(controls.outrosEquipamentos_cofre.value) +
    this.fromCheckbox(controls.outrosEquipamentos_cooler.value) +
    this.fromCheckbox(controls.outrosEquipamentos_cartasNauticas.value) +
    this.fromCheckbox(controls.outrosEquipamentos_USBAUX.value) +
    this.fromCheckbox(controls.outrosEquipametos_motorPopa.value) +
    this.fromCheckbox(controls.outrosEquipamentos_materialCozinha.value) + 
    this.fromCheckbox(controls.outrosEquipamentos_fogaoGas.value) +
    this.fromCheckbox(controls.outrosEquipamentos_geladeiraEletrica.value) +
    this.fromCheckbox(controls.outrosEquipamentos_elevador.value) +
    this.fromCheckbox(controls.outrosEquipamentos_servicoTransfer.value) +
    this.fromCheckbox(controls.outrosEquipamentos_ACCabine.value) +
    this.fromCheckbox(controls.outrosEquipamentos_ACSalao.value) +
    this.fromCheckbox(controls.outrosEquipamentos_aquecedorCadaCabine.value) +
    this.fromCheckbox(controls.outrosEquipamentos_equipamentoSeguranca.value) +
    this.fromCheckbox(controls.outrosEquipamentos_radioBeacons.value);

    barcoDTO.entretenimentoAquatico = 
    this.fromCheckbox(controls.entretenimentoAquatico_jetSki.value) + 
    this.fromCheckbox(controls.entretenimentoAquatico_caiaque.value) +
    this.fromCheckbox(controls.entretenimentoAquatico_equipamentoPesca.value) +
    this.fromCheckbox(controls.entretenimentoAquatico_standPaddle.value) +
    this.fromCheckbox(controls.entretenimentoAquatico_boiaDonut.value) +
    this.fromCheckbox(controls.entretenimentoAquatico_seabob.value) +
    this.fromCheckbox(controls.entretenimentoAquatico_boiaBanana.value) +
    this.fromCheckbox(controls.entretenimentoAquatico_skisAquaticos.value) +
    this.fromCheckbox(controls.entretenimentoAquatico_windsurf.value) +
    this.fromCheckbox(controls.entretenimentoAquatico_bicicleta.value) +
    this.fromCheckbox(controls.entretenimentoAquatico_equipamentoSnorkel.value) +
    this.fromCheckbox(controls.entretenimentoAquatico_equipamentoMergulho.value) +
    this.fromCheckbox(controls.entretenimentoAquatico_skurfer.value);

    barcoDTO.tripulacao = 
    this.fromCheckbox(controls.tripulacao_marinheiro.value) +
    this.fromCheckbox(controls.tripulacao_primeiroOficial.value) +
    this.fromCheckbox(controls.tripulacao_segundoOficial.value) +
    this.fromCheckbox(controls.tripulacao_chefe.value) +
    this.fromCheckbox(controls.tripulacao_comissáriaBordoChefe.value) +
    this.fromCheckbox(controls.tripulacao_segundaComissariaBordo.value) +
    this.fromCheckbox(controls.tripulacao_terceiraComissariaBordo.value) +
    this.fromCheckbox(controls.tripulacao_engenheiroChefe.value) +
    this.fromCheckbox(controls.tripulacao_auxiliarMarinheiro.value) +
    this.fromCheckbox(controls.tripulacao_segundoAuxiliarMarinheiro.value);

    barcoDTO.idiomasFalado =
    this.fromCheckbox(controls.idiomasFalado_portugues.value) +
    this.fromCheckbox(controls.idiomasFalado_ingles.value) +
    this.fromCheckbox(controls.idiomasFalado_espanhol.value) +
    this.fromCheckbox(controls.idiomasFalado_frances.value) +
    this.fromCheckbox(controls.idiomasFalado_italiano.value) +
    this.fromCheckbox(controls.idiomasFalado_alemao.value) +
    this.fromCheckbox(controls.idiomasFalado_holandes.value) +
    this.fromCheckbox(controls.idiomasFalado_croata.value) +
    this.fromCheckbox(controls.idiomasFalado_russo.value) +
    this.fromCheckbox(controls.idiomasFalado_chines.value) +
    this.fromCheckbox(controls.idiomasFalado_grego.value) +
    this.fromCheckbox(controls.idiomasFalado_arabe.value) +
    this.fromCheckbox(controls.idiomasFalado_turco.value);

    return barcoDTO;
  }
  
  public onSubmit(controls: any) {

    if (controls.barcoId.value === "") {
      this.barcoDataService.create(this.mapToDto(controls)).subscribe(
        response => this.createCallbackSuccess(response), error => this.createCallbackError(error));
    } else {
      this.barcoDataService.update(this.mapToDto(controls)).subscribe(
        response => this.createCallbackSuccess(response), error => this.createCallbackError(error));
    }
  }

  public omModeloSelected(modeloId: string) {

    this.modeloDataService.getById(modeloId).subscribe(
      (response) => {  
          if (response.data != null) {
            this.copiarCamposDoModeloEmbarcao(response.data);        
          }

      });
  }

  private copiarCamposDoModeloEmbarcao(modelo: ModeloDTO) {
    this.bancoForm.controls.fabricanteId.setValue(modelo.fabricanteId);
    this.bancoForm.controls.fabricanteNome.setValue(modelo.fabricanteNome);
    this.bancoForm.controls.modeloDescricao.setValue(modelo.descricao);
    this.bancoForm.controls.anoFabricacao.setValue(modelo.inicioFimProducao);
    this.bancoForm.controls.capacidade.setValue(modelo.capacidade);
    this.bancoForm.controls.capacidadePernoite.setValue(modelo.capacidadePernoite);
    this.bancoForm.controls.numeroDeCabines.setValue(modelo.cabines);
    this.bancoForm.controls.numeroDeBanheiros.setValue(modelo.banheiros);
    this.bancoForm.controls.cumprimento.setValue(modelo.comprimentoLOA);
    this.bancoForm.controls.boca.setValue(modelo.boca);
    this.bancoForm.controls.calado.setValue(modelo.caladoMax);
    this.bancoForm.controls.potencia.setValue(modelo.potenciaMinima);
    this.bancoForm.controls.velocidadeMaxima.setValue(modelo.velocidadeMaximaKm);
    this.bancoForm.controls.depositoAguaDoce.setValue(modelo.tanqueAgua);
    this.bancoForm.controls.depositoCombustivel.setValue(modelo.tanqueCombustivel);
  }

  private createCallbackSuccess(response: ResponseDTO){

    this.bancoForm.controls.barcoId.setValue(response.data.barcoId);

    this.notificationService.success('barco incluido com sucesso!');
    //this.router.navigateByUrl('/barco-list');
  }

  private createCallbackError(error): any{
    this.notificationService.error(error);
  }

  public back(){
    this.router.navigateByUrl('/barco-list');
  }

  ngOnDestroy() {
    this.bancoFormComponent.barcoFormSubject.unsubscribe();
  }

  fromCheckbox(value) {
    let result: string = "0;";

    if (value) {
      result = "1;";
    }
    return result;
  }
}
