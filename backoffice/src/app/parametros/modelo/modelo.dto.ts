export class ModeloDTO{
    public modeloId : string;
    public descricao : string;
    public fabricanteId : string;
    public fabricanteNome : string;
    public inicioFimProducao : string;
    public comprimentoLOA : string;
    public comprimentoLWL : string;
    public boca : string;
    public caladoMin : string;
    public caladoMax : string;
    public pesoLeve : string;
    public banheiros : string;
    public cabines : string;
    public alturaBanheiro : string;
    public alturaCabine : string;
    public capacidade : string;
    public capacidadePernoite : string;
    public potenciaMaxima : string;
    public potenciaMinima : string;
    public tanqueAgua : string;
    public tanqueCombustivel : string;
    public casco : string;
    public motorDeSerie : string;
    public origem : string;
    public velocidadeMaximaKm  : string;
}