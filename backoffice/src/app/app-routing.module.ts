import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlannerDatePickerComponent } from './core/dates/date-picker/planner-date-picker.component';
import { HomeComponent } from './layout/home/home/home.component';
import { NavigationComponent } from './layout/navigation/navigation/navigation.component';
import { BarcoCreateComponent } from './parametros/barco/barco-create/barco-create.component';
import { BarcoEditComponent } from './parametros/barco/barco-edit/barco-edit.component';
import { BarcoListComponent } from './parametros/barco/barco-list/barco-list.component';
import { RegistrarComponent } from './parametros/proprietario/registrar/registrar.component';
import { BloqueioBarcoComponent } from './processos/reserva/reservar-barco/reservar-barco.component';
import { DashboardComponent } from './processos/dashboard/dashboard/dashboard.component';
import { GerenciarTemporadaComponent } from './processos/gerenciar-temporada/gerenciar-temporada.component';
import { LoginComponent } from './processos/login/login/login.component';
import { PainelDeMensagensComponent } from './processos/mensagem/painel-de-mensagens/painel-de-mensagens.component';
import { PainelDeReservaComponent } from './processos/reserva/painel-de-reserva/painel-de-reserva.component';
import { TextoAnuncioComponent } from './parametros/barco/barco-texto-anuncio/barco-texto-anuncio.component';

const routes: Routes = 
[  
  //{ path: '', component: LoginComponent, pathMatch:'full'},
  { path: 'home', component: HomeComponent},
  { path: 'login', component: LoginComponent},
  { path: 'barco-list', component: BarcoListComponent},
  { path: 'barco-create', component: BarcoCreateComponent},
  { path: 'barco-edit/:id', component: BarcoEditComponent},
  { path: 'dashboard', component: DashboardComponent},
  { path: 'painel-mensagens', component: PainelDeMensagensComponent},
  { path: 'painel-reserva', component: PainelDeReservaComponent},
  { path: 'bloqueio-barco/:id', component: BloqueioBarcoComponent},
  { path: 'gerenciar-temporada/:id', component: GerenciarTemporadaComponent},
  { path: 'barco-texto-anuncio/:id', component: TextoAnuncioComponent},
  { path: 'registrar', component: RegistrarComponent},
  { path: 'navigation', component: NavigationComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  entryComponents: [PlannerDatePickerComponent],

})
export class AppRoutingModule { }
