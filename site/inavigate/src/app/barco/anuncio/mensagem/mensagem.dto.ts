export class MensagemDTO {
    barcoId: string;
    numeroDias:number;
    incluirMarinheiro:number;
    dataPretendida: string;
    numeroPassageiros:number;
    remetenteNome:string;
    remetenteEmail:string;
    remetenteTelefone:string;
    mensagem:string;
    status:number;
}
