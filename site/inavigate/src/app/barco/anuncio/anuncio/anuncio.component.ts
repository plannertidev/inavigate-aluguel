import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NotificationService } from 'src/app/core/services/notification.service';
import { BarcoDataService } from 'src/app/layout/barco-card/barco-dataservice';
import { MensagemDataService } from '../mensagem/mensagem.dataservice';
import { FormGroup } from '@angular/forms';
import { SolicitarInformacaoComponent } from 'src/app/visitante/informacao/solicitar-informacao.component';
import { PlannerRangeDatePickerComponent } from 'src/app/core/dates/planner-range-date-picker/planner-range-date-picker.component';
import { ReservarComponent } from 'src/app/visitante/reservar/reservar/reservar.component';
import { ReservaDataService } from '../reserva/reserva.dataservice';

@Component({
  selector: 'app-anuncio',
  templateUrl: './anuncio.component.html',
  styleUrls: ['./anuncio.component.scss'],
  providers: [BarcoDataService, MensagemDataService, NotificationService, ReservaDataService]
})
export class AnuncioComponent implements OnInit, AfterViewInit {

@ViewChild(SolicitarInformacaoComponent, { static: true }) public solicitarInformacaoComponent: SolicitarInformacaoComponent;
@ViewChild(PlannerRangeDatePickerComponent, { static: true }) public plannerRangeDatePickerComponent: PlannerRangeDatePickerComponent;
@ViewChild(ReservarComponent, { static: true }) public reservarComponent: ReservarComponent;

  public mensagemForm: FormGroup;
  public reservaForm: FormGroup;
  public controls: any;
  public controlsReserva: any;

  imagesCollection: [any] = null;
  public barcoId: string = "";
  public tituloAnuncio: string;
  constructor(
    private activatedRouter: ActivatedRoute,
    private barcoDataService: BarcoDataService) {
  }

  ngAfterViewInit(): void {
    this.plannerRangeDatePickerComponent.date1PickedSubject.subscribe( data => {
      this.reservarComponent.reservaForm.get("date1").patchValue(data);
    })

    this.plannerRangeDatePickerComponent.date2PickedSubject.subscribe( data => {
      this.reservarComponent.reservaForm.get("date2").patchValue(data);
    })
  }

  ngOnInit(): void {
    this.activatedRouter.params.subscribe(
      params => {
        this.barcoId = params['id'];
        this.barcoDataService.getImages(this.barcoId).subscribe(response => this.getImage(response))
      });
      this.barcoDataService.getById(this.barcoId).subscribe(response => this.tituloAnuncio = response.data.tituloAnuncio)
  }

  getImage = (response) => {
    
    response.images.forEach(element => {
      let imageUrl = 'data:image/jpg;base64,' + element;

      if (this.imagesCollection == null) {
        this.imagesCollection = [imageUrl];
      } else {
        this.imagesCollection.push(imageUrl);
      }

    });
  }

  solicitarInformacoes = () => {
    this.solicitarInformacaoComponent.solicitarInformacoes();
  }
}
