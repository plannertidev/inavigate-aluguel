export class ReservaDTO {
    public reservaId : string;
    public clienteId : string;
    public barcoId: string;
    public inicio : Date;
    public fim : Date;
    public tipo: number;
    public status: number;
}

export enum kdTipoReserva {
    Reserva = 0,
    Bloqueio = 1,
}