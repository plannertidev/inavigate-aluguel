import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { faChevronLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { Lock } from '../lock-dto';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-planner-range-date-picker',
  templateUrl: './planner-range-date-picker.component.html',
  styleUrls: ['./planner-range-date-picker.component.scss'],
  providers: []
})
export class PlannerRangeDatePickerComponent implements OnInit {

  chevronLeft = faChevronLeft;
  chevronRight = faChevronRight;

  public date1 = moment();
  public date2 = moment();
  public daysCollection1;
  public daysCollection2;
  public dateForm: FormGroup;
  private daySelected1 = "";
  private daySelected2 = "";

  public date1PickedSubject: Subject<any> = new Subject();
  public date2PickedSubject: Subject<any> = new Subject();

  public locksCollection = [];

  constructor(private formBuilder: FormBuilder) {
    this.daysCollection1 = this.createCalendar(this.date1);

    this.date2.add(1, 'M')
    this.daysCollection2 = this.createCalendar(this.date2);
    this.initDateForm();

  }

  public initDateForm() {
    return this.dateForm = this.formBuilder.group({
      dateFrom: [null, Validators.required],
      dateTo: [null, Validators.required],
    });
  }

  ngOnInit(): void {

    // let lock = new Lock();
    // lock.start = moment("28/04/2021", 'DD/MM/YYYY');
    // lock.end = moment("02/05/2021", 'DD/MM/YYYY');
    // this.locksCollection.push(lock)
  }

  createLock = (date1, date2): Lock => {
    let lock = new Lock();
    lock.start = moment(date1, 'DD/MM/YYYY');
    lock.end = moment(date2, 'DD/MM/YYYY');
    return lock;
  }

  public createCalendar = (month) => {
    let firstDay = moment(month).startOf('M');
    let days = Array.apply(null, { length: month.daysInMonth() })
      .map(Number.call, Number)
      .map((n) => {
        return moment(firstDay).add(n, 'd');
      })

    for (let n = 0; n < firstDay.weekday(); n++) {
      days.unshift(null);
    }

    if (firstDay.isBefore(moment())) {
      this.locksCollection.push(this.createLock(firstDay, moment()))
    }

    return days;
  }

  public nextMonth = () => {
    this.date1.add(1, 'M')
    this.daysCollection1 = this.createCalendar(this.date1);

    this.date2.add(1, 'M')
    this.daysCollection2 = this.createCalendar(this.date2);
  }

  public previousMonth = () => {
    this.date1.subtract(1, 'M')
    this.daysCollection1 = this.createCalendar(this.date1);

    this.date2.subtract(1, 'M')
    this.daysCollection2 = this.createCalendar(this.date2);
  }

  public displayDayV2 = (day) => {
    let result = "";

    // console.log(day?.date());

    if (this.isLocked(day)) {

      result = "blocked ";

    } else {

      if (day === null) {
        result = "inactive ";
      }

      if (this.todayCheck(day)) {
        result = result + " today";
      }

      if (day === this.daySelected1) {
        result = result + " selected";
      }

      if (day === this.daySelected2) {
        result = result + " selected";
      }

      if (this.isSelected(day)) {
        result = " reserved";
      }
    }

    // console.log(result);

    return result;
  }

  public clickDay = (day) => {

    if (!this.isLocked(day)) {
      let dayFormatted = day.format('DD/MM/YYYY');

      if (this.daySelected1 === "") {
        this.daySelected1 = day;
        this.dateForm.get('dateFrom').patchValue(dayFormatted);
        this.date1PickedSubject.next(dayFormatted);
      } else {
        if (this.daySelected2 === "") {

          if (!this.thereisLockBetween(this.daySelected1, day)) {
            this.daySelected2 = day;
            this.dateForm.get('dateTo').patchValue(dayFormatted);
            this.date2PickedSubject.next(dayFormatted);
          } else {
            this.limparDatas();
          }

        } else {
          this.daySelected1 = day;
          this.dateForm.get('dateFrom').patchValue(dayFormatted);

          this.daySelected2 = "";
          this.dateForm.get('dateTo').patchValue(null);
        }
      }
    }
  }

  private isLocked(day) {

    let result = false;

    this.locksCollection.forEach(function (entry) {

      if (entry.start.isSameOrBefore(day) && entry.end.isSameOrAfter(day)) {
        result = true;
      }
    });

    return result;

  }

  private thereisLockBetween = (dateFrom: any, dateTo: any) => {

    let result = false;
    let startDate = moment(dateFrom);

    while (startDate.isSameOrBefore(dateTo)) {

      if (this.isLocked(startDate)) {
        result = true;
      }

      startDate.add(1, 'd');
    }

    return result;
  }

  public todayCheck(day) {
    if (!day) {
      return false;
    }
    return moment().format('L') === day.format('L');
  }

  public isSelected = (day) => {
    if (!day) {
      return false;
    }

    let dateFrom = moment(this.dateForm.value.dateFrom, 'DD/MM/YYYY');
    let dateTo = moment(this.dateForm.value.dateTo, 'DD/MM/YYYY');
    if (this.dateForm.value) {
      return dateFrom.isSameOrBefore(day) && dateTo.isSameOrAfter(day);
    }

    // if (this.dateForm.get('dateFrom').valid) {
    //     return dateFrom.isSame(day);
    // }
  }

  public limparDatas = () => {
    this.daySelected1 = "";
    this.dateForm.get('dateFrom').patchValue(null);

    this.daySelected2 = "";
    this.dateForm.get('dateTo').patchValue(null);

    this.date1PickedSubject.next(this.daySelected1);
    this.date2PickedSubject.next(this.daySelected1);
  }
}
