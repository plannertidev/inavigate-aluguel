import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlannerDatePickerComponent } from './planner-date-picker.component';

describe('PlannerDatePickerComponent', () => {
  let component: PlannerDatePickerComponent;
  let fixture: ComponentFixture<PlannerDatePickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlannerDatePickerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlannerDatePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
