import { OverlayRef, Overlay, OverlayPositionBuilder } from "@angular/cdk/overlay";
import { ComponentPortal } from "@angular/cdk/portal";
import { Directive, OnInit, Input, HostListener, ComponentRef, ElementRef } from "@angular/core";
import { FormControl } from "@angular/forms";
import { PlannerDatePickerComponent } from "./planner-date-picker.component";

@Directive({ selector: '[plannerDatePicker]' })
export class PlannerDatePickerDirective implements OnInit {

  //@Input("plannerDatePicker") control: ElementRef;
  @Input("plannerDatePicker") control: FormControl;

  private overlayRef: OverlayRef;

  constructor(private overlayPositionBuilder: OverlayPositionBuilder,
    private elementRef: ElementRef,
    private overlay: Overlay) {}
    
  ngOnInit(): void {
    const positionStrategy = this.overlayPositionBuilder
      .flexibleConnectedTo(this.elementRef)
      .withPositions([{
        originX: 'start',
        originY: 'bottom',
        overlayX: 'start',
        overlayY: 'top',
      }]);

    this.overlayRef = this.overlay.create({positionStrategy});
  }

  @HostListener('focus')
  show() {
    // Create tooltip portal
    const tooltipPortal = new ComponentPortal(PlannerDatePickerComponent);

    // Attach tooltip portal to overlay
    const tooltipRef: ComponentRef<PlannerDatePickerComponent> = this.overlayRef.attach(tooltipPortal);
      
    tooltipRef.instance.overlayRef = this.overlayRef;

    tooltipRef.instance.datePickedSubject.subscribe(data => {
      this.elementRef.nativeElement.value = data;
    });
   }

  @HostListener('click')
  hide() {  
  }
}