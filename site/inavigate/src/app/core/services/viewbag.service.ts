import { Injectable } from "@angular/core";

@Injectable()
export class ViewBagService {

    private map: { [key: string]: any; } = { };

    public set(key:string, value:any):any{
            this.map[key] = value;
    }

    public get(key:string):any {
        return this.map[key];
    }
}