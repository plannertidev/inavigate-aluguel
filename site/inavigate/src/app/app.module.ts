import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegistrarComponent } from './proprietario/registrar/registrar/registrar.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HomeComponent } from './layout/home/home/home.component';
import { NotificationService } from './core/services/notification.service';
import { LoginComponent } from './proprietario/login/login.component';
import { UserContextService } from './core/services/user-context-service';
import { RequestService } from './core/services/request.service';
import { BarcoCardComponent } from './layout/barco-card/barco-card.component';
import { AnuncioComponent } from './barco/anuncio/anuncio/anuncio.component';
import { OverlayModule } from '@angular/cdk/overlay';
import { PlannerDatePickerDirective } from './core/dates/date-picker/planner-date-picker-directive';
import { PlannerDatePickerComponent } from './core/dates/date-picker/planner-date-picker.component';
import { PlannerDatePickerModule } from './core/dates/date-picker/planner-date-picker.module';
import { PlannerRangeDatePickerComponent } from './core/dates/planner-range-date-picker/planner-range-date-picker.component';
import { ReservarComponent } from './visitante/reservar/reservar/reservar.component';
import { SolicitarInformacaoComponent } from './visitante/informacao/solicitar-informacao.component';
import { RodapeComponent } from './layout/rodape/rodape.component';

@NgModule({
  declarations: [
    AppComponent,
    RegistrarComponent,
    HomeComponent,
    LoginComponent,
    BarcoCardComponent,
    AnuncioComponent,
    PlannerDatePickerDirective,
    PlannerDatePickerComponent,
    SolicitarInformacaoComponent,
    PlannerRangeDatePickerComponent,
    ReservarComponent,
    RodapeComponent,
  ],
  imports: [
    BrowserModule,
    OverlayModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    FontAwesomeModule,
    PlannerDatePickerModule,
  ],
  providers: [RequestService, NotificationService, UserContextService],
  bootstrap: [AppComponent],
  entryComponents: [PlannerDatePickerComponent],
})
export class AppModule { }
