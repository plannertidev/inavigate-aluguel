import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GridService } from 'src/app/core/services/grid.service';
import { BarcoDataService } from '../../barco-card/barco-dataservice';
import { BarcoDTO } from '../../barco-card/barco-dto';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [BarcoDataService, GridService ]
})
export class HomeComponent implements OnInit {

  constructor(
    private router: Router, 
    private barcoDataService: BarcoDataService,
    public gridService: GridService<BarcoDTO>) { }

  ngOnInit(): void {
    this.barcoDataService.getBarcosAprovadosQuery().subscribe(response => this.atualizarGrid(response.data));
  }

  registrarProprietario(): void {
    this.router.navigateByUrl('/registrar');
  }
  
  loginProprietario(): void {
    this.router.navigateByUrl('/login');
  }

  private atualizarGrid(data) {
    this.gridService.setDataSource(data)
  }
}
