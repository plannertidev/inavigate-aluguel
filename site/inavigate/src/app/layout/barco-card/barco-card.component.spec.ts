import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BarcoCardComponent } from './barco-card.component';

describe('PropertyCardComponent', () => {
  let component: BarcoCardComponent;
  let fixture: ComponentFixture<BarcoCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BarcoCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BarcoCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
