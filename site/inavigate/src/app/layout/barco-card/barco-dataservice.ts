import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { RequestService } from "src/app/core/services/request.service";
import { ResponseDTO } from "src/app/core/services/response.dto";
import { BarcoDTO } from "./barco-dto";

@Injectable()
export class BarcoDataService {
    constructor(private requestService: RequestService) {
    }

    public getAll(): Observable<ResponseDTO> {
        return this.requestService.getApi("api/barco/getAll");
    }

    public getBarcosAprovadosQuery(): Observable<ResponseDTO> {
        return this.requestService.getApi("api/barco/getBarcosAprovadosQuery");
    }

    public getById(PropertyId: string): Observable<ResponseDTO> {
        const params:any = [{key: "parametroJson",value: PropertyId}];
        return this.requestService.getApi("api/barco/getById",  params);
    }

    public getByFilter(propertyDto: BarcoDTO): Observable<ResponseDTO> {
        const params:any = [{key: "parametroJson", value: JSON.stringify(propertyDto)}];
        return this.requestService.getApi("api/barco/getByFilter",  params);
    }

    public create(propertyDto: BarcoDTO): Observable<ResponseDTO> {
        return this.requestService.post("api/barco/create",  propertyDto);
    }

    public update(propertyDto: BarcoDTO): Observable<ResponseDTO> {
        return this.requestService.post("api/barco/update", propertyDto);
    }

    public delete(propertyDto: BarcoDTO): Observable<ResponseDTO> {
        return this.requestService.post("api/barco/delete",  propertyDto);
    }

    public getImages(barcoId: string): Observable<any> {
        const params: any = [{ key: "barcoId", value: barcoId }];
        return this.requestService.getApi("api/image/getBoatImages", params);
    }    

    public getImage(filename: string): Observable<any> {
        const params: any = [{ key: "parametroJson", value: filename }];
        return this.requestService.getApi("api/image/Download", params);
    }    

}