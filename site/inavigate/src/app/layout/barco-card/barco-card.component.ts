import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GridService } from 'src/app/core/services/grid.service';
import { BarcoDataService } from './barco-dataservice';
import { BarcoDTO } from './barco-dto';

@Component({
  selector: 'app-property-card',
  templateUrl: './barco-card.component.html',
  styleUrls: ['./barco-card.component.scss']
})
export class BarcoCardComponent implements OnInit {

  constructor( 
    private barcoDataService: BarcoDataService,
    public gridService: GridService<BarcoDTO>,
    private router: Router) { }

  imageUrl: any;
  imagesCollection: [any];

  @Input()
  barcoId: string;

  ngOnInit(): void {
     this.barcoDataService.getImages( this.barcoId )
     .subscribe( response => this.getImage(response));
     
     this.barcoDataService.getById(this.barcoId )
     .subscribe( );
     this.barcoDataService.getBarcosAprovadosQuery().subscribe(response => console.log(response));
  }

  getImage = (response) => {

    response.images.forEach(element => {
      this.imageUrl = 'data:image/jpg;base64,' + element;

      if (this.imagesCollection == null) {
        this.imagesCollection = [element];
      } else {
        this.imagesCollection.push(element);
      }
      
    });
  }

  mostrarAnuncio = () => {
    this.router.navigate(['/pagina-anuncio', this.barcoId]);
  }
}
