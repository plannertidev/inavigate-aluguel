import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AnuncioComponent } from './barco/anuncio/anuncio/anuncio.component';
import { HomeComponent } from './layout/home/home/home.component';
import { LoginComponent } from './proprietario/login/login.component';
import { RegistrarComponent } from './proprietario/registrar/registrar/registrar.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'registrar', component: RegistrarComponent},
  { path: 'login', component: LoginComponent},
  { path: 'pagina-anuncio/:id', component: AnuncioComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
