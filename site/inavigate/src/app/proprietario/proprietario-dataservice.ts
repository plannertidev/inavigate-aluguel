import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { RequestService } from "src/app/core/services/request.service";
import { ResponseDTO } from "src/app/core/services/response.dto";
import { ProprietarioDTO } from "./proprietario-dto";

@Injectable()
export class ProprietarioDataService {
    constructor(private requestService: RequestService) {
    }

    public getAll(): Observable<ResponseDTO> {
        return this.requestService.getApi("api/proprietario/getAll");
    }

    public getById(proprietarioId: string): Observable<ResponseDTO> {
        const params:any = [{key: "parametroJson",value: proprietarioId}];
        return this.requestService.getApi("api/proprietario/getById",  params);
    }

    public getByFilter(proprietarioDTO: ProprietarioDTO): Observable<ResponseDTO> {
        const params:any = [{key: "parametroJson", value: JSON.stringify(proprietarioDTO)}];
        return this.requestService.getApi("api/proprietario/getByFilter",  params);
    }

    public create(proprietarioDTO: ProprietarioDTO): Observable<ResponseDTO> {
        return this.requestService.postApi("api/proprietario/create",  proprietarioDTO);
    }

    public update(proprietarioDTO: ProprietarioDTO): Observable<ResponseDTO> {
        return this.requestService.post("api/proprietario/update", proprietarioDTO);
    }

    public delete(proprietarioDTO: ProprietarioDTO): Observable<ResponseDTO> {
        return this.requestService.post("api/proprietario/delete",  proprietarioDTO);
    }

    public login(proprietarioDTO: ProprietarioDTO): Observable<ResponseDTO> {
        return this.requestService.postApi("api/account/login",  proprietarioDTO);
    }

}