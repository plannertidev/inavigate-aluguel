export class ProprietarioDTO {
    public proprietarioId: string;
    public nome: string;
    public email: string;
    public celular: string;
    public senha: string;
}
