import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { faEnvelope, faLock } from '@fortawesome/free-solid-svg-icons';
import { NotificationService } from 'src/app/core/services/notification.service';
import { UserContextService } from 'src/app/core/services/user-context-service';
import { ProprietarioDataService } from '../proprietario-dataservice';
import { ProprietarioDTO } from '../proprietario-dto';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [ProprietarioDataService ]
})
export class LoginComponent implements OnInit {

  faEmail = faEnvelope;
  faSenha = faLock;

  public loginForm : FormGroup;
  public controls: any;

  constructor(
    private router: Router,
    private proprietarioDataService: ProprietarioDataService,
    private userContextService: UserContextService,
    private notificationService: NotificationService,
    private formBuilder: FormBuilder) { 
      
      this.loginForm = this.formBuilder.group({
        email:['', Validators.required ],
        password:['',Validators.required],
      });
  
      this.controls = {
        email: this.loginForm.get('email'),
        password: this.loginForm.get('password'),
      }
  
    }
    
  ngOnInit(): void {
  }

  onSubmit(): void{
    let proprietarioDTO : ProprietarioDTO = new ProprietarioDTO();
    proprietarioDTO.email = this.controls.email.value;
    proprietarioDTO.senha = this.controls.password.value;

    this.proprietarioDataService.login(proprietarioDTO).subscribe(
      response => this.loginCallbackSuccess(response), error => this.loginCallbackError(error)      
    );
  }

  loginCallbackSuccess = (response) => {
    if (response.data == "OK") {
      this.userContextService.login();
    } else {
      this.notificationService.error("Credencial inválida.");
    }
  }

  loginCallbackError = (error) => {
    this.notificationService.error(error.error);
  }
}
