import { AfterViewInit, Component, Input, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as moment from 'moment';
import { ReservaDataService } from 'src/app/barco/anuncio/reserva/reserva.dataservice';
import { kdTipoReserva, ReservaDTO } from 'src/app/barco/anuncio/reserva/reserva.dto';
import { NotificationService } from 'src/app/core/services/notification.service';

@Component({
  selector: 'app-reservar',
  templateUrl: './reservar.component.html',
  styleUrls: ['./reservar.component.scss']
})
export class ReservarComponent implements OnInit, AfterViewInit {

  @Input()
  public barcoId:string = "";

  public reservaForm: FormGroup;
  public controls: any;

  constructor(private formBuilder: FormBuilder,
              private reservaDataService: ReservaDataService,
              private notificationService: NotificationService) { 

    this.reservaForm = this.formBuilder.group({
      reservaId: [''],
      date1:[''],
      date2:[''],
      
    });

    this.controls = {
      reservaId: this.reservaForm.get('reservaId'),
      date1: this.reservaForm.get('date1'),
      date2: this.reservaForm.get('date2')
    }    

  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
  }

  enviarReserva = () => {
    let reservaDTO = new ReservaDTO();
    reservaDTO.barcoId = this.barcoId; 
    reservaDTO.clienteId ="7A029DE9-39AC-4853-8FF5-2450A681A398";
    reservaDTO.inicio = moment(this.controls.date1.value, "DD/MM/YYYY").toDate();
    reservaDTO.fim = moment(this.controls.date2.value, "DD/MM/YYYY").toDate();
    reservaDTO.tipo = kdTipoReserva.Reserva.valueOf();
    reservaDTO.status = 1;
    console.log(reservaDTO);
   // this.reservaDataService.create(reservaDTO).subscribe(response => this.onEnvioReservaSuccess(response));
  }

  onEnvioReservaSuccess = (response) => {
    this.notificationService.success("Reserva incluida com sucesso.");
  }
}
