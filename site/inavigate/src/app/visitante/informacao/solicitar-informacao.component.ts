import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MensagemDataService } from 'src/app/barco/anuncio/mensagem/mensagem.dataservice';
import { MensagemDTO } from 'src/app/barco/anuncio/mensagem/mensagem.dto';
import { NotificationService } from 'src/app/core/services/notification.service';

@Component({
  selector: 'app-solicitar-informacao',
  templateUrl: './solicitar-informacao.component.html',
  styleUrls: ['./solicitar-informacao.component.scss']
})
export class SolicitarInformacaoComponent implements OnInit {

 @ViewChild('modalSolicitarInformacoes', { static: false }) modaldiv: ElementRef;
 public mensagemForm: FormGroup;
 public controls: any;

  constructor(
    private renderer: Renderer2,
    private formBuilder: FormBuilder,
    private mensagemDataService: MensagemDataService,
    private notificationService: NotificationService) {}

  ngOnInit(): void {

    this.mensagemForm = this.formBuilder.group({
      mensagemId:[''],
      numeroDias: ['', Validators.required],
      incluirMarinheiro: ['', Validators.required],
      numeroPassageiros: ['', Validators.required],
      remetenteNome: ['', Validators.required],
      remetenteEmail: ['', Validators.required],
      dataPretendida: ['', Validators.required],
      remetenteTelefone: ['', Validators.required]
    });

    this.controls = {
      mensagemId: this.mensagemForm.get('mensagemId'),
      numeroDias: this.mensagemForm.get('numeroDias'),
      incluirMarinheiro: this.mensagemForm.get('incluirMarinheiro'),
      numeroPassageiros: this.mensagemForm.get('numeroPassageiros'),
      remetenteNome: this.mensagemForm.get('remetenteNome'),
      remetenteEmail: this.mensagemForm.get('remetenteEmail'),
      dataPretendida: this.mensagemForm.get('dataPretendida'),
      remetenteTelefone: this.mensagemForm.get('remetenteTelefone')
    }    
  }

  
  solicitarInformacoes = () => {
    this.renderer.addClass(this.modaldiv.nativeElement,"is-active");
    this.mensagemForm.reset();
  }

  closeModal = () => {
    this.renderer.removeClass(this.modaldiv.nativeElement,"is-active");
  }

  enviarMensagem = () => {
    let mensagemDTO = new MensagemDTO();
    mensagemDTO.numeroDias = parseInt(this.controls.numeroDias.value); 
    mensagemDTO.dataPretendida = this.controls.dataPretendida.value;
    mensagemDTO.incluirMarinheiro = parseInt(this.controls.incluirMarinheiro.value);
    mensagemDTO.numeroPassageiros = parseInt(this.controls.numeroPassageiros.value);
    mensagemDTO.remetenteNome = this.controls.remetenteNome.value;
    mensagemDTO.remetenteEmail = this.controls.remetenteEmail.value;
    mensagemDTO.remetenteTelefone = this.controls.remetenteTelefone.value;
    mensagemDTO.mensagem = "Isto é um Teste";
    mensagemDTO.status = 0;
   
    this.mensagemDataService.create(mensagemDTO).subscribe(response => this.onEnvioMenssagemSuccess(response));
  }

  onEnvioMenssagemSuccess = (response) => {
    this.notificationService.success("Mensagem incluida com sucesso.");
    this.closeModal();
  }

}
