import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitarInformacaoComponent } from './solicitar-informacao.component';

describe('SolicitarInformacaoComponent', () => {
  let component: SolicitarInformacaoComponent;
  let fixture: ComponentFixture<SolicitarInformacaoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SolicitarInformacaoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitarInformacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
